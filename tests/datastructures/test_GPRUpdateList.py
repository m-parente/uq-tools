import copy
from uq_tools.datastructures import GPRUpdateList


m = GPRUpdateList(n_pts_diffs_avg=5)

# Test size, average, and list of averages for non-packed updates list
m.add_pt(1)
m.add_pt(2)
assert(m.size() == 2)
assert(m.average_diffs() == 1)
assert(m.all_avgs == [-1, m.average_diffs()])

# Test minimum for desired average for non-packes updates list
N = 15
m_copy = copy.deepcopy(m)
min_ = m_copy.min_for_desired_average(N)
m_copy.add_pt(min_)
assert(m_copy.average_diffs() == N)
assert(m_copy.all_avgs == [-1, 1, N])

# Test size, average, and list of averages for exactly-packed updates list
m.add_pt(3)
m.add_pt(4)
m.add_pt(5)
assert(m.size() == 5)
assert(m.average_diffs() == (5-1)/4)
assert(m.all_avgs == [-1, 1, 1, 1, m.average_diffs()])

# Test minimum for desired average for exactly-packes updates list
N = 34
m_copy = copy.deepcopy(m)
min_ = m_copy.min_for_desired_average(N)
m_copy.add_pt(min_)
assert(m_copy.average_diffs() == N)
assert(m_copy.all_avgs == [-1, 1, 1, 1, 1, N])

# Test size, average, and list of averages for over-packed updates list
m.add_pt(6)
m.add_pt(19)
assert(m.size() == 7)
assert(m.average_diffs() == (19-3)/4)
assert(m.all_avgs == [-1, 1, 1, 1, 1, 1, m.average_diffs()])

# Test minimum for desired average for over-packed updates list
N = 42
m_copy = copy.deepcopy(m)
min_ = m.min_for_desired_average(N)
m.add_pt(min_)
m_copy.add_pt(min_-1)
assert(m.average_diffs() == N)
assert(m_copy.average_diffs() < N)
assert(m.all_avgs == [-1, 1, 1, 1, 1, 1, 4, N])
