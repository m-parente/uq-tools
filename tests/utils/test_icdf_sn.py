from uq_tools import utils

import numpy as np
import numpy.random as rnd

N = 10**4
xs = rnd.normal(size=N)
assert(np.allclose(xs, utils.icdf_sn(utils.cdf_sn(xs))))

us = rnd.uniform(size=N)
assert(np.allclose(us, utils.cdf_sn(utils.icdf_sn(us))))

print("Test succeeded.")
