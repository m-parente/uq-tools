from uq_tools import utils

import numpy as np
import numpy.random as rnd
from scipy import stats


n = 20
N = 100

for i in range(N):
    W = stats.ortho_group.rvs(dim=n)
    ls = np.sort(rnd.uniform(size=n))[::-1]
    L = np.diag(ls)
    A = np.dot(np.dot(W, L), W.T)

    my_ls, my_W = utils.getEigenpairs(A)

    assert(np.allclose(ls, my_ls))
    # Use abs() since eigenvectors can have a different sign
    assert(np.allclose(np.abs(W), np.abs(my_W)))

print("Test successful.")
