from uq_tools import utils

import numpy as np
import numpy.random as rnd
import sklearn.pipeline as pipe
from sklearn.preprocessing import PolynomialFeatures


def f(x):
    return np.sum(x**2, axis=1)


N = 10

for i in range(100):
    n = rnd.randint(low=1, high=10)
    p = rnd.randint(low=1, high=10)

    xs = rnd.uniform(low=-1, high=1, size=(N, n))
    ys = f(xs)

    model = PolynomialFeatures(p)
    model.fit(xs, ys)

    ndof = len(model.powers_)
    my_ndof = utils.dofp(n, p)

    assert(ndof == my_ndof)

print("Test successful.")
