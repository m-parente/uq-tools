from uq_tools import quad

import numpy as np


rtol = 1e-4
a = 2.


def f(t):
    return t**2+a


dt = 1e-2
T = 2.
ts = np.arange(0, T+dt, step=dt)
I = np.array([f(ts)])
integ_approx = quad.trapez_rule(I, dt)
integ_exact = T**3/3. + a*T
assert(np.allclose(integ_exact, integ_approx))
assert(np.allclose(quad.trapez_rule_f(f, 0, T, dt=dt), T**3 / 3. + a*T, rtol=rtol))
print("Test successfully finished.")
