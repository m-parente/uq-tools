from dolfin import *
import numpy as np
import numpy.random as rnd
import sys

import matplotlib.pyplot as plt

eigValsFile = sys.argv[1]
eigVals = np.loadtxt(eigValsFile)

print(np.sum(eigVals))

plt.plot(list(range(1, len(eigVals) + 1)), eigVals, 'o')
plt.show()
