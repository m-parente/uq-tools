from uq_tools import rbf

import matplotlib.pyplot as plt


samples = rbf._sampleSurfaceNSphere(dim=2, n=100, center=(4, 4), r=0.5)

plt.figure()
plt.plot(samples[:, 0], samples[:, 1], 'o')
plt.show()
