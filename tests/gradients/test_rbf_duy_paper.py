from uq_tools import rbf

import itertools as it
import numpy as np

import matplotlib.pyplot as plt

# Example 1: Mai-Duy, Tran-Cong, 2002, p.201 --------------
# Univariate function example


def f1(x):
    return x**3 + x + 0.5


xs = np.arange(-3, 2, 0.1)
ys = f1(xs)

f_approx = rbf.approximate(xs, ys)
ys_approx = [f_approx(x) for x in xs]

plt.figure()
plt.plot(xs, ys)
plt.plot(xs, ys_approx, 'o')
# ---------------------------------------------------------

# Example 3: Mai-Duy, Tran-Cong, 2002, p.210 --------------
# Univariate function example


def f2(x):
    return 0.02 * (12 + 3 * x - 3.5 * x**2 + 7.2 * x**3) * (1 + np.cos(4 * np.pi * x)) * (1 + 0.8 * np.sin(3 * np.pi * x))


xs = np.arange(0, 1, 0.005)
ys = f2(xs)

f_approx = rbf.approximate(xs, ys, widths=0.005)
ys_approx = [f_approx(x) for x in xs]

plt.figure()
plt.plot(xs, ys)
plt.plot(xs, ys_approx, 'o')
# ---------------------------------------------------------

# Example 3: Mai-Duy, Tran-Cong, 2002, p.215 --------------
# Bivariate function example


def f3(x):
    x1 = x[0]
    x2 = x[1]

    return x1**2 * x2 + x2**3 / 3.0 + x2**2 / 2.0


x1s = np.arange(-3, 3, 0.05)
x2s = x1s

xs = [np.array(el) for el in it.product([0.0], x2s)]
ys = [f3(x) for x in xs]

f_approx = rbf.approximate(xs, ys, xs, 0.005 * np.ones(len(xs)))
ys_approx = [f_approx(x) for x in xs]

plt.figure()
plt.plot(x2s, ys)
plt.plot(x2s, ys_approx, 'o')

xs = [np.array(el) for el in it.product(x1s, [1.0])]
ys = [f3(x) for x in xs]

f_approx = rbf.approximate(xs, ys)
ys_approx = [f_approx(x) for x in xs]

plt.figure()
plt.plot(x1s, ys)
plt.plot(x1s, ys_approx, 'o')
# ---------------------------------------------------------

plt.show()
