from uq_tools.gradients import rbf
from uq_tools.gradients import cfd
from uq_tools.gradients import ffd

import numpy.linalg as la
import numpy.random as rnd
import numpy as np

import matplotlib.pyplot as plt


def f1(x):  # -- Example 1: f : R^2 -> R ---------------
    x1 = x[0]
    x2 = x[1]

    return x1**2 * x2 + x2**3 / 3.0 + x2**2 / 2.0


def gradF1(x):
    x1 = x[0]
    x2 = x[1]

    return np.array([2 * x1 * x2, x1**2 + x2**2 + x2])


x = [5, 5]

grad = gradF1(x)
# grad_approx = rbf.approximateGradientAt(x, f1)
# grad_approx = cfd.approximateGradientAt(x, f1, hs=1e-3)
grad_approx = ffd.approximateGradientAt(x, f1, hs=1e-3)

print("Example 1:")
print(grad)
print(grad_approx)
print(la.norm(grad - grad_approx) / la.norm(grad))
print('\n\n')
# ---------------------------------------------------------


def f2(x):  # -- Example 2: f : R^3 -> R ---------------
    x1 = x[0]
    x2 = x[1]
    x3 = x[2]

    return x1**2 + x1 * x2 - 2 * x2**2 - x2 * x3 + x3**2


def gradF2(x):
    x1 = x[0]
    x2 = x[1]
    x3 = x[2]

    return np.array([2 * x1 + x2, x1 - 4 * x2 - x3, -x2 + 2 * x3])


x = [1, 1, 1]

grad = gradF2(x)
grad_approx = rbf.approximateGradientAt(x, f2)

print("Example 2:")
print(grad)
print(grad_approx)
print(la.norm(grad - grad_approx) / la.norm(grad))
print('\n\n')
# ---------------------------------------------------------


# -- Example 3: f : R^10 -> R^10 ---------------
dim = 10
A = 5 * rnd.rand(dim, dim)
b = 5 * rnd.rand(dim)
c = 5 * rnd.rand()


def f3(x):
    return np.dot(np.dot(x.T, A), x) + np.dot(b, x) + c


def gradF3(x):
    return np.dot((A + A.T), x) + b.T


x = np.ones(dim)

grad = gradF3(x)
grad_approx = rbf.approximateGradientAt(x, f3)

print("Example 3:")
print(grad)
print(grad_approx)
print(la.norm(grad - grad_approx) / la.norm(grad))
print('\n\n')
# ---------------------------------------------------------


# -- Example 4: f : R^100 -> R ---------------
dimIn = 100
dimOut = 1
A = 1e-3 * rnd.rand(dimOut, dimIn)


def f4(x):
    return np.dot(A, x)


def jacF4(x):
    return A


x = np.ones(dimIn)

jac = jacF4(x)
jac_approx = rbf.approximateGradientAt(x, f4)

print("Example 4:")
print(jac)
print(jac_approx)
print(la.norm(jac - jac_approx) / la.norm(jac))
print('\n\n')
# ---------------------------------------------------------


def f5(x):  # -- Example 5: f : R^3 -> R^4 ---------------
    x1 = x[0]
    x2 = x[1]
    x3 = x[2]

    return np.array([np.sin(100 * x1**2) * np.exp(-4 * x2 + 3 * x3**2),
                     np.cos(x2) + np.log(x3),
                     x1**4 + 2 * x1 * x3**3 + np.sin(x2),
                     np.exp(30 * np.sin(x1 + x2))])


def jacF5(x):
    x1 = x[0]
    x2 = x[1]
    x3 = x[2]

    return np.array([[200 * x1 * np.cos(100 * x1**2) * np.exp(-4 * x2 + 3 * x3**2), -4 * np.sin(100 * x1**2) * np.exp(-4 * x2 + 3 * x3**2), 6 * x3 * np.sin(100 * x1**2) * np.exp(-4 * x2 + 3 * x3**2)],
                     [0, -np.sin(x2), x3**(-1)],
                     [4 * x1**3 + 2 * x3**3, np.cos(x2), 6 * x1 * x3],
                     [np.exp(30 * np.sin(x1 + x2)) * 30 * np.cos(x1 + x2), np.exp(30 * np.sin(x1 + x2)) * 30 * np.cos(x1 + x2), 0]])


dimIn = 3
x = np.ones(dimIn)

jac = jacF5(x)
# jac_approx = rbf.approximateGradientAt(x, f5, r=1e-3)
jac_approx = cfd.approximateGradientAt(x, f5, h=1e-3)
# jac_approx = ffd.approximateGradientAt(x, f5, h=1e-3)

print("Example 5:")
print(jac)
print(jac_approx)
print(la.norm(jac - jac_approx) / la.norm(jac))
print('\n\n')
# ---------------------------------------------------------


def f6(x):  # -- Example 6: f : R^10 -> R ---------------
    x1 = x[0]
    x2 = x[1]
    x3 = x[2]
    x4 = x[3]
    x5 = x[4]
    x6 = x[5]
    x7 = x[6]
    x8 = x[7]
    x9 = x[8]
    x10 = x[9]

    return np.exp(x1**2) + 4 * x2**4 * x3**2 + np.sin(100 * np.sqrt(x4)) + np.log(x5) + x6**3 * x7**2 * x8 + np.exp(x9 * x10)


def gradF6(x):
    x1 = x[0]
    x2 = x[1]
    x3 = x[2]
    x4 = x[3]
    x5 = x[4]
    x6 = x[5]
    x7 = x[6]
    x8 = x[7]
    x9 = x[8]
    x10 = x[9]

    return np.array([2 * x1 * np.exp(x1**2), 16 * x2**3 * x3**2, 8 * x2**4 * x3,
                     50 * np.sqrt(x4)**(-1) * np.cos(100 * np.sqrt(x4)),
                     x5**(-1), 3 * x6**2 * x7**2 * x8, 2 * x6**3 * x7 * x8,
                     x6**3 * x7**2, x10 * np.exp(x9 * x10), x9 * np.exp(x9 * x10)])


dimIn = 10
x = np.ones(dimIn)

grad = gradF6(x)
grad_approx = rbf.approximateGradientAt(x, f6, r=1e-3)
# grad_approx = ffd.approximateGradientAt(x, f6, hs=1e-3)

print("Example 6:")
print(grad)
print(grad_approx)
print(la.norm(grad - grad_approx) / la.norm(grad))
print('\n\n')
# ---------------------------------------------------------


# -- Example 7: f : R^2 -> R --Check perpendicular directions
dimIn = 10


def f7(x):
    x1 = x[0]
    x2 = x[1]

    return 1e8 * x1


def gradF7(x):
    x1 = x[0]
    x2 = x[1]

    return np.concatenate([[1e8], np.zeros(dimIn - 1)])


x = np.ones(dimIn)

grad = gradF7(x)
# grad_approx = rbf.approximateGradientAt(x, f7, n=5 * dimIn, r=1e-3)
# grad_approx = cfd.approximateGradientAt(x, f7, hs=1e-3)
grad_approx = ffd.approximateGradientAt(x, f7, hs=1e-3)

print("Example 7:")
print(grad)
print(grad_approx)
print(la.norm(grad - grad_approx) / la.norm(grad))
print('\n\n')
# ---------------------------------------------------------
