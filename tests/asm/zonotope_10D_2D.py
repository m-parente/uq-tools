import itertools as it
import numpy as np
from scipy.spatial import ConvexHull

import matplotlib.pyplot as plt
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

n = 10
k = 2

W = np.loadtxt('asmEigVecs.txt')
W_split = np.split(W, [k], axis=1)
W1, W2 = W_split[0], W_split[1]

zo = [0, 1]
es = np.array([el for el in it.product(
    zo, zo, zo, zo, zo, zo, zo, zo, zo, zo)])

ys = np.dot(es, W1)
hull = ConvexHull(ys)
print("Volume convex hull: " + repr(hull.volume))
for simplex in hull.simplices:
    plt.plot(ys[simplex, 0], ys[simplex, 1], 'k-')

plt.plot(ys[:, 0], ys[:, 1], 'ro')

plt.plot(ys[hull.vertices, 0], ys[hull.vertices, 1], 'go', lw=2)
# plt.plot(ys[hull.vertices[0], 0], ys[hull.vertices[0], 1], 'bo')

plt.title(r"Zonotope 10D-2D: $W_1^T\cdot (-1,1)^n$")
plt.show()
