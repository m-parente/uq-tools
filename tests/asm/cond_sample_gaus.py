import numpy as np
import numpy.linalg as la
import numpy.random as rnd

import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

n = 10
k = 2
M = 1000

truth_samples = rnd.multivariate_normal(
    mean=[0.0] * (n - k), cov=np.eye(n - k), size=M)

print("Mean truth: " + repr(np.mean(truth_samples, axis=0)))
print("Variance truth: " + repr(np.var(truth_samples, axis=0)))

y = [0.5, -0.5]

W = np.loadtxt('asmEigVecs.txt')
W_split = np.split(W, [k], axis=1)
W1, W2 = W_split[0], W_split[1]

eps = 10**-2
samples = np.empty([n - k, M])
j = 0
props = 0
propSize = 10**5

while True:
    xs = rnd.multivariate_normal(
        mean=[0.0] * n, cov=np.eye(n), size=propSize).transpose()
    W1xs = np.dot(W1.transpose(), xs)
    props = props + propSize

    for i in range(propSize):
        y_ = W1xs[:, i]
        if la.norm(y_ - y) < eps:
            z = np.dot(W2.transpose(), xs[:, i])
            samples[:, j] = z
            j = j + 1
            # print j

            if j >= M:
                props = props - (propSize - (i + 1))
                break
    if j >= M:
        break

print("Mean samples: " + repr(np.mean(samples, axis=1)))
print("Variance samples: " + repr(np.var(samples, axis=1)))
print("Acceptance rate: " + repr(M / float(propSize)))

nPics = int(np.ceil(np.sqrt(n - k)))
fig, axarr = plt.subplots(nPics, nPics)

for i in range(n - k):
    coord = divmod(i, nPics)
    axes = axarr[coord[0], coord[1]]

    n, bins, patches = axes.hist(
        samples[i, :], 20, normed=True, facecolor='green', alpha=0.75)

    y = mlab.normpdf(bins, 0, 1)
    l = axes.plot(bins, y, 'r--', linewidth=1)
    axes.grid(True)

fig.tight_layout()
plt.suptitle(
    "Conditional samples of 10D Gaussian distribution conditioned on active subspace")
plt.show()
