from uq_tools import asm as asm
import uq_tools.utils as utils

import itertools as it
import numpy as np
import numpy.linalg as la
import numpy.random as rnd
from scipy.spatial import ConvexHull
from time import time as tic

import matplotlib.pyplot as plt


k = 4

a = 0.5
b_ = np.sqrt(1 - a * a)

# W = np.array([[a, -b_], [b_, a]])
# W = np.array([[0.36, 0.48, -0.8],
#               [-0.8, 0.6, 0.0],
#               [0.48, 0.64, 0.6]])
# W = np.array([[1, 0, 0],
#               [0, np.sqrt(3) * 0.5, 0.5],
#               [0, -0.5, np.sqrt(3) * 0.5]])
# W = np.eye(10)
W = np.loadtxt('asmEigVecs.txt')
W_split = np.split(W, [k], axis=1)
W1, W2 = W_split[0], W_split[1]

n = len(W)

# ys = np.arange(-(a + b_), a + b_ + 0.01, 0.1)
ys = np.array([el for el in it.product(
    np.arange(-0.5, 0.5 + 0.01, 0.1), [0], [0], [0])])  # , [0], [0], [0], [0]
print(ys)
# exit()

ts = np.empty(len(ys))

tfs = utils.kOutOfn(2 * n, n - k)

A = np.vstack((W2, -W2))

rowsFig = int(np.ceil(np.sqrt(len(ys))))
f, axarr = plt.subplots(rowsFig, rowsFig)

probs = np.empty(len(ys))

for i_pt in range(len(ys)):
    print(i_pt)
    y = ys[i_pt]

    start = tic()

    W1y = np.dot(W1, y[:, np.newaxis])
    b = np.ones((2 * n, 1)) + np.vstack((-W1y, W1y))

    ex_pts = []
    t = tic()
    for tf in tfs:
        # print A.shape
        A_J = A[tf, :]
        b_J = b[tf]

        # print A
        # print A_J
        # print b
        # print b_J

        # exit()
        try:
            x_ = la.solve(A_J, b_J)

            if np.all(np.less_equal(np.dot(A, x_), b + 10e-10)):
                # Extreme point
                ex_pts.append(x_[:, 0])

        except la.LinAlgError as err:
            # print err.message
            continue
    # print tic() - t
    ex_pts = np.array(ex_pts)
    t = tic()
    # print len(ex_pts)
    if len(ex_pts) >= n - k + 1:
        if n - k > 1:
            hull = ConvexHull(ex_pts)
            probs[i_pt] = hull.volume

            ax = axarr[i_pt // rowsFig, i_pt % rowsFig]
            for simplex in hull.simplices:
                ax.plot(ex_pts[simplex, 0], ex_pts[simplex, 1], 'r-')
            ax.plot(ex_pts[hull.vertices, 0],
                    ex_pts[hull.vertices, 1], 'go', lw=2)
            ax.set_xlabel("Volume: " + repr(hull.volume))
        else:
            probs[i_pt] = np.max(ex_pts) - np.min(ex_pts)
    else:
        probs[i_pt] = 0.0
    # print tic() - t
    end = tic()

    ts[i_pt] = end - start

probs = probs / 2**n

plt.suptitle(
    "1D act, 2D INact subspace, marginal densities as area")
plt.show()


def marg_y(y_):
    return 0.25 * (np.min([(-1 - a * y_) / (-b_), (1 - b_ * y_) / a]) - np.max([(1 - a * y_) / (-b_), (-1 - b_ * y_) / a]))


true_values = np.array([marg_y(e) for e in ys])
maxErr = np.max(np.abs(true_values - probs))
totalErr = la.norm(true_values - probs)

print("Mean time per evaluation: " + repr(np.mean(ts)) + " seconds")
print("Total time elapsed: " + repr(np.sum(ts)) + " seconds")
print("Maximum error: " + repr(maxErr))
print("Total error: " + repr(totalErr))

plt.figure()
plt.plot(ys[:, 0], probs, 'g-')
# plt.plot(ys, true_values, 'r--', linewidth=1)
plt.grid(True)
plt.show()
