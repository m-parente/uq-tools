from uq_tools import asm

import numpy as np
import numpy.random as rnd
from scipy import stats


n = 10
k = 2

for i in range(10000):
    W = stats.ortho_group.rvs(dim=n)
    W1 = W[:, :k]
    W2 = W[:, k:]

    x = rnd.uniform(-1, 1, size=n)
    y = np.dot(x, W1)

    z = asm.sample_cond_uniform(y, W1, W2)

    x_ = np.dot(W1, y)+np.dot(W2, z)
    assert(np.all(np.logical_and(np.greater_equal(x_, -1), np.less_equal(x_, 1))))

print("Test successful.")
