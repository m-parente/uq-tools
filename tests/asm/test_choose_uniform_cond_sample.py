from uq_tools import asm

import numpy as np
import numpy.random as rnd
from scipy import stats


n = 10
k = 2


def sampler(N):
    return rnd.multivariate_normal(mean=np.zeros(n), cov=np.eye(n), size=N)


x_samples = sampler(100000000)

W = stats.ortho_group.rvs(dim=n)
W1 = W[:, :k]
W2 = W[:, k:]

y_tol = 1e-3

for i in range(100000):
    y = rnd.multivariate_normal(mean=np.zeros(k), cov=np.eye(k))
    print("y: %a" % y)
    z_sample = asm.choose_uniform_cond_sample(x_samples, y, W1, W2, sampler, y_tol=y_tol)
