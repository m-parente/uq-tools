import uq_tools.asm as asm
import uq_tools.utils as utils

import itertools as it
import numpy as np
import numpy.linalg as la
import numpy.random as rnd
from scipy.stats import multivariate_normal as mvnorm, norm as norm
from time import time as tic

import matplotlib.pyplot as plt


def weibull(x, lam, k):
    return (k / lam) * (x / lam)**(k - 1) * np.exp(-(x / lam)**k)


def multimodal_gaussian(size):
    us = rnd.uniform(-1, 1, size)
    size_l = np.sum(np.less_equal(us, 0))
    size_r = size - size_l

    return np.concatenate((rnd.normal(-1, 0.5, size=size_l), rnd.normal(1, 0.5, size=size_r)))


k = 7

a = 0.5
b_ = np.sqrt(1 - a * a)

# W = np.array([[a, -b_], [b_, a]])
W = np.loadtxt('asmEigVecs.txt')

W_split = np.split(W, [k], axis=1)
W1, W2 = W_split[0], W_split[1]

n = len(W)

x1s = np.arange(-3, 3, 0.25)
x2 = 0.0
x3 = 0.0
x4 = 0.0
x5 = 0.0
x6 = 0.0
x7 = 0.0
x8 = 0.0
pts = np.array([el for el in it.product(
    x1s, [x2], [x3], [x4], [x5], [x6], [x7])])  # , [x8])])
# pts = np.array([el for el in it.product(x1s, [x2])])
# pts = np.arange(-1., 1., .05)
probs = np.empty(len(pts))

N = 10000000
# , pt[2], pt[3], pt[4], pt[5], pt[6], pt[7]
true_values = [mvnorm.pdf([pt[0], pt[1], pt[2], pt[3], pt[4], pt[5], pt[6]], mean=[
                          0.0] * k) for pt in pts]  # , pt[7]
print("Creating " + repr(N) + " samples...")
prior_samples = rnd.multivariate_normal(
    mean=[0.0] * n, cov=np.eye(n), size=N)
print("Finished creating samples.")

# true_values = [weibull(pt, 1., 1.5) for pt in pts]
# prior_samples = W1 * rnd.weibull(1.5, N) + \
#     np.dot(W2, rnd.uniform(size=(n - k, N)))

# true_values = [0.5 * (norm.pdf(pt, loc=-1, scale=0.5) +
#                       norm.pdf(pt, loc=1, scale=0.5)) for pt in pts]
# prior_samples = (W1 * multimodal_gaussian(N) +
#                  np.dot(W2, rnd.uniform(size=(n - k, N)))).transpose()


# def marg_y(y_):
#     return 0.25 * (np.min([(-1 - a * y_) / (-b_), (1 - b_ * y_) / a]) - np.max([(1 - a * y_) / (-b_), (-1 - b_ * y_) / a]))


# true_values = np.array([marg_y(e) for e in pts])
# prior_samples = rnd.uniform(low=-1.0, high=1.0, size=(N, n))

# def diracMethod(y):
#     dist = mvnorm(mean=y, cov=sigmaGaussApprox * np.eye(k))

#     # return np.sum([dist.pdf(W1xs[:, i]) for i in range(N)]) / N

#     v_pdf = np.vectorize(lambda x: dist.pdf(x), signature='(n)->()')

#     return np.sum(v_pdf(W1xs)) / N


# epss = np.array([1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4,
#  0.3, 0.28, 0.26, 0.24, 0.22, 0.2, 0.18, 0.16, 0.14, 0.12, 0.1, 0.08, 0.06, 0.04, 0.03, 0.02, 0.01, 0.005])
# epss = np.arange(0.1, 1.0 + 0.01, 0.1)
# epss = np.arange(0.01, 0.3 + 0.01, 0.01)
# epss = np.array([1.0])
# maxErrs = np.empty(len(epss))

ts = np.empty(len(pts))

size = 3

for i_pt in range(len(pts)):
    y = pts[i_pt]
    print("Evaluating at y = " + repr(y))

    found = False
    start = tic()

    for i in range(5, int(np.log10(N)) + 1):
        samples = prior_samples[0: 10**i]
        epss = np.arange(0.1**(i - 5), 1.0 + 1e-6, 0.1**(i - 5))

        priorApprx = asm.PriorDensityApproximation(samples, W1)

        probs_eps = np.empty(len(epss))

        nProbabilitiesInARow = 0

        for i_eps in range(len(epss)):
            eps = epss[i_eps]

            pb = priorApprx.evalMarginalDensity(y, eps)

            if pb <= 1e-15:
                nProbabilitiesInARow = 0
                continue

            probs_eps[i_eps] = pb
            nProbabilitiesInARow = nProbabilitiesInARow + 1

            if nProbabilitiesInARow >= size:
                pbs = probs_eps[i_eps - size + 1: i_eps]
                pbs2 = probs_eps[i_eps - size + 2: i_eps + 1]

                var = np.sum(np.abs(pbs - pbs2))

                # TODO: Tune constant in front of mean  # 5 * 1e-5
                if var < 0.75 * np.mean(pbs):
                    found = True
                    break

        if found:
            print("FOUND, var = " + repr(var) + " at i = " +
                  repr(i) + " for eps = " + repr(epss[i_eps]))
            print(np.mean(pbs))
            print(true_values[i_pt])
            print(np.abs(np.mean(pbs) - true_values[i_pt]))

            probs[i_pt] = np.mean(pbs)
            # plt.plot(epss, errs_eps, 'o-')
            # plt.figure()
            # plt.plot(epss[0:i_eps], probs_eps[0:i_eps], 'o-')
            # plt.plot(epss[i_eps - size + 1: i_eps + 1],
            #          [np.mean(pbs)] * size, 'ro--')
            # plt.show()
            break

    if not found:
        print("NOT FOUND: y = " + repr(y))
        probs[i_pt] = -1.0

    end = tic()
    ts[i_pt] = end - start
    print("Time: " + repr(ts[i_pt]))

maxErr = np.max(np.abs(true_values - probs))
totalErr = la.norm(true_values - probs)

# print "N: " + `N` + ", eps: " + `eps`
print("Min./Max. time for evaluation: " + repr(np.min(ts)) + "/" + repr(np.max(ts)) + " seconds")
print("Mean time per evaluation: " + repr(np.mean(ts)) + " seconds")
print("Total time elapsed: " + repr(np.sum(ts)) + " seconds")
print("Maximum error: " + repr(maxErr))
print("Total error: " + repr(totalErr))

plt.plot(pts[:, 0], probs, 'go')
plt.plot(pts[:, 0], true_values, 'r--', linewidth=1)

plt.title(repr(k)+"D active subspace, Gaussian, max. 10e" +
          repr(int(np.log10(N))) + " samples")
plt.grid(True)
plt.show()

# plt.rc('text', usetex=True)
# plt.rc('font', family='serif')

# plt.loglog(epss, maxErrs, 'o-')
# plt.plot(epss, epss**2, 'r--')
# plt.gca().invert_xaxis()
# plt.xlabel(r'$\epsilon$')
# plt.ylabel('error')
# plt.title(
#     r"Error decay of $\rho_{W,n}(y) - \tilde{\rho}_{W,n}(y)$ (sample size = $10^8$)")
# plt.show()
