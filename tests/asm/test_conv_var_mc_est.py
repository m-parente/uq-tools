import numpy as np
import numpy.linalg as la
import numpy.random as rnd

import matplotlib.pyplot as plt


plt.rc('text', usetex=True)
plt.rc('font', family='serif')

linestyles = ['-', '--', ':', 'o-', 'o--']

xs = np.arange(0.001, 0.4, 0.025)

# for k in range(1, 6):
#     ys = 1 / ((1 - (float(k) / (k + 4))) * (float(k) / (k + 4))
#               ** (k / 4.0) * xs**((k + 4) / 2.0))
#     plt.semilogy(xs, ys, linestyles[k - 1], label="k = " + `k`)

# plt.xlabel("$\delta$")
# plt.ylabel("$N_{min}(\delta)$")
# plt.legend()
# plt.show()
# exit()

# for k in range(1, 6):
#     ys = ((k * xs**2 / (k + 4.0))**(1 / 4.0))
#     plt.plot(xs, ys, linestyles[k - 1], label="k = " + `k`)

# plt.xlabel("$\delta$")
# plt.ylabel("$\epsilon_{min}(\delta)$")
# plt.legend()
# plt.show()
# exit()

# delta = 0.01
# for k in range(1, 6):
#     xs = np.arange(0.006, np.sqrt(delta), 0.01)
#     ys = 1 / (xs**k * (delta**2 - xs**4))
#     plt.semilogy(xs, ys, linestyles[k - 1], label="k = " + `k`)

#     eps_min = np.sqrt(np.sqrt((k * delta**2) / (k + 4)))
#     Neps_min = 1.0 / (eps_min**k * (delta**2 - eps_min**4))
#     plt.plot([eps_min],
#              [Neps_min], 'ro')

# plt.title(r"$\delta = 10^{-2}$")
# plt.xlabel(r"$\epsilon$")
# plt.ylabel(r"$N(\epsilon)$")
# plt.legend()
# plt.show()
# exit()

n = 10
k = 3

W = np.loadtxt('asmEigVecs.txt')
W_split = np.split(W, [k], axis=1)
W1, W2 = W_split[0], W_split[1]

N = 10000000

prior_samples = rnd.multivariate_normal(mean=[0.0] * n, cov=np.eye(n), size=N)

epss = np.array([1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4,
                 0.3, 0.2, 0.1, 0.08, 0.06, 0.04])

ps = np.empty(len(epss))

W1xs = np.dot(prior_samples, W1)
y = np.array([0.0] * k)

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

for i_eps in range(len(epss)):
    eps = epss[i_eps]
    p = np.sum(np.less_equal(
        la.norm(W1xs - y, axis=1), eps)) / float(N)

    ps[i_eps] = p - p * p
plt.loglog(epss, ps, 'o-', label="$y=(0.0,0.0,0.0)$")

y = np.array([2.0] * k)
for i_eps in range(len(epss)):
    eps = epss[i_eps]
    p = np.sum(np.less_equal(
        la.norm(W1xs - y, axis=1), eps)) / float(N)

    ps[i_eps] = p - p * p
plt.loglog(epss, ps, 'o--', label="$y=(2.0,2.0,2.0)$")

plt.plot(epss, epss ** k, 'r--')
plt.gca().invert_xaxis()
plt.title(
    r"Decay of $P(\mathcal{X}_\epsilon(y)) - P(\mathcal{X}_\epsilon(y))^2$ (sample size $10^7$, 3D active subspace)")
plt.xlabel(r"$\epsilon$")
plt.ylabel(
    r"$P(\mathcal{X}_\epsilon(y)) - P(\mathcal{X}_\epsilon(y))^2$")
plt.legend()
plt.show()
