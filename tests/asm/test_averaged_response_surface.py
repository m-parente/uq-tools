from uq_tools import asm

import numpy as np
import numpy.random as rnd

W = np.array([[1, -1, 0],
              [1, 1, 0],
              [0, 0, np.sqrt(2)]]) / np.sqrt(2)

W_split = np.split(W, [1], axis=1)
W1, W2 = W_split[0], W_split[1]

M = 50000


def f(x):
    x1, x2, x3 = x[0], x[1], x[2]
    return x1**2 + x2**2 + x3**2


g = asm.averaged_misfit(W1, W2, f,
                        prior_cond_sampler=lambda _: rnd.normal(size=2),
                        M=M)

for i in range(20):
    y = rnd.normal(size=1)
    gy = g(y)
    _sum = 0
    for i in range(M):
        z = rnd.normal(size=2)
        _sum += f(np.dot(W1, y) + np.dot(W2, z))

    print(gy, _sum / float(M))
