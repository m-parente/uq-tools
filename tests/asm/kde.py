import itertools as it
import numpy as np
import numpy.random as rnd
import sklearn.neighbors as skl_nb
# import sklearn.grid_search as skl_gs

import matplotlib.pyplot as plt

W = np.loadtxt('asmEigVecs_2.txt')
W1 = W[:, 0:2]

xs = rnd.uniform(size=(500000, 8))
ys = np.dot(xs, W1)

# print xs
# print ys
# exit()

# grid = skl_gs.GridSearchCV(skl_nb.KernelDensity(kernel='tophat'),
#                            {'bandwidth': np.linspace(0.01, 0.2, 10)},
#                            cv=5)  # 20-fold cross-validation
# grid.fit(ys[:, np.newaxis])
# print grid.best_params_
# exit()

hist = np.histogram2d(ys[:, 0], ys[:, 1], bins=50, normed=True)

kde = skl_nb.KernelDensity(
    bandwidth=0.03, kernel='tophat').fit(ys)

li = [np.arange(ymin, ymax, 0.05)
      for ymin, ymax in zip(np.min(ys, axis=0), np.max(ys, axis=0))]

# xs = np.array([[el[0], el[1]] for el in it.product(li[0], li[1])])
X, Y = np.meshgrid(li[0], li[1])

# print kde.score_samples([[0.0, 0.0], [-1.6210795394071051, -0.030285184216876909]])
# print xs

values = kde.score_samples(ys)
plt.figure()
plt.scatter(ys[:, 0], ys[:, 1], c=values)
# plt.scatter(ys, np.zeros(len(ys)) - 0.3, marker='+')

plt.figure()
plt.hist2d(ys[:, 0], ys[:, 1], 50, normed=True)
# plt.plot(xs, np.exp(log_dens))

t = np.vstack([X.ravel(), Y.ravel()])
xs = np.array([[el[0], el[1]] for el in zip(t[0], t[1])])
Z = np.exp(kde.score_samples(xs))
plt.figure()
plt.contour(X, Y, Z.reshape(X.shape))

ys = ys[rnd.choice(len(ys), 2000)]
plt.figure()
plt.scatter(ys[:, 0], ys[:, 1])

plt.show()
