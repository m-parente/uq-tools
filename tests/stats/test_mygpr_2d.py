from uq_tools.stats.regression import ExtendedGPR

import numpy as np
import numpy.random as rnd
import sklearn.gaussian_process as skl_gp
from sklearn.gaussian_process.kernels import RBF


def f(x):
    return x[:, 0]**2 + x[:, 1]**2


kernel = 1.*RBF()
noise = 2

my_gpr = ExtendedGPR(kernel=kernel, alpha=noise, optimizer=None)
skl_gpr = skl_gp.GaussianProcessRegressor(kernel=kernel, alpha=noise, optimizer=None)

N = 10
xs = rnd.uniform(-5., 5., size=(N, 2))
fs = f(xs)
ys = fs + rnd.multivariate_normal(mean=np.zeros(N), cov=noise*np.eye(N))

my_gpr.fit(xs, ys)
skl_gpr.fit(xs, ys)

X_ = rnd.uniform(-4., 4., size=(10, 2))  # Test points
assert(np.allclose(my_gpr.predict(X_, return_std=True), skl_gpr.predict(X_, return_std=True)))

x_new = [0., 0.]
y_new = 0.5

xs = np.append(xs, [x_new], axis=0)
ys = np.append(ys, y_new)

my_gpr.add_data(x_new, y_new)
skl_gpr.fit(xs, ys)

X_ = rnd.uniform(-4., 4., size=(10, 2))  # Test points
assert(np.allclose(my_gpr.predict(X_, return_std=True), skl_gpr.predict(X_, return_std=True)))
