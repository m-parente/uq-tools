import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd
import scipy.stats as stats
import seaborn as sns

sns.set()


n = 5
nd = 10
A = rnd.normal(size=(nd, n))

phi = stats.norm().cdf


def d_phi(s): return np.exp(-0.5 * s**2) / np.sqrt(2*np.pi)


def Phi(s): return np.array([phi(si) for si in s])


def jac_Phi(s): return np.diag([d_phi(si) for si in s])


def f(x): return 0.5 * x @ A.T @ A @ x


def grad_f(x): return A.T @ A @ x


def f_tilde(s): return f(Phi(s))


def grad_f_tilde(s): return jac_Phi(s).T @ grad_f(Phi(s))


v = np.ones(n)
s = rnd.normal(size=n)
hs = 1/10**np.array([1, 2, 3, 4, 5])
errs = np.empty(len(hs))

for i, h in enumerate(hs):
    errs[i] = np.abs(f_tilde(s+h*v) - f_tilde(s) - h*v @ grad_f_tilde(s))

plt.figure()
plt.loglog(hs, errs)
plt.tight_layout()
plt.show()
