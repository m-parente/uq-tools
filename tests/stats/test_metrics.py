from uq_tools.stats import metrics

import numpy as np
import numpy.random as rnd
from scipy import stats


num_samples = 10000

print("=== Test estimation of KL divergence of two normal distributions ===")
print("Number of samples used: %i" % num_samples)

eps_mu = 1
eps_sigma = 1

mu_1 = 0
mu_2 = mu_1+eps_mu
sigma_1 = 1
sigma_2 = sigma_1+eps_sigma

ps = rnd.normal(loc=mu_1, scale=sigma_1, size=num_samples)
X_1 = stats.norm(loc=mu_1, scale=sigma_1)
X_2 = stats.norm(loc=mu_2, scale=sigma_2)

kl_div_est = metrics.kl_div(X_1.pdf, X_2.pdf, ps)
kl_div_exct = (mu_1-mu_2)**2/(2*sigma_2**2) + 0.5 * \
    (sigma_1**2/sigma_2**2 - 1 - np.log(sigma_1**2/sigma_2**2))
print("Difference of estimated and exact KL divergence: %f"
      % np.abs(kl_div_est-kl_div_exct))


# =================
num_samples = 1000000

print("=== Test estimation of KL divergence of two posteriors ===")
print("Number of samples used: %i" % num_samples)

# The two posteriors is the standard normal distribution and a normal distribution with a non-unit variance.
sigma = 3

samples_1 = rnd.normal(scale=sigma, size=num_samples)
samples_0 = rnd.normal(scale=1, size=num_samples)

kl_div_est1 = metrics.kl_div_post(lambda x: 0.5/sigma**2 * x**2, lambda x: 0.5 * x**2, samples_1)
kl_div_est2 = metrics.kl_div_post2(lambda x: 0.5/sigma**2 * x**2,
                                   lambda x: 0.5 * x**2,
                                   samples_1, samples_0)
kl_div_exct = 0.5 * (sigma**2 - 1 - np.log(sigma**2))

print(kl_div_est1)
print(kl_div_est2)
print(kl_div_exct)
