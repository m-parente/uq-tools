from uq_tools.stats.regression import ExtendedGPR

import numpy as np
import numpy.random as rnd
import sklearn.gaussian_process as skl_gp
from sklearn.gaussian_process.kernels import RBF


kernel = 1.*RBF()
noise = 2

my_gpr = ExtendedGPR(kernel=kernel, alpha=noise, optimizer=None)
skl_gpr = skl_gp.GaussianProcessRegressor(kernel=kernel, alpha=noise, optimizer=None)

N = 10
xs = np.linspace(-5., 5., num=N)
fs = xs**2
ys = fs + rnd.multivariate_normal(mean=np.zeros(N), cov=noise*np.eye(N))

X = xs[:, np.newaxis]
y = ys
my_gpr.fit(X, y)
skl_gpr.fit(X, y)

X_ = np.linspace(-4., 4., num=10)[:, np.newaxis]  # Test points
assert(np.allclose(my_gpr.predict(X_, return_std=True), skl_gpr.predict(X_, return_std=True)))

x_new = 0.
y_new = 0.5

xs = np.append(xs, x_new)
X = xs[:, np.newaxis]
y = np.append(y, y_new)

my_gpr.add_data(x_new, y_new)
skl_gpr.fit(X, y)

X_ = np.linspace(-4., 4., num=10)[:, np.newaxis]  # Test points
assert(np.allclose(my_gpr.predict(X_, return_std=True), skl_gpr.predict(X_, return_std=True)))
