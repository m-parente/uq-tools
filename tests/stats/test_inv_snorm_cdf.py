import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd
import scipy.stats as stats
import seaborn as sns

sns.set()


isf = stats.norm().isf  # Inverse survival function
phi = stats.norm().cdf  # CDF of standard normal distribution


# Inverse CDF of standard normal distribution
def iphi(x): return isf(1-x)


N = 10**4
xs = rnd.uniform(size=N)
assert(np.allclose(xs, phi(iphi(xs))))
assert(np.allclose(xs, iphi(phi(xs))))

fig, (ax1, ax2) = plt.subplots(1, 2)
sns.distplot(xs, ax=ax1)

ss = iphi(xs)
sns.distplot(ss, ax=ax2)

plt.tight_layout()
plt.show()
