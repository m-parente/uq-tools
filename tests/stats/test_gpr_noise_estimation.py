import plotters
from uq_tools.stats.regression import ExtendedGPR

# import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd
from sklearn.gaussian_process.kernels import RBF


n = 2
N = 400
noise_var = 0.1

xs = rnd.uniform(low=-1, high=1., size=(N, n))
fs = xs[:, 0]**2 + xs[:, 1]**2
data = fs + rnd.normal(loc=0.0, scale=np.sqrt(noise_var), size=len(xs))

gpr = ExtendedGPR(kernel=1.0 * RBF(length_scale=1.0), n_restarts_optimizer=20)
gpr.fit(xs, data)

# xs_plt = np.linspace(-1, 1, num=25)
# plotters.plot_skl_gpr_2d(gpr, xs_plt, xs_plt)
# plt.show()
# exit()

estim_noise_var = gpr.estimate_noise_var()

print("True noise variance:\t%.4f" % noise_var)
print("Estimated variance:\t%.4f" % estim_noise_var)
