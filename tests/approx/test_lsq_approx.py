from uq_tools import approx

import itertools as it
import numpy as np
import numpy.random as rnd

import matplotlib.pyplot as plt


def f(x):
    # return 3*x**2 - 4
    return 1/(x+0.1)**2 + np.exp(5*x)


n = 50
xs = np.linspace(0, 1, n)

fs_noisy = f(xs)  # + rnd.normal(loc=0., scale=0.5*1e1, size=n)
xs = xs[:, np.newaxis]
approx_func = approx.LeastSquaresPolynomialApproximation(xs, fs_noisy)

print(approx_func._coeffs())

xs_plot = np.linspace(0, 1, 200)[:, np.newaxis]
fs_approx = approx_func(xs_plot)

plt.figure()
plt.plot(approx_func.x, approx_func.y, 'o')
plt.plot(xs_plot, fs_approx)

new_data_points = [np.array([[0.]]), np.array([20.])]
print(approx_func.update(new_data_points[0], new_data_points[1]))
fs_approx = approx_func(xs_plot)

print(approx_func._coeffs())

plt.plot(new_data_points[0], new_data_points[1], 'o')
plt.plot(xs_plot, fs_approx)
plt.legend(['Data', 'LS approx.', 'New data point', 'Updated LS approx.'])

# plt.show()
