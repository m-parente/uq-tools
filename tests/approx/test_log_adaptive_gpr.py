import plotters
from uq_tools.approx import AdaptiveGPR, LogAdaptiveGPR

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd
from sklearn.gaussian_process.kernels import RBF


n = 2
N = 100
log_noise_var = 0.1

xs = rnd.uniform(low=-1, high=1., size=(N, n))
log_fs = xs[:, 0]**2 + xs[:, 1]**2
log_data = log_fs + rnd.normal(loc=0.0, scale=np.sqrt(log_noise_var), size=len(xs))
data = np.exp(log_data)

xs_pred = rnd.uniform(low=-1, high=1., size=(10, n))

log_gpr = AdaptiveGPR(xs, data,
                      kernel=1.0*RBF(length_scale=1.0),
                      noise_var=log_noise_var,
                      n_restart_optimizer=20,
                      log=True)
log_preds = log_gpr(xs_pred)

log_gpr_wrapper = LogAdaptiveGPR(xs, data,
                                 kernel=1.0*RBF(length_scale=1.0),
                                 log_noise_var=log_noise_var,
                                 n_restart_optimizer=20)
log_wrapper_preds = log_gpr_wrapper(xs_pred)

print(np.abs(log_preds-log_wrapper_preds) / np.abs(log_preds))

xs_plt = np.linspace(-1, 1, num=20)
plotters.plot_gpr_2d(log_gpr, xs_plt, xs_plt)

plt.show()
