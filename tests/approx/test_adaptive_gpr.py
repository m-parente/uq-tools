from uq_tools import approx

import numpy as np
import numpy.random as rnd


N = 10
noise = 2


def f_1d(x):
    return x**2


xs = rnd.uniform(size=N)
data = f_1d(xs) + rnd.multivariate_normal(mean=np.zeros(N), cov=noise*np.eye(N))

gpr = approx.AdaptiveGPR(xs, data, noise_var=noise)
x_new = rnd.uniform()
data_new = f_1d(x_new)
gpr.update(x_new, data_new)

assert(np.allclose(np.append(xs, [x_new], axis=0), gpr.xs.flatten()))
assert(np.allclose(np.append(data, data_new), gpr.data))


def f_2d(x):
    x = np.atleast_2d(x)
    return x[:, 0]**2 + x[:, 1]**2


xs = rnd.uniform(size=(N, 2))
data = f_2d(xs)

gpr = approx.AdaptiveGPR(xs, data, noise_var=noise)
x_new = rnd.uniform(size=2)
data_new = f_2d(x_new)[0]
gpr.update(x_new, data_new)

assert(np.allclose(np.append(xs, [x_new], axis=0), gpr.xs))
assert(np.allclose(np.append(data, data_new), gpr.data))
