from uq_tools import mcmc

import numpy as np
import numpy.random as rnd
import scipy.stats as stats

import matplotlib.pyplot as plt
import seaborn as sns

sns.set()
sns.set_context('talk')

dim = 8
burnIn = 8000
steps = 10000

nrv = stats.multivariate_normal(mean=dim * [0.], cov=np.eye(dim))


def dens(x):
    return nrv.pdf(x)


samples = mcmc.mh_mcmc(dens,
                       lambda xk: rnd.multivariate_normal(
                           mean=xk, cov=0.6 * np.eye(dim)),
                       x1=dim * [0.],
                       steps=10000,
                       nPlotAccptRate=1000)

plt.figure()
plt.plot(list(range(burnIn, burnIn + 2000)), samples[burnIn:burnIn + 2000, 0])
plt.tight_layout()

plt.show()
