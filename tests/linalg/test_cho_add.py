import uq_tools.linalg as la

import numpy as np
import numpy.random as rnd
from scipy.linalg import cholesky


n = 30
A = rnd.uniform(size=(n, n))
A = np.dot(A.T, A)
L_A = cholesky(A, lower=True)

# Test adding multiple rows/columns
k = 25
K = A[:k, :k]
M = A[:k, k:]
N = A[k:, k:]

L_K = cholesky(K, lower=True)

assert(np.allclose(L_A, la.cho_add(L_K, M, N)))

# Test adding one row/column
K = A[:-1, :-1]
v = A[-1, :-1]
vv = A[-1, -1]
L_K = cholesky(K, lower=True)

assert(np.allclose(L_A, la.cho_add(L_K, v, vv)))
