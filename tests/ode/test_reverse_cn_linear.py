from uq_tools import ode

import numpy as np

import matplotlib.pyplot as plt

a = 2.
T = 1
dt = 0.01


def y_exact(t):
    return np.exp(a * t)


y = ode.reverse_cn_linear(lambda _: np.array([[a]]), lambda _: np.array([0.]), np.array([
                          np.exp(a * T)]), T, dt)
print(y[0, 0])
assert(np.abs(y[0, 0] - 1.) < 1e-4)

plt.figure()
ts = np.linspace(0, 1, int(T / dt) + 1)
plt.plot(ts, y[0, :])
plt.plot(ts, y_exact(ts), '--')
plt.legend(['numerical', 'exact'])
plt.show()
