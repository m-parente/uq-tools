from uq_tools import ode

import numpy as np

import matplotlib.pyplot as plt


# Test for backward Euler with y' = a*y, y(0) = y0
a = 2
y0 = np.array([1.])
dt = 0.001
T = 1


def y_exact(t):
    return np.exp(a * t)


def f(y, t):
    return a * y


def jac(y, t):
    return a


y = ode.bw_euler(f, jac, dt, y0, int(T / dt))

plt.figure()
ts = np.linspace(0., T, int(T / dt) + 1)
plt.plot(ts, y[0, :])
plt.plot(ts, y_exact(ts))
plt.legend(['numerical', 'exact'])

plt.show()
