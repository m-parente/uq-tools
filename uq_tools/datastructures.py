import numpy as np


class GPRUpdateList:
    def __init__(self, n_pts_diffs_avg):
        self.n_pts_diffs_avg = n_pts_diffs_avg if n_pts_diffs_avg > 1 else 2
        self.all_pts = []
        self.all_avgs = [-1]
        self.add_pt = self._add_pt_init

    def _add_pt_init(self, pt):
        self.all_pts.append(pt)
        self.add_pt = self._add_pt

    def _add_pt(self, pt):
        self.all_pts.append(pt)
        self.all_avgs.append(self.average_diffs())

    def average_diffs(self):
        pts_avg = self.all_pts[-self.n_pts_diffs_avg:]
        L = len(pts_avg)
        if L < 2:
            raise RuntimeError()

        return (pts_avg[-1] - pts_avg[0]) / (L-1)

    def size(self):
        return len(self.all_pts)

    def packed(self):
        return self.size() >= self.n_pts_diffs_avg

    def min_for_desired_average(self, desired_average):
        L = self.size()
        if L < 1:
            raise RuntimeError()

        if self.packed():
            K = self.n_pts_diffs_avg
            new_last = self.all_pts[-(K-1)]
        else:
            K = L + 1
            new_last = self.all_pts[0]

        return desired_average*(K-1) + new_last
