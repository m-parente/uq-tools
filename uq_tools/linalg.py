import numpy as np
import scipy.linalg as la


def cho_add_1d(L, v, vv):
    l = la.solve_triangular(L, v, lower=True)
    ll = np.sqrt(vv - np.dot(l, l))
    return np.hstack([np.vstack([L, l]), np.append(np.zeros_like(v), ll)[:, np.newaxis]])


def cho_add(L, M, N):
    if np.ndim(M) < 2:
        M = M[:, np.newaxis]
    N = np.atleast_2d(N)

    S = la.solve_triangular(L, M, lower=True)
    U = la.cholesky(N - np.dot(S.T, S), lower=True)

    return np.hstack([np.vstack([L, S.T]), np.vstack([np.zeros_like(M), U])])
