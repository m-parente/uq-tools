import numpy as np
import numpy.linalg as la
import scipy.linalg as spla


# Backward Euler for y' = f(y,t), y(0) = y0
def bw_euler(f, jac, dt, y0, n_steps, rtol_newt=1e-6, atol_newt=1e-6, max_newt=10):
    if np.isscalar(atol_newt):
        atol_newt = atol_newt*np.ones(len(y0))

    n = len(y0)

    def res(y, y_old, t):
        return y - y_old - dt * f(y, t)

    def jac_res(y, t):
        return np.eye(n) - dt * jac(y, t)

    y = np.empty((n, n_steps+1))
    y[:, 0] = y0

    y_cur = y0
    t_cur = 0.

    for step in range(1, n_steps+1):
        # print "BW Euler step %i: %s" % (step, y_cur)
        y_old = y_cur
        t_old = t_cur
        t_cur += dt

        # Newton loop
        res_newt = res(y_cur, y_old, t_cur)

        max_it = max_newt
        while max_it > 0:
            max_it -= 1
            jac_res_newt = jac_res(y_old, t_old)
            y_diff = la.solve(jac_res_newt, -res_newt)
            y_cur = y_diff + y_cur

            if np.all(np.abs(y_diff) <= rtol_newt*dt*y_cur + atol_newt*dt):
                break

            res_newt = res(y_cur, y_old, t_cur)

        y[:, step] = y_cur
        # print "%i Newton steps done" % (max_newt - max_it)

    return y


# Reverse Crank-Nicolson for y' = A(t)y + b(t), y(T) = yT
def reverse_cn_linear(A, b, yT, T, dt):
    n = len(yT)
    n_steps = int(T / dt)

    y = np.empty((n, n_steps + 1))
    y[:, -1] = yT
    t = T

    A_before = A(n_steps)
    b_before = b(n_steps)

    I_n = np.eye(n)

    for step in range(n_steps - 1, -1, -1):
        b_tmp = b(step)
        rhs = np.dot(I_n - 0.5 * dt * A_before, y[:, step + 1]) \
            - 0.5 * dt * (b_before + b_tmp)
        b_before = b_tmp

        A_before = A(step)
        mat = I_n + 0.5 * dt * A_before
        y[:, step] = la.solve(mat, rhs)

    return y
