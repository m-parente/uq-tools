from uq_tools.mcmc import mh_mcmc_post
from uq_tools.stats import metrics

import numpy as np
import numpy.random as rnd
import time


def adapt_mh_temp(misfit_approx, misfit_full, prior, proposal_sampler, x0, n_steps, uncert_tol0,
                  L, T, K, c, tol_kl, proposal_sampler_kl):
    samples = np.empty((n_steps+1, len(x0)))
    samples[0, :] = x0

    xk = np.array(x0)
    prior_xk = prior(xk)
    mf_approx_xk, uncert_xk = misfit_approx(xk, return_uncert=True)

    def temp(uncert):
        return 1/(c*uncert**2 + 1)

    temps = np.empty(n_steps+1)
    temps[0] = temp(uncert_xk)

    update_pts = []
    updates = np.zeros(n_steps)
    last_update_pt = 0
    avg_dists = []
    uncert_tol = uncert_tol0

    kl_divs = []

    k = 1

    n_updates = 0
    n_updates_since_last_check = 0
    n_accptd = 0
    n_steps_wo_update = 0

    misfit_curr = misfit_approx
    misfit_prev = misfit_curr.copy()

    tempering_finished = False
    adapt_finished = False

    i = 0

    def kl_div_post_curr_prev():
        samples_curr = mh_mcmc_post(misfit=misfit_curr,
                                    prior=prior,
                                    proposal_sampler=proposal_sampler_kl,
                                    x0=xk,
                                    steps=20000,
                                    nPlotAccptRate=500)
        return metrics.kl_div_post(misfit_curr, misfit_prev, samples_curr)

    def calc_curr_avg_distance():
        if len(update_pts) > 0:
            L_tmp = min(L, len(update_pts))
            return (k-update_pts[-L_tmp]) / L_tmp

        return k-last_update_pt

    while k <= n_steps:
        x_ = proposal_sampler(xk)
        prior_x_ = prior(x_)
        mf_approx_x_, uncert_x_ = misfit_curr(x_, return_uncert=True)
        print("Uncertainty at step %i: %f (%s)" % (k, uncert_x_, str(x_)))
        print("Uncertainty level: %f" % uncert_tol)

        if not adapt_finished:
            # During adaptation
            if uncert_x_ > uncert_tol:
                print("Updating GP...")
                misfit_curr.update(x_, misfit_full(x_))

                mf_approx_xk, uncert_xk = misfit_curr(xk, return_uncert=True)
                mf_approx_x_, uncert_x_ = misfit_curr(x_, return_uncert=True)

                update_pts.append(k-1)
                last_update_pt = k-1
                n_updates += 1
                n_updates_since_last_check += 1
                updates[k-1] = 1

                avg_dists.append(calc_curr_avg_distance())

                if n_updates_since_last_check >= K:
                    print("Maximum number of updates since last check reached.")
                    print("Updating GPR parameters...")
                    misfit_approx.update_params()
                    print("New GPR parameters: %s" % misfit_approx.kernel)

                    # Check if density has changed with the last updates by computing
                    # the KL divergence between the previous and current density.
                    print("Check KL divergence due to too many updates.")
                    time.sleep(5)
                    kl_div = kl_div_post_curr_prev()
                    print("KL divergence between last two approximations: %.3f" % kl_div)
                    kl_divs.append(kl_div)
                    if kl_div < tol_kl:
                        # if not tempering_finished:
                        #     tempering_finished = True
                        #     print("Tempering finished.")
                        # else:
                        adapt_finished = True
                        print("Adaptation finished.")
                    else:
                        misfit_prev = misfit_curr.copy()
                        n_updates_since_last_check = 0
                        i += 1
                    time.sleep(5)
                    # if i >= 3:
                    #     adapt_finished = True
            else:
                # Test if average of distances between last L update points is over the threshold
                avg_dist = calc_curr_avg_distance()

                print("Average of last L distances: %f" % avg_dist)

                if avg_dist > T:
                    uncert_tol /= 1.5
                    update_pts = []
                    last_update_pt = k-1
                    # n_updates_since_last_check = 0
                    print("Change uncertainty level to %f." % uncert_tol)
                    time.sleep(5)

            print("Total number of updates so far: %i" % n_updates)
            print("Distance from last update: %i" % ((k-1) - last_update_pt))

            temp_xk = temp(uncert_xk) if not tempering_finished else 1.
            print("Temperature: %s" % temp_xk)

            # pre-accept / pre-reject
            accpt_prob = np.min([1, np.exp(temp_xk*mf_approx_xk - temp_xk*mf_approx_x_) * prior_x_ / prior_xk]) \
                if prior_x_ > 0 else 0
        else:
            # After adaptation
            temp_xk = 1.0
            accpt_prob = np.min([1, np.exp(mf_approx_xk - mf_approx_x_) * prior_x_ / prior_xk]) \
                if prior_x_ > 0 else 0

        if rnd.uniform() < accpt_prob:
            # pre-accept
            xk = x_
            prior_xk = prior_x_
            mf_approx_xk = mf_approx_x_
            uncert_xk = uncert_x_
            n_accptd += 1

        samples[k, :] = xk
        temps[k] = temp_xk

        # if (k+1) % 100 == 1:
        print("Acceptance rate at k=%i: %f" % (k, n_accptd/k))

        k += 1

    update_pts = np.argwhere(updates > 0.5).flatten()

    return samples, temps, update_pts, np.array(avg_dists), kl_divs
