import matplotlib.pyplot as plt
import numpy as np


name = 'lined_middle_fine'
kl_eigvals = np.loadtxt("tmp/kl_eigvals_%s.txt" % name)

plt.figure()
# plt.yscale('log')
plt.plot(range(1, len(kl_eigvals)+1), kl_eigvals)

plt.show()
