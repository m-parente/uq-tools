import matplotlib.pyplot as plt
import numpy as np


def dofp(n,p):
	def a(v,l):
		return np.sum(v) + (a(np.cumsum(v),l-1) if l > 0 else 0)
	return a(np.concatenate([[1], np.zeros(n-1, dtype=int)]), p)

print(dofp(3 ,6))
exit()

Z = [[dofp(n,p) for n in range(1,10)] for p in range(10)]

plt.figure()
plt.contour(Z, levels=1000)

plt.show()

