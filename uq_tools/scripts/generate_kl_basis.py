from uq_tools.kl import KLExpansion

from dolfin import *
from dolfin_utils.meshconvert import meshconvert
import numpy as np


name = 'lined_middle'
meshdir = '../../examples/layered_porous_media/meshes/'
meshname = meshdir + name

# meshconvert.convert2xml(meshname + ".msh", meshname + ".xml")
# exit()
mesh = Mesh(meshname + ".xml")
mesh.init()

V = FunctionSpace(mesh, "CG", 1)

# Create the covariance expression to project on the mesh.
corr_len_x = 0.05
corr_len_y = 0.05
var = 1
num_kl = 35


class ExponentialKernel(UserExpression):
    def eval(self, value, x):
        value[0] = var * np.exp(-np.abs(x[0]-x[1])/corr_len_x - np.abs(x[2]-x[3])/corr_len_y)

    def value_dimension(self):
        return 1


class GaussianKernel(UserExpression):
    def eval(self, value, x):
        value[0] = var * np.exp(-(x[0]-x[1])*(x[0]-x[1])/corr_len_x -
                                (x[2]-x[3])*(x[2]-x[3])/corr_len_y)

    def value_dimension(self):
        return 1


# cov_expr = ExponentialKernel(element=V.ufl_element())
cov_expr = GaussianKernel(element=V.ufl_element())

kl = KLExpansion(mesh, cov_expr)
eigVals, eigFuncs = kl.compute(num_kl)

np.savetxt('tmp/kl_eigvals_%s.txt' % name, eigVals)
i = 0
for eigFunc in eigFuncs:
    f = File('tmp/kl_eigfunc_%s_%i.xml' % (name, i))
    f << eigFunc
    i += 1
