from uq_tools.stats.regression import ExtendedGPR

import copy
import numpy as np
import numpy.linalg as la
from sklearn import gaussian_process as gp
from sklearn.gaussian_process import kernels
from sklearn import linear_model as lm
from sklearn import pipeline as pipe
from sklearn import preprocessing as preproc


class FunctionApproximation(object):
    def __init__(self, xs, data):
        assert(len(xs) == len(data))
        self.xs = xs if np.ndim(xs) > 1 else xs[:, np.newaxis]
        self.data = data

    def _add_data(self, x_new, data_new):
        x_new = np.atleast_2d(x_new)
        assert(len(x_new) == (len(data_new) if np.ndim(data_new) > 0 else 1))

        self.xs = np.append(self.xs, x_new, axis=0)
        self.data = np.append(self.data, data_new)

    def _doUpdate(self, x_new, data_new):
        self._fit()

    def _params(self):
        pass

    def data(self):
        return self.data

    def update(self, x_new, data_new):
        self._add_data(x_new, data_new)

        old_params = self._params()

        self._doUpdate(x_new, data_new)

        new_params = self._params()

        return la.norm(old_params-new_params) / la.norm(new_params)


class LeastSquaresPolynomialApproximation(FunctionApproximation):
    def __init__(self, x, y, order=2):
        FunctionApproximation.__init__(self, x, y)
        self.model = pipe.Pipeline([('poly', preproc.PolynomialFeatures(order)),
                                    ('linear', lm.LinearRegression())])
        self._fit()

    def __call__(self, x):
        return self.model.predict(x)

    def _fit(self):
        self.model.fit(self.x, self.y)

    def _params(self):
        lin_props = self.model.named_steps['linear']
        coeffs = np.copy(lin_props.coef_)
        coeffs[0] = lin_props.intercept_
        return coeffs

    def error(self):
        return sum(np.abs(self.y - self(self.x)) / np.abs(self.y)) / len(self.y)


class AdaptiveGPR(FunctionApproximation):
    def __init__(self, xs, data, kernel=None, noise_var=1e-10, n_restart_optimizer=1, log=False):
        assert(len(xs) == len(data))
        if np.ndim(xs) == 1:
            xs = xs[:, np.newaxis]

        if kernel is None:
            kernel = 1.0*kernels.RBF()

        super().__init__(xs, data)

        self.noise_var = noise_var
        self.n_restart_optimizer = n_restart_optimizer
        self.gpr = ExtendedGPR(kernel=kernel,
                               alpha=noise_var,
                               n_restarts_optimizer=n_restart_optimizer)
        self.log = log
        self._fit()  # Initialization; computes actually too much (caused by sklearn workaround)
        # self.update_params()

    def __call__(self, X, return_uncert=False):
        X = np.atleast_2d(X)

        if self.log:
            pred, pred_std = self.gpr.predict(X, return_std=True)
            pred_var = pred_std**2

            mean = np.exp(pred + 0.5*pred_var)

            if not return_uncert:
                return mean
            else:
                return mean, np.sqrt((np.exp(pred_var) - 1) * np.exp(2*pred+pred_var))
        else:
            if return_uncert:
                pred, pred_std = self.gpr.predict(X, return_std=True)
                return pred, pred_std
            else:
                return self.gpr.predict(X, return_std=False)

    def _fit(self):
        self.gpr.fit(self.xs, self.data if not self.log else np.log(self.data))
        self.kernel = self.gpr.kernel_

    def _doUpdate(self, x_new, data_new):
        self.gpr.add_data(x_new, data_new if not self.log else np.log(data_new))

    def _params(self):
        return self.kernel.theta

    def copy(self):
        return copy.deepcopy(self)

    def update_params(self):
        # self.gpr.update_params()
        self._fit()
        self.kernel = self.gpr.kernel_

    def update_noise_var(self):
        self.gpr.alpha = self.gpr.estimate_noise_var()

    def error_at(self, x):
        return self(x, return_uncert=True)[1]

    def sample_y(self, X, n_samples=1, random_state=0):
        if np.ndim(X) == 1:
            X = np.asarray(X)[:, np.newaxis]

        return self.gpr.sample_y(X, n_samples, random_state)

    def log_marginal_likelihood(self, theta=None):
        return self.gpr.log_marginal_likelihood(self._params() if theta is None else theta)


class LogAdaptiveGPR(AdaptiveGPR):  # Do not use so far!
    def __init__(self, xs, data, kernel=None, log_noise_var=1e-10, n_restart_optimizer=1):
        super().__init__(xs=xs, data=np.log(data),
                         kernel=kernel,
                         noise_var=log_noise_var,
                         n_restart_optimizer=n_restart_optimizer)

    def __call__(self, X, return_uncert=False):
        pred, pred_std = super().__call__(X, return_uncert=True)
        pred_var = pred_std**2

        mean = np.exp(pred + 0.5*pred_var)

        if return_uncert:
            return mean, np.sqrt((np.exp(pred_var) - 1) * np.exp(2*pred+pred_var))
        else:
            return mean

    def _doUpdate(self, x_new, data_new):
        super()._doUpdate(x_new, np.log(data_new))
