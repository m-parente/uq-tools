from uq_tools.linalg import cho_add

import numpy as np
import scipy.linalg as la
import sklearn.gaussian_process as skl_gp
from sklearn.gaussian_process.kernels import WhiteKernel
import sklearn.linear_model as lm
import sklearn.pipeline as pipe
import sklearn.preprocessing as pre_proc


def polynomial_fit(x, y, order=2):
    """
    Fits a polynomial to function values.

    :param x: List of locations
    :param y: List of function values
    :param integer order: Polynomial order
    """
    x = x[:, np.newaxis] if len(np.shape(x)) <= 1 else x

    model = pipe.Pipeline([('poly', pre_proc.PolynomialFeatures(order)),
                           ('linear', lm.LinearRegression())])
    model.fit(x, y)
    # print(model.named_steps['poly'].powers_)
    # print(model.named_steps['linear'].coef_)

    return lambda x_: model.predict(x_[np.newaxis, :] if len(np.shape(x_)) <= 1 else x_)


def linear_fit(x, y):
    """Fits a linear function to function values."""
    reg = lm.LinearRegression()
    reg.fit(x[:, np.newaxis], y)

    return reg


def quadratic_fit(x, y):
    """Fits a quadratic function to function values."""
    return polynomial_fit(x, y, order=2)


class ExtendedGPR(skl_gp.GaussianProcessRegressor):
    def __init__(self, kernel=None, alpha=1e-10,
                 optimizer="fmin_l_bfgs_b", n_restarts_optimizer=0,
                 normalize_y=False, copy_X_train=True, random_state=None):
        super().__init__(kernel=kernel, alpha=alpha,
                         optimizer=optimizer, n_restarts_optimizer=n_restarts_optimizer,
                         normalize_y=normalize_y, copy_X_train=copy_X_train, random_state=random_state)

    def add_data(self, x, y, alpha=None):
        x = np.atleast_2d(x)

        self.X_train_ = np.append(self.X_train_, x, axis=0)
        self.y_train_ = np.append(self.y_train_, y)

        K = self.kernel_(self.X_train_)
        if alpha is None and not np.isscalar(self.alpha):
            raise Exception("Alpha value for new training point not specified nor inferable")
        K[np.diag_indices_from(K)] += self.alpha if alpha is None else alpha

        self.L_ = cho_add(self.L_, K[-1, :-1], K[-1, -1])
        self._K_inv = None

        self.alpha_ = la.cho_solve((self.L_, True), self.y_train_)

    def update_params(self):  # Update kernel parameters and noise variance
        kernel = self.kernel + WhiteKernel(noise_level=1.0)
        gpr = skl_gp.GaussianProcessRegressor(kernel=kernel, alpha=self.alpha,
                                              optimizer=self.optimizer,
                                              n_restarts_optimizer=self.n_restarts_optimizer,
                                              normalize_y=self.normalize_y,
                                              copy_X_train=self.copy_X_train,
                                              random_state=self.random_state)
        gpr.fit(self.X_train_, self.y_train_)

        self.L_ = gpr.L_
        self._K_inv = gpr._K_inv
        self.alpha_ = gpr.alpha_

        # First kernel is the kernel without WhiteNoise.
        # This was necessary since the sklearn implementation would add WhiteNoise
        # for the prediction as well.
        self.kernel_ = gpr.kernel_.k1
        self.alpha = gpr.kernel_.k2.get_params()['noise_level']

    def estimate_noise_var(self, xs=None, data=None):
        kernel = self.kernel + WhiteKernel(noise_level=1.0, noise_level_bounds=(1e-6, 1e6))
        gpr = skl_gp.GaussianProcessRegressor(kernel=kernel, alpha=self.alpha,
                                              optimizer=self.optimizer,
                                              n_restarts_optimizer=self.n_restarts_optimizer,
                                              normalize_y=self.normalize_y,
                                              copy_X_train=self.copy_X_train,
                                              random_state=self.random_state)
        gpr.fit(self.X_train_ if xs is None else xs, self.y_train_ if data is None else data)
        return gpr.kernel_.get_params()['k2__noise_level']
