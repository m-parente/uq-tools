import numpy as np


def kl_div(dens_1, dens_2, samples_1):
    return np.average(np.log(dens_1(samples_1)/dens_2(samples_1)))


def kl_div_post(misfit_1, misfit_2, samples_1):
    mfs_1 = misfit_1(samples_1)
    mfs_2 = misfit_2(samples_1)

    A = np.average(mfs_2-mfs_1)
    Z = np.average(np.exp(mfs_1-mfs_2))

    return A + np.log(Z)


# Do not use! See test file.
def kl_div_post2(misfit_1, misfit_2, samples_1, samples_coarse):
    A = np.average(misfit_2(samples_1)-misfit_1(samples_1))
    Z = np.average(np.exp(misfit_2(samples_coarse)-misfit_1(samples_coarse)))

    return A + np.log(1/Z)
