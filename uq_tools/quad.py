import numpy as np


def trapez_rule(I, dt, N_max=None):
    if N_max is None:
        N_max = len(I[0, :])
    assert N_max > 0

    return 0.5 * dt * (I[:, 0] + 2*np.sum(I[:, 1:N_max-1], axis=1) + I[:, N_max-1])


def trapez_rule_f(f, a, b, dt):
    assert a <= b
    assert dt > 0
    n_steps = int((b - a) / dt)

    return 0.5 * dt * (f(b)
                       + 2 * np.sum([f(a + step * dt) for step in range(1, n_steps)], axis=0)
                       + f(a))
