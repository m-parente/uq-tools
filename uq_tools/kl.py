from dolfin import *
import numpy as np
from petsc4py import PETSc as petsc


class KLExpansion:
    """Class representing a Karhunen-Loeve (KL) expansion"""

    def __init__(self, mesh, covExpr):
        self.mesh = mesh
        self.covExpr = covExpr

    def compute(self, numKL):
        """
        Computes a number of KL terms.

        :param integer numKL: Number of KL terms to be computed 
        """
        C = self._buildCovarianceMatrix()
        M = self._buildMassMatrix()

        solver = SLEPcEigenSolver(C, M)
        solver.solve(numKL)
        # print numKL
        # print solver.get_number_converged()

        if numKL > solver.get_number_converged():
            numKL = solver.get_number_converged()

        eigFuncs = np.empty(numKL, dtype=object)
        eigVals = np.empty(numKL)

        V = FunctionSpace(self.mesh, "CG", 1)
        for i_kl in range(0, numKL):
            eigVal, _, eigVec, _ = solver.get_eigenpair(i_kl)
            func = Function(V)
            func.vector()[:] = eigVec[dof_to_vertex_map(V)]
            func.vector()[:] = func.vector()[:] / norm(func)
            eigFuncs[i_kl] = func
            eigVals[i_kl] = eigVal

        return eigVals, eigFuncs

    def _buildCovarianceMatrix(self):
        dim = self.mesh.topology().dim()
        numVertices = self.mesh.num_vertices()

        print("--> Build covariance matrix")

        covMat = petsc.Mat().create()
        covMat.setType('aij')
        covMat.setSizes(numVertices, numVertices)
        covMat.setUp()

        # Set up structure for elements belonging to nodes
        node_elems = {}
        for node in entities(self.mesh, 0):
            node_elems[node.index()] = node.entities(dim)
        # ---

        # Set up structures for elements' centroids and volumes
        elem_centroids = {}
        elem_volumes = {}

        for elem in entities(self.mesh, dim):
            cell = Cell(self.mesh, elem.index())
            elem_centroids[elem.index()] = cell.midpoint()
            elem_volumes[elem.index()] = cell.volume()
        # ---

        cov_ij = np.empty(1)

        for i_node in range(0, numVertices):
            for j_node in range(i_node, numVertices):
                tmp_cov_ij = 0
                for i_elem in node_elems[i_node]:
                    x1, y1 = elem_centroids[i_elem].x(), elem_centroids[
                        i_elem].y()
                    vol_elem_i = elem_volumes[i_elem]

                    for j_elem in node_elems[j_node]:
                        x2, y2 = elem_centroids[j_elem].x(), elem_centroids[
                            j_elem].y()
                        vol_elem_j = elem_volumes[j_elem]

                        self.covExpr.eval(cov_ij, np.array([x1, x2, y1, y2]))
                        if cov_ij[0] > 0:
                            tmp_cov_ij += (1.0 / (dim + 1)) * vol_elem_i * (
                                1.0 / (dim + 1)) * vol_elem_j * cov_ij
                covMat.setValue(i_node, j_node, tmp_cov_ij)
                covMat.setValue(j_node, i_node, tmp_cov_ij)
        covMat.assemblyBegin()
        covMat.assemblyEnd()

        print("--> Finished covariance matrix")

        return PETScMatrix(covMat)

    def _buildMassMatrix(self):

        numVertices = self.mesh.num_vertices()

        print("--> Build mass matrix")

        V = FunctionSpace(self.mesh, "CG", 1)
        u = TrialFunction(V)
        v = TestFunction(V)
        a = u * v * dx

        M_tmp = assemble(a)
        M_tmp = M_tmp.array()

        M = petsc.Mat().create()
        M.setType('aij')
        M.setSizes(numVertices, numVertices)
        M.setUp()

        vertex_to_dof = vertex_to_dof_map(V)

        for i_node in range(0, numVertices):
            for j_node in range(i_node, numVertices):
                M_ij = M_tmp[vertex_to_dof[i_node], vertex_to_dof[j_node]]
                if M_ij > 0:
                    M.setValue(i_node, j_node, M_ij)
                    M.setValue(j_node, i_node, M_ij)

        M.assemblyBegin()
        M.assemblyEnd()

        print("--> Finished mass matrix")

        return PETScMatrix(M)
