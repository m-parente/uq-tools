from os import system


def mesh2xml(meshname):
    system("dolfin-convert '" + meshname +
           ".msh' '" + meshname + ".xml' > /dev/null")
    # meshconvert.convert2xml(meshname + ".msh", meshname + ".xml")


class Settings:
    """Class for settings"""

    def __init__(self, filename):
        """Constructor initializes attributes dynamically according to entries in a settings file."""
        with open(filename, 'r') as f:
            for line in f:
                if (not line) or line.startswith("#"):
                    continue

                tokens = line.strip("\n").split(" ")
                setattr(self, tokens[0],
                        self._extractValue(tokens[1]) if len(tokens) == 2 else
                        [self._extractValue(token) for token in tokens[1:]])

    def _extractValue(self, strValue):
        try:
            intValue = int(strValue)
            return intValue
        except:
            pass

        try:
            floatValue = float(strValue)
            return floatValue
        except:
            pass

        if strValue in ('false', 'False'):
            return False
        elif strValue in ('true', 'True'):
            return True
        elif strValue in ('none', 'None'):
            return None

        return strValue
