from uq_tools import asm

import numpy as np
from sklearn import metrics

from matplotlib import cm
from matplotlib import colors
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd
import seaborn as sns

# sns.set()


def _plotValues(axes, values, ydist_scale=0.05):
    for x, y in values:
        axes.annotate(("{0:.3" + ("e" if y > 1e4 or y < 1e-3 else "f") +
                       "}").format(y), xy=(x + 0.05, y + ydist_scale * y))


def plotEigVals(eigVals, minEigVals, maxEigVals, axes=None):
    numEigVals = len(eigVals)

    if axes is None:
        fig = plt.figure()

    axes = plt.gca()

    axes.set_yscale('log')
    axes.margins(0.03)

    ran = list(range(1, numEigVals + 1))
    plt.plot(ran, eigVals, 'o-', color='C0')
    plt.plot(ran, minEigVals, color='C0')
    plt.plot(ran, maxEigVals, color='C0')
    plt.fill_between(ran, minEigVals, maxEigVals, facecolor='C0', alpha=0.2)
    plt.xticks(list(range(1, numEigVals+1)))

    axes.set_xlabel('Eigenvalue index')
    axes.set_ylabel('Eigenvalue')

    plt.tight_layout()

    # _plotValues(axes, zip(ran, eigVals))


def plotActiveSubspace(eigVals, eigVecs,
                       num_plot_eigvals, minEigVals, maxEigVals,
                       minSubspaceErrors, maxSubspaceErrors, meanSubspaceErrors,
                       plot_sep=False, num_plot_eigvecs=None, step_eigvec=3):
    """
    Plots eigenvalues, eigenvectors and subspace errors resulting from an active subspace computation.

    :param eigVals: List of eigenvalues
    :param eigVecs: Matrix with eigenvectors in columns
    :param integer num_plot_eigvals: Number of eigenvalues to plot
    :param minEigVals: Bootstrap minimum of all eigenvalues
    :param maxEigVals: Bootstrap maximum of all eigenvalues
    :param minSubspaceErrors: Bootstrap minimum of subspace errors in all dimensions
    :param maxSubspaceErrors: Bootstrap maximum of subspace errors in all dimensions
    :param meanSubspaceErrors: Bootstrap mean of subspace errors in all dimensions
    """
    if num_plot_eigvecs is None:
        num_plot_eigvecs = num_plot_eigvals

    eigVals = eigVals[:num_plot_eigvals]
    eigVecs = eigVecs[:, :num_plot_eigvecs]
    minEigVals = minEigVals[0:num_plot_eigvals]
    maxEigVals = maxEigVals[0:num_plot_eigvals]
    minSubspaceErrors = minSubspaceErrors[0:num_plot_eigvals - 1]
    maxSubspaceErrors = maxSubspaceErrors[0:num_plot_eigvals - 1]
    meanSubspaceErrors = meanSubspaceErrors[0:num_plot_eigvals - 1]

    print("Eigenvalues: ", eigVals)
    print("Bootstrap minimum eigenvalues: ", minEigVals)
    print("Bootstrap maximum eigenvalues: ", maxEigVals)
    print("Eigenvectors: ", eigVecs)
    print("Minimums of subspace errors: ", minSubspaceErrors)
    print("Maximums of subspace errors: ", maxSubspaceErrors)
    print("Means of subspace errors: ", meanSubspaceErrors)

    # Plot eigenvalues -------------------------
    plotEigVals(eigVals, minEigVals, maxEigVals)
    # ------------------------------------------

    # Plot eigenvectors ------------------------
    numParameters = len(eigVecs[:, 0])

    if plot_sep:
        for i in range(num_plot_eigvecs):
            plt.figure()
            axes = plt.gca()

            eigVec = eigVecs[:, i]

            axes.bar(np.arange(1, numParameters + 1), eigVec)

            axes.xaxis.set_major_locator(ticker.MaxNLocator(integer=True))
            axes.set_xticks(np.arange(1, numParameters+1, step=step_eigvec))
            axes.set_yticks(np.arange(-1, 1.1, step=0.5))

            plt.tight_layout()
    else:
        n = int(np.ceil(np.sqrt(num_plot_eigvecs)))
        fig, axarr = plt.subplots(n, n)

        for i in range(num_plot_eigvecs):
            eigVec = eigVecs[:, i]

            coord = divmod(i, n)
            axes = axarr[coord[0], coord[1]]

            axes.bar(np.arange(1, numParameters + 1), eigVec)

            axes.xaxis.set_major_locator(ticker.MaxNLocator(integer=True))
            axes.set_xticks(np.arange(1, numParameters+1, step=step_eigvec))
            axes.set_yticks(np.arange(-1, 1.1, step=0.5))
            axes.set_xlabel('$\mathbf{w}_%i$' % (i+1))

        fig.subplots_adjust(hspace=0.5, wspace=0.5)

    # ------------------------------------------

    # Plot subspace estimation errors-----------
    plt.figure()
    axes = plt.gca()

    axes.margins(0.03)
    axes.set_yscale('log')

    ran = np.arange(1, num_plot_eigvals)
    plt.plot(ran, meanSubspaceErrors, 'o-', color='C0')
    plt.plot(ran, minSubspaceErrors, 'o-', color='C0')
    plt.plot(ran, maxSubspaceErrors, 'o-', color='C0')
    plt.fill_between(ran, minSubspaceErrors, maxSubspaceErrors, facecolor='C1')

    # axes.set_xlabel("Subspace dimension")
    axes.set_ylabel("Subspace distance")
    axes.xaxis.set_major_locator(ticker.MaxNLocator(integer=True))

    # _plotValues(axes, zip(ran, meanSubspaceErrors))
    # _plotValues(axes, zip(ran, minSubspaceErrors))
    # _plotValues(axes, zip(ran, maxSubspaceErrors))

    plt.tight_layout()
    # -------------------------------------------

    plt.show()


def summaryPlot(w, xs, values, poly_order=2, with_fit=False, plot_range=None, **kwargs):
    """
    Creates a 1D summary plot for a specified direction.

    :param w: Direction
    :param xs: List of locations
    :param values: List of function values
    :param boolean with_fit: Show a regression fit
    :param integer poly_order: Polynomial order of the regression fit (only active if with_fit is True)
    """
    wTx = np.dot(xs, w)

    fig = plt.figure()
    plt.plot(wTx, values, 'o', color='C1', alpha=0.5)
    plt.xlabel(r'$w_1^Tx$')
    plt.ylabel('Data misfit')

    if with_fit:
        resp_surf = asm.response_surface(xs, values, w, poly_order=poly_order)
        print("1D r2 score: %f" % metrics.r2_score(
            values, resp_surf(np.dot(xs, np.transpose([w])))))

        idx = wTx.argsort()
        plot_xs = wTx[idx] if plot_range is None else plot_range
        plt.plot(plot_xs, resp_surf(plot_xs[:, np.newaxis]), color='C0', **kwargs)

    return fig


def summaryPlot2D(w1, w2, xs, values, with_fit=False, poly_order=2):
    """
    Creates a 2D summary plot for specified directions.

    :param w1: First direction
    :param w2: Second direction
    :param xs: List of locations
    :param values: List of function values
    :param boolean with_fit: Show a regression fit
    :param integer poly_order: Polynomial order of the regression fit (only active if with_fit is True)
    """
    w1Tx = np.dot(xs, w1)
    w2Tx = np.dot(xs, w2)

    fig = plt.figure()
    plt.scatter(w1Tx, w2Tx, c=values)
    plt.colorbar()

    if with_fit:
        resp_surf = asm.response_surface(
            xs, values, np.transpose([w1, w2]), poly_order=poly_order)
        print("2D r2 score: %f" % metrics.r2_score(
            values, resp_surf(np.dot(xs, np.transpose([w1, w2])))))

        # y1s = np.arange(np.min(w1Tx), np.max(w1Tx), step=0.05)
        # y2s = np.arange(np.min(w2Tx), np.max(w2Tx), step=0.05)

        # fig, ax = plotSurface(y1s, y2s, resp_surf, alpha=0.3)

        fig = plt.figure()
        ax = plt.gca(projection='3d')
        X = np.arange(np.min(w1Tx), np.max(w1Tx), step=0.05)
        Y = np.arange(np.min(w2Tx), np.max(w2Tx), step=0.05)

        X, Y = np.meshgrid(X, Y)

        res = np.reshape(resp_surf(np.concatenate(
            [list(zip(x, y)) for x, y in zip(X, Y)])), np.shape(X))
        ax.plot_surface(X, Y, res, cmap=cm.coolwarm,
                        linewidth=0, antialiased=False, alpha=0.3)

        colors = np.abs(np.subtract(values, [resp_surf([[x, y]])[0]
                                             for x, y in zip(w1Tx, w2Tx)]))
        scatter = ax.scatter(w1Tx, w2Tx, values, c=colors)

        ax.set_zlim(np.min(values), np.max(values))
        ax.set_xlabel(r'$w_1^Tx$')
        ax.set_ylabel(r'$w_2^Tx$')
        ax.set_zlabel('Data misfit')
        fig.colorbar(scatter)

        return fig, ax
    return fig


def plotTmap(x_tm, rho, T, push_rho, pi=None, Tx=None, rho_pi_share_x=False):
    if len(np.shape(x_tm)) == 1:
        x_tm = x_tm[:, np.newaxis]

    plt.figure()
    ax = plt.gca()

    ax_twx = ax.twinx()
    ax_twy = ax.twiny()
    ax_twx.grid(False)
    ax_twy.grid(False)

    rho_pl, = ax_twx.plot(x_tm, rho.pdf(x_tm), 'b', label=r'$\rho$')
    ax.plot(x_tm, T(x_tm), 'k--', label='T')
    push_rho_pl, = ax_twy.plot(push_rho.pdf(x_tm), x_tm, 'r--', label=r'$T_\sharp \rho$') if rho_pi_share_x else \
        ax_twy.plot(push_rho.pdf(T(x_tm))[:, np.newaxis], T(x_tm),
                    'r--', label=r'$T_\sharp \rho$')

    if pi is not None:
        ax_twy.plot(pi.pdf(x_tm)[:, np.newaxis], x_tm, 'r', label=r'$\pi$') if rho_pi_share_x else \
            ax_twy.plot(pi.pdf(T(x_tm))[:, np.newaxis],
                        T(x_tm), 'r', label=r'$\pi$')
    if Tx is not None:
        ax.plot(x_tm, Tx(x_tm), 'k', label=r'$T^\star$')

    ax.set_xlabel(r'$x_{tmap}$')
    ax.set_ylabel('Map')
    ax_twx.set_ylabel(r'$\rho$')
    ax_twy.set_xlabel(r'$\pi$ / $T_{\sharp}\rho$')

    ax_twx.yaxis.label.set_color(rho_pl.get_color())
    ax_twy.xaxis.label.set_color(push_rho_pl.get_color())

    ax_twx.legend(bbox_to_anchor=(0.82, 1.03), loc=2, borderaxespad=1.)
    ax.legend(bbox_to_anchor=(0.82, 0.98), loc=2, borderaxespad=1.)
    plt.legend(bbox_to_anchor=(0.82, 0.88), loc=2, borderaxespad=1.)

    plt.tight_layout()


def plotTmap2d(x, y, rho, T, push_rho, pi=None, Tx=None, rho_pi_share_x=False, numContours=10, push_rho_samples=None):
    xx, yy = np.meshgrid(x, y)
    x2d = np.vstack((xx.flatten(), yy.flatten())).T
    rho2d = rho.pdf(x2d).reshape(xx.shape)

    plt.figure()
    plt.contour(xx, yy, rho2d,
                levels=np.linspace(
                    np.min(rho2d), np.max(rho2d), num=numContours))
    plt.tight_layout()

    if not rho_pi_share_x:
        x2d = T(x2d)

    push_rho2d = push_rho.pdf(x2d).reshape(xx.shape)

    plt.figure()
    plt.contour(xx, yy, push_rho2d,
                levels=np.linspace(np.min(push_rho2d), np.max(
                    push_rho2d), num=numContours),
                linestyles='dashed')
    if push_rho_samples is not None:
        plt.scatter(push_rho_samples[:, 0],
                    push_rho_samples[:, 1], s=5., c='g')

    x1d = np.vstack((x, np.zeros(len(x)))).T
    Tx = T(x1d)[:, 0]

    plt.figure(figsize=(10, 5))
    plt.subplot(121)
    plt.title(r'$T_1(x_1)$')
    plt.plot(x, Tx, '--')
    plt.xlabel(r'$x_1$')

    Tx = T(x2d)[:, 1]

    plt.subplot(122)
    plt.title(r'$T_2(x_1,x_2)$')
    plt.contour(xx, yy, Tx.reshape(xx.shape), linestyles='dashed')
    plt.xlabel(r'$x_1$')
    plt.ylabel(r'$x_2$')
    plt.colorbar()
    plt.tight_layout()


def plotSamples(x, bins=20, density=True):
    x_sh = np.shape(x)
    if len(x_sh) < 2:
        x = x[:, np.newaxis]
    n, dim = np.shape(x)

    for i in range(dim):
        plt.figure()
        plt.hist(x[:, i], bins, density=density)


def plot_density_diag(samples):
    dim = np.shape(samples)[1]

    df = pd.DataFrame(samples)
    g = sns.PairGrid(df)
    g = g.map_diag(sns.distplot, kde=False, bins=10)
    g = g.map_lower(sns.kdeplot)

    for i in range(dim):
        for j in range(dim):
            if j > i:
                g.axes[i, j].set_visible(False)


def plot_contour(xs, ys, func, levels=None, ax=None):
    X, Y = np.meshgrid(xs, ys)
    Z = np.reshape([func(np.array([x, y]))
                    for y in ys for x in xs], (len(xs), len(ys)))

    if ax is None:
        fig = plt.figure()
        ax = plt.gca()
    else:
        fig = plt.gcf()

    conts = ax.contourf(X, Y, Z, levels=levels)
    plt.colorbar(conts)
    return fig, ax


def plot_surface(xs, ys, func, alpha=1., ax=None):
    X, Y = np.meshgrid(xs, ys)
    Z = np.reshape([func(np.array([x, y]))
                    for y in ys for x in xs], (len(xs), len(ys)))

    if ax is None:
        fig = plt.figure()
        ax = fig.gca(projection='3d')

    surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False, alpha=alpha)
    ax.set_xlabel('$x_1$')
    ax.set_ylabel('$x_2$')
    fig.colorbar(surf)
    plt.tight_layout()

    return fig, ax


def plot_gpr_data_1d(xs, data, pred, pred_std, xs_plt, log=False, uncert_band=True, ax=None, **kwargs):
    if ax is None:
        plt.figure()
        ax = plt.gca()

    ax.plot(xs, data, 'o', color='C1', alpha=0.5)
    ax.plot(xs_plt, pred, 'C0', lw=2, **kwargs)

    if uncert_band:
        ax.fill_between(xs_plt, pred - 1.96*pred_std, pred + 1.96*pred_std,
                        alpha=0.2, color='C0')
    if log:
        ax.set_yscale('log')

    return ax


def plot_gpr_1d(gpr, xs_plt, log=False, title=True, uncert_band=True, ax=None, **kwargs):
    pred, pred_std = gpr(xs_plt[:, np.newaxis], return_uncert=True)

    ax = plot_gpr_data_1d(gpr.xs, gpr.data, pred, pred_std, xs_plt,
                          log=log, uncert_band=uncert_band, ax=ax, **kwargs)

    if title:
        ax.set_title("Kernel: %s\n Log-Likelihood: %.3f"
                     % (gpr.kernel, gpr.log_marginal_likelihood(gpr.kernel.theta)),
                     fontsize=12)
    return ax


def plot_skl_gpr_1d(skl_gpr, xs_plt, log=False):
    pred, pred_std = skl_gpr.predict(xs_plt[:, np.newaxis], return_std=True)

    fig = plot_gpr_data_1d(skl_gpr.X_train_, skl_gpr.y_train_, pred, pred_std, xs_plt, log=log)
    plt.title("Kernel: %s\n Log-Likelihood: %.3f"
              % (skl_gpr.kernel_, skl_gpr.log_marginal_likelihood(skl_gpr.kernel_.theta)),
              fontsize=12)
    plt.tight_layout()
    return fig


def plot_gpr_2d(gpr, xs_plt, ys_plt, with_samples=False, log=False):
    X, Y = np.meshgrid(xs_plt, ys_plt)
    XY = np.array([np.ravel(X), np.ravel(Y)]).T

    fig = plt.figure(figsize=(12, 6))

    # Plot GPR
    ax1 = plt.subplot(1, 2, 1, projection='3d')
    if log:
        ax1.zaxis._set_scale('log')

    z_mean, z_std = gpr(XY, return_uncert=True)
    Z = np.reshape(z_mean, np.shape(X))

    ax1.plot_surface(X, Y, np.log10(Z) if log else Z, color='C0', alpha=0.8)
    ax1.scatter(gpr.xs[:, 0], gpr.xs[:, 1], np.log10(gpr.data) if log else gpr.data,
                color='C1', s=30, zorder=3, edgecolors='k')
    if log:
        zticks = [10**i for i in range(int(min(np.log10(z_mean))),
                                       min(19, int(max(np.log10(z_mean))))+1)]
        ax1.set_zticks(np.log10(zticks))
        ax1.set_zticklabels(["%.0e" % ztick for ztick in zticks])

    if with_samples:
        samples = gpr.sample_y(XY, 10)
        for sample in samples.T:
            Z_sample = np.reshape(sample, np.shape(X))
            ax1.plot_surface(X, Y, Z_sample, alpha=0.2)

    plt.title("Kernel: %s\n Log-Likelihood: %.3f"
              % (gpr.kernel, gpr.log_marginal_likelihood(gpr.kernel.theta)),
              fontsize=12)
    plt.xlabel(r'$\mathbf{x}_1$')
    plt.ylabel(r'$\mathbf{x}_2$')

    # Plot coefficient of standard variation
    plt.subplot(1, 2, 2)

    Z_std = np.reshape(z_std, np.shape(Z))
    if log:
        plt.pcolor(X, Y, Z_std,
                   norm=colors.LogNorm(vmin=Z_std.min(), vmax=Z_std.max()))
    else:
        plt.pcolor(X, Y, Z_std)
    plt.colorbar()
    plt.scatter(gpr.xs[:, 0], gpr.xs[:, 1], c='C1', edgecolors='k')

    plt.title('Standard deviation')
    plt.xlabel(r'$\mathbf{x}_1$')
    plt.ylabel(r'$\mathbf{x}_2$')

    plt.tight_layout()

    return fig


def plot_skl_gpr_2d(gpr, xs_plt, ys_plt):
    X, Y = np.meshgrid(xs_plt, ys_plt)
    XY = np.array([np.ravel(X), np.ravel(Y)]).T

    fig = plt.figure(figsize=(12, 6))

    # Plot GPR
    ax1 = plt.subplot(1, 2, 1, projection='3d')
    if log:
        ax1.zaxis._set_scale('log')

    z_mean, z_std = gpr.predict(XY, return_std=True)
    Z = np.reshape(z_mean, np.shape(X))

    ax1.plot_surface(X, Y, Z, color='C0', alpha=0.8)
    ax1.scatter(gpr.X_train_[:, 0], gpr.X_train_[:, 1], gpr.y_train_,
                color='C1', s=30, zorder=3, edgecolors='k')

    plt.title("Kernel: %s\n Log-Likelihood: %.3f"
              % (gpr.kernel_, gpr.log_marginal_likelihood(gpr.kernel_.theta)),
              fontsize=12)
    plt.xlabel(r'$\mathbf{x}_1$')
    plt.ylabel(r'$\mathbf{x}_2$')

    # Plot coefficient of standard variation
    plt.subplot(1, 2, 2)

    plt.scatter(XY[:, 0], XY[:, 1], c=z_std, s=100)
    plt.colorbar()
    plt.scatter(gpr.X_train_[:, 0], gpr.X_train_[:, 1], c='C1', edgecolors='k')

    plt.title('Standard deviation')
    plt.xlabel(r'$\mathbf{x}_1$')
    plt.ylabel(r'$\mathbf{x}_2$')

    plt.tight_layout()

    return fig


def plot_adapt_mh_stats(samples, temps, update_pts, avg_dists, L, kl_divs):
    # plt.figure()
    # plt.scatter(samples[:, 0], samples[:, 1], s=2)
    # lim = abs(np.max(samples)) + 0.5
    # plt.xlim([-lim, lim])
    # plt.ylim([-lim, lim])
    # plt.title("Markov chain elements")
    # plt.tight_layout()

    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(16, 12))

    # plt.figure()
    ax1.scatter(np.arange(1, len(temps)+1), temps, s=3)
    ax1.set_title("Temperature evolution")
    ax1.set_xlabel("Step")
    ax1.set_ylabel("Temperature")
    # plt.tight_layout()

    # Update points
    # plt.figure()
    ax2.scatter(update_pts, np.ones(len(update_pts)), s=3)
    ax2.set_yticks([])
    ax2.set_title("Update points")
    ax2.set_xlabel("Step")
    # plt.tight_layout()

    update_dists = update_pts[1:] - update_pts[:-1]  # distances between update points
    # plt.figure()
    ax3.plot(update_dists)
    ax3.set_title("Distances between update points")
    ax3.set_xlabel("Update point")
    ax3.set_ylabel("Distance")
    # plt.tight_layout()

    # Plot averages of distances between update points
    # plt.figure()
    ax4.plot(avg_dists)
    ax4.set_title("Averages of distances between update points (L=%i)" % L)
    ax4.set_xlabel("Update point")
    ax4.set_ylabel("Average")
    # plt.tight_layout()

    fig.tight_layout()

    plt.figure()
    plt.semilogy(range(1, len(kl_divs)+1), kl_divs, 'o-')
    plt.tight_layout()
