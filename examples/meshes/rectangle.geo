//+
Point(1) = {-0.1, 0, 0, 1.0};
//+
Delete {
  Point{1};
}
//+
Point(1) = {-1, 1, 0, 1.0};
//+
Point(2) = {-1, 1, 0, 1.0};
//+
Point(3) = {-1, 1, 0, 1.0};
//+
Point(4) = {-1, 1, 0, 1.0};
//+
Point(5) = {-1, 1, 0, 1.0};
//+
Point(6) = {-1, 1, 0, 1.0};
//+
Point(7) = {-1, 1, 0, 1.0};
//+
Point(8) = {-1, 1, 0, 1.0};
//+
Point(9) = {-1, 1, 0, 1.0};
//+
Point(10) = {-1, 1, 0, 1.0};
//+
Point(11) = {-1, 1, 0, 1.0};
//+
Point(12) = {-1, -1, 0, 1.0};
//+
Point(13) = {1, 1, 0, 1.0};
//+
Point(14) = {1, -1, 0, 1.0};
//+
Delete {
  Point{13, 14};
}
//+
Point(14) = {4, 1, 0, 1.0};
//+
Point(15) = {4, -1, 0, 1.0};
//+
Line(1) = {1, 14};
//+
Line(2) = {14, 15};
//+
Line(3) = {15, 12};
//+
Line(4) = {12, 1};
//+
Line Loop(5) = {1, 2, 3, 4};
//+
Plane Surface(6) = {5};
