// Gmsh project created on Mon Sep 25 14:32:05 2017
//+
Point(1) = {0, 0.2, 0, 1.0};
//+
Point(2) = {0, 0.8, 0, 1.0};
//+
Point(3) = {1, 0.9, 0, 1.0};
//+
Point(4) = {1, 0.1, 0, 1.0};
//+
Line(1) = {1, 1};
//+
Line(2) = {1, 2};
//+
Line(3) = {2, 3};
//+
Line(4) = {3, 4};
//+
Line(5) = {4, 1};
//+
Line(6) = {1, 1};
//+
Line Loop(1) = {2, 3, 4, 5};
//+
Plane Surface(1) = {1};
