// Gmsh project created on Thu May 11 13:08:11 2017
//+
Point(1) = {-1, 1, 0, 1.0};
//+
Delete {
  Point{1};
}
//+
Point(1) = {0, -0, 0, 1.0};
//+
Point(2) = {-0, 1, 0, 1.0};
//+
Point(3) = {0.5, 1, 0, 1.0};
//+
Point(4) = {0.5, 0.5, 0, 1.0};
//+
Point(5) = {1, 0, 0, 1.0};
//+
Point(6) = {1, 0.5, 0, 1.0};
//+
Point(7) = {0.75, 0.75, 0, 1.0};
//+
Line(1) = {2, 1};
//+
Line(2) = {1, 5};
//+
Line(3) = {5, 5};
//+
Line(4) = {5, 6};
//+
Line(5) = {2, 3};
//+
Point(8) = {0.5, 0.75, 0, 1.0};
//+
Point(9) = {0.75, 0.5, 0, 1.0};
//+
Line(6) = {8, 3};
//+
Line(7) = {9, 6};
//+
Circle(8) = {8, 7, 9};
//+
Line Loop(9) = {1, 2, 4, -7, -8, 6, -5};
//+
Plane Surface(10) = {9};
//+
Delete {
  Point{7};
}
//+
Delete {
  Point{7};
}
//+
Delete {
  Point{7};
}
//+
Delete {
  Point{7};
}
//+
Delete {
  Point{4, 7};
}
//+
Delete {
  Point{7};
}
