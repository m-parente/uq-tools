// Gmsh project created on Thu May 11 16:31:06 2017
//+
Point(1) = {0, 0, 0, 1.0};
//+
Point(2) = {0, 1, 0, 1.0};
//+
Point(3) = {4, 1, 0, 1.0};
//+
Point(4) = {4, 0, 0, 1.0};
//+
Line(1) = {2, 1};
//+
Line(2) = {1, 4};
//+
Line(3) = {4, 3};
//+
Line(4) = {3, 2};
//+
Line Loop(5) = {4, 1, 2, 3};
//+
Plane Surface(6) = {5};
//+
Extrude {0, 0, 2} {
  Surface{6};
}
//+
Surface Loop(29) = {28, 15, 6, 19, 23, 27};
//+
Volume(30) = {29};
