import sys
from dolfin_utils.meshconvert import meshconvert

meshname = sys.argv[1]
meshconvert.convert2xml(meshname + ".msh", meshname + ".xml")
