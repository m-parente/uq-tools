import env
import model

import numpy as np

import matplotlib.pyplot as plt


settings = env.settings
asDir = settings.asDir

W = np.loadtxt('%s/asm_eigVecs.txt' % asDir)
W_split = np.split(W, [settings.actVars], axis=1)
W1, W2 = W_split[0], W_split[1]

xs = env.prior_sample(1)
ys = np.dot(xs, W1)

Ns = [5, 10, 20, 50, 100, 500, 1000]
coeff_vars_avgs = np.empty(len(Ns))

# Code due to Constantine et al., 2016, Accelerating MCMC with active subspaces
# for n in range(len(Ns)):
#     N = Ns[n]
#     zs = env.cond_prior_sample(None, N)
#     coeff_vars = np.empty(len(ys))

#     for i in range(len(ys)):
#         y = ys[i]

#         fs = [env.misfit(np.dot(W1, y)+np.dot(W2, z)) for z in zs]
#         gNy = np.average(fs)
#         coeff_vars[i] = np.sqrt(sum([(f - gNy) ** 2 for f in fs])
#                                 / float(N-1)) \
#             / (np.sqrt(N)*gNy)

#     coeff_vars_avgs[n] = np.average(coeff_vars)

L = 10

for n in range(len(Ns)):
    N = Ns[n]
    coeff_vars = np.empty(len(ys))

    for i in range(len(ys)):
        y = ys[i]
        gNys = np.empty(L)

        for l in range(L):
            print("N,y,l: %i,%i,%i" % (N, i, l))
            zs = env.cond_prior_sample(None, N)

            fs = [env.misfit(np.dot(W1, y)+np.dot(W2, z)) for z in zs]
            gNys[l] = np.average(fs)

        gNys_avg = np.average(gNys)
        coeff_vars[i] = np.sqrt(sum([(gNy - gNys_avg) ** 2 for gNy in gNys])
                                / float(L-1)) \
            / (np.sqrt(L)*gNys_avg)

    coeff_vars_avgs[n] = np.average(coeff_vars)

plt.figure()
plt.plot(Ns, coeff_vars_avgs, 'o-')
plt.xlabel(r'Number of samples $N$')
plt.ylabel('Coefficient of variation')
plt.xscale('log')

plt.show()
