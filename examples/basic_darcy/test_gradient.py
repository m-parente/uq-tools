import env
import model
from misc import Settings

import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns

sns.set()

settings = env.settings

x = env.prior_sample()
# print x
# exit()
ds = np.array([1., 0.5, 1e-1, 1e-2, 0.5 * 1e-2, 1e-3, 0.5 * 1e-3, 1e-4, 0.5 * 1e-4])
errs = np.empty((env.n_data, len(ds)))

absPb = model.AbstractHeatBVP(settings.mesh_file, settings.kl_dir,
                              env.data_locs, settings.dirac_approx_factor)
pb_x = absPb.instantiate(x)
q_x = pb_x.getQoI()
jac_x = pb_x.getJacobian()

print(jac_x)

for i in range(len(ds)):
    dx = ds[i] * np.ones(len(x))

    pb_xdx = absPb.instantiate(x + dx)
    q_xdx = pb_xdx.getQoI()

    errs[:, i] = np.abs(q_xdx - q_x - np.dot(jac_x, dx)) / np.abs(q_x)

plt.figure()
for i in range(env.n_data):
    plt.loglog(ds, errs[i, :], 'o-', label=r'$e_%i$' % (i+1))
plt.loglog(ds, ds, label=r'$1^{st}$ order')
plt.loglog(ds, ds**2, label=r'$2^{nd}$ order')
plt.legend()
plt.xlabel('dx')
plt.ylabel('Error')
plt.tight_layout()

plt.show()
