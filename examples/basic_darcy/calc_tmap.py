import env

from uq_tools import asm

import logging
import numpy as np
import numpy.random as rnd
import scipy.stats as stats
import TransportMaps as tmap
from TransportMaps import Distributions as dist

import plotters as plot
import matplotlib.pyplot as plt

tmap.setLogLevel(logging.INFO)


settings = env.settings
as_dir = env.as_dir

dim = settings.act_vars

xs = env.xs
Gs = env.Gs

# Construct the response surface -----------------
W = np.loadtxt('%s/asm_eigVecs.txt' % as_dir)

W_split = np.split(W, [dim], axis=1)
W1, W2 = W_split[0], W_split[1]

misfits = list(map(env.misfitG, Gs))

g_tmp = asm.response_surface(xs, misfits, W1, poly_order=4)
M = 1


def g(ys):
    print(len(ys))

    return g_tmp(ys)
    # def helper(y):
    #     zs = rnd.multivariate_normal(
    #         mean=np.zeros(env.numberOfParameters - settings.actVars),
    #         cov=np.eye(env.numberOfParameters - settings.actVars),
    #         size=M)
    #     # print np.shape(y)
    #     return np.average(map(lambda z: env.bayInvPb.misfit(np.dot(W1, y) + np.dot(W2, z)), zs))
    # return np.array([helper(y) for y in ys])

# ------------------------------------------------


order = 3
# T = tmap.Default_IsotropicIntegratedSquaredTriangularTransportMap(
#     dim, order, span='full')
T = tmap.Default_IsotropicIntegratedExponentialTriangularTransportMap(
    dim, order, span='full')

priorY = dist.StandardNormalDistribution(dim)
push_priorY = dist.PushForwardTransportMapDistribution(T, priorY)


class PosteriorDistribution(dist.Distribution):
    def __init__(self):
        super(PosteriorDistribution, self).__init__(dim)

    def pdf(self, y, params=None):
        return np.exp(-g(y)) * priorY.pdf(y, params)

    def log_pdf(self, y, params=None):
        return priorY.log_pdf(y, params) - g(y)


postY = PosteriorDistribution()

qtype = 3  # Gauss quadrature
qparams = [4] * dim  # Quadrature order
reg = None  # No regularization
tol = 1e-4  # Optimization tolerance
ders = 0  # Use gradient and Hessian
log = push_priorY.minimize_kl_divergence(
    postY, qtype=qtype, qparams=qparams, regularization=reg, tol=tol, ders=ders)

prior_samples = rnd.multivariate_normal(
    mean=np.zeros(dim), cov=np.eye(dim), size=200)
post_samples = T(prior_samples)

ndiscr = 10
x = np.linspace(-4, 4, ndiscr)
y = np.linspace(-4, 4, ndiscr)
plot.plotTmap2d(x, y, priorY, T, push_priorY,
                rho_pi_share_x=True, push_rho_samples=post_samples)

plt.figure()
plt.scatter(post_samples[:, 0], post_samples[:, 1], c='g', s=5.)
plt.xlim((np.min(x), np.max(x)))
plt.ylim((np.min(y), np.max(y)))

plt.show()
