import env
import model
import numpy as np
from uq_tools import asm
from uq_tools import utils


settings = env.settings


def run(id, sample):
    print("Sample " + repr(id))

    absPb = model.AbstractHeatBVP(settings.mesh_file, settings.kl_dir, env.data_locs,
                                  settings.dirac_approx_factor)
    qoi, jac = asm.computeQoIAndJacobian(absPb, id, sample)

    np.savetxt('%s/G%d.txt' % (settings.G_jacG_dir, id), qoi)
    np.savetxt('%s/jacG%d.txt' % (settings.G_jacG_dir, id), jac)


def usage():
    print("usage: id nw ne se sw")


if __name__ == "__main__":
    if len(sys.argv) == env.n + 2:
        run(int(sys.argv[1]), np.array(sys.argv[2:], dtype=float))
    else:
        usage()
