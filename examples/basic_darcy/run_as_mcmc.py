import env
from uq_tools import approx
from uq_tools import asm
from uq_tools import mcmc

import numpy as np
import numpy.linalg as la
import numpy.random as rnd
import scipy.stats as stats
import sys

import matplotlib.pyplot as plt


settings = env.settings

asDir = settings.asDir
mcmcDir = settings.mcmcDir

# Construct the response surface -----------------
W = np.loadtxt('%s/asm_eigVecs.txt' % asDir)

W_split = np.split(W, [settings.actVars], axis=1)
W1, W2 = W_split[0], W_split[1]
# ------------------------------------------------

priorY = stats.multivariate_normal(mean=np.zeros(settings.actVars),
                                   cov=np.eye(settings.actVars)).pdf

# ySamples = asm.as_mcmc_with_averaged_misfit(
#     W1, W2,
#     env.misfit,
#     proposal_sampler=lambda yk: rnd.normal(loc=yk, scale=0.65),
#     priorY=priorY,
#     prior_cond_sampler=env.cond_prior_sample,
#     y1=np.zeros(settings.actVars),
#     M=10,
#     steps=5 * 10**4,
#     nPlotAccptRate=2)

# Initially train approximation with prior samples
prior_xs = env.prior_sample(n=2)
prior_ys = np.dot(prior_xs, W1)

avg_misfit = asm.averaged_misfit(
    W1, W2, env.misfit, cond_prior_sampler=env.cond_prior_sample, M=2)
avg_misfit_values = np.array([avg_misfit(y) for y in prior_ys])

g_approx = approx.LeastSquaresPolynomialApproximation(
    prior_ys, avg_misfit_values)

# X, Y = np.meshgrid(np.linspace())

plt.figure()
ax = plt.gca(projection='3d')
# ax.plot_surface()

print(g_approx([[3., 0.]]))
exit()

ySamples = asm.adaptiveTwoStageASMCMC(
    W1, W2,
    g_approx,
    get_misfit_full=lambda xs: np.array([env.misfit(x) for x in xs]),
    prior=priorY,
    proposal_sampler=lambda yk: rnd.normal(loc=yk, scale=0.65),
    cond_prior_sampler=env.cond_prior_sample,
    N=10,
    y1=np.zeros(settings.actVars),
    steps=5*1e4,
    err_g_tol=1e-2,
    nPlotAccptRate=5)

exit()

burnIn = settings.burnInActVar
minEss, ess, corrTimes, acfs = mcmc.stats(ySamples, burnIn=burnIn)
print("MCMC stats (min ess, ess, corrTimes): %f,%s,%s" % (
    minEss, ess, corrTimes))

effYSamples = mcmc.pickEffSamples(ySamples, burnIn=burnIn)
print("# Eff. samples: %i" % len(effYSamples))

np.savetxt('%s/ySamples.txt' % mcmcDir, ySamples)
np.savetxt('%s/effYSamples.txt' % mcmcDir, effYSamples)
np.savetxt('%s/yAutocorrs.txt' % mcmcDir, acfs)
