import env
import model

import numpy as np
import numpy.random as rnd


settings = env.settings

pb = model.AbstractHeatBVP(settings.mesh_file, settings.kl_dir, env.data_locs,
                           settings.dirac_approx_factor).instantiate(settings.x_true)
values = pb.getQoI()
np.savetxt(settings.data_file, values)
