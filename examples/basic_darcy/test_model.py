import env
import model

import dolfin as dlf
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd
from time import time as tic


settings = env.settings

parameters = rnd.normal(loc=0.0, scale=1.0, size=100)
pb = model.AbstractHeatBVP(settings.mesh_file, settings.kl_dir,
                           env.data_locs, settings.dirac_approx_factor).instantiate(parameters)

u = pb.solveForwardProblem()

print(pb.getQoI())
# print(pb.getJacobian())

plt.figure()
dlf.plot(u)
plt.scatter(env.data_locs[:, 0], env.data_locs[:, 1], c='r')
plt.tight_layout()

plt.figure()
dlf.plot(pb.cond_field)
plt.tight_layout()

# plt.figure()
# dlf.plot(pb.u_a)
# plt.tight_layout()

# for i in range(5):
#     plt.figure()
#     plot(pb.klEigFuncs[i])

plt.show()
