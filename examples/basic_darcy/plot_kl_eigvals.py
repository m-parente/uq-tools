import env

import matplotlib.pyplot as plt
import numpy as np


settings = env.settings

eig_vals = np.loadtxt('%s/klEigVals_unit_square.txt' % settings.kl_dir)

plt.figure()
plt.plot(eig_vals, 'o')

plt.show()
