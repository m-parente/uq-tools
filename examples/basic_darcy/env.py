from misc import Settings
import model
import uq_tools.bayinv as bi

from glob import glob
import itertools as it
import natsort
import numpy as np
import numpy.random as rnd
import os
import scipy.stats as stat


settings = Settings("settings")

tmp_dir = settings.tmp_dir
G_jacG_dir = settings.G_jacG_dir
as_dir = settings.as_dir
mcmc_dir = settings.mcmc_dir
os.system('mkdir -p %s && mkdir -p %s && mkdir -p %s' % (G_jacG_dir, as_dir, mcmc_dir))
samples_dir = G_jacG_dir

sigma = settings.noise_level
x_true = np.array(settings.x_true)

n = settings.n
k = settings.k

L = np.loadtxt('%s/L.txt' % tmp_dir)
W = np.loadtxt('%s/W.txt' % tmp_dir)
L1, L2 = L[:k], L[k:]
W_split = np.split(W, [k], axis=1)
W1, W2 = W_split[0], W_split[1]

G_files = natsort.natsorted(glob('%s/G*.txt' % samples_dir))
jacG_files = natsort.natsorted(glob('%s/jacG*.txt' % samples_dir))

samples = np.loadtxt('%s/samples.txt' % samples_dir)

Gs = [np.loadtxt(G_file) for G_file in G_files]
jacGs = [np.loadtxt(jacG_file) for jacG_file in jacG_files]

data_locs = np.array([[x, y]
                      for x, y in it.product([0.5], np.arange(0.2, 0.81, 0.1))])
n_data = len(data_locs)
data = np.loadtxt(settings.data_file)


prior = stat.multivariate_normal(mean=np.zeros(n), cov=np.eye(n)).pdf
prior_y = stat.multivariate_normal(mean=np.zeros(k)).pdf


def prior_sample(size=1):
    return rnd.multivariate_normal(mean=np.zeros(n), cov=np.eye(n), size=size) if size > 1 \
        else rnd.multivariate_normal(mean=np.zeros(n), cov=np.eye(n))


def prior_y_sample(size=1):
    return rnd.multivariate_normal(mean=np.zeros(k), cov=np.eye(k), size=size) if size > 1 \
        else rnd.multivariate_normal(mean=np.zeros(k), cov=np.eye(k))


def prior_z_y_sample(y=None, size=10):
    return rnd.multivariate_normal(mean=np.zeros(n-k), cov=np.eye(n-k), size=size) if size > 1 \
        else rnd.multivariate_normal(mean=np.zeros(n-k), cov=np.eye(n-k))


_variance = (sigma*data)**2
_invNoiseCovMat = (1 / _variance) * np.eye(len(data))

bayInvPb = bi.BayesianInverseProblem(data, _invNoiseCovMat,
                                     absPdeProblem=model.AbstractHeatBVP(
                                         settings.mesh_file, settings.kl_dir,
                                         data_locs,
                                         settings.dirac_approx_factor))

misfitG = bayInvPb.misfitG
misfit = bayInvPb.misfit


def gN(y, N=10):
    if np.isscalar(y):
        y = np.array([y])
    zs = prior_z_y_sample(y=y, size=N)
    return np.average([misfit(np.dot(W1, y) + np.dot(W2, z)) for z in zs])
