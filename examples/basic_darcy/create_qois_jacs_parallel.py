import env
from uq_tools import utils


settings = env.settings
G_jacG_dir = settings.G_jacG_dir

M = settings.number_samples
samples = env.prior_sample(size=M)

utils.construct_G_jacG_parallel(samples, 'create_qoi_jac.py', G_jacG_dir)
