import env
import misc
import plotters
from uq_tools import asm, utils

import numpy as np
import os


settings = env.settings

n_boots = settings.number_bootstraps

result = asm.computeActiveSubspaceFromSamples(
    env.Gs, env.jacGs, env.bayInvPb, n_boots)

eigVals, eigVecs, minEigVals, maxEigVals, minSubspaceErrors, maxSubspaceErrors, meanSubspaceErrors = result

np.savetxt('%s/L.txt' % env.tmp_dir, eigVals)
np.savetxt('%s/W.txt' % env.tmp_dir, eigVecs)

numPlotEigVals = 7
plotters.plotActiveSubspace(eigVals, eigVecs, numPlotEigVals, minEigVals, maxEigVals,
                            minSubspaceErrors, maxSubspaceErrors, meanSubspaceErrors)
