from dolfin import *
import numpy as np


# set_log_level(WARNING)


class Left(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 0.0)


class Right(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 1.0)


class Top(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 1.0)


class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 0.0)


class HeatBVP:
    def __init__(self, id, parameters, meshFile, klDir, dataPoints, diracApproxFactor):
        self.klModes = parameters

        self.numKL = len(self.klModes)

        self.mesh = Mesh(meshFile)
        self.mesh.init()

        self.V = FunctionSpace(self.mesh, 'CG', 1)

        self.klEigVals = np.loadtxt('%s/klEigVals_unit_square.txt' % klDir)
        self.klEigFuncs = [None] * self.numKL

        log_cond_field = Function(self.V)
        log_cond_field.vector()[:] = 0

        for i in range(self.numKL):
            eigFunc = Function(self.V)
            f = File('%s/klEigFunc_unit_square_%i.xml' % (klDir, i))
            f >> eigFunc
            self.klEigFuncs[i] = eigFunc
            log_cond_field.vector().axpy(
                np.sqrt(self.klEigVals[i]) * self.klModes[i], self.klEigFuncs[i].vector())

        self.log_cond_field = log_cond_field
        self.cond_field = exp(log_cond_field)

        self.dataPoints = dataPoints
        self.numberOfDataPoints = len(dataPoints)

        self.diracApproxFactor = diracApproxFactor

        # Boundaries ------------------------
        left = Left()
        right = Right()
        top = Top()
        bottom = Bottom()

        self.boundaries = MeshFunction('size_t',
                                       self.mesh, self.mesh.topology().dim()-1)
        self.boundaries.set_all(0)
        left.mark(self.boundaries, 1)
        top.mark(self.boundaries, 2)
        right.mark(self.boundaries, 3)
        bottom.mark(self.boundaries, 4)
        # -----------------------------------

        # Measures --------------------------
        self.dx = Measure('dx', domain=self.mesh)
        self.ds = Measure('ds', domain=self.mesh,
                          subdomain_data=self.boundaries)
        # -----------------------------------

    def solveForwardProblem(self):
        if hasattr(self, 'u'):
            return self.u

        u = TrialFunction(self.V)
        v = TestFunction(self.V)

        bcs = [DirichletBC(self.V, 0.0, self.boundaries, 1),
               DirichletBC(self.V, 0.0, self.boundaries, 2),
               DirichletBC(self.V, 0.0, self.boundaries, 4)]

        a = inner(self.cond_field * grad(u), grad(v)) * self.dx
        L = inner(1, v) * self.dx

        self.u = Function(self.V)
        solve(a == L, self.u, bcs)

        return self.u

    def getQoI(self):
        if hasattr(self, 'qoi'):
            return self.qoi

        self.solveForwardProblem()
        self.qoi = np.array([assemble(inner(self.u, self._getFunctional(i)) * self.dx)
                             for i in range(self.numberOfDataPoints)])

        return self.qoi

    def _solveAdjointProblem(self, rszFunctional):
        u_a = TrialFunction(self.V)
        v_a = TestFunction(self.V)

        f_a = rszFunctional

        a_a = inner(self.cond_field * grad(u_a), grad(v_a)) * self.dx
        L_a = f_a * v_a * self.dx

        bcs_a = [DirichletBC(self.V, 0.0, self.boundaries, 1),
                 DirichletBC(self.V, 0.0, self.boundaries, 2),
                 DirichletBC(self.V, 0.0, self.boundaries, 4)]

        self.u_a = Function(self.V)
        solve(a_a == L_a, self.u_a, bcs_a)

        return self.u_a

    def _getFunctional(self, i):
        point = self.dataPoints[i]
        # Riesz representer of the functional
        return Expression(
            "factor/pi*exp(-factor*((x[0]-x0)*(x[0]-x0) + (x[1]-y0)*(x[1]-y0)))",
            x0=point[0],
            y0=point[1],
            factor=self.diracApproxFactor,
            degree=2)

    def _getGradient(self, rszFunctional):
        self.solveForwardProblem()
        u_a = self._solveAdjointProblem(rszFunctional)

        derivs = np.empty(self.numKL)

        for i in range(len(derivs)):
            dcond_dmode_i = np.sqrt(self.klEigVals[i]) * self.klEigFuncs[i] \
                * self.cond_field
            derivs[i] = assemble(
                inner(-dcond_dmode_i * grad(self.u), grad(u_a)) * self.dx)

        return derivs

    def getJacobian(self):
        if hasattr(self, 'jac'):
            return self.jac

        self.jac = np.empty((self.numberOfDataPoints, self.numKL))

        for iData in range(self.numberOfDataPoints):
            rszFunctional = self._getFunctional(iData)

            # Get gradient of a given functional with respect to the parameters
            self.jac[iData, :] = self._getGradient(rszFunctional)

        return self.jac


class AbstractHeatBVP:
    def __init__(self, meshFile, klDir, dataPoints, diracApproxFactor):
        self.meshFile = meshFile
        self.klDir = klDir
        self.dataPoints = dataPoints
        self.diracApproxFactor = diracApproxFactor

    def instantiate(self, parameters, id=-1):
        return HeatBVP(id, parameters, self.meshFile, self.klDir, self.dataPoints, self.diracApproxFactor)
