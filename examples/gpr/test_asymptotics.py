import plotters

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import ConstantKernel as C, Matern, RBF, WhiteKernel


kernel = C()*RBF()  # + WhiteKernel(noise_level=0.1)
ns = [1, 10, 20, 50, 100, 200, 500, 1000]
# ns = [1, 2, 5, 10, 20, 50]
xs_plt = np.linspace(-1., 1., num=100)
noise = 0.1

for n in ns:
    xs = np.zeros((n, 1))
    data = rnd.multivariate_normal(mean=np.zeros(n), cov=noise*np.eye(n))
    # data = np.zeros(n)

    gpr = GaussianProcessRegressor(kernel=kernel, alpha=0.1, optimizer=None)
    gpr.fit(xs, data)

    print(gpr.kernel_)

    pred0, pred0_std = gpr.predict([[0.]], return_std=True)
    print("Predictive stand. dev.: %f" % pred0_std)

    # Check identity with hand-calculated stuff
    assert np.allclose(pred0[0], 1./(noise/n + 1)*np.average(data), rtol=1e-10, atol=1e-10)
    assert np.allclose(pred0_std**2, 1. - 1./(noise/n + 1), rtol=1e-10, atol=1e-10)

    pred, pred_std = gpr.predict(xs_plt[:, np.newaxis], return_std=True)

    plt.figure()
    plt.plot(xs_plt, pred, 'C0', lw=2)
    plt.scatter(xs, data, c='C1', s=20)

    plt.fill_between(xs_plt, pred - 1.96*pred_std, pred + 1.96*pred_std,
                     alpha=0.2, color='C0')

    # y_samples = gpr.sample_y(xs_plt[:, np.newaxis], 10)
    # plt.plot(xs_plt, y_samples, lw=1)

    plt.title("Kernel: %s\n Log-Likelihood: %.3f"
              % (gpr.kernel_, gpr.log_marginal_likelihood(gpr.kernel_.theta)),
              fontsize=12)
    plt.ylim(-1.2, 1.2)
    plt.tight_layout()
    plt.savefig('tmp_pics/%i.png' % n)

plt.show()
