import env

from uq_tools import asm

import numpy as np
import scipy.optimize as optim
from scipy.optimize import Bounds


W1 = env.W1

xs_misfit = np.loadtxt('%s/xs.txt' % env.settings.dir_misfits)
misfits = np.loadtxt('%s/misfits.txt' % env.settings.dir_misfits)

resp_surface = asm.response_surface(xs_misfit, misfits, W1, poly_order=4)

res = optim.minimize(lambda y: resp_surface(y)[0],
                     jac='2-point',
                     x0=np.zeros(env.k),
                     method='SLSQP',
                     bounds=Bounds([-1, -1, -1, -1], [1, 1, 1, 1]),
                     options={'ftol': 1e-8, 'disp': True})

print(res.x)

# y_min = res.x

# x = np.dot(env.W1, y_min) + np.dot(env.W2, -1*np.ones(env.n-env.k))

# print env.misfit(x)
