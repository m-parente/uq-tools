import env
import model

import datetime as dt
from dateutil.relativedelta import relativedelta as reldelta
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import seaborn as sns

sns.set()
sns.set_context('paper', font_scale=1.5)


optimDir = env.optimDir

misfits = np.loadtxt('%s/misfits.txt' % optimDir)
xs_min = np.loadtxt('%s/xs_min.txt' % optimDir)
x_min = xs_min[np.argmin(misfits)]
print(np.min(misfits))

model = model.AbstractModel().instantiate(x_min)
qoi = model.getQoI()
data = env.data
print(env.misfitG(qoi))

date_min = dt.datetime(2009, 1, 1)
days = [date_min + dt.timedelta(days=i) for i in range(len(data))]

plt.figure()
plt.plot(days, qoi, 'C2', label='Simulation')
plt.plot(days, data, label='Data')
plt.xlabel('Date')
plt.ylabel('Discharge [$l/s$]')
plt.legend()

ax = plt.gca()
ax.xaxis.set_major_formatter(mdates.DateFormatter('%m/%Y'))
plt.xticks([date_min, date_min + reldelta(months=6),
            date_min + reldelta(months=12)])  # , date_min + reldelta(months=18),
# date_min + reldelta(months=24), date_min + reldelta(months=30),
# date_min + reldelta(months=36)])

plt.tight_layout()

plt.show()
