import env

import numpy as np


optimDir = env.optimDir

N = 2100
misfits = np.empty(N)

for i in range(N):
    print(i)
    x_map = np.loadtxt('%s/min_x_%i' % (optimDir, i))
    misfits[i] = env.misfit(x_map)

np.savetxt('%s/misfits.txt' % optimDir, misfits)

# misfits = np.loadtxt('%s/misfits.txt' % optimDir)

print("Best misfit: %f (%i)" % (np.min(misfits), np.argmin(misfits)))
