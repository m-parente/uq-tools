import env

import numpy as np
import numpy.random as rnd

import matplotlib as mpl
import matplotlib.cm as cmx
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

# plt.rcParams["patch.force_edgecolor"] = True
sns.set()
sns.set_context('paper', font_scale=1.5)


def get_cmap(cmap):
    color_norm = mpl.colors.Normalize(vmin=-0.5, vmax=1)
    scalar_map = cmx.ScalarMappable(norm=color_norm, cmap=cmap)

    def map_index_to_rgb_color(index):
        return scalar_map.to_rgba(index)
    return map_index_to_rgb_color


cmap = get_cmap('OrRd')


dir_mcmc = env.mcmc_dir


def _pairgrid_heatmap(x, y, **kws):
    cmap = sns.light_palette(kws.pop("color"), as_cmap=True)
    plt.hist2d(x, y, cmap=cmap, cmin=1, **kws)


def plot_posterior():
    samples = np.loadtxt('%s/x_samples_4d.txt' % dir_mcmc)
    # samples = env.toOriginalRange(samples)

    choice = rnd.choice(len(samples), 30000)

    corrcoeffs = np.corrcoef(samples.T)

    # lims = [(_settings.c_LowerBound, _settings.c_UpperBound),
    #         (_settings.alpha_res_min_LowerBound,
    #          _settings.alpha_res_min_UpperBound),
    #         (_settings.alpha_res_diff_LowerBound,
    #          _settings.alpha_res_diff_UpperBound),
    #         (_settings.lambda_dot_max_LowerBound,
    #          _settings.lambda_dot_max_UpperBound),
    #         (_settings.m_LowerBound, _settings.m_UpperBound),
    #         (_settings.beta_max_LowerBound, _settings.beta_max_UpperBound),
    #         (_settings.lambda_max_LowerBound, _settings.lambda_max_UpperBound),
    #         (_settings.n_LowerBound, _settings.n_UpperBound)]

    # x1Samples = samples[choice, 0]
    # x2Samples = samples[choice, 1]
    # x3Samples = samples[choice, 2]
    # x4Samples = samples[choice, 3]
    # x5Samples = samples[choice, 4]
    # x6Samples = samples[choice, 5]
    # x7Samples = samples[choice, 6]
    # x8Samples = samples[choice, 7]
    samples = samples[choice]
    # df = pd.DataFrame(
    #     np.hstack((x1Samples[:, np.newaxis], x2Samples[:, np.newaxis],
    #                x3Samples[:, np.newaxis], x4Samples[:, np.newaxis],
    #                x5Samples[:, np.newaxis], x6Samples[:, np.newaxis],
    #                x7Samples[:, np.newaxis], x8Samples[:, np.newaxis])),
    #     columns=[r'$c$', r'$\alpha_{res}^l$', r'$\Delta\alpha_{res}$', r'$\dot{\lambda}^*$',
    #              r'$m_\alpha$', r'$\beta^*$', r'$\lambda^*$', r'$m_\beta$'])
    df = pd.DataFrame(samples, columns=list(map(str, list(range(1, env.n+1)))))

    g = sns.PairGrid(df, height=0.7)
    g = g.map_diag(sns.distplot, kde=False, bins=10)
    g = g.map_lower(sns.kdeplot)

    for i in range(env.n):
        for j in range(env.n):
            ax = g.axes[i, j]
            if j <= i:
                # ax.set_xlim(lims[j][0], lims[j][1])
                # ax.set_ylim(lims[i][0], lims[i][1])
                ax.set_xlim(-1, 1)
                ax.set_ylim(-1, 1)

                ax.set_xticklabels([])
                ax.set_yticklabels([])
                ax.set_xlabel('')
                ax.set_ylabel('')
            else:
                # ax.set_visible(False)
                ax.set_facecolor('white')
                # ax.text(0.5 * (lims[j][1] + lims[j][0]),
                #         0.5 * (lims[i][1] + lims[i][0]),
                ax.text(0., 0.,
                        r'$%.3f$' % np.round(corrcoeffs[j, i], decimals=3),
                        fontsize=10,
                        color=cmap(np.abs(corrcoeffs[j, i])),
                        ha='center', va='center')

    plt.subplots_adjust(left=0.03, top=0.97, right=0.97,
                        bottom=0.03, hspace=0.1, wspace=0.1)


if __name__ == '__main__':
    plot_posterior()
    plt.show()
