import env

import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns

sns.set()


optimDir = env.optimDir

xs_min = np.loadtxt('%s/xs_min.txt' % optimDir)
misfits = np.loadtxt('%s/misfits.txt' % optimDir)

idx = misfits.argsort()
xs_sorted = xs_min[idx]
mf_sorted = misfits[idx]

xs = env.fromUniformRangeToPhysicalParameters(xs_sorted[:597])
mfs = mf_sorted[:597]

# np.savetxt('param_sorted_597.txt', xs)
# np.savetxt('misfits_sorted_597.txt', mfs)

# exit()

f, ax_arr = plt.subplots(3, 7)

for i in range(env.n):
    ax = ax_arr[i / 7][i % 7]
    ax.hist(xs[:, i], bins=50)

f.subplots_adjust(hspace=0.2, wspace=0.3)
# plt.tight_layout()

plt.show()
