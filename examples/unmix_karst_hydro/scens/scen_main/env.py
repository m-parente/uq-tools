from misc import Settings

import numpy as np


_s_scen = Settings('scens/scen_main/settings')

# Boundaries of log variable
log_k_e_2_num_lb = np.log(_s_scen.k_e_2_num_lb)
log_k_e_2_num_ub = np.log(_s_scen.k_e_2_num_ub)
log_k_is_2_lb = np.log(_s_scen.k_is_2_lb)
log_k_is_2_ub = np.log(_s_scen.k_is_2_ub)
log_k_sec_2_lb = np.log(_s_scen.k_sec_2_lb)
log_k_sec_2_ub = np.log(_s_scen.k_sec_2_ub)
log_k_e_3_num_lb = np.log(_s_scen.k_e_3_num_lb)
log_k_e_3_num_ub = np.log(_s_scen.k_e_3_num_ub)
log_k_is_3_lb = np.log(_s_scen.k_is_3_lb)
log_k_is_3_ub = np.log(_s_scen.k_is_3_ub)
log_k_sec_3_lb = np.log(_s_scen.k_sec_3_lb)
log_k_sec_3_ub = np.log(_s_scen.k_sec_3_ub)
log_k_e_4_num_lb = np.log(_s_scen.k_e_4_num_lb)
log_k_e_4_num_ub = np.log(_s_scen.k_e_4_num_ub)
log_k_is_4_lb = np.log(_s_scen.k_is_4_lb)
log_k_is_4_ub = np.log(_s_scen.k_is_4_ub)
log_k_sec_4_lb = np.log(_s_scen.k_sec_4_lb)
log_k_sec_4_ub = np.log(_s_scen.k_sec_4_ub)

_lbs = np.array([log_k_e_2_num_lb,
                 _s_scen.e_min_2_lb,
                 _s_scen.diff_e_2_lb,
                 _s_scen.alpha_2_lb,
                 log_k_is_2_lb,
                 log_k_sec_2_lb,
                 _s_scen.e_sec_2_lb,
                 _s_scen.diff_k_e_23_num_lb,
                 _s_scen.diff_e_min_23_lb,
                 _s_scen.diff_e_3_lb,
                 _s_scen.diff_alpha_23_lb,
                 _s_scen.diff_k_is_23_lb,
                 _s_scen.diff_k_sec_23_lb,
                 _s_scen.diff_e_sec_23_lb,
                 _s_scen.diff_k_e_34_num_lb,
                 _s_scen.diff_e_min_34_lb,
                 _s_scen.diff_e_4_lb,
                 _s_scen.diff_alpha_34_lb,
                 _s_scen.diff_k_is_34_lb,
                 _s_scen.diff_k_sec_34_lb,
                 _s_scen.diff_e_sec_34_lb])
_ubs = np.array([log_k_e_2_num_ub,
                 _s_scen.e_min_2_ub,
                 _s_scen.diff_e_2_ub,
                 _s_scen.alpha_2_ub,
                 log_k_is_2_ub,
                 log_k_sec_2_ub,
                 _s_scen.e_sec_2_ub,
                 _s_scen.diff_k_e_23_num_ub,
                 _s_scen.diff_e_min_23_ub,
                 _s_scen.diff_e_3_ub,
                 _s_scen.diff_alpha_23_ub,
                 _s_scen.diff_k_is_23_ub,
                 _s_scen.diff_k_sec_23_ub,
                 _s_scen.diff_e_sec_23_ub,
                 _s_scen.diff_k_e_34_num_ub,
                 _s_scen.diff_e_min_34_ub,
                 _s_scen.diff_e_4_ub,
                 _s_scen.diff_alpha_34_ub,
                 _s_scen.diff_k_is_34_ub,
                 _s_scen.diff_k_sec_34_ub,
                 _s_scen.diff_e_sec_34_ub])


def fromCalibrationToInterimParameters(samples):
    return 0.5*(_ubs-_lbs)*samples + 0.5*(_ubs+_lbs)


def fromInterimToCalibrationParameters(samples):
    return 2./(_ubs-_lbs)*samples - (_ubs+_lbs)/(_ubs-_lbs)


def fromCalibrationToPhysicalParameters(samples):
    samples = np.array(samples)
    if len(np.shape(samples)) <= 1:
        samples = samples[:, np.newaxis]
    n = np.shape(samples)[0]
    ones = np.ones(n)

    samples = fromCalibrationToInterimParameters(samples)

    diff_e_2 = samples[:, 2]

    # Hydrotope 2
    log_k_e_2_num = samples[:, 0]
    k_e_2_num = np.exp(log_k_e_2_num)
    e_min_2 = samples[:, 1]
    e_max_2 = e_min_2 + diff_e_2
    alpha_2 = samples[:, 3]
    log_k_is_2 = samples[:, 4]
    k_is_2 = np.exp(log_k_is_2)
    log_k_sec_2 = samples[:, 5]
    k_sec_2 = np.exp(log_k_sec_2)
    e_sec_2 = samples[:, 6]

    diff_k_e_23_num = samples[:, 7]
    diff_e_min_23 = samples[:, 8]
    diff_e_3 = samples[:, 9]
    diff_alpha_23 = samples[:, 10]
    diff_k_is_23 = samples[:, 11]
    diff_k_sec_23 = samples[:, 12]
    diff_e_sec_23 = samples[:, 13]
    max_e_min_23 = np.max((e_min_2, ones*_s_scen.e_min_3_lb), axis=0)
    max_e_sec_23 = np.max((e_sec_2, ones*_s_scen.e_sec_3_lb), axis=0)

    # Hydrotope 3
    log_k_e_3_num = log_k_e_3_num_lb + diff_k_e_23_num * \
        (np.min((ones*log_k_e_3_num_ub, log_k_e_2_num), axis=0) - log_k_e_3_num_lb)
    k_e_3_num = np.exp(log_k_e_3_num)
    e_min_3 = max_e_min_23 + diff_e_min_23 * \
        (_s_scen.e_min_3_ub - max_e_min_23)
    e_max_3 = e_min_3 + diff_e_3
    alpha_3 = _s_scen.alpha_3_lb + diff_alpha_23 * \
        (np.min((ones*_s_scen.alpha_3_ub, alpha_2), axis=0) - _s_scen.alpha_3_lb)
    log_k_is_3 = log_k_is_3_lb + diff_k_is_23 * \
        (np.min((ones*log_k_is_3_ub, log_k_is_2), axis=0) - log_k_is_3_lb)
    k_is_3 = np.exp(log_k_is_3)
    log_k_sec_3 = log_k_sec_3_lb + diff_k_sec_23 * \
        (np.min((ones*log_k_sec_3_ub, log_k_sec_2), axis=0) - log_k_sec_3_lb)
    k_sec_3 = np.exp(log_k_sec_3)
    e_sec_3 = max_e_sec_23 + diff_e_sec_23 * \
        (_s_scen.e_sec_3_ub - max_e_sec_23)

    diff_k_e_34_num = samples[:, 14]
    diff_e_min_34 = samples[:, 15]
    diff_e_4 = samples[:, 16]
    diff_alpha_34 = samples[:, 17]
    diff_k_is_34 = samples[:, 18]
    diff_k_sec_34 = samples[:, 19]
    diff_e_sec_34 = samples[:, 20]
    max_e_min_34 = np.max((e_min_3, ones*_s_scen.e_min_4_lb), axis=0)
    max_e_sec_34 = np.max((e_sec_3, ones*_s_scen.e_sec_4_lb), axis=0)

    # Hydrotope 4
    log_k_e_4_num = log_k_e_4_num_lb + diff_k_e_34_num * \
        (np.min((ones*log_k_e_4_num_ub, log_k_e_3_num), axis=0) - log_k_e_4_num_lb)
    k_e_4_num = np.exp(log_k_e_4_num)
    e_min_4 = max_e_min_34 + diff_e_min_34 * \
        (_s_scen.e_min_4_ub - max_e_min_34)
    e_max_4 = e_min_4 + diff_e_4
    alpha_4 = _s_scen.alpha_4_lb + diff_alpha_34 * \
        (np.min((ones*_s_scen.alpha_4_ub, alpha_3), axis=0) - _s_scen.alpha_4_lb)
    log_k_is_4 = log_k_is_4_lb + diff_k_is_34 * \
        (np.min((ones*log_k_is_4_ub, log_k_is_3), axis=0) - log_k_is_4_lb)
    k_is_4 = np.exp(log_k_is_4)
    log_k_sec_4 = log_k_sec_4_lb + diff_k_sec_34 * \
        (np.min((ones*log_k_sec_4_ub, log_k_sec_3), axis=0) - log_k_sec_4_lb)
    k_sec_4 = np.exp(log_k_sec_4)
    e_sec_4 = max_e_sec_34 + diff_e_sec_34 * \
        (_s_scen.e_sec_4_ub - max_e_sec_34)

    return np.hstack((k_e_2_num[:, np.newaxis], e_min_2[:, np.newaxis], e_max_2[:, np.newaxis], alpha_2[:, np.newaxis], k_is_2[:, np.newaxis], k_sec_2[:, np.newaxis], e_sec_2[:, np.newaxis],
                      k_e_3_num[:, np.newaxis], e_min_3[:, np.newaxis], e_max_3[:, np.newaxis], alpha_3[:,
                                                                                                        np.newaxis], k_is_3[:, np.newaxis], k_sec_3[:, np.newaxis], e_sec_3[:, np.newaxis],
                      k_e_4_num[:, np.newaxis], e_min_4[:, np.newaxis], e_max_4[:, np.newaxis], alpha_4[:, np.newaxis], k_is_4[:, np.newaxis], k_sec_4[:, np.newaxis], e_sec_4[:, np.newaxis]))


def fromPhysicalToCalibrationParameters(samples):
    samples = np.array(samples)
    if len(np.shape(samples)) <= 1:
        samples = samples[:, np.newaxis]
    n = np.shape(samples)[0]
    ones = np.ones(n)

    [k_e_2_num, e_min_2, e_max_2, alpha_2, k_is_2, k_sec_2, e_sec_2,
     k_e_3_num, e_min_3, e_max_3, alpha_3, k_is_3, k_sec_3, e_sec_3,
     k_e_4_num, e_min_4, e_max_4, alpha_4, k_is_4, k_sec_4, e_sec_4] \
        = [samples[:, 0], samples[:, 1], samples[:, 2], samples[:, 3], samples[:, 4], samples[:, 5], samples[:, 6],
           samples[:, 7], samples[:, 8], samples[:, 9], samples[:,
                                                                10], samples[:, 11], samples[:, 12], samples[:, 13],
           samples[:, 14], samples[:, 15], samples[:, 16], samples[:, 17], samples[:, 18], samples[:, 19], samples[:, 20]]

    # Consider log variables
    log_k_e_2_num = np.log(k_e_2_num)
    log_k_is_2 = np.log(k_is_2)
    log_k_sec_2 = np.log(k_sec_2)
    log_k_e_3_num = np.log(k_e_3_num)
    log_k_is_3 = np.log(k_is_3)
    log_k_sec_3 = np.log(k_sec_3)
    log_k_e_4_num = np.log(k_e_4_num)
    log_k_is_4 = np.log(k_is_4)
    log_k_sec_4 = np.log(k_sec_4)

    # Hydrotope 2
    diff_e_2 = e_max_2 - e_min_2

    max_e_min_23 = np.max((e_min_2, ones*_s_scen.e_min_3_lb), axis=0)
    max_e_sec_23 = np.max((e_sec_2, ones*_s_scen.e_sec_3_lb), axis=0)

    # Hydrotope 3
    diff_k_e_23_num = (log_k_e_3_num - log_k_e_3_num_lb) / \
        (np.min((ones*log_k_e_3_num_ub, log_k_e_2_num), axis=0) - log_k_e_3_num_lb)
    diff_e_min_23 = (e_min_3 - max_e_min_23) / \
        (_s_scen.e_min_3_ub - max_e_min_23)
    diff_e_3 = e_max_3 - e_min_3
    diff_alpha_23 = (alpha_3 - _s_scen.alpha_3_lb) / \
        (np.min((ones*_s_scen.alpha_3_ub, alpha_2), axis=0) - _s_scen.alpha_3_lb)
    diff_k_is_23 = (log_k_is_3 - log_k_is_3_lb) / \
        (np.min((ones*log_k_is_3_ub, log_k_is_2), axis=0) - log_k_is_3_lb)
    diff_k_sec_23 = (log_k_sec_3 - log_k_sec_3_lb) / \
        (np.min((ones*log_k_sec_3_ub, log_k_sec_2), axis=0) - log_k_sec_3_lb)
    diff_e_sec_23 = (e_sec_3 - max_e_sec_23) / \
        (_s_scen.e_sec_3_ub - max_e_sec_23)

    max_e_min_34 = np.max((e_min_3, ones*_s_scen.e_min_4_lb), axis=0)
    max_e_sec_34 = np.max((e_sec_3, ones*_s_scen.e_sec_4_lb), axis=0)

    # Hydrotope 4
    diff_k_e_34_num = (log_k_e_4_num - log_k_e_4_num_lb) / \
        (np.min((ones*log_k_e_4_num_ub, log_k_e_3_num), axis=0) - log_k_e_4_num_lb)
    diff_e_min_34 = (e_min_4 - max_e_min_34) / \
        (_s_scen.e_min_4_ub - max_e_min_34)
    diff_e_4 = e_max_4 - e_min_4
    diff_alpha_34 = (alpha_4 - _s_scen.alpha_4_lb) / \
        (np.min((ones*_s_scen.alpha_4_ub, alpha_3), axis=0) - _s_scen.alpha_4_lb)
    diff_k_is_34 = (log_k_is_4 - log_k_is_4_lb) / \
        (np.min((ones*log_k_is_4_ub, log_k_is_3), axis=0) - log_k_is_4_lb)
    diff_k_sec_34 = (log_k_sec_4 - log_k_sec_4_lb) / \
        (np.min((ones*log_k_sec_4_ub, log_k_sec_3), axis=0) - log_k_sec_4_lb)
    diff_e_sec_34 = (e_sec_4 - max_e_sec_34) / \
        (_s_scen.e_sec_4_ub - max_e_sec_34)

    samples = np.hstack((log_k_e_2_num[:, np.newaxis], e_min_2[:, np.newaxis], diff_e_2[:, np.newaxis], alpha_2[:, np.newaxis], log_k_is_2[:, np.newaxis], log_k_sec_2[:, np.newaxis], e_sec_2[:, np.newaxis],
                         diff_k_e_23_num[:, np.newaxis], diff_e_min_23[:, np.newaxis], diff_e_3[:, np.newaxis], diff_alpha_23[:,
                                                                                                                              np.newaxis], diff_k_is_23[:, np.newaxis], diff_k_sec_23[:, np.newaxis], diff_e_sec_23[:, np.newaxis],
                         diff_k_e_34_num[:, np.newaxis], diff_e_min_34[:, np.newaxis], diff_e_4[:, np.newaxis], diff_alpha_34[:, np.newaxis], diff_k_is_34[:, np.newaxis], diff_k_sec_34[:, np.newaxis], diff_e_sec_34[:, np.newaxis]))

    return fromInterimToCalibrationParameters(samples)
