from misc import Settings

import numpy as np


_s_scen = Settings('scens/scen_1_all_hyd_3/settings')

# Boundaries of log variable
log_k_e_2_num_lb = np.log(_s_scen.k_e_2_num_lb)
log_k_e_2_num_ub = np.log(_s_scen.k_e_2_num_ub)
log_k_is_2_lb = np.log(_s_scen.k_is_2_lb)
log_k_is_2_ub = np.log(_s_scen.k_is_2_ub)
log_k_sec_2_lb = np.log(_s_scen.k_sec_2_lb)
log_k_sec_2_ub = np.log(_s_scen.k_sec_2_ub)
log_k_e_3_num_lb = np.log(_s_scen.k_e_3_num_lb)
log_k_e_3_num_ub = np.log(_s_scen.k_e_3_num_ub)
log_k_is_3_lb = np.log(_s_scen.k_is_3_lb)
log_k_is_3_ub = np.log(_s_scen.k_is_3_ub)
log_k_sec_3_lb = np.log(_s_scen.k_sec_3_lb)
log_k_sec_3_ub = np.log(_s_scen.k_sec_3_ub)
log_k_e_4_num_lb = np.log(_s_scen.k_e_4_num_lb)
log_k_e_4_num_ub = np.log(_s_scen.k_e_4_num_ub)
log_k_is_4_lb = np.log(_s_scen.k_is_4_lb)
log_k_is_4_ub = np.log(_s_scen.k_is_4_ub)
log_k_sec_4_lb = np.log(_s_scen.k_sec_4_lb)
log_k_sec_4_ub = np.log(_s_scen.k_sec_4_ub)

_lbs = np.array([log_k_e_2_num_lb,
                 _s_scen.e_min_2_lb,
                 _s_scen.diff_e_2_lb,
                 _s_scen.alpha_2_lb,
                 log_k_is_2_lb,
                 log_k_sec_2_lb,
                 _s_scen.e_sec_2_lb,
                 log_k_e_3_num_lb,
                 _s_scen.e_min_3_lb,
                 _s_scen.diff_e_3_lb,
                 _s_scen.alpha_3_lb,
                 log_k_is_3_lb,
                 log_k_sec_3_lb,
                 _s_scen.e_sec_3_lb,
                 log_k_e_4_num_lb,
                 _s_scen.e_min_4_lb,
                 _s_scen.diff_e_4_lb,
                 _s_scen.alpha_4_lb,
                 log_k_is_4_lb,
                 log_k_sec_4_lb,
                 _s_scen.e_sec_4_lb])
_ubs = np.array([log_k_e_2_num_ub,
                 _s_scen.e_min_2_ub,
                 _s_scen.diff_e_2_ub,
                 _s_scen.alpha_2_ub,
                 log_k_is_2_ub,
                 log_k_sec_2_ub,
                 _s_scen.e_sec_2_ub,
                 log_k_e_3_num_ub,
                 _s_scen.e_min_3_ub,
                 _s_scen.diff_e_3_ub,
                 _s_scen.alpha_3_ub,
                 log_k_is_3_ub,
                 log_k_sec_3_ub,
                 _s_scen.e_sec_3_ub,
                 log_k_e_4_num_ub,
                 _s_scen.e_min_4_ub,
                 _s_scen.diff_e_4_ub,
                 _s_scen.alpha_4_ub,
                 log_k_is_4_ub,
                 log_k_sec_4_ub,
                 _s_scen.e_sec_4_ub])


def fromCalibrationToInterimParameters(samples):
    return 0.5*(_ubs-_lbs)*samples + 0.5*(_ubs+_lbs)


def fromInterimToCalibrationParameters(samples):
    return 2./(_ubs-_lbs)*samples - (_ubs+_lbs)/(_ubs-_lbs)


def fromCalibrationToPhysicalParameters(samples):
    samples = np.array(samples)
    if len(np.shape(samples)) <= 1:
        samples = samples[:, np.newaxis]

    samples = fromCalibrationToInterimParameters(samples)

    # Hydrotope 2
    k_e_2_num = np.exp(samples[:, 0])
    e_min_2 = samples[:, 1]
    diff_e_2 = samples[:, 2]
    e_max_2 = e_min_2 + diff_e_2
    alpha_2 = samples[:, 3]
    k_is_2 = np.exp(samples[:, 4])
    k_sec_2 = np.exp(samples[:, 5])
    e_sec_2 = samples[:, 6]

    # Hydrotope 3
    k_e_3_num = np.exp(samples[:, 7])
    e_min_3 = samples[:, 8]
    diff_e_3 = samples[:, 9]
    e_max_3 = e_min_3 + diff_e_3
    alpha_3 = samples[:, 10]
    k_is_3 = np.exp(samples[:, 11])
    k_sec_3 = np.exp(samples[:, 12])
    e_sec_3 = samples[:, 13]

    # Hydrotope 4
    k_e_4_num = np.exp(samples[:, 14])
    e_min_4 = samples[:, 15]
    diff_e_4 = samples[:, 16]
    e_max_4 = e_min_4 + diff_e_4
    alpha_4 = samples[:, 17]
    k_is_4 = np.exp(samples[:, 18])
    k_sec_4 = np.exp(samples[:, 19])
    e_sec_4 = samples[:, 20]

    return np.hstack((k_e_2_num[:, np.newaxis], e_min_2[:, np.newaxis], e_max_2[:, np.newaxis], alpha_2[:, np.newaxis], k_is_2[:, np.newaxis], k_sec_2[:, np.newaxis], e_sec_2[:, np.newaxis],
                      k_e_3_num[:, np.newaxis], e_min_3[:, np.newaxis], e_max_3[:, np.newaxis], alpha_3[:,
                                                                                                        np.newaxis], k_is_3[:, np.newaxis], k_sec_3[:, np.newaxis], e_sec_3[:, np.newaxis],
                      k_e_4_num[:, np.newaxis], e_min_4[:, np.newaxis], e_max_4[:, np.newaxis], alpha_4[:, np.newaxis], k_is_4[:, np.newaxis], k_sec_4[:, np.newaxis], e_sec_4[:, np.newaxis]))


def fromPhysicalToCalibrationParameters(samples):
    samples = np.array(samples)
    if len(np.shape(samples)) <= 1:
        samples = samples[:, np.newaxis]

    [k_e_2_num, e_min_2, e_max_2, alpha_2, k_is_2, k_sec_2, e_sec_2,
     k_e_3_num, e_min_3, e_max_3, alpha_3, k_is_3, k_sec_3, e_sec_3,
     k_e_4_num, e_min_4, e_max_4, alpha_4, k_is_4, k_sec_4, e_sec_4] \
        = [samples[:, 0], samples[:, 1], samples[:, 2], samples[:, 3], samples[:, 4], samples[:, 5], samples[:, 6],
           samples[:, 7], samples[:, 8], samples[:, 9], samples[:,
                                                                10], samples[:, 11], samples[:, 12], samples[:, 13],
           samples[:, 14], samples[:, 15], samples[:, 16], samples[:, 17], samples[:, 18], samples[:, 19], samples[:, 20]]

    # Hydrotope 2
    log_k_e_2_num = np.log(k_e_2_num)
    diff_e_2 = e_max_2 - e_min_2
    log_k_is_2 = np.log(k_is_2)
    log_k_sec_2 = np.log(k_sec_2)

    # Hydrotope 3
    log_k_e_3_num = np.log(k_e_3_num)
    diff_e_3 = e_max_3 - e_min_3
    log_k_is_3 = np.log(k_is_3)
    log_k_sec_3 = np.log(k_sec_3)

    # Hydrotope 4
    log_k_e_4_num = np.log(k_e_4_num)
    diff_e_4 = e_max_4 - e_min_4
    log_k_is_4 = np.log(k_is_4)
    log_k_sec_4 = np.log(k_sec_4)

    samples = np.hstack((log_k_e_2_num[:, np.newaxis], e_min_2[:, np.newaxis], diff_e_2[:, np.newaxis], alpha_2[:, np.newaxis], log_k_is_2[:, np.newaxis], log_k_sec_2[:, np.newaxis], e_sec_2[:, np.newaxis],
                         log_k_e_3_num[:, np.newaxis], e_min_3[:, np.newaxis], diff_e_3[:, np.newaxis], alpha_3[:,
                                                                                                                np.newaxis], log_k_is_3[:, np.newaxis], log_k_sec_3[:, np.newaxis], e_sec_3[:, np.newaxis],
                         log_k_e_4_num[:, np.newaxis], e_min_4[:, np.newaxis], diff_e_4[:, np.newaxis], alpha_4[:, np.newaxis], log_k_is_4[:, np.newaxis], log_k_sec_4[:, np.newaxis], e_sec_4[:, np.newaxis]))

    return fromInterimToCalibrationParameters(samples)
