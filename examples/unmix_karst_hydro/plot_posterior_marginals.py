import env

import numpy as np
import numpy.random as rnd
import matplotlib.pyplot as plt
import seaborn as sns


def plot_posterior_marginals():
    dir_mcmc = env.mcmc_dir
    k = env.k
    noise_level = env.noise_level

    x_samples = np.loadtxt('%s/x_samples_%id_%.2f.txt' % (dir_mcmc, k, noise_level))

    params = np.array([5, 12, 19])
    choice = rnd.choice(len(x_samples), 30000)

    x_samples = x_samples[choice][:, params-1]

    bins = 20

    for i in range(len(params)):
        plt.figure()
        sns.distplot(x_samples[:, i], kde=False, hist=True, norm_hist=True, bins=bins,
                     hist_kws={"linewidth": 0, "alpha": 1})
        plt.xlim((-1, 1))
        plt.xlabel(r'$\mathbf{x}_{%i}$' % params[i])
        plt.ylabel('Posterior prob.')


if __name__ == '__main__':
    plot_posterior_marginals()
    plt.show()
