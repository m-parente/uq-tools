import env

from uq_tools import asm
import plotters

import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate


W = env.W
W1 = env.W1

samples = env.prior_sample(N=10000)
y_samples = np.dot(samples, W1)

w1 = W[:, 0]
w2 = W[:, 1]
w3 = W[:, 2]

prior_y = asm.marginalPriorY(
    samples, W1, kde_bandwidth=0.12, kde_kernel='gaussian')

lb = -2.
ub = 2.

# y1s = y2s = np.linspace(lb, ub, 100)
# plotters.plotContour(y1s, y2s, prior_y)
y1s = np.linspace(lb, ub, 100)
plt.plot(y1s, prior_y(y1s[:, np.newaxis]))
plt.hist(y_samples, bins=20, density=True)

plt.show()
exit()

j = 0


def f(y1, y2):  # , y2, y3, y4):
    global j
    print(j)
    j += 1
    return prior_y(np.array([y1, y2]))[0]  # , y2, y3, y4]))[0]


def prior_y1(y1):
    return integrate.quad(lambda y2: f(y1, y2),
                          lb, ub)[0]
    # return integrate.dblquad(lambda y2, y3: f(y1, y2, y3),
    #                          lb, ub,
    #                          lambda y3: lb, lambda y3: ub,
    #                          epsrel=1e-3)[0]
    # return integrate.tplquad(lambda y2, y3, y4: f(y1, y2, y3, y4),
    #                          lb, ub,
    #                          lambda y3: lb, lambda y3: ub,
    #                          lambda y3, y4: lb, lambda y3, y4: ub,
    #                          epsrel=1e-3)


# print prior_y1(0.)
# exit()
y1s = np.linspace(lb, ub, 100)
prior_y1s = [prior_y1(y1) for y1 in y1s]
plt.figure()
plt.plot(y1s, prior_y1s)

# plt.show()
# exit()

f, axarr = plt.subplots(1, env.k)
for i in range(env.k):
    ax = axarr[i]
    ax.hist(y_samples[:, i], bins=30, density=True)
    ax.set_xlabel(r'$\mathbf{w}_%i^\top \mathbf{x}$' % (i+1))

# ys = np.arange(np.min(y_samples[:, 0]), np.max(y_samples[:, 0]), step=0.01)

# plt.plot(ys, prior_y(ys[:, np.newaxis]))
# plt.xlabel(r'$\mathbf{w}_1^\top \mathbf{x}$')
# plt.tight_layout()

plt.show()
