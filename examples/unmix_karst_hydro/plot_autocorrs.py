import env

import numpy as np

import matplotlib.pyplot as plt

dir_mcmc = env.mcmc_dir


def plot_autocorrs():
    y_autocorrs_4d = np.loadtxt('%s/y_autocorrs_4d.txt' % dir_mcmc)

    plt.figure()
    for i in range(env.k):
        plt.scatter(list(range(1, len(y_autocorrs_4d) + 1)),
                    y_autocorrs_4d[:, i], label=r'$y_%i$' % (i+1), s=5)
    plt.xlabel(r'Lag $j$')
    plt.ylabel(r'Autocorrelation $r_j$ with lag $j$')
    plt.legend()
    plt.tight_layout()


if __name__ == '__main__':
    plot_autocorrs()
    plt.show()
