import env
import model

import numpy as np

import matplotlib.pyplot as plt

unif_samples = env.prior_sample(N=10000)

assert(np.allclose(env.fromInterimToCalibrationParameters(
    env.fromCalibrationToInterimParameters(unif_samples)), unif_samples))

samples_physical = env.fromCalibrationToPhysicalParameters(unif_samples)

k_e_2_num = samples_physical[:, 0]
e_2_min = samples_physical[:, 1]
alpha_2 = samples_physical[:, 3]
k_is_2 = samples_physical[:, 4]
k_sec_2 = samples_physical[:, 5]
e_sec_2 = samples_physical[:, 6]

k_e_3_num = samples_physical[:, 7]
e_3_min = samples_physical[:, 8]
alpha_3 = samples_physical[:, 10]
k_is_3 = samples_physical[:, 11]
k_sec_3 = samples_physical[:, 12]
e_sec_3 = samples_physical[:, 13]

k_e_4_num = samples_physical[:, 14]
e_4_min = samples_physical[:, 15]
alpha_4 = samples_physical[:, 17]
k_is_4 = samples_physical[:, 18]
k_sec_4 = samples_physical[:, 19]
e_sec_4 = samples_physical[:, 20]

# Constraints -------------------------------
assert(np.all(k_e_2_num >= k_e_3_num))
# assert(np.all(k_e_2_num >= k_e_4_num))
assert(np.all(k_e_3_num >= k_e_4_num))

assert(np.all(e_2_min <= e_3_min))
# assert(np.all(e_2_min <= e_4_min))
assert(np.all(e_3_min <= e_4_min))

assert(np.all(alpha_2 >= alpha_3))
# assert(np.all(alpha_2 >= alpha_4))
assert(np.all(alpha_3 >= alpha_4))

assert(np.all(k_is_2 >= k_is_3))
# assert(np.all(k_is_2 >= k_is_4))
assert(np.all(k_is_3 >= k_is_4))

assert(np.all(k_sec_2 >= k_sec_3))
# assert(np.all(k_sec_2 >= k_sec_4))
assert(np.all(k_sec_3 >= k_sec_4))

assert(np.all(e_sec_2 <= e_sec_3))
# assert(np.all(e_sec_2 <= e_sec_4))
assert(np.all(e_sec_3 <= e_sec_4))
# -------------------------------------------

assert(np.allclose(unif_samples,
                   env.fromPhysicalToCalibrationParameters(samples_physical)))

print("All tests passed.")
# exit()

for i in range(env.n):
    params = samples_physical[:, i]
    print("Parameter %i\t%f\t%f" % (i+1, np.min(params), np.max(params)))
