import env
import plotters
from uq_tools import asm
from uq_tools import utils

from natsort import natsort
from glob import glob
import numpy as np


settings = env.settings

nBoot = settings.numberBootstrapIterations

# GFiles = natsort.natsorted(glob('tmp/G*.txt'))
# jacGFiles = natsort.natsorted(glob('tmp/jacG*.txt'))
# Gs = map(lambda GFile: np.loadtxt(GFile), GFiles)
# jacGs = map(lambda jacGFile: np.loadtxt(jacGFile), jacGFiles)

prefix = None
result = asm.computeActiveSubspaceFromSamples(env.Gs, env.jacGs, env.bayInvPb, nBoot)

# print asm.computeIntrinsicDimensionFromSamples(env.Gs, env.jacGs, env.bayInvPb)
# grads = [utils.grad_nse(jacG, G, env.data) for G, jacG in zip(Gs, jacGs)]
# grads_outer = [np.outer(grad, grad) for grad in grads]
# result = asm.computeActiveSubspace(grads_outer, nBoot)
# prefix = 'nse'


eigVals, eigVecs, minEigVals, maxEigVals, minSubspaceErrors, maxSubspaceErrors, meanSubspaceErrors = result

# utils.save_active_subspace(eigVals, eigVecs, minEigVals, maxEigVals,
#                            minSubspaceErrors, maxSubspaceErrors, meanSubspaceErrors,
#                            dir=env.as_dir, prefix=prefix)

numPlotEigVals = 9
plotters.plotActiveSubspace(eigVals, eigVecs, numPlotEigVals, minEigVals, maxEigVals,
                            minSubspaceErrors, maxSubspaceErrors, meanSubspaceErrors)
