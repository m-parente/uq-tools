import env

import multiprocessing as mp
import numpy as np
import numpy.random as rnd
import os
import sys


# dir = 'tmp_misfits_G'
# dir = 'tmp_post_G_%id_%.2f' % (env.k, env.noise_level)
dir = env.dir_as_G_jacG
os.system('mkdir -p %s' % dir)

N = 1000
samples = env.prior_sample(N)
# samples = np.loadtxt('%s/x_samples_%id_%.2f.txt' % (env.mcmc_dir, env.k, env.noise_level))
# choice = rnd.choice(len(samples), N)
# samples = samples[choice]
np.savetxt('%s/xs.txt' % dir, samples)

scriptName = 'calc_template.py'

cmds = ''
for i in range(N):
    sample = samples[i]
    cmd = sys.executable + " " + scriptName + " " + \
        str(i) + " " + ' '.join(str(p) for p in sample)
    cmds += cmd + '\n'

launcherFile = '%s/asmLauncher_runs.txt' % dir

with open(launcherFile, 'w') as file:
    file.write(cmds)

os.environ["LAUNCHER_JOB_FILE"] = launcherFile
os.environ["LAUNCHER_DIR"] = os.environ["SW_DIR"] + "/launcher"
os.environ["LAUNCHER_PPN"] = repr(mp.cpu_count() - 1)

os.system("bash $LAUNCHER_DIR/paramrun")
