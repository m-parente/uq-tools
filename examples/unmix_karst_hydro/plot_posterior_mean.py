import env
import model

import numpy as np

import matplotlib.pyplot as plt


def plot_posterior_mean():
    post_samples = np.loadtxt('%s/x_samples_4d.txt' % env.mcmc_dir)
    x_mean = np.mean(post_samples, axis=0)
    G_mean = model.AbstractModel().instantiate(x_mean).getQoI()

    print("Mean misfit: %f" % env.misfitG(G_mean))

    plt.figure()
    plt.plot(env.data, label='data')
    plt.plot(G_mean, label=r'$\mathbf{\mu}_{post}$')
    # ax1.set_ylabel(r'Volumetric strain [$-$]')
    # ax1.get_yaxis().set_label_coords(-0.1, 0.5)

    plt.tight_layout()


if __name__ == '__main__':
    plot_posterior_mean()
    plt.show()
