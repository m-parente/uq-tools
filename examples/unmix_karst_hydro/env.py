from misc import Settings

from uq_tools import bayinv as bi

from glob import glob
import natsort
import numpy as np
import numpy.random as rnd
import os


settings = Settings('settings')
_s = settings

noise_level = _s.noiseLevel

scenario = _s.scenario
scenario_dir = 'scens/' + _s.scenario
data_dir = _s.data_dir
scenario_data_dir = '/'.join([data_dir, scenario])
dir_as_G_jacG = '/'.join([scenario_data_dir, _s.dir_as_G_jacG])
as_dir = '/'.join([scenario_data_dir, _s.prefix_as_dir + ("_%.2f" % noise_level)])
mcmc_dir = '/'.join([scenario_data_dir, _s.mcmc_dir])
dir_misfits = '/'.join([scenario_data_dir, _s.dir_misfits])
os.system('mkdir -p %s && mkdir -p %s && mkdir -p %s' % (dir_as_G_jacG, as_dir, mcmc_dir))

samples_dir = dir_as_G_jacG

n = 21

# xs = np.loadtxt('%s/xs.txt' % samples_dir)

# GFiles = natsort.natsorted(glob('%s/G*.txt' % samples_dir))
# jacGFiles = natsort.natsorted(glob('%s/jacG*.txt' % samples_dir))
# Gs = [np.loadtxt(GFile) for GFile in GFiles]
# jacGs = [np.loadtxt(jacGFile) for jacGFile in jacGFiles]

W = np.loadtxt('%s/asm_eigVecs.txt' % as_dir)
k = _s.actVars
W1 = W[:, :k]
W2 = W[:, k:]

# scenario settings
_s_scen = Settings('/'.join([scenario_dir, settings.scenario_settings_file]))
scen_exe = '/'.join([scenario_dir, _s_scen.exe])

data_file = '/'.join([scenario_dir, _s_scen.data_file])
data = np.loadtxt(data_file, comments='"')

env_scen = __import__('scens.%s.env' % scenario, fromlist=['object'])
fromCalibrationToInterimParameters = env_scen.fromCalibrationToInterimParameters
fromInterimToCalibrationParameters = env_scen.fromInterimToCalibrationParameters
fromCalibrationToPhysicalParameters = env_scen.fromCalibrationToPhysicalParameters
fromPhysicalToCalibrationParameters = env_scen.fromPhysicalToCalibrationParameters


def prior_sample(N=None):
    return rnd.uniform(low=-1, high=1, size=(N, n)) if N != None \
        else rnd.uniform(low=-1, high=1, size=n)


def prior(x):
    return 2**-n if np.all(np.logical_and(x <= 1, x >= -1)) else 0


_variance = (data * noise_level)**2
_invNoiseCovMat = np.diag(1 / _variance)

bayInvPb = bi.BayesianInverseProblem(data, _invNoiseCovMat)
# misfit = bayInvPb.misfit
misfitG = bayInvPb.misfitG


# def posterior_unnorm(x):
#     return np.exp(-misfit(x)) * prior(x)
