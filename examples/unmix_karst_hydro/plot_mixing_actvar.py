import env

import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt


settings = env.settings
dir_mcmc = env.mcmc_dir


def plot_mixing_actvar(length=2000):
    y_samples = np.loadtxt('%s/y_samples_4d.txt' % dir_mcmc)

    burnin = settings.burnin_actvar

    for i in range(env.k):
        plt.figure()
        plt.plot(list(range(burnin + 1, burnin + length + 1)),
                 y_samples[burnin:burnin + length, i], label=r'$y_%i$, 4D' % (i+1))
        plt.xticks([1e5, 1.005e5, 1.01e5, 1.015e5, 1.02e5])
        plt.axes().get_xaxis().set_major_formatter(
            mpl.ticker.ScalarFormatter(useMathText=True))
        plt.axes().ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
        plt.xlabel('Chain index')
        # plt.ylabel(r'$2^{nd}$ component of active variable')
        plt.legend()
        plt.tight_layout()


if __name__ == '__main__':
    plot_mixing_actvar()
    plt.show()
