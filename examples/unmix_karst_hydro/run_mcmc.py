import env

import uq_tools.mcmc as mcmc

import numpy as np
import numpy.random as rnd
import time as tm


settings = env.settings
mcmcDir = env.mcmcDir

steps = 20000
x1 = np.array([-0.46727537, 0.66460239, 0.04591628, 0.40788835, -0.97805634, 0.01231994,
               -0.12802944, -0.00343065, 0.97369536, -0.25529052, -0.98789961,  0.901027,
               -0.90519626, 0.12572082, 0.14600122, -0.98471383, 0.93152471, -0.986544,
               0.57736806, -0.50815498, 0.15416217])

start = tm.time()

std_proposal = 0.008
prop_cov = std_proposal**2*np.eye(env.numberOfParameters)
# prop_cov = (2.38)**2 / env.numberOfParameters * \
#     np.loadtxt('tmp_output/sample_cov.txt')
# + std_proposal**2 * np.eye(env.numberOfParameters)

samples = mcmc.bi_mh_mcmc(env.misfit,
                          prior=env.prior,
                          proposal_sampler=lambda xk: rnd.multivariate_normal(
                              mean=xk, cov=prop_cov),
                          x1=x1,
                          steps=steps,
                          nPlotAccptRate=10)

durat = tm.time() - start

np.savetxt('%s/samples_%i_%i.txt' % (mcmcDir, steps, std_proposal), samples)

burnIn = settings.burnIn
minEss, ess, corrTimes, acfs = mcmc.stats(samples, burnIn=burnIn, maxlag=None)
print("MCMC stats (min ess, ess, corrTimes): %f,%s,%s" % (
    minEss, ess, corrTimes))

effSamples = mcmc.pickEffSamples(samples, burnIn, maxlag=None)
print("# Eff. samples: %i" % len(effSamples))
print("Elapsed seconds: %f" % durat)
print("# Eff. samples / sec: %f" % (len(effSamples) / durat))

np.savetxt('%s/effSamples.txt' % mcmcDir, effSamples)
np.savetxt('%s/yAutocorrs.txt' % mcmcDir, acfs)
