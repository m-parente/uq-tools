import env

import plotters
from uq_tools import asm
from uq_tools import mcmc

import numpy as np
import matplotlib.pyplot as plt


mcmc_dir = env.mcmc_dir
burnin = env.settings.burnin_actvar
