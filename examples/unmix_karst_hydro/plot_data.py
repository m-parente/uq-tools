import env

import datetime as dt
from dateutil.relativedelta import relativedelta as reldelta
import matplotlib.pyplot as plt
import matplotlib.dates as mdates


def plot_data():
    data = env.data
    # noise_level = env.noise_level
    noise_levels = [0.01, 0.05, 0.10]

    times = list(range(len(env.data)))
    date_min = dt.datetime(2006, 1, 1)
    days = [date_min + dt.timedelta(days=i) for i in range(len(env.data))]

    n_std = 1.96  # 0.975 quantile of the standard normal distribution

    plt.figure(figsize=(12, 5))
    plt.plot(days, data, label='Measured discharge')
    for noise_level in noise_levels:
        std_data = (noise_level*env.data)
        # plt.fill_between(days, data-n_std*std_data, data+n_std*std_data,
        #                  alpha=0.35,  # label='95% quantile band of data',
        #                  label='%i%s noise level' % (int(noise_level*100), '%'))

    plt.xlabel('Date')
    plt.ylabel('Discharge [$l/s$]')
    ax = plt.gca()
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%m/%Y'))
    plt.xticks([date_min, date_min + reldelta(months=6),
                date_min + reldelta(months=12),
                date_min + reldelta(months=18),
                date_min + reldelta(months=24),
                date_min + reldelta(months=30),
                date_min + reldelta(months=36)])
    plt.legend()
    plt.tight_layout()


if __name__ == '__main__':
    plot_data()
    plt.show()
