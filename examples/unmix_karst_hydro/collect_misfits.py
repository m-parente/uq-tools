import env

from glob import glob
from natsort import natsort
import numpy as np


dir_misfits = env.dir_misfits

files_G = natsort.natsorted(glob('%s/G*.txt' % dir_misfits))
misfits = [env.misfitG(np.loadtxt(file_G)) for file_G in files_G]

np.savetxt("%s/misfits_%.2f.txt" % (dir_misfits, env.noise_level), misfits)
