import env
import model

from uq_tools import asm
from uq_tools import utils

import numpy as np
import numpy.linalg as la
import sys

settings = env.settings


def run(id, sample):
    print("Sample " + repr(id))

    absPb = model.AbstractModel()
    qoi, jac = asm.computeQoIAndJacobian(absPb, id, sample)

    np.savetxt('%s/G%d.txt' % (settings.tmpDir, id), qoi)
    np.savetxt('%s/jacG%d.txt' % (settings.tmpDir, id), jac)


def usage():
    print("usage: id parameters")


if __name__ == "__main__":
    if len(sys.argv) == env.n + 2:
        run(int(sys.argv[1]), np.array(sys.argv[2:], dtype=float))
    else:
        usage()
