import env
import model

import numpy as np


settings = env.settings

misfits = list(map(env.misfitG, env.Gs))

print(np.min(misfits), np.argmin(misfits))
print(env.toPhysicalParameters([env.xs[np.argmin(misfits)]]))

print(np.max(misfits), np.argmax(misfits))
print(env.toPhysicalParameters([env.xs[np.argmax(misfits)]]))
