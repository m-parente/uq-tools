import env

import numpy as np
import numpy.random as rnd
import sklearn.neighbors as skl_nb

import matplotlib.pyplot as plt
import seaborn as sns


mcmc_dir = env.mcmc_dir


def plot_prior_posterior_actvar():
    y_samples_4d = np.loadtxt('%s/eff_y_samples_4d_%.2f.txt' % (mcmc_dir, env.noise_level))

    W1 = env.W[:, :4]

    samples = env.prior_sample(N=20000)
    y_samples_prior = np.dot(samples, W1)

    choice = rnd.choice(len(y_samples_prior), 50000)
    y1_samples_prior = y_samples_prior[choice, 0]
    y2_samples_prior = y_samples_prior[choice, 1]
    y3_samples_prior = y_samples_prior[choice, 2]
    y4_samples_prior = y_samples_prior[choice, 3]

    choice = rnd.choice(len(y_samples_4d), 50000)

    y1_samples_4d = y_samples_4d[choice, 0]
    y2_samples_4d = y_samples_4d[choice, 1]
    y3_samples_4d = y_samples_4d[choice, 2]
    y4_samples_4d = y_samples_4d[choice, 3]

    f = plt.figure()
    ax1 = f.add_subplot(2, 4, 1)
    ax2 = f.add_subplot(2, 4, 2, sharey=ax1)
    ax3 = f.add_subplot(2, 4, 3, sharey=ax1)
    ax4 = f.add_subplot(2, 4, 4, sharey=ax1)
    ax5 = f.add_subplot(2, 4, 5)
    ax6 = f.add_subplot(2, 4, 6)
    ax7 = f.add_subplot(2, 4, 7)
    ax8 = f.add_subplot(2, 4, 8)

    bins = 20

    sns.distplot(y1_samples_prior, kde=True, hist=True, norm_hist=True, bins=bins, ax=ax1)
    sns.distplot(y2_samples_prior, kde=True, hist=True, norm_hist=True, bins=bins, ax=ax2)
    sns.distplot(y3_samples_prior, kde=True, hist=True, norm_hist=True, bins=bins, ax=ax3)
    sns.distplot(y4_samples_prior, kde=True, hist=True, norm_hist=True, bins=bins, ax=ax4)

    sns.distplot(y1_samples_4d, kde=True, hist=True, norm_hist=True, bins=bins, ax=ax5,
                 axlabel=r'$\mathbf{\~w}_1^\top \mathbf{x}$')
    sns.distplot(y2_samples_4d, kde=True, hist=True, norm_hist=True, bins=bins, ax=ax6,
                 axlabel=r'$\mathbf{\~w}_2^\top \mathbf{x}$')
    sns.distplot(y3_samples_4d, kde=True, hist=True, norm_hist=True, bins=bins, ax=ax7,
                 axlabel=r'$\mathbf{\~w}_3^\top \mathbf{x}$')
    sns.distplot(y4_samples_4d, kde=True, hist=True, norm_hist=True, bins=bins, ax=ax8,
                 axlabel=r'$\mathbf{\~w}_4^\top \mathbf{x}$')

    # ax1.set_ylabel('Prior prob.')
    # ax5.set_ylabel('Posterior prob.')

    ax1.set_xlim([-1.8, 1.8])
    ax2.set_xlim([-1.8, 1.8])
    ax3.set_xlim([-1.8, 1.8])
    ax4.set_xlim([-1.8, 1.8])

    ax1.set_xticks([-1.5, 1.5])
    ax2.set_xticks([-1.5, 1.5])
    ax3.set_xticks([-1.5, 1.5])
    ax4.set_xticks([-1.5, 1.5])

    # ax5.set_xticks([-1.5, 1.5])
    # ax6.set_xticks([-1.5, 1.5])
    # ax7.set_xticks([-1.5, 1.5])
    # ax8.set_xticks([-1.5, 1.5])
    ax5.set_xticks([0.01, 0.05])
    ax6.set_xticks([0.25, 0.4])
    ax7.set_xticks([0.16, 0.3])
    ax8.set_xticks([-0.66, -0.56])

    ax5.set_yticks([0, 20, 40, 60, 80])
    ax6.set_yticks([0, 20, 40, 60, 80])
    ax7.set_yticks([0, 20, 40, 60, 80])
    ax8.set_yticks([0, 20, 40, 60, 80])

    plt.tight_layout()
    plt.subplots_adjust(hspace=0.3, wspace=0.7)


if __name__ == '__main__':
    plot_prior_posterior_actvar()
    plt.show()
