import env

import numpy as np
import numpy.linalg as la
from scipy.optimize import minimize
import sys


tmpDir = env.optimDir


nfev = 0


def f(x):
    global nfev
    nfev += 1

    res = -env.posterior_unnorm(x)
    print(nfev, res, end=' ')

    return res


def run(id, sample):
    print("Sample " + repr(id))
    res = minimize(f, sample, method='nelder-mead',
                   options={'disp': True,
                            'xatol': 1e-3,
                            'maxfev': 1000})
    np.savetxt('%s/min_x_%i' % (tmpDir, id), res.x)


def usage():
    print("usage: id parameters")


if __name__ == "__main__":
    if len(sys.argv) == env.n + 2:
        run(int(sys.argv[1]), np.array(sys.argv[2:], dtype=float))
    else:
        usage()
