import env
from uq_tools import asm
from uq_tools import mcmc

import numpy as np
import numpy.linalg as la
import numpy.random as rnd
import sys
import time as tm

import matplotlib.pyplot as plt


settings = env.settings

mcmc_dir = env.mcmc_dir

k = env.k
W1, W2 = env.W1, env.W2


if len(sys.argv) < 2:
    # Construct the response surface -----------------
    xs_misfit = np.loadtxt('%s/xs.txt' % env.settings.dir_misfits)
    misfits = np.loadtxt('%s/misfits_%.2f.txt' % (env.settings.dir_misfits, env.noise_level))

    resp_surface = asm.response_surface(xs_misfit, misfits, W1, poly_order=4)
    # ------------------------------------------------

    prior_y = asm.marginalPriorY(env.prior_sample(N=10000), W1,
                                 kde_bandwidth=0.12, kde_kernel='gaussian')

    start = tm.time()

    y_samples = asm.as_mcmc_with_response_surface(
        resp_surface,
        W1, W2,
        proposal_sampler=lambda yk: rnd.normal(loc=yk, scale=0.00275),
        priorY=prior_y,
        y1=np.zeros(k),
        steps=1 * 10**6,
        nPlotAccptRate=5000)

    durat = tm.time() - start

    burnin = settings.burnin_actvar
    min_ess, ess, corr_times, acfs = mcmc.stats(
        y_samples, burnIn=burnin, maxlag=None)
    print("MCMC stats (min ess, ess, corr times): %f,%s,%s" % (min_ess, ess, corr_times))

    eff_y_samples = mcmc.pickEffSamples(y_samples, burnin, maxlag=None)
    print("# Eff. samples: %i" % len(eff_y_samples))
    print("Elapsed seconds: %f" % durat)
    print("# Eff. samples / sec: %f" % (len(eff_y_samples) / durat))

    np.savetxt('%s/y_samples_%id_%.2f.txt' % (mcmc_dir, k, env.noise_level), y_samples)
    np.savetxt('%s/eff_y_samples_%id_%.2f.txt' % (mcmc_dir, k, env.noise_level), eff_y_samples)
    np.savetxt('%s/y_autocorrs_%id_%.2f.txt' % (mcmc_dir, k, env.noise_level), acfs)
    exit()
else:
    if sys.argv[1] == 'f':
        eff_y_samples = np.loadtxt('%s/eff_y_samples_%id_%.2f.txt' %
                                   (mcmc_dir, k, env.noise_level))[:, np.newaxis]
    else:
        print("Unknown argument '%s'" % sys.argv[1])
        exit()

eff_y_samples = eff_y_samples[rnd.choice(len(eff_y_samples), 100)]

start = tm.time()
x_samples = asm.activeToOriginalMCMC(eff_y_samples,
                                     W1, W2,
                                     prior=env.prior,
                                     proposal_sampler=lambda zk: rnd.normal(
                                         loc=zk, scale=0.13),
                                     z1=np.zeros(env.n-k),
                                     stepsPerActiveSample=1 * 10**5,
                                     burnIn=settings.burnin_inactvar,
                                     nPlotAccptRate=10000)
x_samples = np.concatenate(x_samples)
print("%f sec elapsed." % (tm.time() - start))
print('# of x samples: %i' % len(x_samples))

np.savetxt('%s/x_samples_%id_%.2f.txt' % (mcmc_dir, k, env.noise_level), x_samples)
