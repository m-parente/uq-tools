import env

import multiprocessing as mp
import numpy as np
import os
import sys


tmpOptimDir = env.optimDir

N = 2
samples = env.prior_sample(N)
np.savetxt('%s/xs.txt' % tmpOptimDir, samples)

scriptName = 'calc_map.py'

cmds = ''
for i in range(N):
    sample = samples[i]
    cmd = sys.executable + " " + scriptName + " " + \
        str(i) + " " + ' '.join(str(p) for p in sample)
    cmds += cmd + '\n'

launcherFile = '%s/asmLauncher_runs.txt' % tmpOptimDir

with open(launcherFile, 'w') as file:
    file.write(cmds)

os.environ["LAUNCHER_JOB_FILE"] = launcherFile
os.environ["LAUNCHER_DIR"] = os.environ["SW_DIR"] + "/launcher"
os.environ["LAUNCHER_PPN"] = repr(mp.cpu_count() - 1)

os.system("bash $LAUNCHER_DIR/paramrun")
