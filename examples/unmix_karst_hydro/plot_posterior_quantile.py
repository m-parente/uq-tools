import env

import datetime as dt
from dateutil.relativedelta import relativedelta as reldelta
from glob import glob
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import natsort
import numpy as np
import pandas as pd
from scipy.stats.mstats import mquantiles


noise_level = env.noise_level
k = env.k
dir_post_G = '%s/G_post_%id_%.2f' % (env.scenario_data_dir, k, noise_level)
dir_post_G2 = '%s/G_post_%id_%.2f' % (env.scenario_data_dir, 1, 0.10)
dir_prior_G = '%s/G_prior' % env.scenario_data_dir


def plot_posterior_quantile():
    GFiles = natsort.natsorted(glob('%s/G*.txt' % dir_post_G))
    Gs = np.array([np.loadtxt(GFile) for GFile in GFiles])

    # GFiles2 = natsort.natsorted(glob('%s/G*.txt' % dir_post_G2))
    # Gs2 = np.array([np.loadtxt(GFile2) for GFile2 in GFiles2])

    GFiles2 = natsort.natsorted(glob('%s/G*.txt' % dir_prior_G))
    Gs2 = np.array([np.loadtxt(GFile2) for GFile2 in GFiles2])

    times = list(range(len(env.data)))
    date_min = dt.datetime(2006, 1, 1)
    days = [date_min + dt.timedelta(days=i) for i in range(len(env.data))]

    n_std = 1.96  # 0.975 quantile of the standard normal distribution

    # plt.figure(figsize=(10, 14))

    ps = [0.75, 0.75]
    Gss = [Gs, Gs2]
    noise_levels = [0.05, 0.05]
    titles = ['a)', 'b)']
    distr = ['post.', 'prior']

    for i in range(2):
        Gs = Gss[i]
        p = ps[i]  # Probability that posterior forward evaluations lie inside the according quantile band
        noise_level = noise_levels[i]

        std_data = (noise_level*env.data)

        # plt.subplot(2, 1, i+1)
        plt.figure()
        plt.plot(days, env.data, label='Measured discharge')
        plt.fill_between(days, env.data-n_std*std_data, env.data+n_std*std_data,
                         alpha=0.2, label='95% quantile band of meas.')

        median_path = np.median(Gs, axis=0)
        mean_path = np.mean(Gs, axis=0)
        quantiles = mquantiles(Gs, prob=[(1-p)/2, (1+p)/2], alphap=0, betap=1, axis=0)

        plt.plot(days, median_path, color='C1', label='Median pushf. %s' % distr[i])
        plt.fill_between(days, quantiles[0, :], quantiles[1, :],
                         color='C1', alpha=0.2, label='%i%% quantile band of %s' % (int(p*100), distr[i]))
        plt.plot(days, mean_path, color='C2', label='Mean pushf. %s' % distr[i])

        # plt.title(titles[i])
        plt.legend()

        plt.xlabel('Date')
        plt.ylabel('Discharge [$l/s$]')
        if i == 0:
            plt.xlabel(' ')
            plt.ylabel(' ')

        plt.ylim([15, 65])
        ax = plt.gca()
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%m/%y'))
        plt.xticks([date_min, date_min + reldelta(months=6),
                    date_min + reldelta(months=12),
                    date_min + reldelta(months=18),
                    date_min + reldelta(months=24),
                    date_min + reldelta(months=30),
                    date_min + reldelta(months=36)])
        plt.tight_layout()


if __name__ == '__main__':
    plot_posterior_quantile()
    plt.show()
