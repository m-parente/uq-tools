import env

import plotters
from uq_tools import asm

import matplotlib.pyplot as plt


k = env.k
xs = env.xs
misfits = list(map(env.misfitG, env.Gs))
W = env.W

if k == 1:
    plotters.summaryPlot(W[:, 0], xs, misfits, with_fit=True, poly_order=2)
elif k == 2:
    plotters.summaryPlot2D(W[:, 0], W[:, 1], xs, misfits,
                           with_fit=True, poly_order=2)

plt.show()
