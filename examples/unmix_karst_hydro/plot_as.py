import env
from plot_activity_scores import plot_activity_scores

import numpy as np

from matplotlib.lines import Line2D
from matplotlib.patches import Patch
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import seaborn as sns

sns.set()
sns.set_context('paper', font_scale=1.5)


def plot_as():
    as_dir = env.as_dir

    eigVals = np.loadtxt('%s/asm_eigVals.txt' % as_dir)
    minEigVals = np.loadtxt('%s/asm_minEigVals.txt' % as_dir)
    maxEigVals = np.loadtxt('%s/asm_maxEigVals.txt' % as_dir)

    eigVecs = np.loadtxt('%s/asm_eigVecs.txt' % as_dir)

    meanSubspaceErrors = np.loadtxt('%s/asm_meanSubspaceErrors.txt' % as_dir)
    minSubspaceErrors = np.loadtxt('%s/asm_minSubspaceErrors.txt' % as_dir)
    maxSubspaceErrors = np.loadtxt('%s/asm_maxSubspaceErrors.txt' % as_dir)

    numEigVals = len(eigVals)

    # Plot eigenvalues
    plt.figure()
    # plt.title(env.scenario)

    ax = plt.gca()

    ax.set_yscale('log')
    ax.margins(0.03)

    num_plot_eigvals = 9
    ran = list(range(1, num_plot_eigvals + 1))
    plt.plot(ran, eigVals[:num_plot_eigvals], 'o-', color='C0')
    plt.plot(ran, minEigVals[:num_plot_eigvals], color='C0')
    plt.plot(ran, maxEigVals[:num_plot_eigvals], color='C0')
    plt.fill_between(ran, minEigVals[:num_plot_eigvals],
                     maxEigVals[:num_plot_eigvals], facecolor='C0', alpha=0.2)
    plt.xticks(list(range(1, num_plot_eigvals + 1)))
    # plt.title('a)')

    ax.set_xlabel("Eigenvalue index")
    ax.set_ylabel("Eigenvalue")
    plt.tight_layout()

    # Plot eigenvectors
    # f = plt.figure(figsize=(12, 6))
    f = plt.figure()
    # plt.suptitle(env.scenario)

    colors = ['C0', 'C1', 'C2']
    titles = ['a)', 'b)', 'c)', 'd)', 'e)', 'f)', 'g)', 'h)', 'j)']

    # ax1 = plt.subplot2grid((2, 6), (0, 0), colspan=3)
    # ax2 = plt.subplot2grid((2, 6), (0, 3), colspan=3)
    # ax3 = plt.subplot2grid((2, 6), (1, 0), colspan=2)
    # ax4 = plt.subplot2grid((2, 6), (1, 2), colspan=2)
    # ax5 = plt.subplot2grid((2, 6), (1, 4), colspan=2)

    ax1 = plt.subplot2grid((3, 2), (0, 0))
    ax2 = plt.subplot2grid((3, 2), (0, 1))
    ax3 = plt.subplot2grid((3, 2), (1, 0))
    ax4 = plt.subplot2grid((3, 2), (1, 1))
    ax5 = plt.subplot2grid((3, 2), (2, 0), colspan=2)
    axes = [ax1, ax2, ax3, ax4, ax5]

    # f, axes = plt.subplots(2, 2, figsize=(12, 6))

    for i in range(4):
        eigVec = eigVecs[:, i]

        # plt.subplot(221 + i)
        # ax = plt.gca()
        # ax = axes[int(i/2)][i % 2]
        ax = axes[i]

        bars = ax.bar(list(range(1, env.n + 1)), eigVec)
        for b in range(len(bars)):
            bars[b].set_facecolor(colors[int(b / 7)])

        ax.set_xticks(list(range(1, numEigVals + 1, 2)))
        ax.set_yticks((-1, -0.5, 0, 0.5, 1))
        if i == 0:
            ax.set_xlabel("Parameter index")
            ax.set_ylabel("Value in eigenvector")
        # elif i == 1:
            # legend_elements = [Patch(facecolor='C0', label='Hyd. 1'),
            #                    Patch(facecolor='C1', label='Hyd. 2'),
            #                    Patch(facecolor='C2', label='Hyd. 3')]
            # ax.legend(handles=legend_elements,  # bbox_to_anchor=(1.05, 1.),
            #           loc='upper right', fontsize='x-small')  # , borderaxespad=0.)
        ax.set_title(r'%s' % titles[i])

    plot_activity_scores(ax5)
    ax5.set_title('e)')

    f.subplots_adjust(hspace=0.5, wspace=0.2)
    # plt.tight_layout()


if __name__ == '__main__':
    plot_as()
    plt.show()
