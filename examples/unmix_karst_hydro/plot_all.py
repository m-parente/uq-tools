import env

from plot_activity_scores import plot_activity_scores
# from plot_qoi import plotQoI
from plot_data import plot_data
from plot_as import plot_as
# from plot_summary import plotSummary
# from plot_prior_actvar import plotPriorActVar
from plot_autocorrs import plot_autocorrs
from plot_mixing_actvar import plot_mixing_actvar
# from plot_posterior_actvar import plotPosteriorActVar
from plot_prior_posterior_actvar import plot_prior_posterior_actvar
from plot_posterior_marginals import plot_posterior_marginals
# from plot_posterior import plotPosterior
# from plot_posterior_mean import plotPosteriorMean
from plot_posterior_quantile import plot_posterior_quantile

import numpy as np
import numpy.random as rnd

import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

plt.rcParams["patch.force_edgecolor"] = True
sns.set()
sns.set_style('whitegrid')
sns.set_context('paper', font_scale=1.5)
mpl.rcParams['lines.linewidth'] = 2

# plot_data()
# plot_activity_scores()
# plotQoI()
# plot_as()
# plotSummary()
# plotPriorActVar()
# plot_autocorrs()
# plot_mixing_actvar()
# plotPosteriorActVar()
# plot_prior_posterior_actvar()
# plot_posterior_marginals()
# plotPosterior()
# plotPosteriorMean()
# plot_posterior_quantile()

plt.show()
