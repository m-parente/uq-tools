import env
import model

import numpy.linalg as la
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns

sns.set()


x = np.zeros(env.n)
ds = np.array([1e-1, 1e-2, 0.5 * 1e-2, 1e-3, 0.5 * 1e-3, 1e-4, 0.5 * 1e-4])
errs = np.empty(len(ds))

pb_x = model.Model(0, x)
q_x = pb_x.getQoI()
jac_x = pb_x.getJacobian()

for i in range(len(ds)):
    dx = ds[i] * np.ones(len(x))

    pb_xdx = model.Model(1, x + dx)
    q_xdx = pb_xdx.getQoI()

    errs[i] = la.norm(q_xdx - q_x - np.dot(jac_x, dx))

plt.figure()
plt.loglog(ds, errs, 'o-')
plt.loglog(ds, ds)
plt.loglog(ds, ds**2)
plt.legend(['Error', r'$1^{st}$ order', r'$2^{nd}$ order'])
plt.xlabel('dx')
plt.ylabel('Error')
plt.show()
