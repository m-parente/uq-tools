import env

import numpy as np

import matplotlib.pyplot as plt

xs_phys = env.fromCalibrationToModelParameters(env.xs)

shape = (3, 7)
nrows = shape[0]
ncols = shape[1]
assert(nrows*ncols == env.n)

f, axarr = plt.subplots(nrows=nrows, ncols=ncols)

for row in range(nrows):
    for col in range(ncols):
        ax = axarr[row, col]
        ax.hist(xs_phys[:, row*ncols + col], bins=20, density=True)

plt.show()
