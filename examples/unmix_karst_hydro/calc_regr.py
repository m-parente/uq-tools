import env
from uq_tools import asm

import numpy as np
from sklearn import metrics


xs = np.loadtxt('%s/xs.txt' % env.dir_misfits)
misfits = np.loadtxt('%s/misfits_%.2f.txt' % (env.dir_misfits, env.noise_level))

for k in range(1, env.n+1):
    W1 = env.W[:, :k]

    g_regr = asm.response_surface(xs, misfits, W1, poly_order=4)
    print("%i: %f" % (k, metrics.r2_score(misfits, g_regr(np.dot(xs, W1)))))
