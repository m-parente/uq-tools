import env
from uq_tools.gradients import cfd

import matplotlib.dates as md
import numpy as np
import os


_settings = env.settings


class Model:
    def __init__(self, id, parameters):
        self.id = id
        self.parameters = parameters

        self.parameters_model = \
            env.fromCalibrationToPhysicalParameters([parameters]).flatten()
        [self.k_e_2_num, self.e_min_2, self.e_max_2, self.alpha_2, self.k_is_2, self.k_sec_2, self.e_sec_2,
         self.k_e_3_num, self.e_min_3, self.e_max_3, self.alpha_3, self.k_is_3, self.k_sec_3, self.e_sec_3,
         self.k_e_4_num, self.e_min_4, self.e_max_4, self.alpha_4, self.k_is_4, self.k_sec_4, self.e_sec_4] \
            = self.parameters_model

    def getQoI(self):
        if hasattr(self, 'qoi'):
            return self.qoi

        os.system('Rscript %s %s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f > /dev/null 2>&1' %
                  (env.scen_exe, self.id,
                   self.k_e_2_num, self.e_min_2, self.e_max_2, self.alpha_2, self.k_is_2, self.k_sec_2, self.e_sec_2,
                   self.k_e_3_num, self.e_min_3, self.e_max_3, self.alpha_3, self.k_is_3, self.k_sec_3, self.e_sec_3,
                   self.k_e_4_num, self.e_min_4, self.e_max_4, self.alpha_4, self.k_is_4, self.k_sec_4, self.e_sec_4))

        result = np.loadtxt('%s/output_%s.txt' %
                            (_settings.modelOutputDir, self.id),
                            comments='"')

        self.qoi = result

        return self.qoi

    def getJacobian(self):
        if hasattr(self, 'jac'):
            return self.jac

        def f(x):
            pb = Model('%d_%d' % (self.id, hash(str(x))), x)
            return pb.getQoI()

        # print "Jac:%d" % (self.id)
        # return [0.]

        self.jac = cfd.approximateGradientAt(self.parameters, f, h=1e-5)

        return self.jac


class AbstractModel:
    def __init__(self):
        pass

    def instantiate(self, parameters, id=None):
        if id is None:
            id = hash(str(parameters))
        return Model(id, parameters)
