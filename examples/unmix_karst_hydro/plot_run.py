import env
import model

import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns

sns.set()
sns.set_context('paper', font_scale=1.5)


def plot_run():
    data = env.data

    mean_calib = np.array([3.79223411e-01, -6.79281892e-03, -3.69559769e-02,  4.12631087e-02,
                           4.12062515e-01, -8.31732779e-02, 1.25578096e-02,  2.46214594e-01,
                           -8.39492192e-03, -2.80281173e-02,  4.87028920e-02, -7.13558164e-02,
                           -1.14001258e-01,  1.91462319e-02,  4.22157831e-02,  3.55552123e-03,
                           -3.12586149e-03,  6.73400664e-03, -3.55693794e-01, -1.84072944e-02,
                           -3.63374255e-04])  # Mean of calibration parameters
    # print env.fromCalibrationToPhysicalParameters([mean_calib])
    mean_phys_to_calib = env.fromPhysicalToCalibrationParameters(
        np.array([[3.07895947e+02, 2.98641436e+01, 4.44945839e+01, 1.16856840e+00,
                   5.18046211e-02, 1.74367449e-01, 4.77825507e+01, 7.06184331e+01,
                   6.04564902e+01, 1.19895928e+02, 8.20916211e-01, 4.51593892e-03,
                   2.02482983e-02, 1.75861580e+02, 2.59419632e+01, 9.57133662e+01,
                   2.05635220e+02, 4.34773227e-01, 6.35138040e-04, 6.21432845e-03,
                   3.84976381e+02]]))

    m1 = model.AbstractModel().instantiate(mean_calib)
    m2 = model.AbstractModel().instantiate(mean_phys_to_calib[0])

    qoi1 = m1.getQoI()
    qoi2 = m2.getQoI()

    plt.figure()
    plt.plot(data)
    plt.plot(qoi1)
    plt.plot(qoi2)
    plt.legend(['Data', 'Calibration mean', 'Dein Mean'])


if __name__ == '__main__':
    plot_run()
    plt.show()
