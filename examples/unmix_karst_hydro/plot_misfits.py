import env

import numpy as np
import matplotlib.pyplot as plt


xs = env.xs
Gs = env.Gs
W = env.W

misfits = list(map(env.misfitG, Gs))

for i in range(21):
    plt.figure()
    plt.semilogy(xs[:, i], misfits, 'o')

plt.show()
