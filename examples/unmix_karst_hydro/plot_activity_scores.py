import env

from uq_tools import asm

import matplotlib.pyplot as plt
import numpy as np
import numpy.linalg as la


def plot_activity_scores(ax=None):
    eig_vecs = np.loadtxt('%s/asm_eigVecs.txt' % env.as_dir)
    eig_vals = np.loadtxt('%s/asm_eigVals.txt' % env.as_dir)
    scores = asm.compute_activity_scores(eig_vals, eig_vecs)

    print("ratio max/min: %1.1e" % (np.max(scores) / np.min(scores)))

    index = list(range(1, env.n+1))
    ticks = index[::2]

    if ax is None:
        fig, ax = plt.subplots()
    ax.bar(index, scores / la.norm(scores, 2))
    ax.set_xticks(ticks)
    ax.set_yticks(np.linspace(0., 1.0, 6))
    ax.set_xlabel('Parameter index')
    ax.set_ylabel('Sensitivity')

    # plt.title(env.scenario)
    plt.tight_layout()

    return ax


if __name__ == '__main__':
    plot_activity_scores()
    plt.show()
