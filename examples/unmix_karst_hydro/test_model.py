import env
import model

import numpy as np
import time as tm

import matplotlib.pyplot as plt


parameters = [0., 0., 0., 0., 0., 0., 0.,
              0., 0., 0., 0., 0., 0., 0.,
              0., 0., 0., 0., 0., 0., 0.]

m = model.AbstractModel().instantiate(parameters)

start = tm.time()
qoi = m.getQoI()
print(tm.time() - start)
# jac = m.getJacobian()

print(env.misfitG(qoi))

# np.savetxt('%s/G-2.txt' % env.tmpDir, qoi)

plt.figure()
plt.plot(qoi)

plt.show()
