import env
import model

import numpy as np
import numpy.linalg as la
import sys


# dir = 'tmp_misfits_G'
# dir = 'tmp_post_G_%id_%.2f' % (env.k, env.noise_level)
dir = env.dir_as_G_jacG


def run(id, sample):
    print("Sample %i" % id)

    m = model.Model(id, sample)

    # qoi = m.getQoI()
    # np.savetxt('%s/G%i.txt' % (dir, id), qoi)

    qoi = m.getQoI()
    jac = m.getJacobian()

    np.savetxt('%s/G%i.txt' % (dir, id), qoi)
    np.savetxt('%s/jacG%i.txt' % (dir, id), jac)


def usage():
    print("usage: id parameters")


if __name__ == "__main__":
    if len(sys.argv) == env.n + 2:
        run(int(sys.argv[1]), np.array(sys.argv[2:], dtype=float))
    else:
        usage()
