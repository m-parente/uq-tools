import env
import model

from dolfin import *
from fenics import plot as fen_plt
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
# import seaborn as sns

# sns.set()
# sns.set_context('paper', font_scale=1.5)
# sns.set_style('whitegrid')
# mpl.rcParams['lines.linewidth'] = 2


def plot_post_mean_cond_field(it):
    dir_tmp_it = "tmp_it_%i" % it

    x_post_samples = np.loadtxt("%s/x_post_samples.txt" % dir_tmp_it)
    mean_x_post_samples = np.mean(x_post_samples, axis=0)
    median_x_post_samples = np.median(x_post_samples, axis=0)

    model_mean = model.FlowModelFixedCurvelyLayeredMediaAndWell(-1, mean_x_post_samples)

    plt.figure()
    plot(model_mean.cond_field)
    env.export_cond_field(model_mean, name="it_%s" % it, dir="tmp", postfix="_mean")

    # model_median = model.FlowModelFixedCurvelyLayeredMediaAndWell(-1, median_x_post_samples)


model_prior_mean = model.FlowModelFixedCurvelyLayeredMediaAndWell(-1, env.prior_mean)
plt.figure()
plot(model_prior_mean.cond_field)
env.export_cond_field(model_prior_mean, name="prior", dir="tmp", postfix="_mean")

iters = np.arange(-1, 2)
for i in range(len(iters)):
    it = iters[i]
    plot_post_mean_cond_field(it)

model_param_true = model.FlowModelFixedCurvelyLayeredMediaAndWell(-1, env.param_true)
plt.figure()
plot(model_param_true.cond_field)
env.export_cond_field(model_param_true, name="param_true", dir="tmp")

# plt.tight_layout()
plt.show()
