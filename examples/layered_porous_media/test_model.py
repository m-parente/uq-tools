import env
import model

from dolfin import *
import matplotlib.pyplot as plt
import numpy as np
import time


params = env.param_true
# params = env.prior_sample()
# params = np.zeros(env.n)

# print(' '.join(['%.3f' % param for param in params]))

t0 = time.time()
m = model.FlowModelFixedCurvelyLayeredMediaAndWell(-1, params)
qoi = m.qoi()
print("Elapsed time: %s s" % (time.time() - t0))
# print(env.misfitG(qoi))
# exit()

env.export_cond_field(m, name="param_true", dir="tmp")

cond_field = m.cond_field
plt.figure()
plot(cond_field)

plt.figure()
plot(m.cond_upper)

plt.figure()
plot(m.cond_middle)

plt.figure()
plot(m.cond_lower)

u = m.solveForwardProblem()

W = VectorFunctionSpace(m.mesh, 'CG', 1)
vel = project(-m.cond_field * grad(u), W)
plt.figure()
plot(vel)

plt.figure()
plot(m.domains)

plt.figure()
plot(u)
plt.plot(model.eval_pts[:, 0], model.eval_pts[:, 1], 'ro')
plt.plot([0., 1.], [model.y_nw_0, model.y_ne_0], 'k')
plt.plot([0., 1.], [model.y_sw_0, model.y_se_0], 'k')
plt.plot([0.5, model.x_source], [1., model.y_source], 'brown', lw=4)

plt.show()
