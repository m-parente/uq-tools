import env
import plotters
from uq_tools.approx import AdaptiveGPR
from uq_tools import asm
from uq_tools.mcmc import mh_mcmc_post
from uq_tools.stats.regression import ExtendedGPR
from uq_tools.sampling import adapt_mh_temp
from uq_tools.stats import metrics

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd
import pickle
import seaborn as sns
from sklearn.gaussian_process.kernels import RBF

# rnd.seed(1)
# sns.set()


k = env.k
W1 = env.W1

xs = env.samples
ys = np.dot(xs, W1)

data = np.array([env.misfitG(G) for G in env.Gs])

kernel = 1.0 * Matern(length_scale=np.ones(k), nu=1.5)

# Estimate noise for logarithmic data
# gpr = ExtendedGPR(kernel=kernel, n_restarts_optimizer=20)
# gpr.fit(ys, data)

# estim_noise_var = gpr.estimate_noise_var()
estim_noise_var = 1639.
print("Estimated noise level: %f" % estim_noise_var)
# print(1/estim_noise_var)
# exit()
##

# Get prior on active subspace
prior_y = env.prior_y
##

gpr = AdaptiveGPR(xs=ys, data=data, kernel=kernel,
                  noise_var=estim_noise_var, n_restart_optimizer=20)
gpr0 = gpr.copy()
with open("%s/gpr0_%id.bin" % (env.dir_tmp_it, k), 'wb') as gpr_file:
    pickle.dump(gpr0, gpr_file)
# exit()

ylim = np.max(np.abs(ys)) + 0.2
ys_plt = np.linspace(-ylim, ylim, num=100)


def posterior_y(gpr):
    return lambda y: np.exp(-gpr(y))*prior_y(y)


if k == 1:
    plt.figure()
    plt.plot(ys_plt, (posterior_y(gpr0))(ys_plt[:, np.newaxis]))
    plotters.plot_gpr_1d(gpr, ys_plt)
elif k == 2:
    plotters.plot_contour(ys_plt, ys_plt, posterior_y(gpr0))
    plotters.plot_gpr_2d(gpr, ys_plt, ys_plt)
# plt.show()
# exit()

# y0 = ys[np.argmin(data)]
y0 = np.zeros(k)

L = 50
T = 100
K = 100
# c = 1/estim_noise_var
c = 0.01
tol_kl = 1.0

# 1D
# samples, temps, update_pts, \
#     avg_dists, kl_divs = adapt_mh_temp(misfit_approx=gpr,
#                                        misfit_full=env.gN,
#                                        prior=prior_y,
#                                        proposal_sampler=lambda xk: rnd.normal(loc=xk, scale=0.4),
#                                        x0=y0,
#                                        n_steps=10000,
#                                        uncert_tol0=50.0,
#                                        L=L, T=T, K=K, c=c, tol_kl=tol_kl,
#                                        proposal_sampler_kl=lambda yk: rnd.normal(loc=yk, scale=0.3))

# 2D
samples, temps, update_pts, \
    avg_dists, kl_divs = adapt_mh_temp(misfit_approx=gpr,
                                       misfit_full=env.gN,
                                       prior=prior_y,
                                       proposal_sampler=lambda xk: rnd.normal(loc=xk, scale=0.2),
                                       x0=y0,
                                       n_steps=20000,
                                       uncert_tol0=10.0,
                                       L=L, T=T, K=K, c=c, tol_kl=tol_kl,
                                       proposal_sampler_kl=lambda yk: rnd.normal(loc=yk, scale=0.2))

# # 3D
# samples, temps, update_pts, \
#     avg_dists, kl_divs = adapt_mh_temp(misfit_approx=gpr,
#                                        misfit_full=env.gN,
#                                        prior=prior_y,
#                                        proposal_sampler=lambda xk: rnd.normal(loc=xk, scale=0.25),
#                                        x0=y0,
#                                        n_steps=20000,
#                                        uncert_tol0=50.0,
#                                        L=L, T=T, K=K, c=c, tol_kl=tol_kl,
#                                        proposal_sampler_kl=lambda yk: rnd.normal(loc=yk, scale=0.35))

# with open("tmp/gpr_adapt_%id.bin" % k, 'wb') as gpr_file:
#     pickle.dump(gpr, gpr_file)

samples_fine = mh_mcmc_post(misfit=gpr,
                            prior=prior_y,
                            proposal_sampler=lambda yk: rnd.normal(loc=yk, scale=0.35),
                            x0=samples[-1, :],
                            steps=20000,
                            nPlotAccptRate=500)
print("KL(gpr||gpr0): %f" % metrics.kl_div_post(gpr, gpr0, samples_fine))

# np.savetxt("tmp/y_post_samples_%id.txt" % k, samples_fine)
# exit()

if k == 1:
    plt.figure()
    plt.plot(ys_plt, posterior_y(gpr)(ys_plt[:, np.newaxis]))
    plotters.plotSamples(samples_fine, bins=40)
    plotters.plot_gpr_1d(gpr, ys_plt)
else:
    plotters.plot_density_diag(samples_fine)

    if k == 2:
        plotters.plot_contour(ys_plt, ys_plt, posterior_y(gpr))
        plotters.plot_gpr_2d(gpr, ys_plt, ys_plt)

sns.set()
plotters.plot_adapt_mh_stats(samples, temps, update_pts, avg_dists, L, kl_divs)

print("Number updates: %i" % len(update_pts))
print(kl_divs)

plt.show()
