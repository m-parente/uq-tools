import model
from misc import Settings
from uq_tools import asm
import uq_tools.bayinv as bi

import dolfin
from glob import glob
import natsort
import numpy as np
import numpy.random as rnd
from scipy import stats


settings = Settings("settings")
_s = settings

dir_ = _s.dir

n_data = len(model.eval_pts)
n = 60


def prior_sample(N=1):
    return rnd.normal(size=(N, n)) if N > 1 else rnd.normal(size=n)


prior_mean = np.zeros(n)
param_true = np.zeros(n) + 0.2

data_file = dir_ + _s.data_file
data = np.loadtxt(data_file)
noise_level = _s.noise_level
inv_noise_cov = np.diag(1./(data*noise_level)**2)
assert(len(data) == n_data)

bay_inv_pb = bi.BayesianInverseProblem(data, inv_noise_cov,
                                       absPdeProblem=model.AbstractFlowModelFixedCurvelyLayeredMediaAndWell())
misfitG = bay_inv_pb.misfitG
misfit = bay_inv_pb.misfit

n_boots = _s.n_boots

k = _s.k

# Stored files #####################
dir_tmp = _s.dir_tmp
dir_tmp_it = "tmp_it_%i" % _s.iteration
dir_as_G_jacG = "%s/%s" % (dir_tmp_it, _s.dir_as_G_jacG)
dir_as = "%s/%s" % (dir_tmp_it, _s.dir_as)

samples = np.loadtxt('%s/samples.txt' % dir_as_G_jacG)

G_files = natsort.natsorted(glob('%s/G*.txt' % dir_as_G_jacG))
jacG_files = natsort.natsorted(glob('%s/jacG*.txt' % dir_as_G_jacG))
Gs = np.array([np.loadtxt(G_file) for G_file in G_files])
jacGs = np.array([np.loadtxt(jacG_file) for jacG_file in jacG_files])

misfits = np.array([misfitG(G) for G in Gs])

L = np.loadtxt('%s/asm_eigVals.txt' % dir_as)
W = np.loadtxt('%s/asm_eigVecs.txt' % dir_as)
W1 = W[:, :k]
W2 = W[:, k:]

# dir_tmp_prev = "%s/tmp_it_prev" % dir_tmp_it
# dir_as_G_jacG_prev = "%s/%s" % (dir_tmp_prev, _s.dir_as_G_jacG)
# dir_as_prev = "%s/%s" % (dir_tmp_prev, _s.dir_as)
# W_prev = np.loadtxt('%s/asm_eigVecs.txt' % dir_as_prev)
# k_prev = 1
# W1_prev = W_prev[:, :k_prev]
####################################

prior_y_dist = stats.multivariate_normal(mean=np.zeros(k), cov=np.eye(k))


def prior_y(y):
    return prior_y_dist.pdf(y)


def prior_z_y_sample(N=1):
    return rnd.multivariate_normal(mean=np.zeros(n-k), cov=np.eye(n-k)) if N == 1 else \
        rnd.multivariate_normal(mean=np.zeros(n-k), cov=np.eye(n-k), size=N)


# MC approximation for data misfit function on active subspace
prior_samples = prior_sample(N=100000)
# x_samples_prev = np.loadtxt("%s/x_samples_prev.txt" % dir_tmp_it)
# x_samples_prev = prior_samples


def gN(y):
    ys = np.atleast_2d(y)
    M = len(ys)
    N = 1

    misfits = np.empty(len(ys))

    for i in range(M):
        zs = asm.choose_uniform_cond_sample(x_samples_prev, ys[i], W1, W2, y_tol=1.5*1e-1, N=N)
        # misfits[i] = np.average([misfit(np.dot(W1, ys[i]) + np.dot(W2, z)) for z in zs])
        misfits[i] = misfit(np.dot(W1, ys[i]) + np.dot(W2, zs))

    return misfits if M > 1 else misfits[0]


def export_cond_field(model, name, dir, prefix="", postfix=""):
    file = dolfin.File("%s/%scond_upper_%s%s.pvd" % (dir, prefix, name, postfix))
    file << dolfin.project(model.cond_upper_layer, V=model.V_layer_upper)

    file = dolfin.File("%s/%scond_middle_%s%s.pvd" % (dir, prefix, name, postfix))
    file << dolfin.project(model.cond_middle_layer, V=model.V_layer_middle)

    file = dolfin.File("%s/%scond_lower_%s%s.pvd" % (dir, prefix, name, postfix))
    file << dolfin.project(model.cond_lower_layer, V=model.V_layer_lower)
