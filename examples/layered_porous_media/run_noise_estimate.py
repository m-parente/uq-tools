import env
from uq_tools.stats.regression import ExtendedGPR

import numpy as np
import pickle
from sklearn.gaussian_process.kernels import RBF


k = env.k
W1 = env.W1

xs = env.samples
ys = np.dot(xs, W1)

data = env.misfits

kernel = 1.0 * RBF(length_scale=np.ones(k))
gpr = ExtendedGPR(kernel=kernel, n_restarts_optimizer=20)
gpr.fit(ys, data)

estim_noise = gpr.estimate_noise_var()

print("Estimated noise level: %f" % estim_noise)
