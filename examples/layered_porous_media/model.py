from misc import mesh2xml
from misc import Settings

from dolfin import *
import numpy as np
import os
from scipy.stats import norm
import sys

import matplotlib.pyplot as plt

settings = Settings("settings")
set_log_level(LogLevel.WARNING)


x_source = settings.x_source
y_source = settings.y_source

y_nw_0 = settings.y_nw_0
y_ne_0 = settings.y_ne_0
amp_n_0 = settings.amp_n_0
y_sw_0 = settings.y_sw_0
y_se_0 = settings.y_se_0
amp_s_0 = settings.amp_s_0

eval_pts = np.array([[0.35, 0.9], [0.35, 0.5], [0.35, 0.1],
                     [0.65, 0.9], [0.65, 0.5], [0.65, 0.1]])


def get_qoi_riesz_repr(i):
    point = eval_pts[i]
    # Riesz representer of the i-th qoi
    return Expression('(factor / pi) * exp(-factor * ((x[0]-x0)*(x[0]-x0) + (x[1]-y0)*(x[1]-y0)))',
                      x0=point[0], y0=point[1],
                      factor=settings.scaleFactorDiracApprox,
                      degree=2)


class Left(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 0.0)


class Right(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 1.0)


class Top(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 1.0)


class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 0.0)


class Region(SubDomain):
    def __init__(self, pt1, pt2, pt3, pt4):
        SubDomain.__init__(self)
        self.pt1 = pt1  # sw
        self.pt2 = pt2  # nw
        self.pt3 = pt3  # ne
        self.pt4 = pt4  # se

    def inside(self, x, on_boundary):
        # Give space on the boundary for gmsh
        return between(x[1], ((self.pt4[1] - self.pt1[1]) * x[0] + self.pt1[1] - 1e-10, (self.pt3[1] - self.pt2[1]) * x[0] + self.pt2[1] + 1e-10))


class IndicatorFunction(UserExpression):
    def __init__(self, domain_, **kwargs):
        super().__init__(kwargs)
        self.domain_ = domain_

    def eval(self, value, x):
        value[0] = 1 if self.domain_.inside(x, False) else 0

    def value_shape(self):
        return ()


class CurvedRegion(SubDomain):
    def __init__(self, ptsLeft, ptsTop, ptsRight, ptsBottom):
        super().__init__()

        self.ptsLeft = ptsLeft
        self.ptsTop = ptsTop
        self.ptsRight = ptsRight
        self.ptsBottom = ptsBottom

        self.nTop = len(ptsTop)
        self.nBottom = len(ptsBottom)

        self.lenIntervalsTop = 1.0 / (self.nTop - 1)
        self.lenIntervalsBottom = 1.0 / (self.nBottom - 1)

    def inside(self, x, on_boundary):
        i = int(np.floor(x[0] / self.lenIntervalsTop))
        pt1, pt2 = self.ptsTop[i:i + 2] if i < self.nTop - 1 \
            else self.ptsTop[i - 1:i + 1]
        x1, y1 = pt1
        x2, y2 = pt2

        if x[1] - DOLFIN_EPS >= (y2 - y1) / (x2 - x1) * (x[0] - x1) + y1:
            return False

        # Take care of reverse order!
        i = self.nBottom - 1 - int(np.floor(x[0] / self.lenIntervalsBottom))
        pt2, pt1 = self.ptsBottom[i - 1:i + 1] if i > 0 else self.ptsBottom[:2]
        x1, y1 = pt1
        x2, y2 = pt2

        if x[1] + DOLFIN_EPS < (y2 - y1) / (x2 - x1) * (x[0] - x1) + y1:
            return False

        return True


def generateCurvedRegions(n, y_nw, y_ne, amp_n, y_sw, y_se, amp_s):
    # Upper region
    ptsLeft = [(0.0, y_nw), (0.0, 1.0)]
    ptsTop = [(0.0, 1.0), (1.0, 1.0)]
    ptsRight = [(1.0, 1.0), (1.0, y_ne)]

    xs = np.flipud(np.arange(0.0, 1.0 + 1e-5, 1.0 / (n + 1)))  # Reverse order!
    ys = [-4 * amp_n * x * x + (y_ne - y_nw + 4 * amp_n) * x + y_nw
          for x in xs]  # Quadratic function connecting (0.0, y_nw) and (1.0, y_ne) with amplitude amp_n
    ptsBottom = [pt for pt in zip(xs, ys)]
    upper = CurvedRegion(ptsLeft, ptsTop, ptsRight, ptsBottom)

    # Middle region
    ptsLeft = [(0.0, y_sw), (0.0, y_nw)]
    ptsTop = ptsBottom[::-1]
    ptsRight = [(1.0, y_ne), (1.0, y_se)]

    ys = [-4 * amp_s * x * x + (y_se - y_sw + 4 * amp_s) * x + y_sw
          for x in xs]
    ptsBottom = [pt for pt in zip(xs, ys)]
    middle = CurvedRegion(ptsLeft, ptsTop, ptsRight, ptsBottom)

    # Lower region
    ptsLeft = [(0.0, 0.0), (0.0, y_sw)]
    ptsTop = ptsBottom[::-1]
    ptsRight = [(1.0, y_se), (1.0, 0.0)]
    ptsBottom = [(1.0, 0.0), (0.0, 0.0)]
    lower = CurvedRegion(ptsLeft, ptsTop, ptsRight, ptsBottom)

    return (upper, middle, lower)


def _generateCurvelyLayeredMesh(meshname, upper, middle, lower):
    tmpGeoFile = meshname + ".geo"

    regions = [upper, middle, lower]
    s = ""
    id_start_pts = 1
    id_start_lines = 1

    idsLinesBottom = []
    ptIdNw = -1
    ptIdNe = -1
    ptIdSw = -1
    ptIdSe = -1

    for i_region in range(len(regions)):
        region = regions[i_region]

        ptIdNw = ptIdSw

        # Left points
        pts = region.ptsLeft[:-1] if len(idsLinesBottom) > 0 \
            else region.ptsLeft
        ptIds = list(range(id_start_pts, id_start_pts + len(pts)))
        id_start_pts = id_start_pts + len(pts)

        s += "\n".join(["Point(%d) = {%f, %f, 0, 1};" % (ptId, pt[0], pt[1])
                        for (pt, ptId) in zip(pts, ptIds)])
        s += "\n"

        if len(idsLinesBottom) > 0:
            ptIds = ptIds + [ptIdNw]

        lineIds = list(range(id_start_lines, id_start_lines +
                             len(pts) - (0 if len(idsLinesBottom) > 0 else 1)))
        id_start_lines = id_start_lines + len(lineIds)

        s += "\n".join(["Line(%d) = {%d, %d};" % (lineId, ptId1, ptId2)
                        for (lineId, (ptId1, ptId2)) in zip(lineIds, list(zip(ptIds[:-1], ptIds[1:])))])
        s += "\n"

        idsLinesLeft = lineIds
        ptIdSw = ptIds[0]
        ptIdNw = ptIds[-1]

        # Top points
        if len(idsLinesBottom) > 0:
            idsLinesTop = [-i for i in idsLinesBottom[::-1]]
        else:
            pts = region.ptsTop[1:]
            ptIds = list(range(id_start_pts, id_start_pts + len(pts)))
            id_start_pts = id_start_pts + len(pts)

            s += "\n".join(["Point(%d) = {%f, %f, 0, 1};" % (ptId, pt[0], pt[1])
                            for (pt, ptId) in zip(pts, ptIds)])
            s += "\n"

            ptIds = [ptIdNw] + ptIds

            lineIds = list(range(id_start_lines, id_start_lines + len(pts)))
            id_start_lines = id_start_lines + len(lineIds)

            s += "\n".join(["Line(%d) = {%d, %d};" % (lineId, ptId1, ptId2)
                            for (lineId, (ptId1, ptId2)) in zip(lineIds, list(zip(ptIds[:-1], ptIds[1:])))])
            s += "\n"

            idsLinesTop = lineIds
            ptIdNe = ptIds[-1]

        # Right points
        pts = region.ptsRight[1:]
        ptIds = list(range(id_start_pts, id_start_pts + len(pts)))
        id_start_pts = id_start_pts + len(pts)

        s += "\n".join(["Point(%d) = {%f, %f, 0, 1};" % (ptId, pt[0], pt[1])
                        for (pt, ptId) in zip(pts, ptIds)])
        s += "\n"

        ptIds = [ptIdSe if len(idsLinesBottom) > 0 else ptIdNe] + ptIds

        lineIds = list(range(id_start_lines, id_start_lines + len(pts)))
        id_start_lines = id_start_lines + len(lineIds)

        s += "\n".join(["Line(%d) = {%d, %d};" % (lineId, ptId1, ptId2)
                        for (lineId, (ptId1, ptId2)) in zip(lineIds, list(zip(ptIds[:-1], ptIds[1:])))])
        s += "\n"

        idsLinesRight = lineIds
        ptIdSe = ptIds[-1]

        # Bottom points
        pts = region.ptsBottom[1:-1]
        ptIds = list(range(id_start_pts, id_start_pts + len(pts)))
        id_start_pts = id_start_pts + len(pts)

        s += "\n".join(["Point(%d) = {%f, %f, 0, 1};" % (ptId, pt[0], pt[1])
                        for (pt, ptId) in zip(pts, ptIds)])
        s += "\n"

        ptIds = [ptIdSe] + ptIds + [ptIdSw]

        # - One more than above since we want to close the loop
        lineIds = list(range(id_start_lines, id_start_lines + len(pts) + 1))
        id_start_lines = id_start_lines + len(lineIds)

        s += "\n".join(["Line(%d) = {%d, %d};" % (lineId, ptId1, ptId2)
                        for (lineId, (ptId1, ptId2)) in zip(lineIds, list(zip(ptIds[:-1], ptIds[1:])))])
        s += "\n"

        idsLinesBottom = lineIds

        lineIds = idsLinesLeft + idsLinesTop + idsLinesRight + idsLinesBottom
        s += "\n".join(["Line Loop(%d) = {%s}; " % (i_region + 1,
                                                    ", ".join([repr(i) for i in lineIds]))]
                       + ["Plane Surface(%d) = {%d};" % (i_region + 1, i_region + 1)])
        s += "\n"

    with open(tmpGeoFile, 'w+') as f:
        f.write(s)

    os.system("gmsh %s -2 -clscale %f > /dev/null" % (tmpGeoFile, settings.meshScaleFactor))
    mesh2xml(meshname)


def diracApprox(s):
    return 'sqrt(%i / pi) * exp(-%i * ((%s) * (%s)))' % (settings.scaleFactorDiracApprox, settings.scaleFactorDiracApprox, s, s)


class FlowModelCurvelyLayeredMediaAndWell:
    def __init__(self, id, parameters, meshname=None):
        self.id = id
        self.parameters = parameters
        self.meshname = meshname

        self.numParameters = len(parameters)

        self.y_nw = 0.1 * parameters[0] + y_nw_0  # nw
        self.y_ne = 0.1 * parameters[1] + y_ne_0  # ne
        self.amp_n = 0.1 * parameters[2] + amp_n_0  # north amplitude

        self.y_sw = 0.1 * parameters[3] + y_sw_0  # sw
        self.y_se = 0.1 * parameters[4] + y_se_0  # se
        self.amp_s = 0.1 * parameters[5] + amp_s_0  # south amplitude

        self.magn_well = parameters[6] * 0.5 * (settings.source_ub - settings.source_lb) + \
            0.5 * (settings.source_ub + settings.source_lb)

        self.start_klTerms = 7

        klModesUpper = parameters[self.start_klTerms:self.start_klTerms + settings.lenKLUpper]
        klModesMiddle = parameters[self.start_klTerms + settings.lenKLUpper:
                                   self.start_klTerms + settings.lenKLUpper + settings.lenKLMiddle]
        klModesLower = parameters[self.start_klTerms + settings.lenKLUpper + settings.lenKLMiddle:
                                  self.start_klTerms + settings.lenKLUpper + settings.lenKLMiddle + settings.lenKLLower]

        # Define parabolas separating layers (P1 upper, P2 lower parabola)
        self.P1 = '-4*A1*x[0]*x[0] + (y_ne - y_nw + 4*A1)*x[0] + y_nw'
        self.P2 = '-4*A2*x[0]*x[0] + (y_se - y_sw + 4*A2)*x[0] + y_sw'

        upper, middle, lower = generateCurvedRegions(1.0 / settings.meshScaleFactor - 1,
                                                     self.y_nw, self.y_ne, self.amp_n,
                                                     self.y_sw, self.y_se, self.amp_s)

        if meshname is None:
            meshname = "tmp/tmpMesh" + repr(id)
            _generateCurvelyLayeredMesh(meshname, upper, middle, lower)

        self.mesh = Mesh(meshname + ".xml")
        self.mesh.init()

        self.V = FunctionSpace(self.mesh, "CG", 1)
        self.V_a = FunctionSpace(self.mesh, "CG", 1)

        # Region ----------------------------
        self.domains = MeshFunction("size_t", self.mesh, self.mesh.topology().dim())
        self.domains.set_all(1)
        upper.mark(self.domains, 1)
        middle.mark(self.domains, 2)
        lower.mark(self.domains, 3)
        # -----------------------------------

        # Conductivities --------------------
        # - Upper layer
        meshname_upper = "lined_upper"
        # meshname_upper = "lined_upper_fine"

        self.eigVals_upper = np.loadtxt("kl/kl_eigvals_%s.txt" % meshname_upper)
        self.eigVecs_upper = [None] * len(klModesUpper)

        mesh_layer = Mesh('meshes/%s.xml' % meshname_upper)
        self.V_layer_upper = FunctionSpace(mesh_layer, 'CG', 1)

        log_cond_upper = Function(self.V)
        log_cond_upper_layer = Function(self.V_layer_upper)
        for i_kl in range(len(klModesUpper)):
            eigFunc = Function(self.V_layer_upper)
            eigFunc.set_allow_extrapolation(True)
            f = File('kl/kl_eigfunc_%s_%i.xml' % (meshname_upper, i_kl))
            f >> eigFunc
            self.eigVecs_upper[i_kl] = interpolate(eigFunc, self.V)
            log_cond_upper.vector().axpy(
                np.sqrt(self.eigVals_upper[i_kl]) * klModesUpper[i_kl], self.eigVecs_upper[i_kl].vector())
            log_cond_upper_layer.vector().axpy(
                np.sqrt(self.eigVals_upper[i_kl]) * klModesUpper[i_kl], eigFunc.vector())
        self.cond_upper = exp(Constant(settings.meanCondUpper) + log_cond_upper) * \
            IndicatorFunction(upper, element=self.V.ufl_element())
        self.cond_upper_layer = exp(Constant(settings.meanCondUpper) + log_cond_upper_layer)

        # - Middle layer
        meshname_middle = "lined_middle"
        # meshname_middle = "lined_middle_fine"

        self.eigVals_middle = np.loadtxt("kl/kl_eigvals_%s.txt" % meshname_middle)
        self.eigVecs_middle = [None] * len(klModesMiddle)

        mesh_layer = Mesh('meshes/%s.xml' % meshname_middle)
        self.V_layer_middle = FunctionSpace(mesh_layer, 'CG', 1)

        log_cond_middle = Function(self.V)
        log_cond_middle_layer = Function(self.V_layer_middle)
        for i_kl in range(len(klModesMiddle)):
            eigFunc = Function(self.V_layer_middle)
            eigFunc.set_allow_extrapolation(True)
            f = File('kl/kl_eigfunc_%s_%i.xml' % (meshname_middle, i_kl))
            f >> eigFunc
            self.eigVecs_middle[i_kl] = interpolate(eigFunc, self.V)
            log_cond_middle.vector().axpy(
                np.sqrt(self.eigVals_middle[i_kl]) * klModesMiddle[i_kl], self.eigVecs_middle[i_kl].vector())
            log_cond_middle_layer.vector().axpy(
                np.sqrt(self.eigVals_middle[i_kl]) * klModesMiddle[i_kl], eigFunc.vector())
        self.cond_middle = exp(Constant(settings.meanCondMiddle) + log_cond_middle) * \
            IndicatorFunction(middle, element=self.V.ufl_element())
        self.cond_middle_layer = exp(Constant(settings.meanCondMiddle) + log_cond_middle_layer)

        # - Lower layer
        meshname_lower = "lined_lower"
        # meshname_lower = "lined_lower_fine"

        self.eigVals_lower = np.loadtxt("kl/kl_eigvals_%s.txt" % meshname_lower)
        self.eigVecs_lower = [None] * len(klModesLower)

        mesh_layer = Mesh('meshes/%s.xml' % meshname_lower)
        self.V_layer_lower = FunctionSpace(mesh_layer, 'CG', 1)

        log_cond_lower = Function(self.V)
        log_cond_lower_layer = Function(self.V_layer_lower)
        for i_kl in range(len(klModesLower)):
            eigFunc = Function(self.V_layer_lower)
            eigFunc.set_allow_extrapolation(True)
            f = File('kl/kl_eigfunc_%s_%i.xml' % (meshname_lower, i_kl))
            f >> eigFunc
            self.eigVecs_lower[i_kl] = interpolate(eigFunc, self.V)
            log_cond_lower.vector().axpy(
                np.sqrt(self.eigVals_lower[i_kl]) * klModesLower[i_kl], self.eigVecs_lower[i_kl].vector())
            log_cond_lower_layer.vector().axpy(
                np.sqrt(self.eigVals_lower[i_kl]) * klModesLower[i_kl], eigFunc.vector())
        self.cond_lower = exp(Constant(settings.meanCondLower) + log_cond_lower) * \
            IndicatorFunction(lower, element=self.V.ufl_element())
        self.cond_lower_layer = exp(Constant(settings.meanCondLower) + log_cond_lower_layer)

        self.cond_field = self.cond_upper + self.cond_middle + self.cond_lower
        # -----------------------------------

        # Boundaries ------------------------
        left = Left()
        right = Right()
        top = Top()
        bottom = Bottom()

        self.boundaries = MeshFunction("size_t", self.mesh, self.mesh.topology().dim() - 1)
        self.boundaries.set_all(0)
        left.mark(self.boundaries, 1)
        top.mark(self.boundaries, 2)
        right.mark(self.boundaries, 3)
        bottom.mark(self.boundaries, 4)
        # -----------------------------------

        # Measures --------------------------
        self.dx = Measure('dx', domain=self.mesh, subdomain_data=self.domains)
        self.ds = Measure('ds', domain=self.mesh, subdomain_data=self.boundaries)
        # -----------------------------------

    def qoi(self):
        self.solveForwardProblem()
        return np.array([assemble(inner(get_qoi_riesz_repr(i), self.u) * self.dx)
                         for i in range(len(eval_pts))])

    def solveForwardProblem(self):
        if hasattr(self, 'u'):
            return self.u

        # Weak formulation + solving --------
        u = TrialFunction(self.V)
        v = TestFunction(self.V)

        f = Expression('x[1] < y0-0.02 || x[1] > y0+0.02 ? 0 : M*' + diracApprox('x[0]-x0'),
                       M=self.magn_well,
                       x0=settings.x_source,
                       y0=settings.y_source,
                       element=self.V.ufl_element())

        bcs = [DirichletBC(self.V, 1.4, self.boundaries, 1),
               DirichletBC(self.V, 1.0, self.boundaries, 3)]

        F = inner(self.cond_upper * grad(u), grad(v)) * self.dx(1) \
            + inner(self.cond_middle * grad(u), grad(v)) * self.dx(2) \
            + inner(self.cond_lower * grad(u), grad(v)) * self.dx(3) \
            - f * v * self.dx
        a, L = lhs(F), rhs(F)

        self.u = Function(self.V)
        solve(a == L, self.u, bcs)
        # -----------------------------------

        return self.u

    def _solveAdjointProblem(self, qoi_rsz_repr):
        # Weak formulation + solving --------
        u_a = TrialFunction(self.V_a)
        v_a = TestFunction(self.V_a)

        f_a = qoi_rsz_repr

        bcs_a = [DirichletBC(self.V_a, 0.0, self.boundaries, 1),
                 DirichletBC(self.V_a, 0.0, self.boundaries, 3)]

        a_a = inner(self.cond_upper * grad(u_a), grad(v_a)) * self.dx(1) \
            + inner(self.cond_middle * grad(u_a), grad(v_a)) * self.dx(2) \
            + inner(self.cond_lower * grad(u_a), grad(v_a)) * self.dx(3)
        L_a = f_a * v_a * self.dx

        u_a = Function(self.V_a)
        solve(a_a == L_a, u_a, bcs_a)

        return u_a
        # -----------------------------------

    def _grad(self, qoi_rsz_repr):
        self.solveForwardProblem()
        u_a = self._solveAdjointProblem(qoi_rsz_repr)

        dF = [None] * self.numParameters
        derivs = np.empty(self.numParameters)

        # Note: Apply the chain rule since parameter inputs are normalized and had to be scaled in the constructor.

        tmpExpr = (self.cond_upper - self.cond_middle) * \
            Expression('0.1 * ' + diracApprox('x[1]-(' + self.P1 + ')'),
                       A1=self.amp_n,
                       y_nw=self.y_nw,
                       y_ne=self.y_ne,
                       element=self.V.ufl_element())

        dF[0] = tmpExpr * Expression('x[0]-1', element=self.V.ufl_element())
        dF[1] = tmpExpr * Expression('-x[0]', element=self.V.ufl_element())
        dF[2] = tmpExpr * Expression('4*x[0]*(x[0]-1)', element=self.V.ufl_element())

        tmpExpr = (self.cond_lower - self.cond_middle) * \
            Expression('0.1 * ' + diracApprox('(' + self.P2 + ')-x[1]'),
                       A2=self.amp_s,
                       y_sw=self.y_sw,
                       y_se=self.y_se,
                       element=self.V.ufl_element())

        dF[3] = tmpExpr * Expression('1-x[0]', element=self.V.ufl_element())
        dF[4] = tmpExpr * Expression('x[0]', element=self.V.ufl_element())
        dF[5] = tmpExpr * Expression('4*x[0]*(1-x[0])', element=self.V.ufl_element())

        # Well parameter
        dF[6] = Expression('x[1] < y0 ? 0 : 0.5 * (source_ub - source_lb) * ' + diracApprox('x[0]-x0'),
                           source_ub=settings.source_ub,
                           source_lb=settings.source_lb,
                           y0=settings.y_source,
                           x0=settings.x_source,
                           element=self.V.ufl_element())

        # Upper KL modes
        dF[self.start_klTerms:self.start_klTerms + len(self.eigVals_upper)] = \
            [self.cond_upper * np.sqrt(self.eigVals_upper[i]) * self.eigVecs_upper[i]
             for i in range(len(self.eigVals_upper))]

        # Middle KL modes
        dF[self.start_klTerms + len(self.eigVals_upper):self.start_klTerms + len(self.eigVals_upper) + len(self.eigVals_middle)] = \
            [self.cond_middle * np.sqrt(self.eigVals_middle[i]) * self.eigVecs_middle[i]
             for i in range(len(self.eigVals_middle))]

        # Lower KL modes
        dF[self.start_klTerms + len(self.eigVals_upper) + len(self.eigVals_middle):self.start_klTerms + len(self.eigVals_upper) + len(self.eigVals_middle) + len(self.eigVals_lower)] = \
            [self.cond_lower * np.sqrt(self.eigVals_lower[i]) * self.eigVecs_lower[i]
             for i in range(len(self.eigVals_lower))]

        derivs[:6] = [assemble(inner(-dF[i] * grad(self.u), grad(u_a)) * self.dx(1)
                               + inner(-dF[i] * grad(self.u), grad(u_a)) * self.dx(2)
                               + inner(-dF[i] * grad(self.u), grad(u_a)) * self.dx(3))
                      for i in range(6)]

        derivs[6] = assemble(inner(dF[6] * grad(self.u), grad(u_a)) * self.dx)

        derivs[self.start_klTerms:self.start_klTerms + len(self.eigVals_upper)] = \
            [assemble(inner(-dF[i] * grad(self.u), grad(u_a)) * self.dx(1))
                for i in range(self.start_klTerms, self.start_klTerms + len(self.eigVals_upper))]
        derivs[self.start_klTerms + len(self.eigVals_upper):self.start_klTerms + len(self.eigVals_upper) + len(self.eigVals_middle)] = \
            [assemble(inner(-dF[i] * grad(self.u), grad(u_a)) * self.dx(2))
                for i in range(self.start_klTerms + len(self.eigVals_upper), self.start_klTerms + len(self.eigVals_upper) + len(self.eigVals_middle))]
        derivs[self.start_klTerms + len(self.eigVals_upper) + len(self.eigVals_middle):self.start_klTerms + len(self.eigVals_upper) + len(self.eigVals_middle) + len(self.eigVals_lower)] = \
            [assemble(inner(-dF[i] * grad(self.u), grad(u_a)) * self.dx(3))
                for i in range(self.start_klTerms + len(self.eigVals_upper) + len(self.eigVals_middle), self.start_klTerms + len(self.eigVals_upper) + len(self.eigVals_middle) + len(self.eigVals_lower))]

        return derivs

    def jac(self):
        if hasattr(self, '_jac'):
            return self._jac

        self._jac = np.empty((len(eval_pts), self.numParameters))

        for i in range(len(eval_pts)):
            self._jac[i, :] = self._grad(get_qoi_riesz_repr(i))

        return self._jac

    def jac_cfd(self):
        from uq_tools.gradients import cfd

        def f(x):
            pb = FlowModelCurvelyLayeredMediaAndWell(
                '%d_%d' % (self.id, hash(str(x))), x, self.meshname)
            return pb.qoi()

        return cfd.approximateGradientAt(self.parameters, f, h=1e-7)


class FlowModelFixedCurvelyLayeredMediaAndWell(FlowModelCurvelyLayeredMediaAndWell):
    def __init__(self, id, parameters):
        parameters = np.concatenate((np.zeros(7), parameters))

        super().__init__(id, parameters, meshname='meshes/lined_all')
        # super().__init__(id, parameters, meshname='meshes/lined_all_fine')

    def jac(self):
        jac = super().jac()

        return jac[:, 7:]

    def jac_cfd(self):
        jac = super().jac_cfd()

        return jac[:, 7:]


class AbstractFlowModelFixedCurvelyLayeredMediaAndWell:
    def instantiate(self, parameters):
        return FlowModelFixedCurvelyLayeredMediaAndWell(id=-1, parameters=parameters)
