import env
import plotters

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

sns.set()


dir_as = env.dir_as
n_plot_eigvals = 10

L = env.L
W = env.W
min_eigvals = np.loadtxt('%s/asm_minEigVals.txt' % dir_as)
max_eigvals = np.loadtxt('%s/asm_maxEigVals.txt' % dir_as)
min_subsperrs = np.loadtxt('%s/asm_minSubspaceErrors.txt' % dir_as)
max_subsperrs = np.loadtxt('%s/asm_maxSubspaceErrors.txt' % dir_as)
mean_subsperrs = np.loadtxt('%s/asm_meanSubspaceErrors.txt' % dir_as)

plotters.plotActiveSubspace(L, W, n_plot_eigvals, min_eigvals, max_eigvals,
                            min_subsperrs, max_subsperrs, mean_subsperrs,
                            plot_sep=False, num_plot_eigvecs=4, step_eigvec=10)
plt.show()
