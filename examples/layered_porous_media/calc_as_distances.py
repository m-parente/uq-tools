import env

import glob
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import natsort
import numpy as np
import numpy.linalg as la
import scipy.linalg as sla
import seaborn as sns

sns.set()
sns.set_context('paper', font_scale=1.5)
sns.set_style('whitegrid')
mpl.rcParams['lines.linewidth'] = 2


def get_as_matrices(dir_as):
    L = np.loadtxt('%s/asm_eigVals.txt' % dir_as)
    W = np.loadtxt('%s/asm_eigVecs.txt' % dir_as)
    return np.dot(np.dot(W, np.diag(L)), W.T), W


k = 4
dirs_as = natsort.natsorted(glob.glob("tmp_it_?/as/"))
print("Directories: %s" % dirs_as)

mats = [get_as_matrices(dir_as) for dir_as in dirs_as]

for i in range(0, len(mats)-1):
    C_i = mats[i][0]
    C_i1 = mats[i+1][0]
    diff_norm = la.norm(C_i1-C_i, ord=2) / la.norm(C_i1, ord=2)
    print("Relative norm of difference between C_%i and C_%i: %e" % (i, i+1, diff_norm))

print()

for i in range(0, len(mats)-1):
    W_i = mats[i][1][:, :k]
    W_i1 = mats[i+1][1][:, :k]
    # Relative norm is equal to the absolute since W's are orthogonal
    diff_norm = la.norm(W_i1-W_i, ord=2)
    print("Relative norm of difference between W_%i and W_%i: %e" % (i, i+1, diff_norm))

print()

for i in range(0, len(mats)-1):
    W_i = mats[i][1][:, :k]
    W_i1 = mats[i+1][1][:, :k]
    subsp_angls = np.rad2deg(sla.subspace_angles(W_i, W_i1))
    print("Principal angle between W_%i and W_%i: %s" % (i, i+1, subsp_angls))


fig, axes = plt.subplots(1, len(mats)-1, sharex=True, sharey=True, figsize=(10, 5))
ks = np.arange(1, 20)
for i in range(0, len(mats)-1):
    ax = axes[i]

    W_i = mats[i][1]
    W_i1 = mats[i+1][1]

    for k1 in range(1, 5):
        ks_ = ks[ks >= k1]
        max_subsp_angls = [np.max(np.rad2deg(sla.subspace_angles(W_i[:, :k_], W_i1[:, :k1])))
                           for k_ in ks_]
        ax.plot(ks_, max_subsp_angls, label='$k^{(%i)}=%i$' % (i+1, k1))

    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.set_xlabel("$k^{(%i)}$" % (i))
    ax.set_ylabel("Max. principal angle betw. $W^{(%i)}_{1:k^{(%i)}}$ and $W^{(%i)}_{1:k^{(%i)}}$"
                  % (i, i, i+1, i+1))
    ax.legend()
plt.tight_layout()

plt.show()
