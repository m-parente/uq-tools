import env
import model
import plotters
from uq_tools import asm
from uq_tools import utils

import matplotlib.pyplot as plt
import numpy as np
import pickle


k = env.k

# np.savetxt("%s/x_post_samples.txt" % env.dir_tmp_it, env.prior_sample(N=1000))
# exit()

# print(utils.dofp(4, 4))
# exit()

y_samples = np.loadtxt("%s/y_post_samples_%id.txt" % (env.dir_tmp_it, k))
# y_samples = np.loadtxt("%s/y_samples_prev_%id.txt" % (env.dir_tmp_it, k))
if len(np.shape(y_samples)) <= 1:
    y_samples = y_samples[:, np.newaxis]


plotters.plot_density_diag(y_samples)
plt.show()
exit()
