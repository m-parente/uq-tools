import env
import model

import numpy as np
import sys


def run(id, sample):
    print("Sample %i" % id)

    m = model.FlowModelFixedCurvelyLayeredMediaAndWell(id, sample)

    qoi = m.qoi()
    jac = m.jac()

    np.savetxt('%s/G%i.txt' % (env.dir_as_G_jacG, id), qoi)
    np.savetxt('%s/jacG%i.txt' % (env.dir_as_G_jacG, id), jac)


def usage():
    print("usage: id nw ne se sw")


if __name__ == "__main__":
    if len(sys.argv) == env.n + 2:
        run(int(sys.argv[1]), np.array(sys.argv[2:], dtype=float))
    else:
        usage()
