import env
from misc import mesh2xml, Settings

from dolfin import *
import numpy as np
import os
from scipy.stats import norm

import matplotlib.pyplot as plt


set_log_level(WARNING)

settings = Settings("settings")


def toUniform(x):
    return np.array([norm.cdf(xi) for xi in x])


def generateMesh(meshname, layers):
    (y1, y2, y3, y4) = layers
    tmpGeoFile = meshname + ".geo"
    with open(tmpGeoFile, "w+") as f:
        f.write("""Point(1) = {0, 0, 0, 1};
Point(2) = {0, 1, 0, 1};
Point(3) = {1, 1, 0, 1};
Point(4) = {1, 0, 0, 1};
Point(5) = {0, """ + str(y4) + """, 0, 1};
Point(6) = {0, """ + str(y1) + """, 0, 1};
Point(7) = {1, """ + str(y2) + """, 0, 1};
Point(8) = {1, """ + str(y3) + """, 0, 1};
Line(1) = {1, 5};
Line(2) = {5, 6};
Line(3) = {6, 2};
Line(4) = {2, 3};
Line(5) = {3, 7};
Line(6) = {7, 8};
Line(7) = {8, 4};
Line(8) = {4, 1};
Line(9) = {5, 8};
Line(10) = {6, 7};
Line Loop(12) = {1, 9, 7, 8};
Plane Surface(12) = {12};
Line Loop(14) = {2, 10, 6, -9};
Plane Surface(14) = {14};
Line Loop(16) = {4, 5, -10, 3};
Plane Surface(16) = {16};""")
    os.system("gmsh '" + tmpGeoFile + "' -2 -clscale " +
              str(settings.meshScaleFactor) + " > /dev/null")
    # os.system("gmsh '" + meshname + ".msh'")
    mesh2xml(meshname)


class Left(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 0.0)


class Right(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 1.0)


class Top(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 1.0)


class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 0.0)


class Region(SubDomain):
    def __init__(self, pt1, pt2, pt3, pt4):
        SubDomain.__init__(self)
        self.pt1 = pt1  # sw
        self.pt2 = pt2  # nw
        self.pt3 = pt3  # ne
        self.pt4 = pt4  # se

    def inside(self, x, on_boundary):
        # Give space on the boundary for gmsh
        return between(x[1], ((self.pt4[1] - self.pt1[1]) * x[0] + self.pt1[1] - 1e-10, (self.pt3[1] - self.pt2[1]) * x[0] + self.pt2[1] + 1e-10))


class IndicatorFunction(Expression):
    def __init__(self, domain, **kwargs):
        self.domain = domain

    def eval(self, value, x):
        value[0] = 1 if self.domain.inside(x, False) else 0

    # def value_shape(self):
    #     return (1,)


class ModelBase:
    def __init__(self, id, parameters):
        self.y1 = parameters[0] / 8 + (6 / 8.0)  # nw
        self.y2 = parameters[1] / 8 + (7 / 8.0)  # ne
        self.y3 = parameters[2] / 8 + (2 / 8.0)  # se
        self.y4 = parameters[3] / 8 + (3 / 8.0)  # sw

        conds = (parameters[4], parameters[5], parameters[6])

        meshname = "tmp/tmpMesh" + repr(id)
        generateMesh(meshname, (self.y1, self.y2, self.y3, self.y4))

        # self.mesh = UnitSquareMesh(30, 30)
        self.mesh = Mesh(meshname + ".xml")
        self.mesh.init()

        self.V = FunctionSpace(self.mesh, "CG", 1)
        self.V_a = FunctionSpace(self.mesh, "CG", 1)

        # Regions ---------------------------
        (pt1, pt2, pt3, pt4) = ((0, self.y4),
                                (0, self.y1),
                                (1, self.y2),
                                (1, self.y3))
        upper = Region(pt2, (0, 1), (1, 1), pt3)
        middle = Region(pt1, pt2, pt3, pt4)
        lower = Region((0, 0), pt1, pt4, (1, 0))

        self.domains = CellFunction("size_t", self.mesh)
        self.domains.set_all(1)
        upper.mark(self.domains, 1)
        middle.mark(self.domains, 2)
        lower.mark(self.domains, 3)
        # -----------------------------------

        # Conductivities --------------------
        self.condUpper = Constant(
            settings.meanCondUpper * (1 + 0.05 * conds[0]))
        self.condMiddle = Constant(
            settings.meanCondMiddle * (1 + 0.05 * conds[1]))
        self.condLower = Constant(
            settings.meanCondLower * (1 + 0.05 * conds[2]))
        # -----------------------------------

        # Boundaries ------------------------
        left = Left()
        right = Right()
        top = Top()
        bottom = Bottom()

        self.boundaries = FacetFunction("size_t", self.mesh)
        self.boundaries.set_all(0)
        left.mark(self.boundaries, 1)
        top.mark(self.boundaries, 2)
        right.mark(self.boundaries, 3)
        bottom.mark(self.boundaries, 4)
        # -----------------------------------

        # Measures --------------------------
        self.dx = Measure('dx', domain=self.mesh, subdomain_data=self.domains)
        self.ds = Measure('ds', domain=self.mesh,
                          subdomain_data=self.boundaries)
        # -----------------------------------

    def solveForwardProblem(self):
        raise NotImplementedError()

    def _solveAdjointProblem(self, qoi_rsz_repr):
        raise NotImplementedError()

    def getQoIValues(self):
        self.solveForwardProblem()
        return np.array([self.u(pt[0], pt[1]) for pt in eval_pts])

    def getGradient(self, qoi_rsz_repr):
        raise NotImplementedError()


class CurvedRegion(SubDomain):
    def __init__(self, ptsLeft, ptsTop, ptsRight, ptsBottom):
        SubDomain.__init__(self)

        self.ptsLeft = ptsLeft
        self.ptsTop = ptsTop
        self.ptsRight = ptsRight
        self.ptsBottom = ptsBottom

        self.nTop = len(ptsTop)
        self.nBottom = len(ptsBottom)

        self.lenIntervalsTop = 1.0 / (self.nTop - 1)
        self.lenIntervalsBottom = 1.0 / (self.nBottom - 1)

    def inside(self, x, on_boundary):
        i = int(np.floor(x[0] / self.lenIntervalsTop))
        pt1, pt2 = self.ptsTop[i:i + 2] if i < self.nTop - 1 \
            else self.ptsTop[i - 1:i + 1]
        x1, y1 = pt1
        x2, y2 = pt2

        if x[1] - 1e-6 >= (y2 - y1) / (x2 - x1) * (x[0] - x1) + y1:
            return False

        # Take care of reverse order!
        i = self.nBottom - 1 - int(np.floor(x[0] / self.lenIntervalsBottom))
        pt2, pt1 = self.ptsBottom[i - 1:i + 1] if i > 0 else self.ptsBottom[:2]
        x1, y1 = pt1
        x2, y2 = pt2

        if x[1] + 1e-6 < (y2 - y1) / (x2 - x1) * (x[0] - x1) + y1:
            return False

        return True


def generateCurvedRegions(n, y_nw, y_ne, amp_n, y_sw, y_se, amp_s):
    # Upper region
    ptsLeft = [(0.0, y_nw), (0.0, 1.0)]
    ptsTop = [(0.0, 1.0), (1.0, 1.0)]
    ptsRight = [(1.0, 1.0), (1.0, y_ne)]

    xs = np.flipud(np.arange(0.0, 1.0 + 1e-5, 1.0 / (n + 1)))  # Reverse order!
    ys = [-4 * amp_n * x * x + (y_ne - y_nw + 4 * amp_n) * x + y_nw
          for x in xs]  # Quadratic function connecting (0.0, y_nw) and (1.0, y_ne) with amplitude amp_n
    ptsBottom = [pt for pt in zip(xs, ys)]
    upper = CurvedRegion(ptsLeft, ptsTop, ptsRight, ptsBottom)

    # Middle region
    ptsLeft = [(0.0, y_sw), (0.0, y_nw)]
    ptsTop = ptsBottom[::-1]
    ptsRight = [(1.0, y_ne), (1.0, y_se)]

    ys = [-4 * amp_s * x * x + (y_se - y_sw + 4 * amp_s) * x + y_sw
          for x in xs]
    ptsBottom = [pt for pt in zip(xs, ys)]
    middle = CurvedRegion(ptsLeft, ptsTop, ptsRight, ptsBottom)

    # Lower region
    ptsLeft = [(0.0, 0.0), (0.0, y_sw)]
    ptsTop = ptsBottom[::-1]
    ptsRight = [(1.0, y_se), (1.0, 0.0)]
    ptsBottom = [(1.0, 0.0), (0.0, 0.0)]
    lower = CurvedRegion(ptsLeft, ptsTop, ptsRight, ptsBottom)

    return (upper, middle, lower)


def _generateCurvelyLayeredMesh(meshname, upper, middle, lower):
    tmpGeoFile = meshname + ".geo"

    regions = [upper, middle, lower]
    s = ""
    id_start_pts = 1
    id_start_lines = 1

    idsLinesBottom = []
    ptIdNw = -1
    ptIdNe = -1
    ptIdSw = -1
    ptIdSe = -1

    for i_region in range(len(regions)):
        region = regions[i_region]

        ptIdNw = ptIdSw

        # Left points
        pts = region.ptsLeft[:-1] if len(idsLinesBottom) > 0 \
            else region.ptsLeft
        ptIds = list(range(id_start_pts, id_start_pts + len(pts)))
        id_start_pts = id_start_pts + len(pts)

        s += "\n".join(["Point(%d) = {%f, %f, 0, 1};" % (ptId, pt[0], pt[1])
                        for (pt, ptId) in zip(pts, ptIds)])
        s += "\n"

        if len(idsLinesBottom) > 0:
            ptIds = ptIds + [ptIdNw]

        lineIds = list(range(id_start_lines, id_start_lines +
                        len(pts) - (0 if len(idsLinesBottom) > 0 else 1)))
        id_start_lines = id_start_lines + len(lineIds)

        s += "\n".join(["Line(%d) = {%d, %d};" % (lineId, ptId1, ptId2)
                        for (lineId, (ptId1, ptId2)) in zip(lineIds, list(zip(ptIds[:-1], ptIds[1:])))])
        s += "\n"

        idsLinesLeft = lineIds
        ptIdSw = ptIds[0]
        ptIdNw = ptIds[-1]

        # Top points
        if len(idsLinesBottom) > 0:
            idsLinesTop = [-i for i in idsLinesBottom[::-1]]
        else:
            pts = region.ptsTop[1:]
            ptIds = list(range(id_start_pts, id_start_pts + len(pts)))
            id_start_pts = id_start_pts + len(pts)

            s += "\n".join(["Point(%d) = {%f, %f, 0, 1};" % (ptId, pt[0], pt[1])
                            for (pt, ptId) in zip(pts, ptIds)])
            s += "\n"

            ptIds = [ptIdNw] + ptIds

            lineIds = list(range(id_start_lines, id_start_lines + len(pts)))
            id_start_lines = id_start_lines + len(lineIds)

            s += "\n".join(["Line(%d) = {%d, %d};" % (lineId, ptId1, ptId2)
                            for (lineId, (ptId1, ptId2)) in zip(lineIds, list(zip(ptIds[:-1], ptIds[1:])))])
            s += "\n"

            idsLinesTop = lineIds
            ptIdNe = ptIds[-1]

        # Right points
        pts = region.ptsRight[1:]
        ptIds = list(range(id_start_pts, id_start_pts + len(pts)))
        id_start_pts = id_start_pts + len(pts)

        s += "\n".join(["Point(%d) = {%f, %f, 0, 1};" % (ptId, pt[0], pt[1])
                        for (pt, ptId) in zip(pts, ptIds)])
        s += "\n"

        ptIds = [ptIdSe if len(idsLinesBottom) > 0 else ptIdNe] + ptIds

        lineIds = list(range(id_start_lines, id_start_lines + len(pts)))
        id_start_lines = id_start_lines + len(lineIds)

        s += "\n".join(["Line(%d) = {%d, %d};" % (lineId, ptId1, ptId2)
                        for (lineId, (ptId1, ptId2)) in zip(lineIds, list(zip(ptIds[:-1], ptIds[1:])))])
        s += "\n"

        idsLinesRight = lineIds
        ptIdSe = ptIds[-1]

        # Bottom points
        pts = region.ptsBottom[1:-1]
        ptIds = list(range(id_start_pts, id_start_pts + len(pts)))
        id_start_pts = id_start_pts + len(pts)

        s += "\n".join(["Point(%d) = {%f, %f, 0, 1};" % (ptId, pt[0], pt[1])
                        for (pt, ptId) in zip(pts, ptIds)])
        s += "\n"

        ptIds = [ptIdSe] + ptIds + [ptIdSw]

        # - One more than above since we want to close the loop
        lineIds = list(range(id_start_lines, id_start_lines + len(pts) + 1))
        id_start_lines = id_start_lines + len(lineIds)

        s += "\n".join(["Line(%d) = {%d, %d};" % (lineId, ptId1, ptId2)
                        for (lineId, (ptId1, ptId2)) in zip(lineIds, list(zip(ptIds[:-1], ptIds[1:])))])
        s += "\n"

        idsLinesBottom = lineIds

        lineIds = idsLinesLeft + idsLinesTop + idsLinesRight + idsLinesBottom
        s += "\n".join(["Line Loop(%d) = {%s}; " % (i_region + 1,
                                                    ", ".join([repr(i) for i in lineIds]))]
                       + ["Plane Surface(%d) = {%d};" % (i_region + 1, i_region + 1)])
        s += "\n"

    with open(tmpGeoFile, 'w+') as f:
        f.write(s)

    os.system("gmsh '" + tmpGeoFile + "' -2 -clscale " +
              str(settings.meshScaleFactor) + " > /dev/null")
    mesh2xml(meshname)


def diracApprox(s):
    return 'sqrt(%i / pi) * exp(-%i * ((%s) * (%s)))' % (settings.scaleFactorDiracApprox, settings.scaleFactorDiracApprox, s, s)


class ModelFlowUpToBottomWithCurvelyLayeredMediaAndWell:
    def __init__(self, id, parameters, meshname=None):
        self.id = id
        self.parameters = parameters
        self.meshname = meshname

        self.numParameters = len(parameters)

        self.y_nw = 0.1 * parameters[0] + 0.7  # nw
        self.y_ne = 0.1 * parameters[1] + 0.8  # ne
        self.amp_n = 0.1 * parameters[2]  # north amplitude

        self.y_sw = 0.1 * parameters[3] + 0.3  # sw
        self.y_se = 0.1 * parameters[4] + 0.2  # se
        self.amp_s = 0.1 * parameters[5]  # south amplitude

        # self.y_well = parameters[6] * 0.5 * (settings.wellYUpperBound - settings.wellYLowerBound) + \
        #     0.5 * (settings.wellYUpperBound + settings.wellYLowerBound)
        self.magn_well = parameters[6] * 0.5 * (settings.sourceUpperBound - settings.sourceLowerBound) + 0.5 * (
            settings.sourceUpperBound + settings.sourceLowerBound)

        self.start_klTerms = 7

        klModesUpper = parameters[self.start_klTerms:self.start_klTerms + settings.lenKLUpper]
        klModesMiddle = parameters[self.start_klTerms + settings.lenKLUpper:
                                   self.start_klTerms + settings.lenKLUpper + settings.lenKLMiddle]
        klModesLower = parameters[self.start_klTerms + settings.lenKLUpper + settings.lenKLMiddle:
                                  self.start_klTerms + settings.lenKLUpper + settings.lenKLMiddle + settings.lenKLLower]

        # Define parabolas separating layers (P1 upper, P2 lower parabola)
        self.P1 = '-4*A1*x[0]*x[0] + (y_ne - y_nw + 4*A1)*x[0] + y_nw'
        self.P2 = '-4*A2*x[0]*x[0] + (y_se - y_sw + 4*A2)*x[0] + y_sw'

        upper, middle, lower = generateCurvedRegions(1.0 / settings.meshScaleFactor - 1,
                                                     self.y_nw, self.y_ne, self.amp_n,
                                                     self.y_sw, self.y_se, self.amp_s)

        if mesh is None:
            meshname = "tmp/tmpMesh" + repr(id)
            _generateCurvelyLayeredMesh(meshname, upper, middle, lower)

        self.mesh = Mesh(meshname + ".xml")
        self.mesh.init()

        self.V = FunctionSpace(self.mesh, "CG", 1)
        self.V_a = FunctionSpace(self.mesh, "CG", 1)

        # Region ----------------------------
        self.domains = MeshFunction("size_t", self.mesh, self.mesh.topology().dim())
        self.domains.set_all(1)
        upper.mark(self.domains, 1)
        middle.mark(self.domains, 2)
        lower.mark(self.domains, 3)
        # -----------------------------------

        # Conductivities --------------------
        # - Upper layer
        self.eigVals_upper = np.loadtxt("kl/kl_eigvals_lined_upper.txt")
        self.eigVecs_upper = [None] * len(klModesUpper)

        mesh_layer = Mesh('meshes/lined_upper.xml')
        V_layer = FunctionSpace(mesh_layer, 'CG', 1)

        log_cond_upper = Function(self.V)
        log_cond_upper.vector()[:] = 0
        for i_kl in range(len(klModesUpper)):
            eigFunc = Function(V_layer)
            eigFunc.set_allow_extrapolation(True)
            f = File('kl/kl_eigfunc_lined_upper_%i.xml' % i_kl)
            f >> eigFunc
            self.eigVecs_upper[i_kl] = interpolate(eigFunc, self.V)
            log_cond_upper.vector().axpy(
                np.sqrt(self.eigVals_upper[i_kl]) * klModesUpper[i_kl], self.eigVecs_upper[i_kl].vector())
        self.condUpper = exp(Constant(settings.meanCondUpper) + log_cond_upper) * IndicatorFunction(upper, element=self.V.ufl_element())

        # - Middle layer
        self.eigVals_middle = np.loadtxt("kl/kl_eigvals_lined_middle.txt")
        self.eigVecs_middle = [None] * len(klModesMiddle)

        mesh_layer = Mesh('meshes/lined_middle.xml')
        V_layer = FunctionSpace(mesh_layer, 'CG', 1)

        log_cond_middle = Function(self.V)
        log_cond_middle.vector()[:] = 0
        for i_kl in range(len(klModesMiddle)):
            eigFunc = Function(V_layer)
            eigFunc.set_allow_extrapolation(True)
            f = File('kl/kl_eigfunc_lined_middle_%i.xml' % i_kl)
            f >> eigFunc
            self.eigVecs_middle[i_kl] = interpolate(eigFunc, self.V)
            log_cond_middle.vector().axpy(
                np.sqrt(self.eigVals_middle[i_kl]) * klModesMiddle[i_kl], self.eigVecs_middle[i_kl].vector())
        self.condMiddle = exp(Constant(settings.meanCondMiddle) + log_cond_middle) * IndicatorFunction(middle, element=self.V.ufl_element())

        # - Lower layer
        self.eigVals_lower = np.loadtxt("kl/kl_eigvals_lined_lower.txt")
        self.eigVecs_lower = [None] * len(klModesLower)

        mesh_layer = Mesh('meshes/lined_lower.xml')
        V_layer = FunctionSpace(mesh_layer, 'CG', 1)

        log_cond_lower = Function(self.V)
        log_cond_lower.vector()[:] = 0
        for i_kl in range(len(klModesLower)):
            eigFunc = Function(V_layer)
            eigFunc.set_allow_extrapolation(True)
            f = File('kl/kl_eigfunc_lined_lower_%i.xml' % i_kl)
            f >> eigFunc
            self.eigVecs_lower[i_kl] = interpolate(eigFunc, self.V)
            log_cond_lower.vector().axpy(
                np.sqrt(self.eigVals_lower[i_kl]) * klModesLower[i_kl], self.eigVecs_lower[i_kl].vector())
        self.condLower = exp(Constant(settings.meanCondLower) + log_cond_lower) * IndicatorFunction(lower, element=self.V.ufl_element())

        self.condField = self.condUpper + self.condMiddle + self.condLower
        # -----------------------------------

        # Boundaries ------------------------
        left = Left()
        right = Right()
        top = Top()
        bottom = Bottom()

        self.boundaries = MeshFunction("size_t", self.mesh, self.mesh.topology().dim() - 1)
        self.boundaries.set_all(0)
        left.mark(self.boundaries, 1)
        top.mark(self.boundaries, 2)
        right.mark(self.boundaries, 3)
        bottom.mark(self.boundaries, 4)
        # -----------------------------------

        # Measures --------------------------
        self.dx = Measure('dx', domain=self.mesh, subdomain_data=self.domains)
        self.ds = Measure('ds', domain=self.mesh,
                          subdomain_data=self.boundaries)
        # -----------------------------------

    def getCondField(self):
        return self.condField

    def qoi(self):
        self.solveForwardProblem()
        return np.array([assemble(inner(env.get_qoi_riesz_repr(i), self.u) * self.dx) for i in range(env.n_data)])

    def solveForwardProblem(self):
        if hasattr(self, 'u'):
            return self.u

        # Weak formulation + solving --------
        u = TrialFunction(self.V)
        v = TestFunction(self.V)

        f = Expression('x[1] < y0 ? 0 : M*' + diracApprox('x[0]-x0'),
                       M=self.magn_well,
                       x0=settings.x_source,
                       y0=settings.y_source,
                       element=self.V.ufl_element())

        bcs = [DirichletBC(self.V, 0.0, self.boundaries, 4)]

        F = inner(self.condUpper * grad(u), grad(v)) * self.dx(1) \
            + inner(self.condMiddle * grad(u), grad(v)) * self.dx(2) \
            + inner(self.condLower * grad(u), grad(v)) * self.dx(3) \
            - f * v * self.dx
        a, L = lhs(F), rhs(F)

        self.u = Function(self.V)
        solve(a == L, self.u, bcs)
        # -----------------------------------

        return self.u

    def _solveAdjointProblem(self, qoi_rsz_repr):
        # Weak formulation + solving --------
        u_a = TrialFunction(self.V_a)
        v_a = TestFunction(self.V_a)

        f_a = qoi_rsz_repr

        bcs_a = [DirichletBC(self.V_a, 0.0, self.boundaries, 4)]

        a_a = inner(self.condUpper * grad(u_a), grad(v_a)) * self.dx(1) \
            + inner(self.condMiddle * grad(u_a), grad(v_a)) * self.dx(2) \
            + inner(self.condLower * grad(u_a), grad(v_a)) * self.dx(3)
        L_a = f_a * v_a * self.dx

        u_a = Function(self.V_a)
        solve(a_a == L_a, u_a, bcs_a)

        return u_a
        # -----------------------------------

    def _grad(self, qoi_rsz_repr):
        self.solveForwardProblem()
        u_a = self._solveAdjointProblem(qoi_rsz_repr)

        dF = [None] * self.numParameters
        derivs = np.empty(self.numParameters)

        # Note: Apply the chain rule since parameter inputs are normalized and had to be scaled in the constructor.

        tmpExpr = (self.condUpper - self.condMiddle) * \
            Expression('0.1 * ' + diracApprox('x[1]-(' + self.P1 + ')'),
                       A1=self.amp_n,
                       y_nw=self.y_nw,
                       y_ne=self.y_ne,
                       element=self.V.ufl_element())

        dF[0] = tmpExpr * \
            Expression('x[0]-1', element=self.V.ufl_element())
        dF[1] = tmpExpr * Expression('-x[0]', element=self.V.ufl_element())
        dF[2] = tmpExpr * \
            Expression('4*x[0]*(x[0]-1)', element=self.V.ufl_element())

        tmpExpr = (self.condLower - self.condMiddle) * \
            Expression('0.1 * ' + diracApprox(
                '(' + self.P2 + ')-x[1]'),
                A2=self.amp_s,
                y_sw=self.y_sw,
                y_se=self.y_se,
                element=self.V.ufl_element())

        dF[3] = tmpExpr * \
            Expression('1-x[0]', element=self.V.ufl_element())
        dF[4] = tmpExpr * Expression('x[0]', element=self.V.ufl_element())
        dF[5] = tmpExpr * \
            Expression('4*x[0]*(1-x[0])', element=self.V.ufl_element())

        # Well parameter
        dF[6] = Expression('x[1] < y0 ? 0 : 0.5 * (source_upper_bound - source_lower_bound) * ' + diracApprox('x[0]-x0'),
                           source_upper_bound=settings.sourceUpperBound,
                           source_lower_bound=settings.sourceLowerBound,
                           y0=settings.y_source,
                           x0=settings.x_source,
                           element=self.V.ufl_element())

        # Upper KL modes
        dF[self.start_klTerms:self.start_klTerms + len(self.eigVals_upper)] = \
            [self.condUpper * np.sqrt(self.eigVals_upper[i]) * self.eigVecs_upper[i] #*
                # Expression('x[1] < ' + self.P1 + ' ? 0 : 1',
                #            A1=self.amp_n,
                #            y_nw=self.y_nw,
                #            y_ne=self.y_ne,
                #            element=self.V.ufl_element())
             for i in range(len(self.eigVals_upper))]

        # Middle KL modes
        dF[self.start_klTerms + len(self.eigVals_upper):self.start_klTerms + len(self.eigVals_upper) + len(self.eigVals_middle)] = \
            [self.condMiddle * np.sqrt(self.eigVals_middle[i]) * self.eigVecs_middle[i] #*
            #  Expression('x[1] >= ' + self.P1 + ' || x[1] < ' + self.P2 + ' ? 0 : 1',
            #             A1=self.amp_n, y_nw=self.y_nw, y_ne=self.y_ne,
            #             A2=self.amp_s, y_sw=self.y_sw, y_se=self.y_se,
            #             element=self.V.ufl_element())
             for i in range(len(self.eigVals_middle))]

        # Lower KL modes
        dF[self.start_klTerms + len(self.eigVals_upper) + len(self.eigVals_middle):self.start_klTerms + len(self.eigVals_upper) + len(self.eigVals_middle) + len(self.eigVals_lower)] = \
            [self.condLower * np.sqrt(self.eigVals_lower[i]) * self.eigVecs_lower[i] #*
            #  Expression('x[1] >= ' + self.P2 + ' ? 0 : 1',
            #             A2=self.amp_s,
            #             y_sw=self.y_sw,
            #             y_se=self.y_se,
            #             element=self.V.ufl_element())
             for i in range(len(self.eigVals_lower))]

        derivs[:6] = [assemble(inner(-dF[i] * grad(self.u), grad(u_a)) * self.dx(1)
                               + inner(-dF[i] * grad(self.u), grad(u_a)) * self.dx(2)
                               + inner(-dF[i] * grad(self.u), grad(u_a)) * self.dx(3))
                      for i in range(6)]

        derivs[6] = assemble(inner(dF[6] * grad(self.u), grad(u_a)) * self.dx)

        derivs[self.start_klTerms:self.start_klTerms + len(self.eigVals_upper)] = \
            [assemble(inner(-dF[i] * grad(self.u), grad(u_a)) * self.dx(1))
                for i in range(self.start_klTerms, self.start_klTerms + len(self.eigVals_upper))]
        derivs[self.start_klTerms + len(self.eigVals_upper):self.start_klTerms + len(self.eigVals_upper) + len(self.eigVals_middle)] = \
            [assemble(inner(-dF[i] * grad(self.u), grad(u_a)) * self.dx(2))
                for i in range(self.start_klTerms + len(self.eigVals_upper), self.start_klTerms + len(self.eigVals_upper) + len(self.eigVals_middle))]
        derivs[self.start_klTerms + len(self.eigVals_upper) + len(self.eigVals_middle):self.start_klTerms + len(self.eigVals_upper) + len(self.eigVals_middle) + len(self.eigVals_lower)] = \
            [assemble(inner(-dF[i] * grad(self.u), grad(u_a)) * self.dx(3))
                for i in range(self.start_klTerms + len(self.eigVals_upper) + len(self.eigVals_middle), self.start_klTerms + len(self.eigVals_upper) + len(self.eigVals_middle) + len(self.eigVals_lower))]

        return derivs
    
    def jacobian(self):
        if hasattr(self, 'jac'):
            return self.jac
        
        self.jac = np.empty((len(env.eval_pts), env.n))

        for i in range(len(env.eval_pts)):
            self.jac[i,:] = self._grad(env.get_qoi_riesz_repr(i))
        
        return self.jac
    
    def jacobian_cfd(self):
        from uq_tools.gradients import cfd

        def f(x):
            pb = ModelFlowUpToBottomWithCurvelyLayeredMediaAndWell('%d_%d' % (self.id, hash(str(x))), x, self.meshname)
            return pb.qoi()

        return cfd.approximateGradientAt(self.parameters, f, h=1e-7)



class ModelFlowUpToBottomWithFixedCurvelyLayeredMediaAndWell(ModelFlowUpToBottomWithCurvelyLayeredMediaAndWell):
    def __init__(self, id, parameters):
        parameters[:7] = 0

        ModelFlowUpToBottomWithCurvelyLayeredMediaAndWell.__init__(
            self, id, parameters, meshname='meshes/lined_all')

    def jacobian(self):
        jac = ModelFlowUpToBottomWithCurvelyLayeredMediaAndWell.jacobian(self)

        return jac[:,7:]
    
    def jacobian_cfd(self):
        jac = ModelFlowUpToBottomWithCurvelyLayeredMediaAndWell.jacobian_cfd(self)

        return jac[:,7:]


class ModelFlowTopToBottomWithWell(ModelBase):
    def __init__(self, id, parameters):
        unifParameters = 2 * toUniform(parameters) - 1
        ModelBase.__init__(self, id, unifParameters[:-2])

        self.x0_well = 0.5
        self.y0_well = (unifParameters[-1] + 1) * 0.5 * (
            settings.wellYUpperBound - settings.wellYLowerBound) + settings.wellYLowerBound
        self.magn_well = (unifParameters[-2] + 1) * 0.5 * (settings.sourceUpperBound -
                                                           settings.sourceLowerBound) + settings.sourceLowerBound

        self.numParameters = len(parameters)
        self.gradToUniform = 2 * np.diag([norm.pdf(x) for x in parameters])

    def solveForwardProblem(self):
        if hasattr(self, "u"):
            return self.u

        # Setup problem in DOLFIN and solve it
        g_B = Constant(0.0)

        # Weak formulation + solving --------
        u = TrialFunction(self.V)
        v = TestFunction(self.V)

        f = Expression("x[1] < y0 ? 0 : M*sqrt(factor/pi)*exp(-factor*((x[0]-x0)*(x[0]-x0)))",
                       M=self.magn_well,
                       x0=self.x0_well,
                       y0=self.y0_well,
                       factor=400,
                       degree=2)

        bcs = [DirichletBC(self.V, g_B, self.boundaries, 4)]

        F = inner(self.condUpper * grad(u), grad(v)) * self.dx(1) + inner(self.condMiddle *
                                                                          grad(u), grad(v)) * self.dx(2) + inner(self.condLower * grad(u), grad(v)) * self.dx(3) - f * v * self.dx
        a, L = lhs(F), rhs(F)

        self.u = Function(self.V)
        solve(a == L, self.u, bcs)
        # -----------------------------------

        return self.u

    def _solveAdjointProblem(self, qoi_rsz_repr):
        # Weak formulation + solving --------
        u_a = TrialFunction(self.V_a)
        v_a = TestFunction(self.V_a)
        f_a = qoi_rsz_repr
        g_a = Constant(0.0)
        a_a = inner(self.condUpper * grad(u_a), grad(v_a)) * self.dx(1) + inner(self.condMiddle *
                                                                                grad(u_a), grad(v_a)) * self.dx(2) + inner(self.condLower * grad(u_a), grad(v_a)) * self.dx(3)
        L_a = f_a * v_a * self.dx + g_a * v_a * self.ds

        bcs_a = [DirichletBC(self.V_a, 0.0, self.boundaries, 4)]

        u_a = Function(self.V_a)
        solve(a_a == L_a, u_a, bcs_a)

        return u_a
        # -----------------------------------

    def getGradient(self, qoi_rsz_repr):
        self.solveForwardProblem()
        u_a = self._solveAdjointProblem(qoi_rsz_repr)

        dconds = [None] * self.numParameters

        diracLine = "(1/intervalScaleFactor)*(1/8.0)*factor/pi*exp(-factor * (x[0] - (x[1] - b1) / (b2 - b1))*(x[0] - (x[1] - b1) / (b2 - b1)))"

        # Derivatives w.r.t. to boundary points y
        dconds[0] = Expression(
            "(K1-K2)*(x[0]-1)*" + diracLine,
            intervalScaleFactor=settings.intervalScaleFactor,
            K1=self.condUpper,
            K2=self.condMiddle,
            b1=self.y1,
            b2=self.y2,
            factor=400,
            degree=2)
        dconds[1] = Expression(
            "(K1-K2)*(-x[0])*" + diracLine,
            intervalScaleFactor=settings.intervalScaleFactor,
            K1=self.condUpper,
            K2=self.condMiddle,
            b1=self.y1,
            b2=self.y2,
            factor=400,
            degree=2)
        dconds[2] = Expression(
            "(K3-K2)*(x[0])*" + diracLine,
            intervalScaleFactor=settings.intervalScaleFactor,
            K2=self.condMiddle,
            K3=self.condLower,
            b1=self.y4,
            b2=self.y3,
            factor=400,
            degree=2)
        dconds[3] = Expression(
            "(K3-K2)*(1-x[0])*" + diracLine,
            intervalScaleFactor=settings.intervalScaleFactor,
            K2=self.condMiddle,
            K3=self.condLower,
            b1=self.y4,
            b2=self.y3,
            factor=400,
            degree=2)
        # Derivatives w.r.t. conductivities
        dconds[4] = Expression(
            "x[1] >= (b2 - b1)*x[0] - b1 ? (1/intervalScaleFactor)*0.05*meanCondUpper : 0",
            intervalScaleFactor=settings.intervalScaleFactor,
            meanCondUpper=settings.meanCondUpper,
            b1=self.y1,
            b2=self.y2,
            degree=2)
        dconds[5] = Expression(
            "x[1] >= (y2 - y1)*x[0] - y1 ? 0 : (x[1] <= (y3 - y4)*x[0] + y4 ? 0 : (1/intervalScaleFactor)*0.05*meanCondMiddle)",
            intervalScaleFactor=settings.intervalScaleFactor,
            meanCondMiddle=settings.meanCondMiddle,
            y1=self.y1,
            y2=self.y2,
            y3=self.y3,
            y4=self.y4,
            degree=2)
        dconds[6] = Expression(
            "x[1] <= (b2 - b1)*x[0] + b1 ? (1/intervalScaleFactor)*0.05*meanCondLower : 0",
            intervalScaleFactor=settings.intervalScaleFactor,
            meanCondLower=settings.meanCondLower,
            b1=self.y4,
            b2=self.y3,
            degree=2)

        derivs = [assemble(inner(-dconds[i] * grad(self.u), grad(u_a)) * self.dx)
                  for i in range(7)]

        # Derivative w.r.t. well's magnitude
        dconds[7] = Expression("x[1] < y0 ? 0 : (1/intervalScaleFactor)*0.5*(b-a)*factor/pi*exp(-factor*( (x[0]-x0)*(x[0]-x0) ))",
                               intervalScaleFactor=settings.intervalScaleFactor,
                               b=settings.sourceUpperBound,
                               a=settings.sourceLowerBound,
                               x0=self.x0_well,
                               y0=self.y0_well,
                               factor=400,
                               degree=2)
        derivs.append(assemble(inner(-dconds[7], u_a) * self.dx))
        # Derivative with respect to well's y-location
        dconds[8] = Expression("(1/intervalScaleFactor)*M*0.5*(b-a)*(factor/pi)*exp(-factor*( (x[0]-x0)*(x[0]-x0) + (x[1]-y0)*(x[1]-y0) ))",
                               intervalScaleFactor=settings.intervalScaleFactor,
                               M=self.magn_well,
                               b=settings.wellYUpperBound,
                               a=settings.wellYLowerBound,
                               x0=self.x0_well,
                               y0=self.y0_well,
                               factor=400,
                               degree=2)
        derivs.append(assemble(inner(-dconds[8], u_a) * self.dx))

        return np.dot(derivs, self.gradToUniform)


class ModelFlowLeftToRight(ModelBase):
    def __init__(self, id, parameters):
        ModelBase.__init__(self, id, parameters)

        self.numParameters = len(parameters)

    def solveForwardProblem(self):
        if hasattr(self, "u"):
            return self.u

        # Setup problem in DOLFIN and solve it
        g_L = Constant(0.0)
        g_R = Constant(1.0)

        # Weak formulation + solving --------
        u = TrialFunction(self.V)
        v = TestFunction(self.V)

        bcs = [
            DirichletBC(self.V, g_L, self.boundaries, 1),
            DirichletBC(self.V, g_R, self.boundaries, 3)
        ]

        F = inner(self.condUpper * grad(u), grad(v)) * self.dx(1) + inner(self.condMiddle *
                                                                          grad(u), grad(v)) * self.dx(2) + inner(self.condLower * grad(u), grad(v)) * self.dx(3)
        a, L = lhs(F), rhs(F)

        self.u = Function(self.V)
        solve(a == L, self.u, bcs)
        # -----------------------------------

        return self.u

    def _solveAdjointProblem(self, qoi_rsz_repr):
        # Weak formulation + solving --------
        u_a = TrialFunction(self.V_a)
        v_a = TestFunction(self.V_a)
        f_a = qoi_rsz_repr
        g_a = Constant(0.0)
        a_a = inner(self.condUpper * grad(u_a), grad(v_a)) * self.dx(1) + inner(self.condMiddle *
                                                                                grad(u_a), grad(v_a)) * self.dx(2) + inner(self.condLower * grad(u_a), grad(v_a)) * self.dx(3)
        L_a = f_a * v_a * self.dx + g_a * v_a * self.ds

        bcs_a = [
            DirichletBC(self.V_a, 0.0, self.boundaries, 1),
            DirichletBC(self.V_a, 0.0, self.boundaries, 3)
        ]

        u_a = Function(self.V_a)
        solve(a_a == L_a, u_a, bcs_a)

        return u_a
        # -----------------------------------

    def getGradient(self, qoi_rsz_repr):
        self.solveForwardProblem()
        u_a = self._solveAdjointProblem(qoi_rsz_repr)

        dconds = [None] * self.numParameters

        diracLine = "(1/8.0)*factor/pi*exp(-factor * (x[0] - (x[1] - b1) / (b2 - b1))*(x[0] - (x[1] - b1) / (b2 - b1)))"

        # Derivatives w.r.t. to boundary points y
        dconds[0] = Expression(
            "(K1-K2)*(x[0]-1)*" + diracLine,
            K1=self.condUpper,
            K2=self.condMiddle,
            b1=self.y1,
            b2=self.y2,
            factor=400,
            degree=2)
        dconds[1] = Expression(
            "(K1-K2)*(-x[0])*" + diracLine,
            K1=self.condUpper,
            K2=self.condMiddle,
            b1=self.y1,
            b2=self.y2,
            factor=400,
            degree=2)
        dconds[2] = Expression(
            "(K3-K2)*(x[0])*" + diracLine,
            K2=self.condMiddle,
            K3=self.condLower,
            b1=self.y4,
            b2=self.y3,
            factor=400,
            degree=2)
        dconds[3] = Expression(
            "(K3-K2)*(1-x[0])*" + diracLine,
            K2=self.condMiddle,
            K3=self.condLower,
            b1=self.y4,
            b2=self.y3,
            factor=400,
            degree=2)
        # Derivatives w.r.t. conductivities
        dconds[4] = Expression(
            "x[1] >= (b2 - b1)*x[0] - b1 ? 0.05*meanCondUpper : 0",
            meanCondUpper=settings.meanCondUpper,
            b1=self.y1,
            b2=self.y2,
            degree=2)
        dconds[5] = Expression(
            "x[1] >= (y2 - y1)*x[0] - y1 ? 0 : (x[1] <= (y3 - y4)*x[0] + y4 ? 0 : 0.05*meanCondMiddle)",
            meanCondMiddle=settings.meanCondMiddle,
            y1=self.y1,
            y2=self.y2,
            y3=self.y3,
            y4=self.y4,
            degree=2)
        dconds[6] = Expression(
            "x[1] <= (b2 - b1)*x[0] + b1 ? 0.05*meanCondLower : 0",
            meanCondLower=settings.meanCondLower,
            b1=self.y4,
            b2=self.y3,
            degree=2)

        derivs = [assemble(inner(-dconds[i] * grad(self.u), grad(u_a)) * self.dx)
                  for i in range(self.numParameters)]

        return derivs
