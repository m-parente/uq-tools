import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

sns.set()


uncerts = np.linspace(0, 100, num=100)
cs = np.linspace(0.001, 1.0, num=20)

plt.figure()

for c in cs:
    temps = 1/(c*uncerts**2 + 1)
    plt.plot(uncerts, temps, label=r'$c=%.3f$' % c)

plt.xlabel(r'Uncertainty $\xi$')
plt.ylabel(r'Temperature $\beta$')
plt.legend()
plt.tight_layout()

plt.show()
