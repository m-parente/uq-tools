import env
import model

import numpy as np


m = model.FlowModelFixedCurvelyLayeredMediaAndWell(-1, env.param_true)
qoi = m.qoi()

np.savetxt(env.data_file, qoi)
