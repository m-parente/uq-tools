import env
import model

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

sns.set()


# x_ = env.prior_sample()
x_ = env.param_true
m_ = model.FlowModelFixedCurvelyLayeredMediaAndWell(-1, x_)

print("Computing QoI at x_ ...")
G_ = m_.qoi()
print("Computing Jacobian at x_ ...")
jac_ = m_.jac()

ds = np.array([1e-2, 1e-3, 1e-4, 1e-5, 1e-6])
diffs = np.empty((env.n_data, len(ds)))

for i in range(len(ds)):
    dx = ds[i]
    print("dx: %f" % dx)

    x = x_ + dx * np.ones(env.n)

    m = model.FlowModelFixedCurvelyLayeredMediaAndWell(-2, x)
    G = m.qoi()

    diffs_G = abs(G - G_ - np.dot(jac_, x-x_)) / abs(G_)
    diffs[:, i] = diffs_G

plt.figure()
for i in range(env.n_data):
    plt.loglog(ds, diffs[i, :], 'o-', label=r'$e_%i$' % (i+1))
plt.loglog(ds, ds, label=r'$1^{st}$ order')
plt.loglog(ds, ds**2, label=r'$2^{nd}$ order')
plt.legend()
plt.xlabel('dx')
plt.ylabel('Error')
plt.tight_layout()

plt.show()
