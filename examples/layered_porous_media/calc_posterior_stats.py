import env

import numpy as np


dir_tmp_it = env.dir_tmp_it
k = env.k

x_samples = np.loadtxt('%s/x_post_samples.txt' % dir_tmp_it)

print("Mean: ", ["%.3e" % v for v in np.mean(x_samples, axis=0)])
print("Std: ", ["%.3e" % v for v in np.std(x_samples, axis=0)])

print("Corr.coeffs: ")
corrs = np.corrcoef(x_samples.T)
# for i in range(len(corrs)):
#     s = ''
#     for j in range(i + 1):
#         s += '%.4f' % np.round(corrs[i, j],
#                                decimals=4) + (', ' if j < i else '')
#     print(s)

corrs = np.triu(corrs, 1)

flat_corrs = np.ravel(corrs)
n_max = len(flat_corrs[flat_corrs >= 0.2])
num_corrs = len(flat_corrs)

flat_idx = np.argpartition(np.ravel(corrs), num_corrs-n_max)[-n_max:]
row_idx, col_idx = np.unravel_index(flat_idx, np.shape(corrs))

max_corrs = corrs[row_idx, col_idx]

max_corrs_order = np.argsort(max_corrs)[::-1]
row_idx = row_idx[max_corrs_order]
col_idx = col_idx[max_corrs_order]

max_corrs = np.sort(max_corrs)[::-1]

print("Max. correlations: " + ", ".join(["{:.2f}".format(corr) for corr in max_corrs]))
print("Parameters: " + ", ".join([repr(t) for t in zip(row_idx+1, col_idx+1)]))

n_max = len(flat_corrs[flat_corrs <= -0.2])
num_corrs = len(flat_corrs)

flat_idx = np.argpartition(np.ravel(corrs), n_max)[:n_max]
row_idx, col_idx = np.unravel_index(flat_idx, np.shape(corrs))

min_corrs = corrs[row_idx, col_idx]

min_corrs_order = np.argsort(min_corrs)
row_idx = row_idx[min_corrs_order]
col_idx = col_idx[min_corrs_order]

min_corrs = np.sort(min_corrs)

print("Min. correlations: " + ", ".join(["{:.2f}".format(corr) for corr in min_corrs]))
print("Parameters: " + ", ".join([repr(t) for t in zip(row_idx+1, col_idx+1)]))
