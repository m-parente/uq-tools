import env
from misc import Settings
from pde import *
from uq_tools.utils import BayesianInverseProblem
import uq_tools.mcmc as mcmc

import numpy as np
import numpy.random as rnd


settings = env.settings

W = np.loadtxt('asmEigVecs.txt.tmp')
n = 1  # Active subspace delimiter

W_split = np.split(W, [n], axis=1)
W1, W2 = W_split[0], W_split[1]

variance, data = env.data
invNoiseCovMat = (1 / variance) * np.eye(len(data))

absPdePb = AbstractHeatBVP()
bayInvPb = BayesianInverseProblem(data, absPdePb, invNoiseCovMat)

mcmc.as_mcmc(W1, W2,
             misfit=lambda y: bayInvPb.misfit(y),
             priorY=lambda y: 1 /
             float(2 * 1)**n if np.max(y) < 1 and np.min(y) > -1 else 0,
             prior_cond_sampler=lambda _: rnd.uniform(
                 -1, 1, bayInvPb.dimParameters - n),
             proposal_sampler=lambda c: rnd.normal(loc=c, scale=1.0),
             y1=[0.0] * n,
             steps=10
             )
