import env
import plotters
from uq_tools import asm
from uq_tools import mcmc
from uq_tools.approx import AdaptiveGPR

import numpy as np
import numpy.linalg as la
import numpy.random as rnd
import sys
import time as tm

import matplotlib.pyplot as plt
from sklearn.gaussian_process.kernels import RBF


k = env.k
W1, W2 = env.W1, env.W2

# Construct the response surface -----------------
xs = env.samples
misfits = env.misfits

poly_order = 4
resp_surface = asm.response_surface(xs, misfits, W1, poly_order=poly_order)

# kernel = 1.0 * RBF(length_scale=np.ones(k))
# noise_var = 2502.
# resp_surface = AdaptiveGPR(ys, misfits, kernel=kernel, noise_var=noise_var, n_restart_optimizer=20)
# ------------------------------------------------

# plotters.summaryPlot2D(W1[:, 0], W1[:, 1], xs, misfits, with_fit=True, poly_order=poly_order)
# plt.show()
# exit()

prior_y = env.prior_y

# y0 = ys[np.argmin(misfits)]
y0 = np.zeros(k)

y_samples = asm.as_mcmc_with_response_surface(
    lambda y: resp_surface(y)*0.05,
    W1, W2,
    proposal_sampler=lambda yk: rnd.normal(loc=yk, scale=0.425),
    priorY=prior_y,
    y1=y0,
    steps=25000,
    nPlotAccptRate=500)

burnin = 5000
y_samples = y_samples[burnin:]
np.savetxt('%s/y_post_samples_%id.txt' % (env.dir_tmp_it, k), y_samples)

plotters.plot_density_diag(y_samples)
plt.show()
