import env
import plotters
from uq_tools.approx import AdaptiveGPR

import matplotlib.pyplot as plt
import numpy as np
import pickle
from sklearn.gaussian_process.kernels import RBF


k = env.k
W1 = env.W1

xs = env.samples
ys = np.dot(xs, W1)

data = env.misfits

kernel = 1.0 * RBF(length_scale=np.ones(k))
# gpr = AdaptiveGPR(ys, data, kernel=kernel, noise_var=2502., n_restart_optimizer=20)  # 1D, It. 0
gpr = AdaptiveGPR(ys, data, kernel=kernel, noise_var=1639., n_restart_optimizer=20)  # 2D, It. 0
# gpr = AdaptiveGPR(ys, data, kernel=kernel, noise_var=160., n_restart_optimizer=20)  # 2D, It. 1

ys_plt = np.linspace(-2.5, 2.5, num=100)

if k == 1:
    plotters.plot_gpr_1d(gpr, ys_plt)
elif k == 2:
    plotters.plot_gpr_2d(gpr, ys_plt, ys_plt)

plt.show()
