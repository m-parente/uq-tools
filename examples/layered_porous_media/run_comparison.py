import env
from uq_tools import mcmc
from uq_tools.stats import metrics

import matplotlib.pyplot as plt
import numpy as np
import pickle


k = env.k

with open("tmp/regr_%id.bin" % k, 'rb') as regr_file:
    regr = pickle.dump(regr_file)

with open("tmp/gpr0_%id.bin" % k, 'rb') as gpr0_file:
    gpr0 = pickle.dump(gpr0_file)

with open("tmp/gpr_adapt_%id.bin" % k, 'rb') as gpr_file:
    gpr_adapt = pickle.dump(gpr_file)

prior_y = env.prior_y

samples_gpr_adapt = mh_mcmc_post(misfit=gpr_adapt,
                                 prior=prior_y,
                                 proposal_sampler=lambda yk: rnd.normal(loc=yk, scale=1.5),
                                 x0=xx,
                                 steps=20000,
                                 nPlotAccptRate=500)

kl_div_gpr_adapt_regr = metrics.kl_div_post(gpr_adapt, regr, samples_gpr_adapt)
print("KL(gpr||regr): %f" % kl_div_gpr_adapt_regr)

kl_div_gpr_adapt_gpr0 = metrics.kl_div_post(gpr_adapt, gpr0, samples_gpr_adapt)
print("KL(gpr||gpr0): %f" % kl_div_gpr_adapt_gpr0)
