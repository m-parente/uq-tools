import env
import model

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
import seaborn as sns

sns.set()
sns.set_context('paper', font_scale=1.5)
sns.set_style('whitegrid')
mpl.rcParams['lines.linewidth'] = 2


noise_level = env.noise_level
data = env.data
std_data = noise_level*data


def plot_forward_post(it, axes):
    dir_tmp_it = "tmp_it_%i" % it
    Gs_samples = np.loadtxt("%s/Gs_post_samples.txt" % dir_tmp_it)

    for i in range(len(data)):
        ax = axes[i % 3][int(i/3)]
        if i == 0:
            sns.distplot(Gs_samples[:, i], hist=False, rug=False,
                         label="It. %i" % it if it >= 0 else "Prior push fwd.", ax=ax)
        else:
            sns.distplot(Gs_samples[:, i], hist=False, rug=False, ax=ax)


fig, axes = plt.subplots(3, 2, figsize=(10, 12))

for i in range(len(data)):
    mean = data[i]
    std = std_data[i]
    std_norm_pdf = stats.norm(loc=mean, scale=std).pdf

    xs = np.linspace(mean-4*std, mean+4*std, num=100)

    ax = axes[i % 3][int(i/3)]
    ax.set_title(i+1)

    if i == 0:
        ax.plot(xs, std_norm_pdf(xs), label="Noise distribution")
        ax.legend()
    else:
        ax.plot(xs, std_norm_pdf(xs))

iters = np.arange(-1, 2)
for i in range(len(iters)):
    it = iters[i]
    plot_forward_post(it, axes)

plt.tight_layout()
plt.show()
