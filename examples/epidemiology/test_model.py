import env
import model

import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns

sns.set()


m = model.SEIRModel(env.calib_param_rki)
m.solve_ivp()
ts = m.t
qoi = m.qoi()

plt.figure()
plt.plot(env.t_obs, qoi[0, :], 'o')
plt.plot(ts, m.S, label='S')
plt.plot(ts, m.E, label='E')
plt.plot(ts, m.I, label='I')
plt.plot(ts, m.R, label='R')
plt.plot(ts, m.C, label='C')
# plt.plot(ts, m.S+m.E+m.I+m.R, label='N')
plt.legend()
plt.tight_layout()

plt.show()
