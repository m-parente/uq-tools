from plot_forward_quantiles import plot_forward_quantiles

import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns

plt.rcParams["patch.force_edgecolor"] = True
sns.set()
sns.set_style('whitegrid')
sns.set_context('paper', font_scale=1.5)
mpl.rcParams['lines.linewidth'] = 2

plot_forward_quantiles()

plt.show()
