from misc import Settings

from glob import glob
import natsort
import numpy as np
import numpy.random as rnd


settings = Settings('settings')
_s = settings

pert_perc = _s.pert_perc
dir_tmp = 'tmp_%iperc' % pert_perc

_lbs = (1-0.01*pert_perc) * np.array([_s.beta, _s.gamma_inv, _s.alpha_inv, _s.I0])
_ubs = (1+0.01*pert_perc) * np.array([_s.beta, _s.gamma_inv, _s.alpha_inv, _s.I0])

n_param = len(_lbs)
T = 60
t_obs = np.arange(0., T+1)


def from_calib_to_phys_params(calib_params):
    interim_params = 0.5*(_ubs-_lbs)*calib_params + 0.5*(_ubs+_lbs)

    phys_params = interim_params
    phys_params[:, 1] = 1./phys_params[:, 1]
    phys_params[:, 2] = 1./phys_params[:, 2]

    return phys_params


def from_phys_to_calib_params(phys_params):
    interim_params = phys_params

    interim_params[:, 1] = 1./interim_params[:, 1]
    interim_params[:, 2] = 1./interim_params[:, 2]

    return 2./(_ubs-_lbs)*interim_params - (_ubs+_lbs)/(_ubs-_lbs)


def prior_sample(N=1):
    return rnd.uniform(low=-1.0, high=1.0, size=(N, n_param)) if N > 1 else \
        rnd.uniform(low=-1.0, high=1.0, size=n_param)


phys_param_rki = np.array([_s.beta, 1./_s.gamma_inv, 1./_s.alpha_inv, _s.I0])
calib_param_rki = from_phys_to_calib_params(np.array([phys_param_rki]))[0]

# samples = np.loadtxt('%s/samples.txt' % dir_tmp)

# G_files = natsort.natsorted(glob('%s/G*.txt' % dir_tmp))
# Gs = np.array([np.loadtxt(G_file) for G_file in G_files])
