import env
from uq_tools import ode

import numpy as np
from scipy.integrate import solve_ivp


class SIRModel:
    def __init__(self, id, params):
        self.id = id
        self.params = params
        self.i0 = params[0]
        self.mu = params[1]  # birth rate
        self.nu = params[2]  # death rate
        self.beta = params[3]  # contact rate
        self.gamma = params[4]  # recovery rate

        self.T = 140
        self.dt = 0.01
        self.n_steps = int(self.T / self.dt)
        self.y0 = np.array([1., self.i0, 0.])

    def getQoI(self):
        if hasattr(self, 'qoi'):
            return self.qoi

        def f(y, t):
            s, i, r = y[0], y[1], y[2]
            return np.array([self.mu - self.beta * i * s - self.nu * s,
                             self.beta * i * s - (self.gamma + self.nu) * i,
                             self.gamma * i - self.nu * r])

        def jac(y, t):
            s, i, r = y[0], y[1], y[2]
            return np.array([[-self.beta * i - self.nu, -self.beta * s, 0.],
                             [self.beta * i, self.beta * s -
                                 (self.gamma + self.nu), 0.],
                             [0., self.gamma, -self.nu]])

        y = ode.bw_euler(f, jac, dt=self.dt, y0=self.y0, n_steps=self.n_steps)
        self.S, self.I, self.R = y[0, :], y[1, :], y[2, :]

        return 0.


class AbstractSIRModel:
    def instantiate(self, params, id=-1):
        return SIRModel(id, params)


class SEIRModel:
    def __init__(self, calib_params):
        self.phys_params = env.from_calib_to_phys_params(np.array([calib_params]))[0]

        self.beta = self.phys_params[0]  # transmission rate
        self.gamma = self.phys_params[1]  # recovery rate
        self.alpha = self.phys_params[2]  # latency rate
        self.I0 = self.phys_params[3]  # initial number of infectives

        self.T = env.T
        self.dt = 0.01
        self.t = np.arange(0., self.T+self.dt, step=self.dt)
        self.n_steps = int(self.T / self.dt)

        self.N = 80*10**6
        self.E0 = 0
        self.R0 = 0
        self.S0 = self.N - (self.E0 + self.I0 + self.R0)
        self.C0 = 0
        self.y0 = np.array([self.S0, self.E0, self.I0, self.R0, self.C0])

    def _t_to_step(self, t):
        fs = np.round(t/self.dt)
        if np.isscalar(fs):
            return int(fs)
        return np.array([int(f) for f in fs], dtype=int)

    def solve_ivp(self):
        if hasattr(self, 'y'):
            return self.y

        def f(t, y):
            S, E, I, R, C = y
            return np.array([-self.beta * I * S / self.N,
                             self.beta * I * S / self.N - self.alpha * E,
                             self.alpha * E - self.gamma * I,
                             self.gamma * I,
                             self.alpha * E])

        sol = solve_ivp(f, [0., self.T], self.y0, method='RK45',
                        dense_output=True, t_eval=self.t,
                        rtol=1e-6, atol=1e-6)
        self.y = sol.y

        self.S, self.E, self.I, self.R, self.C = \
            self.y[0, :], self.y[1, :], self.y[2, :], self.y[3, :], self.y[4, :]

        return self.y

    def qoi(self):
        if hasattr(self, '_qoi'):
            return self._qoi

        y = self.solve_ivp()
        self._qoi = y[:, self._t_to_step(env.t_obs)]

        return self._qoi
