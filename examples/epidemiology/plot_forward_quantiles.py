import env
import model

import matplotlib.pyplot as plt
import numpy as np
from scipy.stats.mstats import mquantiles
import seaborn as sns


dir_tmp = env.dir_tmp
Gs = env.Gs
t_obs = env.t_obs


def plot_forward_quantiles():
    Is = Gs[:, 2, :]
    qois = Is

    p = 0.95

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10, 5))
    fig.suptitle("%i%% perturbation" % env.pert_perc)

    median_path = np.median(qois, axis=0)
    mean_path = np.mean(qois, axis=0)
    quantiles = mquantiles(qois, prob=[(1-p)/2, (1+p)/2], alphap=0, betap=1, axis=0)
    ax1.plot(t_obs, mean_path, '--', color='C2', label="Mean")
    ax1.plot(t_obs, median_path, color='C1', label="Median")
    ax1.fill_between(t_obs, quantiles[0, :], quantiles[1, :],
                     color='C1', alpha=0.2, label="$[q_{0.025},q_{0.975}]$")
    ax1.set_xlim([0, env.T])
    ax1.set_ylim([1, 1.3*10**7])
    ax1.set_xlabel('Time $t$ [weeks]')
    ax1.set_ylabel('Number of infectious hosts')
    ax1.set_xticks(range(0, env.T+1, 20))
    ax1.ticklabel_format(style='sci', axis='y', scilimits=(6, 6))
    ax1.yaxis.major.formatter._useMathText = True
    ax1.legend()

    ts_I_max = np.argmax(Is, axis=1)
    t_I_max_quantiles = mquantiles(ts_I_max, prob=[(1-p)/2, (1+p)/2], alphap=0, betap=1)
    print(t_I_max_quantiles)
    sns.distplot(ts_I_max, bins=np.array(list(set(ts_I_max)))-0.5,
                 kde=False, norm_hist=True, ax=ax2)
    ax2.set_ylim([0, 0.35])
    ax2.set_xticks(range(15, 31, 5))
    ax2.set_xlabel(ax1.get_xlabel())
    ax2.set_ylabel('Distribution of infectious peaks')

    plt.tight_layout()


if __name__ == '__main__':
    plot_forward_quantiles()
    plt.show()
