import env
import model

import numpy as np
import sys


dir = env.dir_tmp


def run(id, sample):
    print("Sample %i" % id)

    m = model.SEIRModel(sample)
    G = m.qoi()

    np.savetxt('%s/G%d.txt' % (dir, id), G)


def usage():
    print("usage: id parameters")


if __name__ == "__main__":
    if len(sys.argv) == env.n_param + 2:
        run(int(sys.argv[1]), np.array(sys.argv[2:], dtype=float))
    else:
        usage()
