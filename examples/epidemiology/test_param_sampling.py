import env

import numpy as np


calib_params = env.prior_sample(N=10000)

assert(np.allclose(calib_params,
                   env.from_phys_to_calib_params(
                       env.from_calib_to_phys_params(calib_params))))

print("Test succeeded.")
