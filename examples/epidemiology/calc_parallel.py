import env

import multiprocessing as mp
import numpy as np
import numpy.random as rnd
import os
import sys


dir_tmp = env.dir_tmp
os.system('mkdir -p %s' % dir_tmp)

N = 1000
samples = env.prior_sample(N)
np.savetxt('%s/samples.txt' % dir_tmp, samples)

script_name = 'calc_template.py'

cmds = ''
for i in range(len(samples)):
    sample = samples[i]
    cmds += "%s %s %i %s\n" % (sys.executable, script_name, i, ' '.join(str(p) for p in sample))

launcherFile = '%s/asmLauncher_runs.txt' % dir_tmp

with open(launcherFile, 'w') as file:
    file.write(cmds)

os.environ["LAUNCHER_JOB_FILE"] = launcherFile
os.environ["LAUNCHER_DIR"] = os.environ["SW_DIR"] + "/launcher"
os.environ["LAUNCHER_PPN"] = repr(mp.cpu_count() - 1)

os.system("bash $LAUNCHER_DIR/paramrun")  # > /dev/null")
