import env

import numpy as np

import matplotlib.pyplot as plt

_settings = env.settings

_alpha = 1
# _alpha = settings.alpha


def plotQoI():
    data = np.loadtxt('data_triax25_orig.txt')

    every10th = list(range(1, len(data), 10))

    f, (ax1, ax2) = plt.subplots(2, sharex=True)
    ax1.plot(data[:, 0], data[:, 1], zorder=1)
    ax1.scatter(data[every10th, 0], data[every10th, 1], color='C1', alpha=_alpha, zorder=2)
    ax1.set_xlim(-0.005, 0.145)
    ax1.set_ylabel(r'Volumetric strain [$-$]')
    ax1.get_yaxis().set_label_coords(-0.12, 0.5)

    ax2.plot(data[:, 0], data[:, 2], zorder=1)
    ax2.scatter(data[every10th, 0], data[every10th, 2], color='C1', alpha=_alpha, zorder=2)
    ax2.set_xlabel(r'Axial strain [$-$]')
    ax2.set_ylabel('Shear stress [MPa]')
    ax2.get_yaxis().set_label_coords(-0.12, 0.5)

    plt.tight_layout()


if __name__ == '__main__':
    plotQoI()
    plt.show()
