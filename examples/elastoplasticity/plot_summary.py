import env

from uq_tools import asm

import numpy as np

from matplotlib import cm
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn import metrics

_settings = env.settings

_asDir = _settings.asDir
_alpha = 0.5
# _alpha = _settings.alpha


def plotSummary():
    W = np.loadtxt('%s/asm_eigVecs.txt' % _asDir)

    xs = env.xs
    Gs = env.Gs
    misfits = list(map(env.misfit, Gs))

    W1 = W[:, :2]
    w1 = W1[:, 0]
    w1Tx = np.dot(xs, w1)

    # 1D summary plot
    plt.figure()
    plt.scatter(w1Tx, misfits, color='C1', alpha=_alpha)
    plt.xlabel(r'${\tilde{\mathbf{w}}}_1^\top \mathbf{x}$')
    plt.ylabel('Data misfit')

    resp_surf = asm.response_surface(xs, misfits, w1, poly_order=2)

    idx = w1Tx.argsort()
    plot_xs = w1Tx[idx]
    plt.plot(plot_xs, resp_surf(plot_xs[:, np.newaxis]), color='C0', alpha=0.7)
    plt.xlim([-1.6, 1.6])

    plt.tight_layout()

    # 2D summary plot
    w2 = W1[:, 1]
    w2Tx = np.dot(xs, w2)
    plt.figure()

    plt.scatter(w1Tx, w2Tx, c=misfits, cmap='pink')
    plt.xlabel(r'${\tilde{\mathbf{w}}}_1^\top \mathbf{x}$')
    plt.ylabel(r'${\tilde{\mathbf{w}}}_2^\top \mathbf{x}$')
    plt.xlim([-1.6, 1.6])
    plt.ylim([-1.6, 1.6])
    plt.colorbar()

    plt.tight_layout()

    # 2D summary plot with fit
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    resp_surf = asm.response_surface(
        xs, misfits, np.transpose([w1, w2]), poly_order=2)

    X = np.arange(np.min(w1Tx), np.max(w1Tx), step=0.05)
    Y = np.arange(np.min(w2Tx), np.max(w2Tx), step=0.05)

    X, Y = np.meshgrid(X, Y)

    res = np.reshape(resp_surf(np.concatenate(
        [list(zip(x, y)) for x, y in zip(X, Y)])), np.shape(X))
    # ax.plot_surface(X, Y, res, cmap='pink',
    #                 linewidth=0, antialiased=False, alpha=0.3)

    # colors = np.abs(np.subtract(misfits, [resp_surf([[x, y]])[0]
    #                                       for x, y in zip(w1Tx, w2Tx)]))
    scatter = ax.scatter(w1Tx, w2Tx, misfits, c=misfits, cmap='pink')

    ax.set_zlim(np.min(misfits), np.max(misfits))
    ax.set_xlabel(r'$w_1^Tx$')
    ax.set_ylabel(r'$w_2^Tx$')
    # ax.set_zlabel('Data misfit')
    ax.set_zticks([])
    ax.grid(False)
    fig.colorbar(scatter)

    plt.tight_layout()

    for iDim in range(len(W)):
        resp_surf = asm.response_surface(
            xs, misfits, W[:, :iDim + 1], poly_order=2)
        print("%iD r2 score: %f" % (
            iDim + 1, metrics.r2_score(misfits, resp_surf(np.dot(xs, W[:, :iDim + 1])))))


if __name__ == '__main__':
    plotSummary()
    plt.show()
