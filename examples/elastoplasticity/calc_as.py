import env
import pde

import plotters
from uq_tools import asm, utils

import numpy as np
import numpy.linalg as la


settings = env.settings

asDir = settings.asDir

nBoot = settings.numberBootstrapIterations

absPb = pde.AbstractElastoplasticityPDEProblem()
bayInvPb = utils.BayesianInverseProblem(env.data, absPb, env.invNoiseCovMat)

result = asm.computeActiveSubspaceFromSamples(
    env.Gs, env.jacGs, bayInvPb, nBoot, asDir=asDir)

eigVals, eigVecs, minEigVals, maxEigVals, minSubspaceErrors, maxSubspaceErrors, meanSubspaceErrors = result

numPlotEigVals = 8
plotters.plotActiveSubspace(eigVals, eigVecs, numPlotEigVals, minEigVals, maxEigVals,
                            minSubspaceErrors, maxSubspaceErrors, meanSubspaceErrors, asDir=asDir)
