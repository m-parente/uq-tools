import env

import numpy as np


settings = env.settings

asDir = settings.asDir

eigVecs = np.loadtxt('%s/asm_eigVecs.txt' % asDir)

w1 = eigVecs[:, 0]

for i in range(0, np.shape(eigVecs)[1]):
    wi = eigVecs[:, i]

    print(np.dot(w1, wi))
