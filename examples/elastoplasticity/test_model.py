import env
import pde

import numpy as np
import sys

import matplotlib.pyplot as plt


settings = env.settings

argc = len(sys.argv)

if (argc <= 1):
    c = 2e6
    alpha_res_min = 0.5
    alpha_res_max = 0.8
    lambda_dot_max = 0.0018
    m = 1.
    beta_max = 0.4
    lambda_max = 0.01
    n = 0.75

    # c = 1.99558e+06
    # alpha_res_min = 0.517959
    # alpha_res_max = 0.825033
    # lambda_dot_max = 0.00176
    # m = 0.98306
    # beta_max = 0.383451
    # lambda_max = 0.010481
    # n = 0.733838

    x = np.empty(8)
    x[0] = 2 * (c - settings.c_LowerBound) / \
        (settings.c_UpperBound - settings.c_LowerBound) - 1
    x[1] = 2 * (alpha_res_min - settings.alpha_res_min_LowerBound) / \
        (settings.alpha_res_min_UpperBound - settings.alpha_res_min_LowerBound) - 1
    x[2] = 2 * (alpha_res_max - settings.alpha_res_diff_LowerBound - alpha_res_min) / \
        (settings.alpha_res_diff_UpperBound -
         settings.alpha_res_diff_LowerBound) - 1
    x[3] = 2 * (lambda_dot_max - settings.lambda_dot_max_LowerBound) / \
        (settings.lambda_dot_max_UpperBound -
         settings.lambda_dot_max_LowerBound) - 1
    x[4] = 2 * (m - settings.m_LowerBound) / \
        (settings.m_UpperBound - settings.m_LowerBound) - 1
    x[5] = 2 * (beta_max - settings.beta_max_LowerBound) / \
        (settings.beta_max_UpperBound - settings.beta_max_LowerBound) - 1
    x[6] = 2 * (lambda_max - settings.lambda_max_LowerBound) / \
        (settings.lambda_max_UpperBound - settings.lambda_max_LowerBound) - 1
    x[7] = 2 * (n - settings.n_LowerBound) / \
        (settings.n_UpperBound - settings.n_LowerBound) - 1

    pb = pde.ElastoplasticityPDEProblem(-1, x)

    G = pb.getQoI()

    np.savetxt('tmp/_G-1.txt', G)
elif argc == 2 and sys.argv[1] != 'f':
    print("Do not know what to do.")
    exit()

G = np.loadtxt('tmp/_G-1.txt')

data_orig = np.loadtxt('data_triax25_orig.txt')

plt.figure()
plt.plot(data_orig[:, 0], data_orig[:, 1], label='data')
plt.plot(data_orig[:, 0], G[::3], label='model')

plt.figure()
plt.plot(data_orig[:, 0], data_orig[:, 2], label='data')
plt.plot(data_orig[:, 0], G[1::3], label='model')
plt.xlabel('Axial strain')
plt.ylabel('Shear stress')
plt.legend()

plt.figure()
plt.plot(data_orig[:, 0], data_orig[:, 3], label='data')
plt.plot(data_orig[:, 0], G[2::3], label='model')


plt.show()
