import env

from uq_tools import asm

import numpy as np
import numpy.random as rnd
from scipy import integrate
import sklearn.neighbors as skl_nb
from sklearn.model_selection import GridSearchCV

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

_settings = env.settings

_asDir = _settings.asDir
_alpha = 1
# _alpha = _settings.alpha


def plotPriorActVar():
    W = np.loadtxt('%s/asm_eigVecs.txt' % _asDir)
    W1 = W[:, : _settings.actVars]

    samples = env.prior_sample(n=20000)
    ySamples = np.dot(samples, W1)

    # grid = GridSearchCV(skl_nb.KernelDensity(kernel='tophat'),
    #                     {'bandwidth': np.linspace(0.05, 0.2, 3)})
    # x = np.dot(samples, W1)
    # grid.fit(x[:, None])
    # print grid.best_params_
    # exit()

    # priorY = asm.marginalPriorY(
    #     samples, W1, kde_bandwidth=0.12, kde_kernel='tophat')

    # plt.figure()
    # plt.hist(ySamples, bins=50, normed=True, facecolor='C1', alpha=_alpha)
    # xs = np.arange(np.min(ySamples[:, 0]), np.max(ySamples[:, 0]), step=0.01)
    # plt.plot(xs, priorY(xs), color='C0')
    # plt.xlabel(r'$\mathbf{w}_1^\top \mathbf{x}$')
    # # plt.xlim([-1.75, 1.75])
    # plt.tight_layout()

    # g = sns.jointplot(x=ySamples[:, 0], y=ySamples[:, 1],
    #                   kind="hex", stat_func=None)
    # g.set_axis_labels(r'$\mathbf{w}_1^\top \mathbf{x}$',
    #                   r'$\mathbf{w}_2^\top \mathbf{x}$')
    # plt.tight_layout()
    # plt.figure()
    # plt.hist(ySamples[:, 0], bins=50, normed=True,
    #          facecolor='C1', alpha=_alpha)
    # plt.figure()
    # h, x, y, p = plt.hist2d(ySamples[:, 0], ySamples[:, 1], bins=20,
    #                         normed=True, alpha=_alpha)
    # plt.clf()
    # plt.imshow(h, origin="lower", interpolation="bicubic",
    #            cmap=sns.light_palette('blue', as_cmap=True))

    choice = rnd.choice(len(ySamples), 5000)
    y1Samples = ySamples[choice, 0]
    y2Samples = ySamples[choice, 1]
    y3Samples = ySamples[choice, 2]
    y4Samples = ySamples[choice, 3]
    y5Samples = ySamples[choice, 4]
    df = pd.DataFrame(
        np.hstack((y1Samples[:, np.newaxis], y2Samples[:, np.newaxis],
                   y3Samples[:, np.newaxis], y4Samples[:, np.newaxis],
                   y5Samples[:, np.newaxis])),
        columns=[r'$\mathbf{w}_1^\top \mathbf{x}$',
                 r'$\mathbf{w}_2^\top \mathbf{x}$',
                 r'$\mathbf{w}_3^\top \mathbf{x}$',
                 r'$\mathbf{w}_4^\top \mathbf{x}$',
                 r'$\mathbf{w}_5^\top \mathbf{x}$'])
    g = sns.PairGrid(df, size=1.5)
    g = g.map_diag(sns.distplot, kde=False)
    g = g.map_lower(sns.kdeplot)

    for i in range(_settings.actVars):
        for j in range(_settings.actVars):
            if j <= i:
                pass
                g.axes[i, j].set_xlim(-2., 2.)
                g.axes[i, j].set_ylim(-2., 2.)
            else:
                g.axes[i, j].set_visible(False)

    plt.tight_layout()
    plt.subplots_adjust(hspace=0.15, wspace=0.15)


if __name__ == '__main__':
    plotPriorActVar()
    plt.show()
