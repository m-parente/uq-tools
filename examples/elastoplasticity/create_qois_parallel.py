import env

import multiprocessing as mp
import numpy as np
import os
import sys

tmpDir = 'tmp'
scriptName = 'run_model.py'

M = 1000

samples = env.prior_sample(M)
np.savetxt('%s/xs.txt' % tmpDir, samples)

cmds = ''
for i in range(M):
    sample = samples[i]
    cmd = sys.executable + " " + scriptName + " " + \
        str(i) + " " + ' '.join(str(p) for p in sample)
    cmds += cmd + '\n'

launcherFile = '%s/asmLauncher_runs.txt' % tmpDir

with open(launcherFile, 'w') as file:
    file.write(cmds)

os.environ["LAUNCHER_JOB_FILE"] = launcherFile
os.environ["LAUNCHER_DIR"] = os.environ["SW_DIR"] + "/launcher"
os.environ["LAUNCHER_PPN"] = str(mp.cpu_count() - 1)

os.system("bash $LAUNCHER_DIR/paramrun")
