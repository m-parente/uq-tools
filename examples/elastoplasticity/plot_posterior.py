import env

import numpy as np
import numpy.random as rnd

import matplotlib as mpl
import matplotlib.cm as cmx
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

# plt.rcParams["patch.force_edgecolor"] = True
sns.set()
sns.set_context('paper', font_scale=1.5)


def get_cmap(cmap):
    color_norm = mpl.colors.Normalize(vmin=-0.5, vmax=1)
    scalar_map = cmx.ScalarMappable(norm=color_norm, cmap=cmap)

    def map_index_to_rgb_color(index):
        return scalar_map.to_rgba(index)
    return map_index_to_rgb_color


cmap = get_cmap('OrRd')


_settings = env.settings
_mcmcDir = _settings.mcmcDir


def _pairgrid_heatmap(x, y, **kws):
    cmap = sns.light_palette(kws.pop("color"), as_cmap=True)
    plt.hist2d(x, y, cmap=cmap, cmin=1, **kws)


def plotPosterior():
    samples = np.loadtxt('%s/xSamples_2d.txt' % _mcmcDir)
    samples = env.toOriginalRange(samples)

    choice = rnd.choice(len(samples), 10000)

    corrcoeffs = np.corrcoef(samples.T)

    # f, ((ax1, ax2, ax3, ax4), (ax5, ax6, ax7, ax8)) = plt.subplots(2, 4)

    # unif_xs = np.arange(-1.2, 1.2 + 0.01, step=0.001)
    # plot_xs = np.array([unif_xs, unif_xs, unif_xs, unif_xs,
    #                     unif_xs, unif_xs, unif_xs, unif_xs]).T
    # plot_xs = env.toOriginalRange(plot_xs)

    # axes = [ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8]
    lims = [(_settings.c_LowerBound, _settings.c_UpperBound),
            (_settings.alpha_res_min_LowerBound,
             _settings.alpha_res_min_UpperBound),
            (_settings.alpha_res_diff_LowerBound,
             _settings.alpha_res_diff_UpperBound),
            (_settings.lambda_dot_max_LowerBound,
             _settings.lambda_dot_max_UpperBound),
            (_settings.m_LowerBound, _settings.m_UpperBound),
            (_settings.beta_max_LowerBound, _settings.beta_max_UpperBound),
            (_settings.lambda_max_LowerBound, _settings.lambda_max_UpperBound),
            (_settings.n_LowerBound, _settings.n_UpperBound)]

    # symbs = [r'$c$', r'$\alpha_{res}^l$', r'$\Delta\alpha_{res}$', r'$\dot{\lambda}^*$',
    #          r'$m_\alpha$', r'$\beta^*$', r'$\lambda^*$', r'$m_\beta$']
    # sciticks = [True, False, False, True, False, False, True, False]

    # for i in range(len(axes)):
    #     ax = axes[i]
    #     ax.set_yticks([])
    #     plot_ys = map(lambda x: 1. / (lims[i][1] - lims[i][0])
    #                   if x >= -1. and x <= 1. else 0, unif_xs)
    #     ax.plot(plot_xs[:, i], plot_ys, color='C2')
    #     sns.distplot(samples[:, i], kde=False,
    #                  norm_hist=True, axlabel=symbs[i], ax=ax)
    #     if sciticks[i]:
    #         ax.get_xaxis().set_major_formatter(mpl.ticker.ScalarFormatter(useMathText=True))
    #         ax.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))

    # plt.tight_layout()

    x1Samples = samples[choice, 0]
    x2Samples = samples[choice, 1]
    x3Samples = samples[choice, 2]
    x4Samples = samples[choice, 3]
    x5Samples = samples[choice, 4]
    x6Samples = samples[choice, 5]
    x7Samples = samples[choice, 6]
    x8Samples = samples[choice, 7]
    df = pd.DataFrame(
        np.hstack((x1Samples[:, np.newaxis], x2Samples[:, np.newaxis],
                   x3Samples[:, np.newaxis], x4Samples[:, np.newaxis],
                   x5Samples[:, np.newaxis], x6Samples[:, np.newaxis],
                   x7Samples[:, np.newaxis], x8Samples[:, np.newaxis])),
        columns=[r'$c$', r'$\alpha_{res}^l$', r'$\Delta\alpha_{res}$', r'$\dot{\lambda}^*$',
                 r'$m_\alpha$', r'$\beta^*$', r'$\lambda^*$', r'$m_\beta$'])
    g = sns.PairGrid(df, height=0.7)
    g = g.map_diag(sns.distplot, kde=False, bins=10)
    g = g.map_lower(sns.kdeplot)

    for i in range(env.numberOfParameters):
        for j in range(env.numberOfParameters):
            ax = g.axes[i, j]
            if j <= i:
                ax.set_xlim(lims[j][0], lims[j][1])
                ax.set_ylim(lims[i][0], lims[i][1])
                ax.set_xticklabels([])
                ax.set_yticklabels([])
                ax.set_xlabel('')
                ax.set_ylabel('')

                # if i == env.numberOfParameters - 1 or j == 0:
                #     k = j if i == env.numberOfParameters - 1 else i
                #     if sciticks[k]:
                #         ax.get_xaxis().set_major_formatter(
                #             mpl.ticker.ScalarFormatter(useMathText=True))
                #         ax.get_yaxis().set_major_formatter(
                #             mpl.ticker.ScalarFormatter(useMathText=True))
                #         ax.ticklabel_format(
                #             style='sci',
                #             axis='x' if i == env.numberOfParameters - 1 else 'y',
                #             scilimits=(0, 0))
            else:
                # ax.set_visible(False)
                ax.grid(False)
                ax.set_facecolor('white')
                ax.text(0.5 * (lims[j][1] + lims[j][0]),
                        0.5 * (lims[i][1] + lims[i][0]),
                        r'$%.3f$' % np.round(corrcoeffs[j, i], decimals=3),
                        fontsize=10,
                        color=cmap(np.abs(corrcoeffs[j, i])),
                        ha='center', va='center')

    plt.subplots_adjust(left=0.03, top=0.97, right=0.97,
                        bottom=0.03, hspace=0.1, wspace=0.1)


if __name__ == '__main__':
    plotPosterior()
    plt.show()
