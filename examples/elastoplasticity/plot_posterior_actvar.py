import env

import numpy as np
import numpy.random as rnd
import sklearn.neighbors as skl_nb

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

plt.rcParams["patch.force_edgecolor"] = True
sns.set()
sns.set_context('paper', font_scale=1.5)

_settings = env.settings

_mcmcDir = _settings.mcmcDir
_alpha = 1
# _alpha = _settings.alpha


def plotPosteriorActVar():
    effYSamples = np.loadtxt('%s/effYSamples_2d.txt' % _mcmcDir)

    if _settings.actVars == 1:
        plt.figure()
        plt.hist(effYSamples, bins=50, density=True,
                 color='C1', alpha=_alpha, edgecolor='none')
        kde = skl_nb.KernelDensity(
            bandwidth=0.005, kernel='gaussian').fit(effYSamples[:, np.newaxis])
        xs = np.arange(np.min(effYSamples), np.max(
            effYSamples) + 0.01, step=0.001)
        plt.plot(xs, np.exp(kde.score_samples(
            xs[:, np.newaxis])), color='C0')
        plt.xlabel(r'$\mathbf{w}_1^\top \mathbf{x}$')
        plt.tight_layout()
    elif _settings.actVars == 2:
        g = sns.JointGrid(x=effYSamples[:, 0], y=effYSamples[:, 1],
                          xlim=(-2, 2), ylim=(-2, 2))
        g.plot_joint(sns.kdeplot)
        g.plot_marginals(sns.distplot, kde=False)
        g.set_axis_labels(r'$\mathbf{w}_1^\top \mathbf{x}$',
                          r'$\mathbf{w}_2^\top \mathbf{x}$')
        plt.tight_layout()

        f, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
        ax1.hist(effYSamples[:, 0], bins=50, density=True,
                 alpha=_alpha, edgecolor='none')
        ax1.set_xlabel(r'$\mathbf{w}_1^\top \mathbf{x}$')
        ax2.hist(effYSamples[:, 1], bins=50, density=True,
                 alpha=_alpha, edgecolor='none')
        ax2.set_xlabel(r'$\mathbf{w}_2^\top \mathbf{x}$')
        plt.xlim([-2, 2])

        plt.tight_layout()
        # plt.hist2d(effYSamples[:, 0], effYSamples[:, 0], bins=50,
        #            density=True, alpha=_alpha)
    else:
        choice = rnd.choice(len(effYSamples), 100)
        y1Samples = effYSamples[choice, 0]
        y2Samples = effYSamples[choice, 1]
        y3Samples = effYSamples[choice, 2]
        y4Samples = effYSamples[choice, 3]
        y5Samples = effYSamples[choice, 4]
        df = pd.DataFrame(
            np.hstack((y1Samples[:, np.newaxis], y2Samples[:, np.newaxis],
                       y3Samples[:, np.newaxis], y4Samples[:, np.newaxis],
                       y5Samples[:, np.newaxis])),
            columns=[r'$\mathbf{w}_1^\top \mathbf{x}$',
                     r'$\mathbf{w}_2^\top \mathbf{x}$',
                     r'$\mathbf{w}_3^\top \mathbf{x}$',
                     r'$\mathbf{w}_4^\top \mathbf{x}$',
                     r'$\mathbf{w}_5^\top \mathbf{x}$'])
        g = sns.PairGrid(df, size=1.5)
        g = g.map_diag(sns.distplot, kde=False)
        g = g.map_lower(sns.kdeplot, shade=False)

        for i in range(_settings.actVars):
            for j in range(_settings.actVars):
                if j <= i:
                    g.axes[i, j].set_xlim(-2., 2.)
                    g.axes[i, j].set_ylim(-2., 2.)
                else:
                    g.axes[i, j].set_visible(False)

        plt.tight_layout()
        plt.subplots_adjust(hspace=0.15, wspace=0.15)


if __name__ == '__main__':
    plotPosteriorActVar()
    plt.show()
