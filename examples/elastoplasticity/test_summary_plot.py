import env

import plotters
from uq_tools import asm

import numpy as np

import matplotlib.pyplot as plt
from sklearn import metrics

settings = env.settings

samplesDir = settings.samplesDir
asDir = settings.asDir

W = np.loadtxt('%s/asm_eigVecs.txt' % asDir)
w1 = W[:, 0]
w2 = W[:, 1]
w3 = W[:, 2]
w4 = W[:, 3]
w5 = W[:, 4]
w6 = W[:, 5]
w7 = W[:, 6]
w8 = W[:, 7]

xs = env.xs
Gs = env.Gs

misfits = list(map(env.misfit, Gs))

print(np.max(misfits), np.argmax(misfits))
print(np.min(misfits), np.argmin(misfits))

# w = asm.lstsq_direction(xs, misfits)
# np.savetxt('%s/asm_eigVec.txt' % asDir, w)
# print w
print(w1)

# plotters.summaryPlot(w, xs, misfits, with_fit=True, poly_order=2)
plotters.summaryPlot(w1, xs, misfits, with_fit=True, poly_order=2)
plotters.summaryPlot2D(w1, w2, xs, misfits, with_fit=True, poly_order=2)

poly_order = 2
resp_surf = asm.response_surface(
    xs, misfits, np.transpose([w1, w2, w3]), poly_order=poly_order)
print("3D r2 score: %f" % metrics.r2_score(misfits, resp_surf(np.dot(xs, np.transpose([w1, w2, w3])))))

resp_surf = asm.response_surface(xs, misfits, np.transpose(
    [w1, w2, w3, w4]), poly_order=poly_order)
print("4D r2 score: %f" % metrics.r2_score(misfits, resp_surf(np.dot(xs, np.transpose([w1, w2, w3, w4])))))

resp_surf = asm.response_surface(
    xs, misfits, np.transpose([w1, w2, w3, w4, w5]), poly_order=poly_order)
print("5D r2 score: %f" % metrics.r2_score(misfits, resp_surf(np.dot(xs, np.transpose([w1, w2, w3, w4, w5])))))

resp_surf = asm.response_surface(
    xs, misfits, np.transpose([w1, w2, w3, w4, w5, w6, w7, w8]), poly_order=poly_order)
print("8D r2 score: %f" % metrics.r2_score(misfits, resp_surf(np.dot(xs, np.transpose([w1, w2, w3, w4, w5, w6, w7, w8])))))

plt.show()
