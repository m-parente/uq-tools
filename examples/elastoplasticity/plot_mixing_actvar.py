import env

import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

_settings = env.settings
_mcmcDir = _settings.mcmcDir

_alpha = 1
# _alpha = _settings.alpha


def plotMixingActVar(length=2000):
    ySamples_2d = np.loadtxt('%s/ySamples_2d.txt' % _mcmcDir)
    ySamples_5d = np.loadtxt('%s/ySamples_5d.txt' % _mcmcDir)

    burnIn = 100000  # _settings.burnInActVar

    for i in range(2):
        plt.figure()
        plt.plot(list(range(burnIn + 1, burnIn + length + 1)),  # n + 1),
                 ySamples_2d[burnIn:burnIn + length, i], alpha=_alpha, label='2D')
        plt.plot(list(range(burnIn + 1, burnIn + length + 1)),  # n + 1),
                 ySamples_5d[burnIn:burnIn + length, i], alpha=_alpha, label='5D')
        plt.xticks([1e5, 1.005e5, 1.01e5, 1.015e5, 1.02e5])
        plt.axes().get_xaxis().set_major_formatter(
            mpl.ticker.ScalarFormatter(useMathText=True))
        plt.axes().ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
        plt.xlabel('Chain index')
        plt.ylabel(r'$2^{nd}$ component of active variable')
        plt.legend()
        plt.tight_layout()


if __name__ == '__main__':
    plotMixingActVar()
    plt.show()
