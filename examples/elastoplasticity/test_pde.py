import pde

import numpy as np
import numpy.random as rnd
from time import time as tic


parameters = rnd.normal(loc=0.0, scale=1.0, size=5)
pb = pde.ElastoplasticityPDEProblem(-1, parameters)

u = pb.solveForwardProblem()

print(pb.getQoIValues())
