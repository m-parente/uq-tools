import env

import numpy as np
import numpy.linalg as la

n = len(env.jacGs)
norms = np.zeros((n, n))

for i in range(n):
    for j in range(n):
        norms[i, j] = la.norm(env.jacGs[i] - env.jacGs[j]) \
            / la.norm(env.jacGs[i])
        # print norms[i, j]

print("Average rel. norm diff.: %f" % (np.sum(norms) / (n**2-n)))
print("Max rel. norm diff.: %f" % np.max(norms))
