import env

import numpy as np
import numpy.random as rnd
import sklearn.neighbors as skl_nb

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns


_settings = env.settings

_asDir = _settings.asDir
_mcmcDir = _settings.mcmcDir

_alpha = 1
# _alpha = _settings.alpha


def plotPriorPosteriorActVar():
    W = np.loadtxt('%s/asm_eigVecs.txt' % _asDir)
    W1 = W[:, :5]

    ySamples_2d = np.loadtxt('%s/effYSamples_2d.txt' % _settings.mcmcDir)
    ySamples_5d = np.loadtxt('%s/effYSamples_5d.txt' % _settings.mcmcDir)

    samples = env.prior_sample(n=20000)
    ySamples_prior = np.dot(samples, W1)

    choice = rnd.choice(len(ySamples_prior), 50000)
    y1Samples_prior = ySamples_prior[choice, 0]
    y2Samples_prior = ySamples_prior[choice, 1]
    y3Samples_prior = ySamples_prior[choice, 2]
    y4Samples_prior = ySamples_prior[choice, 3]
    y5Samples_prior = ySamples_prior[choice, 4]

    choice = rnd.choice(len(ySamples_2d), 50000)
    y1Samples_2d = ySamples_2d[choice, 0]
    y2Samples_2d = ySamples_2d[choice, 1]

    choice = rnd.choice(len(ySamples_5d), 50000)

    y1Samples5d = ySamples_5d[choice, 0]
    y2Samples5d = ySamples_5d[choice, 1]
    y3Samples5d = ySamples_5d[choice, 2]
    y4Samples5d = ySamples_5d[choice, 3]
    y5Samples5d = ySamples_5d[choice, 4]

    f, ((ax1, ax2, ax3, ax4, ax5),
        (ax6, ax7, ax8, ax9, ax10),
        (ax11, ax12, ax13, ax14, ax15)) = plt.subplots(3, 5)

    bins = 20

    sns.distplot(y1Samples_prior, kde=True, hist=True,
                 norm_hist=True, bins=bins, ax=ax1)
    sns.distplot(y2Samples_prior, kde=True, hist=True,
                 norm_hist=True, bins=bins, ax=ax2)
    sns.distplot(y3Samples_prior, kde=True, hist=True,
                 norm_hist=True, bins=bins, ax=ax3)
    sns.distplot(y4Samples_prior, kde=True, hist=True,
                 norm_hist=True, bins=bins, ax=ax4)
    sns.distplot(y5Samples_prior, kde=True, hist=True,
                 norm_hist=True, bins=bins, ax=ax5)

    sns.distplot(y1Samples_2d, kde=True, hist=True,
                 norm_hist=True, bins=bins, ax=ax6)
    sns.distplot(y2Samples_2d, kde=True, hist=True,
                 norm_hist=True, bins=bins, ax=ax7)
    ax8.set_visible(False)
    ax9.set_visible(False)
    ax10.set_visible(False)

    sns.distplot(y1Samples5d, kde=True, hist=True, norm_hist=True, bins=bins, ax=ax11,
                 axlabel=r'${\tilde{\mathbf{w}}}_1^\top \mathbf{X}$')
    sns.distplot(y2Samples5d, kde=True, hist=True, norm_hist=True, bins=bins, ax=ax12,
                 axlabel=r'${\tilde{\mathbf{w}}}_2^\top \mathbf{X}$')
    sns.distplot(y3Samples5d, kde=True, hist=True, norm_hist=True, bins=bins, ax=ax13,
                 axlabel=r'${\tilde{\mathbf{w}}}_3^\top \mathbf{X}$')
    sns.distplot(y4Samples5d, kde=True, hist=True, norm_hist=True, bins=bins, ax=ax14,
                 axlabel=r'${\tilde{\mathbf{w}}}_4^\top \mathbf{X}$')
    sns.distplot(y5Samples5d, kde=True, hist=True, norm_hist=True, bins=bins, ax=ax15,
                 axlabel=r'${\tilde{\mathbf{w}}}_5^\top \mathbf{X}$')

    ax1.set_xlim([-1.8, 1.8])
    ax2.set_xlim([-1.8, 1.8])
    ax3.set_xlim([-1.8, 1.8])
    ax4.set_xlim([-1.8, 1.8])
    ax5.set_xlim([-1.8, 1.8])

    ax1.set_xticks([-1.5, 1.5])
    ax2.set_xticks([-1.5, 1.5])
    ax3.set_xticks([-1.5, 1.5])
    ax4.set_xticks([-1.5, 1.5])
    ax5.set_xticks([-1.5, 1.5])
    ax6.set_xticks([-0.5, -0.3])
    ax7.set_xticks([-0.35, -0.05])
    ax11.set_xticks([-0.45, -0.3])
    ax12.set_xticks([-0.35, -0.15])
    ax13.set_xticks([0.4, 0.65])
    ax14.set_xticks([0.3, 0.5])
    ax15.set_xticks([0.25, 0.5])

    ax1.set_ylim([0, 0.8])
    ax2.set_ylim([0, 0.8])
    ax3.set_ylim([0, 0.8])
    ax4.set_ylim([0, 0.8])
    ax5.set_ylim([0, 0.8])
    ax6.set_ylim([0, 45])
    ax7.set_ylim([0, 45])
    ax11.set_ylim([0, 60])
    ax12.set_ylim([0, 60])
    ax13.set_ylim([0, 60])
    ax14.set_ylim([0, 60])
    ax15.set_ylim([0, 60])

    ax6.set_yticks([0, 20])
    ax7.set_yticks([0, 20])
    ax11.set_yticks([0, 20, 40])
    ax12.set_yticks([0, 20, 40])
    ax13.set_yticks([0, 20, 40])
    ax14.set_yticks([0, 20, 40])
    ax15.set_yticks([0, 20, 40])

    plt.tight_layout()
    plt.subplots_adjust(hspace=0.5, wspace=1.0)


if __name__ == '__main__':
    plotPriorPosteriorActVar()
    plt.show()
