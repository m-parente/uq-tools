import env

from glob import glob
import natsort
import numpy as np
import numpy.linalg as la

import matplotlib.pyplot as plt

settings = env.settings

samplesDir = settings.samplesDir

GFiles = env.GFiles
# jacGFiles = env.jacGFiles
invNoiseCovMat = env.invNoiseCovMat
data = env.data

misfits = list(map(env.misfit, env.Gs))
# misfit_grads = map(lambda grad: la.norm(grad),
#                    map(lambda pair: np.dot(np.dot(pair[1].T, invNoiseCovMat), data - pair[0]), zip(env.Gs, env.jacGs)))

xs = np.loadtxt('%s/xs.txt' % samplesDir)

# for _ in range(2):
#     amax = np.argmax(misfits)
#     print amax, misfits[np.argmax(misfits)], xs[amax]

#     xs = np.delete(xs, amax, 0)
#     misfits = np.delete(misfits, amax)

toRemove = [ix[0] for ix in [ix for ix in zip(list(range(len(xs))), xs) if (ix[1][6] < -0.7 or ix[1][6] > 0.0)]]

# misfits = np.delete(misfits, toRemove)
# xs = np.delete(xs, toRemove, axis=0)

print(len(toRemove))
print(len(misfits))
print(len(xs))
# exit()

# zip(range(len(misfits)), misfits)

for i in range(np.shape(xs)[1]):
    plt.figure()
    plt.yscale('log')
    plt.scatter(xs[:, i], misfits)


# for i in range(np.shape(xs)[1]):
#     plt.figure()
#     plt.yscale('log')
#     plt.scatter(xs[:, i], misfit_grads)

plt.show()
