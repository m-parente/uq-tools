import env
import pde

import numpy as np

x_ = env.sample()
x = x_ + 0.01 * np.ones(env.numberOfParameters)

absPb = pde.AbstractElastoplasticityPDEProblem()
pb = pde.ElastoplasticityPDEProblem(-1, x)
pb_ = pde.ElastoplasticityPDEProblem(-2, x_)

diff_x = x - x_

diffs = np.empty(env.numberOfDataPoints)

for iData in range(len(diffs)):
    qoi = pb.getQoIValues()[iData]

    qoi_ = pb_.getQoIValues()[iData]
    grad_ = pb_.getGradient()

    diffs[iData] = abs(qoi - qoi_ - np.dot(grad_, diff_x.transpose())) / qoi_

print(diffs)
