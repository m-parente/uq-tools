import env

import numpy as np

import matplotlib.pyplot as plt

_settings = env.settings
_mcmcDir = _settings.mcmcDir


def plotAutocorrs():
    yAutocorrs_2d = np.loadtxt('%s/yAutocorrs_2d.txt' % _mcmcDir)[1]
    yAutocorrs_5d = np.loadtxt('%s/yAutocorrs_5d.txt' % _mcmcDir)[1]

    plt.figure()
    idx = 500  # np.argwhere(yAutocorr < 0.0)[0][0]
    plt.scatter(list(range(1, idx + 2)),
                yAutocorrs_2d[:idx + 1], label='2D', zorder=2)
    plt.scatter(list(range(1, idx + 2)),
                yAutocorrs_5d[:idx + 1], label='5D', zorder=1)
    plt.xlabel(r'Lag $k$')
    plt.ylabel(r'Autocorrelation $r_k$')
    plt.legend()
    plt.tight_layout()


if __name__ == '__main__':
    plotAutocorrs()
    plt.show()
