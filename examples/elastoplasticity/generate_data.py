import pde
from misc import Settings

import numpy as np
import numpy.linalg as la


settings = Settings('settings_triax25')

absPb = pde.AbstractElastoplasticityPDEProblem()
pb = absPb.instantiate(settings.truth)

qoi = pb.getQoI()
qoi = qoi.reshape(len(qoi) / 3, 3)

# Add noise -------------------------
stddevs = qoi * [settings.noiseLevelStrain,
                 settings.noiseLevelStress, settings.noiseLevelStress]
noises = np.random.normal(0, stddevs**2)

data = qoi + noises

np.savetxt('simData_triax25.txt', data)
