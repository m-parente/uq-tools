import env
import plotters

import numpy as np

import matplotlib.pyplot as plt

_settings = env.settings

_asDir = _settings.asDir
_alpha = 1
# _alpha = _settings.alpha


def plotAs():
    eigVals = np.loadtxt('%s/asm_eigVals.txt' % _asDir)
    eigVecs = np.loadtxt('%s/asm_eigVecs.txt' % _asDir)
    minEigVals = np.loadtxt('%s/asm_minEigVals.txt' % _asDir)
    maxEigVals = np.loadtxt('%s/asm_maxEigVals.txt' % _asDir)
    minSubspaceErrors = np.loadtxt('%s/asm_minSubspaceErrors.txt' % _asDir)
    maxSubspaceErrors = np.loadtxt('%s/asm_maxSubspaceErrors.txt' % _asDir)
    meanSubspaceErrors = np.loadtxt('%s/asm_meanSubspaceErrors.txt' % _asDir)

    # For thesis:
    plotters.plotActiveSubspace(eigVals, eigVecs,
                                num_plot_eigvals=8,
                                minEigVals=minEigVals,
                                maxEigVals=maxEigVals,
                                minSubspaceErrors=minSubspaceErrors,
                                maxSubspaceErrors=maxSubspaceErrors,
                                meanSubspaceErrors=meanSubspaceErrors,
                                num_plot_eigvecs=6,
                                step_eigvec=2)
    # return

    plt.figure()

    xs = list(range(1, len(eigVals) + 1))
    plt.plot(xs, eigVals, 'o-')
    plt.fill_between(xs, minEigVals, maxEigVals, facecolor='C1', alpha=_alpha)
    plt.yscale('log')
    plt.xlabel('Index')
    plt.ylabel('Eigenvalue')
    plt.tight_layout()

    plt.figure()

    for i in range(1, 7):

        ax = plt.subplot(230+i)
        ax.bar(list(range(1, len(eigVals) + 1)), eigVecs[:, i-1])
        ax.set_xticks(np.arange(1, 9, step=2))
        ax.set_yticks(np.arange(-1, 1.1, step=0.5))
        ax.set_xlabel(r'${\tilde{\mathbf{w}}}_%i$' % i)

    plt.tight_layout()
    plt.subplots_adjust(hspace=0.5, wspace=0.5)


if __name__ == '__main__':
    plotAs()
    plt.show()
