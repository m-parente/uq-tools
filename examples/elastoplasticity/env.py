from misc import Settings

from glob import glob
import natsort
import numpy as np
import numpy.linalg as la
import numpy.random as rnd
import os


settings = Settings('settings_triax25_33')

tmpDir = settings.tmpDir
elastDir = settings.elastDir
asDir = settings.asDir
mcmcDir = settings.mcmcDir
os.system('mkdir -p %s && mkdir -p %s && mkdir -p %s && mkdir -p %s' %
          (tmpDir, elastDir, asDir, mcmcDir))

samplesDir = settings.samplesDir

GFiles = natsort.natsorted(glob('%s/G*.txt' % samplesDir))
jacGFiles = natsort.natsorted(glob('%s/jacG*.txt' % samplesDir))

_G0 = np.loadtxt(GFiles[0])
_nDist = 10
_every3rd = np.arange(2, len(_G0), 3)
_onlyVolStrainAndShearStress = np.setdiff1d(np.arange(0, len(_G0)), _every3rd)
_every10thPair = np.array([[i * 2 * _nDist, i * 2 * _nDist + 1]
                           for i in range(int(np.ceil(len(_G0) * (2 / 3.) / (2. * _nDist))))]).flatten()


def cut(full):
    tmp = full[_onlyVolStrainAndShearStress]
    return tmp[_every10thPair]


xs = np.loadtxt('%s/xs.txt' % samplesDir)


def toOriginalRange(samples):
    transl = 0.5 * np.array([settings.c_UpperBound + settings.c_LowerBound,
                             settings.alpha_res_min_UpperBound + settings.alpha_res_min_LowerBound,
                             settings.alpha_res_diff_UpperBound + settings.alpha_res_diff_LowerBound,
                             settings.lambda_dot_max_UpperBound + settings.lambda_dot_max_LowerBound,
                             settings.m_UpperBound + settings.m_LowerBound,
                             settings.beta_max_UpperBound + settings.beta_max_LowerBound,
                             settings.lambda_max_UpperBound + settings.lambda_max_LowerBound,
                             settings.n_UpperBound + settings.n_LowerBound])
    scale = 0.5 * np.array([settings.c_UpperBound - settings.c_LowerBound,
                            settings.alpha_res_min_UpperBound - settings.alpha_res_min_LowerBound,
                            settings.alpha_res_diff_UpperBound - settings.alpha_res_diff_LowerBound,
                            settings.lambda_dot_max_UpperBound - settings.lambda_dot_max_LowerBound,
                            settings.m_UpperBound - settings.m_LowerBound,
                            settings.beta_max_UpperBound - settings.beta_max_LowerBound,
                            settings.lambda_max_UpperBound - settings.lambda_max_LowerBound,
                            settings.n_UpperBound - settings.n_LowerBound])

    return scale * samples + transl


Gs = list(map(cut, [np.loadtxt(GFile) for GFile in GFiles]))
jacGs = list(map(cut, [np.loadtxt(jacGFile) for jacGFile in jacGFiles]))

numberOfParameters = 8


def prior_sample(n=None):
    return rnd.uniform(low=-1, high=1, size=(n, numberOfParameters)) if n != None \
        else rnd.uniform(low=-1, high=1, size=numberOfParameters)


def prior(x):
    return 2**-numberOfParameters if np.all(np.logical_and(x <= 1, x >= -1)) else 0


_dataMat = np.loadtxt(settings.dataFile)
_dataAll = _dataMat[::_nDist][:, [0, 1]].flatten()
_stddevsAll = (_dataMat[::_nDist][:, [0, 1]] * [settings.noiseLevelStrain,
                                                settings.noiseLevelStress]).flatten()
_invNoiseCovMatAll = np.diag(1 / _stddevsAll**2)

_dataVolStrain = _dataMat[::_nDist][:, 0]
_stddevsVolStrain = _dataVolStrain * settings.noiseLevelStrain
_invNoiseCovMatVolStrain = np.diag(1 / _stddevsVolStrain**2)
_sqrtGammaVolStrainInv = np.sqrt(_invNoiseCovMatVolStrain)

_dataShearStress = _dataMat[::_nDist][:, 1]
_stddevsShearStress = _dataShearStress * settings.noiseLevelStress
_invNoiseCovMatShearStress = np.diag(1 / _stddevsShearStress**2)
_sqrtGammaShearStressInv = np.sqrt(_invNoiseCovMatShearStress)


# def misfitVolStrain(G):
#     return 0.5 * la.norm(np.dot(_sqrtGammaVolStrainInv, _dataVolStrain - G[0::3 * _nDist]))**2


# def misfitShearStress(G):
#     return 0.5 * la.norm(np.dot(_sqrtGammaShearStressInv, _dataShearStress - G[1::3 * _nDist]))**2


def misfitVolStrain(G):
    return 0.5 * la.norm(np.dot(_sqrtGammaVolStrainInv, _dataVolStrain - G[0::2]))**2


def misfitShearStress(G):
    return 0.5 * la.norm(np.dot(_sqrtGammaShearStressInv, _dataShearStress - G[1::2]))**2


def misfitAll(G):
    return misfitVolStrain(G) + misfitShearStress(G)


# data = _dataVolStrain
# invNoiseCovMat = _invNoiseCovMatVolStrain


# def misfit(G):
#     return misfitVolStrain(G)


# data = _dataShearStress
# invNoiseCovMat = _invNoiseCovMatShearStress


# def misfit(G):
#     return misfitShearStress(G)


data = _dataAll
invNoiseCovMat = _invNoiseCovMatAll


def misfit(G):
    return misfitAll(G)
