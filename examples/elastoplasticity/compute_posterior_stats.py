import env

import numpy as np

settings = env.settings

mcmcDir = settings.mcmcDir

samples = np.loadtxt('%s/xSamples_5d.txt' % mcmcDir)
# samples = np.loadtxt('%s/xSamples_2d.txt' % mcmcDir)
samples = env.toOriginalRange(samples)

print("Mean: ", ["%.3e" % v for v in np.mean(samples, axis=0)])
print("Std: ", ["%.3e" % v for v in np.std(samples, axis=0)])

print("Corr.coeffs: ")
corrcoeffs = np.corrcoef(samples.T)
for i in range(len(corrcoeffs)):
    s = ''
    for j in range(i + 1):
        s += '%.4f' % np.round(corrcoeffs[i, j],
                               decimals=4) + (', ' if j < i else '')
    print(s)
