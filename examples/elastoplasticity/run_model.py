import env
import pde

import numpy as np
import sys


settings = env.settings


def run(id, parameters):
    print("Forward run with id %i" % id)

    pb = pde.ElastoplasticityPDEProblem(id, parameters)
    qoi = pb.getQoI()

    np.savetxt('%s/G%i.txt' % ('tmp', id), qoi)


def usage():
    print("usage: id parameters")


if __name__ == "__main__":
    if len(sys.argv) == env.numberOfParameters + 2:
        run(int(sys.argv[1]), np.array(sys.argv[2:], dtype=float))
    else:
        usage()
