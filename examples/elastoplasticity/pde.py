import env
from uq_tools.gradients import cfd

import numpy as np
import os


_settings = env.settings


class ElastoplasticityPDEProblem:
    def __init__(self, id, parameters):
        self.id = id
        self.parameters = parameters
        self.parameters_orig = env.toOriginalRange(parameters)

        self.c = self.parameters_orig[0]
        self.alpha_res_min = self.parameters_orig[1]
        self.alpha_res_max = self.alpha_res_min + self.parameters_orig[2]
        self.lambda_dot_max = self.parameters_orig[3]
        self.m = self.parameters_orig[4]
        self.beta_max = self.parameters_orig[5]
        self.lambda_max = self.parameters_orig[6]
        self.n = self.parameters_orig[7]

    def getQoI(self):
        if hasattr(self, 'qoi'):
            return self.qoi

        name = 'elast_%s' % self.id

        print("Get QoI for ID %s" % self.id)
        if os.system('%s %s/ %s %i %i %f %f %s %f %f %f %f %f %f %f %f %f %f > /dev/null' %
                     (os.environ[_settings.elastExe],
                      _settings.elastDir,
                      name,
                      _settings.op_jump_number,
                      _settings.total_load_steps,
                      _settings.confining_stress,
                      _settings.axial_strain_rate,
                      _settings.hardening_flag,
                      _settings.E,
                      _settings.nu,
                      self.c,
                      self.alpha_res_min,
                      self.alpha_res_max,
                      self.lambda_dot_max,
                      self.m,
                      self.beta_max,
                      self.lambda_max,
                      self.n)) == 0:
            result = np.loadtxt('%s/elast_%s_result.txt' % (_settings.elastDir, self.id))
            os.system('rm %s/*.vtu' % _settings.elastDir)
            # Neglect the first column (axial strain) since it stays constant for every parameter
            self.qoi = result[1:, 1:].flatten()
            # self.qoi = result.flatten()
            return self.qoi
        else:
            raise Exception('Elast executable did not execute successfully.\n ID: %s\nParameters: %s' % (
                self.id, (self.c, self.alpha_res_min, self.alpha_res_max, self.lambda_dot_max, self.m, self.beta_max, self.lambda_max, self.n)))

    def getJacobian(self):
        if hasattr(self, 'jac'):
            return self.jac

        def f(x):
            pb = ElastoplasticityPDEProblem(
                '%d_%d' % (self.id, hash(str(x))), x)
            return pb.getQoI()

        self.jac = cfd.approximateGradientAt(self.parameters, f)

        return self.jac


class AbstractElastoplasticityPDEProblem:
    def instantiate(self, parameters, id=-1):
        return ElastoplasticityPDEProblem(id, parameters)
