import env

from glob import glob
import linecache
import natsort
import numpy as np
import numpy.random as rnd
import re


settings = env.settings
samplesDir = settings.samplesDir

GFiles = env.GFiles
# jacGFiles = env.jacGFiles

# xs = env.xs

maxLines = 30000
i = 0

choice = rnd.choice(list(range(int(1e7))), 30000)

with open('%s/xSamples_5d.txt' % settings.mcmcDir, 'w+') as fil:
    for idx in choice:
        fil.write(linecache.getline('%s/xSamples_5d_full.txt' %
                                    settings.mcmcDir, idx))

exit()

# Collect samples from reports into xs.txt ------------------------------
reports = natsort.natsorted(
    glob('%s/../%s/test_*_report.txt' % (samplesDir, 'reports')))

xs = [None] * len(reports)
i = 0
for report in reports:
    with open(report) as reportFile:
        lines = reportFile.readlines()
        params = lines[12:20]
        xs[i] = [float(s) for s in [re.search('(?<= = ).+', p).group(0) for p in params]]
        i += 1

xs = np.array(xs)
xs[:, 2] = xs[:, 2] - xs[:, 1]

xs = (xs - [settings.c_LowerBound,
            settings.alpha_res_min_LowerBound,
            settings.alpha_res_diff_LowerBound,
            settings.lambda_dot_max_LowerBound,
            settings.m_LowerBound,
            settings.beta_max_LowerBound,
            settings.lambda_max_LowerBound,
            settings.n_LowerBound]) * 2 / \
    [settings.c_UpperBound - settings.c_LowerBound,
     settings.alpha_res_min_UpperBound - settings.alpha_res_min_LowerBound,
     settings.alpha_res_diff_UpperBound - settings.alpha_res_diff_LowerBound,
     settings.lambda_dot_max_UpperBound - settings.lambda_dot_max_LowerBound,
     settings.m_UpperBound - settings.m_LowerBound,
     settings.beta_max_UpperBound - settings.beta_max_LowerBound,
     settings.lambda_max_UpperBound - settings.lambda_max_LowerBound,
     settings.n_UpperBound - settings.n_LowerBound] - 1
np.savetxt('%s/xs.txt' % samplesDir, xs)
# -----------------------------------------------------------------------

# Change sign of volumetric strain and mean stress in G*.txt and jacG*.txt
# for GFile, jacGFile in zip(GFiles, jacGFiles):
#     G = np.loadtxt(GFile)
#     jacG = np.loadtxt(jacGFile)

#     G[::3] = -G[::3]
#     G[2::3] = -G[2::3]
#     jacG[::3, :] = -jacG[::3, :]
#     jacG[2::3, :] = -jacG[2::3, :]

#     np.savetxt(GFile, G)
#     np.savetxt(jacGFile, jacG)
# ------------------------------------------------------------------------
