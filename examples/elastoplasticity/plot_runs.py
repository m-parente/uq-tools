import env

from glob import glob
import natsort
import numpy as np
import numpy.linalg as la
import numpy.random as rnd

import matplotlib.pyplot as plt

data = np.loadtxt('data_triax25_orig.txt')
settings = env.settings

GFiles = natsort.natsorted(glob('%s/G*.txt' % settings.samplesDir))
misfits = list(map(env.misfit, env.Gs))

argmin = np.argmin(misfits)
argmax = np.argmax(misfits)
print(np.min(misfits), argmin, misfits[argmin])
print(np.max(misfits), argmax)

G_min = np.loadtxt('%s/G%i.txt' %
                   (settings.samplesDir, argmin)).reshape(225, 3)
G_max = np.loadtxt('%s/G%i.txt' %
                   (settings.samplesDir, argmax)).reshape(225, 3)

G_tmp = np.loadtxt('tmp/_G-1.txt').reshape(225, 3)
misfit_x = env.misfit(env.cut(G_tmp.flatten()))
print("Misfit Shub.: %f" % misfit_x)

plt.figure()
plt.plot(data[:, 0], data[:, 1], label='data')
plt.plot(data[:, 0], G_min[:, 0], label='G%i.txt (min)' % argmin)
plt.plot(data[:, 0], G_max[:, 0], label='G%i.txt (max)' % argmax)
plt.plot(data[:, 0], G_tmp[:, 0], label='Shub.')
plt.legend()

plt.figure()
plt.plot(data[:, 0], data[:, 2], label='data')
plt.plot(data[:, 0], G_min[:, 1], label='G%i.txt (min)' % argmin)
plt.plot(data[:, 0], G_max[:, 1], label='G%i.txt (max)' % argmax)
plt.plot(data[:, 0], G_tmp[:, 1], label='Shub.')
plt.legend()

plt.figure()
plt.plot(data[:, 0], data[:, 3], label='data')
plt.plot(data[:, 0], G_min[:, 2], label='G%i.txt (min)' % argmin)
plt.plot(data[:, 0], G_max[:, 2], label='G%i.txt (max)' % argmax)
plt.plot(data[:, 0], G_tmp[:, 2], label='Shub.')
plt.legend()

GFiles = rnd.choice(GFiles, 20)

plt.figure()
plt.plot(data[:, 0], data[:, 1], label='data')

for GFile in GFiles:
    G = np.loadtxt(GFile)
    G = G.reshape(len(G) / 3, 3)

    plt.plot(data[:, 0], G[:, 0], label=GFile)
plt.legend()

plt.figure()
plt.plot(data[:, 0], data[:, 2], label='data')

for GFile in GFiles:
    G = np.loadtxt(GFile)
    G = G.reshape(len(G) / 3, 3)

    plt.plot(data[:, 0], G[:, 1], label=GFile)
plt.legend()

plt.figure()
plt.plot(data[:, 0], data[:, 3], label='data')

for GFile in GFiles:
    G = np.loadtxt(GFile)
    G = G.reshape(len(G) / 3, 3)

    plt.plot(data[:, 0], G[:, 2], label=GFile)
plt.legend()


plt.show()
