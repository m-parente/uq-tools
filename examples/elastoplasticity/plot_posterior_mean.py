import env

import numpy as np

import matplotlib.pyplot as plt

_settings = env.settings


def plotPosteriorMean():
    data = np.loadtxt('data_triax25_orig.txt')

    misfits = list(map(env.misfit, env.Gs))
    argmin = np.argmin(misfits)
    G_2d = np.loadtxt('%s/G1.txt' % 'tmp').reshape(225, 3)
    G_5d = np.loadtxt('%s/G2.txt' % 'tmp').reshape(225, 3)

    f, (ax1, ax2) = plt.subplots(2, sharex=True)
    ax1.plot(data[:, 0], data[:, 1], label='data')
    ax1.plot(data[:, 0], G_2d[:, 0], '-.',
             label=r'$\mathcal{G}_\epsilon\left(\tilde{\bar{\mathbf{\mu}}}_{post}^{2D}\right)$')
    ax1.plot(data[:, 0], G_5d[:, 0], '--',
             label=r'$\mathcal{G}_\epsilon\left(\tilde{\bar{\mathbf{\mu}}}_{post}^{5D}\right)$')
    ax1.set_ylabel(r'Volumetric strain [$-$]')
    ax1.get_yaxis().set_label_coords(-0.1, 0.5)
    ax1.legend(loc=1)

    ax2.plot(data[:, 0], data[:, 2], label='data')
    ax2.plot(data[:, 0], G_2d[:, 1], '-.',
             label=r'$\mathcal{G}_\sigma\left(\tilde{\bar{\mathbf{\mu}}}_{post}^{2D}\right)$')
    ax2.plot(data[:, 0], G_5d[:, 1], '--',
             label=r'$\mathcal{G}_\sigma\left(\tilde{\bar{\mathbf{\mu}}}_{post}^{5D}\right)$')
    ax2.set_xlabel(r'Axial strain [$-$]')
    ax2.set_ylabel('Shear stress [MPa]')
    ax2.get_yaxis().set_label_coords(-0.1, 0.5)
    ax2.legend(loc=4)

    plt.tight_layout()


if __name__ == '__main__':
    plotPosteriorMean()
    plt.show()
