import env
from uq_tools import asm
from uq_tools import mcmc

import numpy as np
import numpy.linalg as la
import numpy.random as rnd
import sys
import time as tm

import matplotlib.pyplot as plt

settings = env.settings

asDir = settings.asDir
mcmcDir = settings.mcmcDir

xs = env.xs
Gs = env.Gs

# Construct the response surface -----------------
W = np.loadtxt('%s/asm_eigVecs.txt' % asDir)

W_split = np.split(W, [settings.actVars], axis=1)
W1, W2 = W_split[0], W_split[1]

misfits = list(map(env.misfit, Gs))

resp_surface = asm.response_surface(xs, misfits, W1, poly_order=2)
# ------------------------------------------------

if len(sys.argv) < 2:
    priorY = asm.marginalPriorY(env.prior_sample(
        n=50000), W1, kde_bandwidth=0.12, kde_kernel='tophat')

    start = tm.time()

    ySamples = asm.as_mcmc_with_response_surface(
        resp_surface,
        W1, W2,
        proposal_sampler=lambda yk: rnd.normal(loc=yk, scale=0.0025),
        priorY=priorY,
        y1=np.zeros(settings.actVars),
        steps=2 * 10**6,
        nPlotAccptRate=5000)

    durat = tm.time() - start

    burnIn = settings.burnInActVar
    minEss, ess, corrTimes, acfs = mcmc.stats(
        ySamples, burnIn=burnIn, maxlag=None)
    print("MCMC stats (min ess, ess, corrTimes): %f,%s,%s" % (
        minEss, ess, corrTimes))

    effYSamples = mcmc.pickEffSamples(ySamples, burnIn, maxlag=None)
    print("# Eff. samples: %i" % len(effYSamples))
    print("Elapsed seconds: %f" % durat)
    print("# Eff. samples / sec: %f" % (len(effYSamples) / durat))

    np.savetxt('%s/ySamples.txt' % mcmcDir, ySamples)
    np.savetxt('%s/effYSamples.txt' % mcmcDir, effYSamples)
    np.savetxt('%s/yAutocorrs.txt' % mcmcDir, acfs)
    exit()
else:
    if sys.argv[1] == 'f':
        effYSamples = np.loadtxt('%s/effYSamples_5d.txt' % mcmcDir)
    else:
        print("Unknown argument '%s'" % sys.argv[1])
        exit()

effYSamples = effYSamples[rnd.choice(len(effYSamples), 100)]

start = tm.time()
xSamples = asm.activeToOriginalMCMC(effYSamples,
                                    W1, W2,
                                    prior=env.prior,
                                    proposal_sampler=lambda zk: rnd.normal(
                                        loc=zk, scale=0.8),
                                    z1=np.zeros(np.shape(W2)[1]),
                                    stepsPerActiveSample=1 * 10**5,
                                    burnIn=settings.burnInInactVar,
                                    nPlotAccptRate=10000)
xSamples = np.concatenate(xSamples)
print("%f sec elapsed." % (tm.time() - start))
print('# of xSamples: %i' % len(xSamples))

np.savetxt('%s/xSamples_5d.txt' % mcmcDir, xSamples)
