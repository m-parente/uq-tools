import env

from plot_qoi import plotQoI
from plot_as import plotAs
from plot_summary import plotSummary
from plot_prior_actvar import plotPriorActVar
from plot_autocorrs import plotAutocorrs
from plot_mixing_actvar import plotMixingActVar
from plot_posterior_actvar import plotPosteriorActVar
from plot_prior_posterior_actvar import plotPriorPosteriorActVar
from plot_posterior import plotPosterior
from plot_posterior_mean import plotPosteriorMean

import numpy as np
import numpy.random as rnd

import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

# plt.rcParams["patch.force_edgecolor"] = True
sns.set()
sns.set_context('paper', font_scale=1.5)
sns.set_style('whitegrid')
mpl.rcParams['lines.linewidth'] = 2


# plotQoI()
# plotAs()
# plotSummary()
# plotPriorActVar()
# plotAutocorrs()
# plotMixingActVar()
# plotPosteriorActVar()
# plotPriorPosteriorActVar()
# plotPosterior()
# plotPosteriorMean()

plt.show()
