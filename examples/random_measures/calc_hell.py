import env
from env_hell import *

import numpy as np
import time as ti

Ns = [2, 5, 10, 20, 50, 100]  # , 200, 500]
expct_hells = np.empty(len(Ns))
std_hells = np.empty(len(Ns))

Nx = 1000
xs = prior_sample(num=Nx)
fs = np.array([f(x) for x in xs])
Z = np.mean(np.exp(-fs))

Nz = 1000

startTime = ti.time()

for iN in range(len(Ns)):
    print(iN)
    N = Ns[iN]
    d_hells = np.empty(Nz)

    for nz in range(Nz):
        zs_all = np.array(
            [prior_z_y_sample(y=np.dot(W1.T, x), num=N) for x in xs])
        # assert(np.shape(zs_all) == (Nx, N, n-k))

        f_gNs = np.array([f_gN(x, zs=zs_all[i])
                          for (i, x) in zip(list(range(Nx)), xs)])
        Z_gN = np.mean(np.exp(-f_gNs))

        d_hells[nz] = np.sqrt(0.5 * np.mean((np.sqrt(np.exp(-fs)/Z)
                                             - np.sqrt(np.exp(-f_gNs)/Z_gN))**2))

    expct_hells[iN] = np.mean(d_hells)
    std_hells[iN] = np.std(d_hells)

    print(expct_hells[iN])
    print(std_hells[iN]/expct_hells[iN])

durat = ti.time() - startTime
print('Elapsed seconds: %i' % durat)

cv_hells = std_hells / expct_hells

np.savetxt('%s/expct_hells.txt' % env.resultsDir, expct_hells)
np.savetxt('%s/cv_hells.txt' % env.resultsDir, cv_hells)
