import env

from uq_tools import asm

import numpy as np
import scipy.stats as stat


settings = env.settings

dim = 10

np.random.seed(0)
W = stat.ortho_group.rvs(dim=dim)
L = np.diag(
    10**np.insert(np.linspace(0.75, 0, 8, endpoint=False), 0, [1., 0.95]))

A = np.dot(np.dot(W, L), W.T)

assert(np.allclose(np.dot(W, W.T), np.eye(dim)))

np.savetxt('%s/A_hell.txt' % env.tmpDir, A)
