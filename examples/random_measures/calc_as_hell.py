import env_hell as env
import plotters
from uq_tools import asm


settings = env.settings

nBoot = settings.numberBootstrapIterations

result = asm.computeActiveSubspaceFromSamples(
    env.Gs, env.jacGs, env.bayInvPb, nBoot, asDir=settings.asDir)

eigVals, eigVecs, minEigVals, maxEigVals, minSubspaceErrors, maxSubspaceErrors, meanSubspaceErrors = result

numPlotEigVals = 9
plotters.plotActiveSubspace(eigVals, eigVecs, numPlotEigVals, minEigVals, maxEigVals,
                            minSubspaceErrors, maxSubspaceErrors, meanSubspaceErrors, asDir=settings.asDir)
