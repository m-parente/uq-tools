from misc import Settings

import os

settings = Settings('settings')

asDir = settings.asDir
outputDir = settings.outputDir
resultsDir = settings.resultsDir
tmpDir = settings.tmpDir

os.system('mkdir -p %s && mkdir -p %s && mkdir -p %s' %
          (asDir, outputDir, tmpDir))
