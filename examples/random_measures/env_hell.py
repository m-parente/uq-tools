from env import *
import model

import uq_tools.bayinv as bi
from uq_tools import utils

from glob import glob
import natsort
import numpy as np
import numpy.random as rnd
import os
import scipy.stats as stats


samplesDir = settings.outputDir

# xs = np.loadtxt('%s/xs.txt' % samplesDir)

GFiles = natsort.natsorted(glob('%s/G*.txt' % samplesDir))
jacGFiles = natsort.natsorted(glob('%s/jacG*.txt' % samplesDir))
Gs = [np.loadtxt(GFile) for GFile in GFiles]
jacGs = [np.loadtxt(jacGFile)[np.newaxis, :] for jacGFile in jacGFiles]

A = np.loadtxt('%s/A_hell.txt' % tmpDir)

n = len(A)
k = settings.actVars

if os.path.isfile('%s/L_hell.txt' % tmpDir):
    L = np.loadtxt('%s/L_hell.txt' % tmpDir)
    W = np.loadtxt('%s/W_hell.txt' % tmpDir)
    L1, L2 = L[:k], L[k:]
    W_split = np.split(W, [k], axis=1)
    W1, W2 = W_split[0], W_split[1]
    minEigVals_hell = np.loadtxt('%s/minEigVals_hell.txt' % tmpDir)
    maxEigVals_hell = np.loadtxt('%s/maxEigVals_hell.txt' % tmpDir)

x_truth = np.array(settings.truth)


def prior(x):
    return stats.multivariate_normal.pdf(x)


def prior_sample(num=1):
    return rnd.standard_normal(size=(num, n))


def prior_z_y_sample(y=None, num=10):
    return rnd.standard_normal(size=(num, n-k))


data = np.array([np.loadtxt('data.tmp')])

_variance = (data * settings.noiseLevel)**2
_invNoiseCovMat = np.diag(1 / _variance)

bayInvPb = bi.BayesianInverseProblem(
    data, model.AbstractModel(), _invNoiseCovMat)

f = bayInvPb.misfit


def gN(y, zs=None, N=10):
    zs_ = prior_z_y_sample(y=y, num=N) if zs is None else zs
    return np.average([f(np.dot(W1, y) + np.dot(W2, z)) for z in zs_])


# def g(y):
#     return gN(y, N=100)


# def f_g(x):
#     return g(np.dot(W1.T, x))


def f_gN(x, zs=None, N=10):
    return gN(np.dot(W1.T, x), zs, N)
