import env_hell as env
import model

from uq_tools import asm

import numpy as np


settings = env.settings

outputDir = settings.outputDir

M = settings.numberSamples
samples = env.prior_sample(M)
np.savetxt('%s/xs.txt' % outputDir, samples)

for i in range(M):
    sample = samples[i]

    absPb = model.AbstractModel()
    qoi, jac = asm.computeQoIAndJacobian(absPb, i, sample)

    np.savetxt('%s/G%d.txt' % (settings.outputDir, i), [qoi])
    np.savetxt('%s/jacG%d.txt' % (settings.outputDir, i), jac)
