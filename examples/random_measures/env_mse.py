from env import *

from uq_tools import utils

import numpy as np
import numpy.random as rnd

# rnd.seed(0)

k = 2

A = np.loadtxt('%s/A_mse.txt' % tmpDir)
C = np.loadtxt('%s/C_mse.txt' % tmpDir)

L, W = utils.getEigenpairs(C)
L1, L2 = L[:k], L[k:]
W_split = np.split(W, [k], axis=1)
W1, W2 = W_split[0], W_split[1]

n = len(A)


def rho_sample(size=1):
    return rnd.standard_normal(size=size)


def rho_z_y_sample(y=None, size=10):
    return rnd.standard_normal(size=size)


def f(x):
    return 0.5 * np.dot(np.dot(x.T, A), x)


def g(y):
    return 0.5 * (np.sum(np.dot(np.sqrt(L1), y**2)) + np.sum(np.sqrt(L2)))


def gN(y, N=10):
    zs = rho_z_y_sample(y=y, size=(N, n-k))
    return np.average([f(np.dot(W1, y) + np.dot(W2, z)) for z in zs])


def f_g(x):
    return g(np.dot(W1.T, x))


def f_gN(x, N=10):
    return gN(np.dot(W1.T, x), N)


x = rnd.standard_normal(size=n)
assert(f_g(x) == g(np.dot(W1.T, x)))

# y = np.dot(W1.T, x)
# print f_g(x), f_gN(x, N=100)
# print g(y), gN(y, N=100)
