import env

import numpy as np
import scipy.stats as stat

dim = 10

np.random.seed(0)
W = stat.ortho_group.rvs(dim=dim)
L = np.diag(
    10**np.insert(np.linspace(2, 0, 8, endpoint=False), [0, 1], [4, 3.8]))

C = np.dot(np.dot(W, L), W.T)
A = np.dot(np.dot(W, np.sqrt(L)), W.T)

assert(np.allclose(np.dot(W, W.T), np.eye(dim)))
assert(np.allclose(C, np.dot(A, A)))

np.savetxt('%s/C_mse.txt' % env.tmpDir, C)
np.savetxt('%s/A_mse.txt' % env.tmpDir, A)
