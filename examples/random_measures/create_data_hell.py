import env_hell as env
import model

import numpy as np


x_truth = env.x_truth
m = model.AbstractModel().instantiate(x_truth)

np.savetxt('data.tmp', [m.getQoI()])
