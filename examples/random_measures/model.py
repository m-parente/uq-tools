from env import tmpDir

import numpy as np


A = np.loadtxt('%s/A_hell.txt' % tmpDir)


class Model:
    def __init__(self, id, x):
        self.x = x

    def getQoI(self):
        return 0.5 * np.dot(np.dot(self.x.T, A), self.x)

    def getJacobian(self):
        return np.array([np.dot(A, self.x)])


class AbstractModel:
    def instantiate(self, x, id=-1):
        return Model(id, x)
