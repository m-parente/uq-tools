import env_hell as env

from uq_tools import asm

import numpy as np


settings = env.settings

nBoot = settings.numberBootstrapIterations

result = asm.computeActiveSubspaceFromSamples(
    env.Gs, env.jacGs, env.bayInvPb, nBoot, asDir=settings.asDir)

L, W, minEigVals, maxEigVals = result[0], result[1], result[2], result[3]

np.savetxt('%s/L_hell.txt' % env.tmpDir, L)
np.savetxt('%s/minEigVals_hell.txt' % env.tmpDir, minEigVals)
np.savetxt('%s/maxEigVals_hell.txt' % env.tmpDir, maxEigVals)
np.savetxt('%s/W_hell.txt' % env.tmpDir, W)
