import env
from env_mse import *

import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns
import time as ti

sns.set()

Ns = [2, 5, 10, 20, 50, 100]
expct_mses_f = np.empty(len(Ns))
std_mses_f = np.empty(len(Ns))
expct_mses_fg = np.empty(len(Ns))
std_mses_fg = np.empty(len(Ns))

Nx = 10000
Nz = 1000

startTime = ti.time()

for iN in range(len(Ns)):
    print(iN)
    N = Ns[iN]
    mses_f = np.empty(Nz)
    mses_fg = np.empty(Nz)

    for nz in range(Nz):
        xs = rho_sample(size=(Nx, n))
        all_fs = np.array([[f(x), f_g(x), f_gN(x, N=N)] for x in xs])
        fs = all_fs[:, 0]
        f_gs = all_fs[:, 1]
        f_gNs = all_fs[:, 2]
        mses_f[nz] = np.average(
            [(fs[i] - f_gNs[i])**2 for i in range(len(xs))])
        mses_fg[nz] = np.average(
            [(f_gs[i] - f_gNs[i])**2 for i in range(len(xs))])

    expct_mses_f[iN] = np.mean(mses_f)
    std_mses_f[iN] = np.std(mses_f)
    expct_mses_fg[iN] = np.mean(mses_fg)
    std_mses_fg[iN] = np.std(mses_fg)

durat = ti.time() - startTime
print('Elapsed seconds: %i' % durat)

cv_mses_f = std_mses_f / expct_mses_f
cv_mses_fg = std_mses_fg / expct_mses_fg

np.savetxt('%s/expct_mses_f.txt' % env.resultsDir, expct_mses_f)
np.savetxt('%s/expct_mses_fg.txt' % env.resultsDir, expct_mses_fg)
np.savetxt('%s/cv_mses_f.txt' % env.resultsDir, cv_mses_f)
np.savetxt('%s/cv_mses_fg.txt' % env.resultsDir, cv_mses_fg)
