import env
import env_mse
import env_hell

import plotters

import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns

sns.set()
sns.set_context('paper', font_scale=1.5)


expct_mses_f = np.loadtxt('%s/expct_mses_f.txt' % env.resultsDir)
expct_mses_fg = np.loadtxt('%s/expct_mses_fg.txt' % env.resultsDir)
cov_mses_f = np.loadtxt('%s/cv_mses_f.txt' % env.resultsDir)
cov_mses_fg = np.loadtxt('%s/cv_mses_fg.txt' % env.resultsDir)

expct_hells = np.loadtxt('%s/expct_hells.txt' % env.resultsDir)
cv_hells = np.loadtxt('%s/cv_hells.txt' % env.resultsDir)

Ns = [2, 5, 10, 20, 50, 100]

L_mse = env_mse.L
L2_mse = env_mse.L2
L_hell = env_hell.L
minEigVals_hell = env_hell.minEigVals_hell
maxEigVals_hell = env_hell.maxEigVals_hell

fig = plt.figure()
axes = plt.gca()
axes.set_yscale('log')
plt.plot(list(range(1, env_mse.n+1)), L_mse, '-o')
plt.xlabel(r'Index $i$')
plt.ylabel(r'Eigenvalue $\lambda_i$')
plt.tight_layout()

plt.figure()
plt.plot(Ns, expct_mses_f, 'o-',
         label=r'$\approx$E$[$MSE$_{f,f_{g_N}}]$')
plt.plot(Ns, expct_mses_fg, 'v-',
         label=r'$\approx$E$[$MSE$_{f_g,f_{g_N}}]$')
plt.xticks(Ns)
plt.xlabel(r'$N$')
plt.legend()
plt.tight_layout()

plt.figure()
ax = plt.gca()
plt.loglog(Ns, 1./np.array(Ns)*10**(0.95*2), '--', label=r'$N^{-1}$')
plt.loglog(Ns, np.sum(L2_mse)/np.array(Ns),
           label=r'Bound for MSE$_{f_g,f_{g_N}}$')
plt.loglog(Ns, expct_mses_f, 'o-',
           label=r'$\approx$E$[$MSE$_{f,f_{g_N}}]$')
plt.loglog(Ns, expct_mses_fg, 'v-',
           label=r'$\approx$E$[$MSE$_{f_g,f_{g_N}}]$')
plt.xticks(Ns)
ax.set_xticklabels(Ns)
plt.xlabel(r'$N$')
plt.legend()
plt.tight_layout()

plt.figure()
plt.plot(Ns, cov_mses_f, 'o-',
         label=r'$\approx$CV$($MSE$_{f,f_{g_N}})$')
plt.plot(Ns, cov_mses_fg, 'v-',
         label=r'$\approx$CV$($MSE$_{f_g,f_{g_N}})$')
plt.xticks(Ns)
plt.ylim([0.0, 0.04])
plt.xlabel(r'$N$')
plt.legend()
plt.tight_layout()

fig = plt.figure()
axes = plt.gca()
axes.set_yscale('log')
plt.plot(list(range(1, env_hell.n+1)), L_hell, '-o')
plt.xlabel(r'Index $i$')
plt.ylabel(r'Eigenvalue $\lambda_i$')
plt.tight_layout()

plt.figure()
axes = plt.gca()
plotters.plotEigVals(L_hell, minEigVals_hell, maxEigVals_hell, axes=axes)
axes.set_xlabel(r'Index $i$')
axes.set_ylabel(r'Eigenvalue $\lambda_i$')

plt.figure()
plt.plot(Ns, expct_hells, '-o', label=r'$\approx$E$[d_H]$')
plt.xticks(Ns)
plt.ylim([0.5, 2.5])
plt.xlabel(r'$N$')
plt.legend()
plt.tight_layout()

plt.figure()
plt.plot(Ns, cv_hells, '-o', label=r'$\approx$CV$(d_H)$')
plt.xticks(Ns)
plt.ylim([0.0, 0.04])
plt.xlabel(r'$N$')
plt.legend()
plt.tight_layout()

plt.show()
