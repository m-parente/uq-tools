import env

import numpy as np


calib_params = env.prior_sample(N=10000)
inter_params = env.fromCalibrationToInterimParameters(calib_params)

# Check if inter_params are in between the specified lower and upper bounds
assert(np.all(inter_params >= env._lbs))
assert(np.all(inter_params <= env._ubs))

# Check rel. distance to lower and upper bounds
assert(np.min((inter_params-env._lbs) / np.abs(env._lbs)) < 1e-4)
assert(np.min((env._ubs-inter_params) / np.abs(env._ubs)) < 1e-4)

print("Test succeeded.")
