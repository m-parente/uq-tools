import env
import model

import numpy as np
import numpy.random as rnd


dir_tmp_it = env.dir_tmp_it

N = 1000

x_samples = np.loadtxt("%s/x_post_samples.txt" % dir_tmp_it)
x_samples = x_samples[rnd.randint(low=0, high=len(x_samples), size=N)]

Gs_post_samples = [model.EbolaModel(post_sample, 0.5*1e-2).qoi()
                   for post_sample in x_samples]
np.savetxt("%s/Gs_post_samples.txt" % dir_tmp_it, Gs_post_samples)
