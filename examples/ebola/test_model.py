import env
import model

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import time

sns.set()


param = env.calib_params_fitted

t0 = time.time()
m = model.EbolaModel(param, dt=0.5*1e-2)
print("Elapsed time: %s s" % (time.time() - t0))

qoi = m.qoi()
qoi_C = qoi
# qoi_C = qoi[:len(env.C_t_obs)]
# qoi_D = qoi[len(env.C_t_obs):env.n_data]

print("QoI: %s" % qoi)
print(env.misfitG(qoi))

# Test if population size is constant
P = m.S + m.E + m.I + m.R + m.D + m.H + m.B
assert(len([e for e in P[1:] if not np.isclose(e, P[0])]) == 0)

data = env.data
data_C = data
# data_C = data[:len(env.C_t_obs)]
# data_D = data[len(env.C_t_obs):env.n_data]

# Plot all components
plt.figure()
plt.plot(m.t, m.S, m.t, m.E, m.t, m.I, m.t, m.R, m.t, m.D, m.t, m.H, m.t, m.B, m.t, m.C)
plt.plot(env.C_t_obs, qoi_C, 'o')
# plt.plot(env.D_t_obs, qoi_D, 'o')
plt.plot(env.C_t_obs, data_C, 'o')
# plt.plot(env.D_t_obs, data_D, 'o')
plt.legend(['S', 'E', 'I', 'R', 'D', 'H', 'B', 'C', 'QoI C', 'Data C'])
# plt.legend(['S', 'E', 'I', 'R', 'D', 'H', 'B', 'C', 'QoI C', 'QoI D', 'Data C', 'Data D'])
plt.tight_layout()

# Plot all components except S
plt.figure()
plt.plot(m.t, m.E, m.t, m.I, m.t, m.R, m.t, m.D, m.t, m.H, m.t, m.B, m.t, m.C)
plt.plot(env.C_t_obs, qoi_C, 'o')
# plt.plot(env.D_t_obs, qoi_D, 'o')
plt.plot(env.C_t_obs, data_C, 'o')
# plt.plot(env.D_t_obs, data_D, 'o')
plt.legend(['E', 'I', 'R', 'D', 'H', 'B', 'C', 'QoI C', 'Data C'])
# plt.legend(['E', 'I', 'R', 'D', 'H', 'B', 'C', 'QoI C', 'QoI D', 'Data C', 'Data D'])
plt.tight_layout()

# Plot cumulative component C
plt.figure()
plt.plot(m.t, m.C)
plt.plot(env.C_t_obs, qoi_C, 'o')
plt.plot(env.C_t_obs, data_C, 'o')
plt.legend(['C', 'QoI', 'Data'])
# plt.yscale('log')
plt.tight_layout()

# plt.show()
# exit()

# Plot an adjoint solution
# adj_sol = m._solve_adjoint_problem(env.get_qoi_riesz_repr(20))
adj_sol = m._solve_adjoint_problem2(4, 20.)

plt.figure()
plt.plot(m.t, adj_sol[0, :], m.t, adj_sol[1, :], m.t, adj_sol[2, :], m.t, adj_sol[3, :],
         m.t, adj_sol[4, :], m.t, adj_sol[5, :], m.t, adj_sol[6, :], m.t, adj_sol[7, :])
plt.legend(['S', 'E', 'I', 'R', 'D', 'H', 'B', 'C'])
plt.tight_layout()

plt.show()
