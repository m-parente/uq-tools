import env

import matplotlib.pyplot as plt
import numpy as np


data = env.data
C_calib_params_fitted = [4.00619030e+00, 9.34681800e+00, 1.54521103e+01, 2.24528198e+01,
                         3.05110413e+01, 3.97913774e+01, 5.04788623e+01, 6.27865836e+01,
                         7.69601315e+01, 9.32823452e+01, 1.12078956e+02, 1.33725104e+02,
                         1.58652695e+02, 1.87359243e+02, 2.20417593e+02, 2.58487441e+02,
                         3.02328484e+02, 3.52815606e+02, 4.10956259e+02, 4.77910634e+02,
                         5.55014763e+02, 6.43807213e+02, 7.46059742e+02, 8.63812557e+02,
                         9.99415182e+02, 1.15557319e+03, 1.33540218e+03, 1.54248983e+03,
                         1.78096736e+03, 2.05559219e+03, 2.37184230e+03, 2.73602562e+03,
                         3.15540548e+03, 3.63834512e+03, 4.19447468e+03, 4.83488194e+03,
                         5.57233323e+03, 6.42152679e+03, 7.39938495e+03, 8.52538961e+03,
                         9.79454700e+03, 1.11220939e+04, 1.24444569e+04, 1.37230616e+04,
                         1.49349986e+04, 1.60684181e+04, 1.71188306e+04, 1.80863213e+04,
                         1.89736550e+04, 1.97850596e+04, 2.05254717e+04, 2.12000788e+04,
                         2.18140476e+04, 2.23723684e+04, 2.28797695e+04, 2.33406763e+04,
                         2.37591958e+04, 2.41391175e+04, 2.44839235e+04]


def plot_data_fitted():
    plt.figure()
    plt.plot(env.C_t_obs, data, 'o', alpha=0.6, label=r'Measured data')
    plt.plot(env.C_t_obs, C_calib_params_fitted, 'o-',
             alpha=0.6, label=r'Output for ${\bf x}_{fit}$')
    plt.xlabel('Time $t$ [weeks]')
    plt.ylabel('Cumulative number of cases $C$ [$-$]')
    plt.xticks([0, 10, 20, 30, 40, 50, 60])
    plt.ticklabel_format(style='sci', axis='y', scilimits=(3, 3))
    plt.gca().yaxis.major.formatter._useMathText = True
    plt.legend()
    plt.tight_layout()


if __name__ == '__main__':
    plot_data_fitted()
    plt.show()
