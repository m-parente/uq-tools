import env
import model

import matplotlib.pyplot as plt
import numpy as np


m_ = model.EbolaModel(env.calib_params_fitted)

t = 30
y_ = np.array([6517, 440, 141, 978, 139, 129, 1676, 3051])
f_ = m_._f(y_, t)
jac_f_ = m_._jac_f(y_, t)

ds = np.array([1e-1, 1e-2, 1e-3, 1e-4, 1e-5])

diffs = np.empty((len(y_), len(ds)))

for i in range(len(ds)):
    dy = ds[i]

    y = y_ + dy*np.ones(len(y_))
    f = m_._f(y, t)

    diffs[:, i] = np.abs(f - f_ - np.dot(jac_f_, y-y_)) / np.abs(f_)

plt.figure()
for i in range(len(y_)):
    plt.loglog(ds, diffs[i, :], 'o-', label=r'$e_{%i}$' % (i+1))
plt.loglog(ds, ds, label=r'$1^{st}$ order')
plt.loglog(ds, ds**2, label=r'$2^{nd}$ order')
plt.legend()
plt.xlabel('dx')
plt.ylabel('Error')
plt.tight_layout()

plt.show()
