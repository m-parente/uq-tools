import env
import plotters
from uq_tools import asm

import matplotlib.pyplot as plt
import numpy as np
from sklearn import metrics

import numpy.random as rnd


W = env.W
# W = np.loadtxt("tmp_it_0/as_orig/asm_eigVecs.txt")

xs = env.samples
misfits = env.misfits

# N = 1000
# xs = env.x_samples_prev[rnd.randint(0, len(env.x_samples_prev), size=N)]
# misfits = np.array([env.misfit(x) for x in xs])

# misfits = np.log(misfits)


def plot_poly_surr():
    p = 4  # polynomial order

    for k in range(1, 6):
        W1 = W[:, :k]

        g_regr = asm.response_surface(xs, misfits, W1, poly_order=p)
        print("%i: %f" % (k, metrics.r2_score(misfits, g_regr(np.dot(xs, W1)))))

    plotters.summaryPlot(W[:, 0], xs, misfits, with_fit=True, poly_order=p, alpha=0.7)
    # plotters.summaryPlot2D(W[:, 0], W[:, 1], xs, misfits, with_fit=True, poly_order=p)
    plt.xlabel(r'${\tilde{\bf w}^{(%i)}_1}^\top{\bf x}$' % env.iteration)
    plt.ylabel(r'$f^{\bf d}({\bf x})$')
    plt.legend([r"Samples ${\bf X}^{(1)}_j$", "Polynomial surrogate for $g^{(1)}$"])
    plt.ticklabel_format(style='sci', axis='y', scilimits=(3, 3))
    plt.gca().yaxis.major.formatter._useMathText = True
    plt.tight_layout()


if __name__ == '__main__':
    plot_poly_surr()
    plt.show()
