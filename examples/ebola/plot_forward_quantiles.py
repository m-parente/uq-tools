import env
import model

import matplotlib.pyplot as plt
import numpy as np
from scipy.stats.mstats import mquantiles


noise_level = env.noise_level
data = env.data
C_t_obs = env.C_t_obs

# std_data = noise_level*data
# normal_95quantile = 1.96

titles = ["$\mu^{(0)}(\mathcal{G}^{-1}(\cdot))=\mu_0(\mathcal{G}^{-1}(\cdot))$",
          "$\mu^{(1)}(\mathcal{G}^{-1}(\cdot))$",
          "$\mu^{(2)}(\mathcal{G}^{-1}(\cdot))$"]


def plot_forward_quantiles():
    def plot_forward_quantile(it, axis):
        dir_tmp_it = "tmp_it_%i" % it

        axis.plot(C_t_obs, data, 'o-', alpha=0.6, label='Measured data')
        # axis.fill_between(C_t_obs, data-normal_95quantile*std_data, data+normal_95quantile*std_data,
        #                   alpha=0.2)

        # param = env.calib_params_fitted
        # m = model.EbolaModel(param, dt=0.5*1e-2)
        # axis.plot(C_t_obs, m.qoi())

        # x_samples = np.loadtxt("%s/x_post_samples.txt" % dir_tmp_it)
        # x_samples_mean = np.mean(x_samples, axis=0)
        # qoi_post_mean = model.EbolaModel(x_samples_mean, dt=0.5*1e-2).qoi()
        # print("It. %i, QoI Post. mean: %f" % (it, env.misfitG(qoi_post_mean)))
        # axis.plot(C_t_obs, qoi_post_mean)

        Gs_samples = np.loadtxt("%s/Gs_post_samples.txt" % dir_tmp_it)
        p = 0.95
        median_path = np.median(Gs_samples, axis=0)
        mean_path = np.mean(Gs_samples, axis=0)
        quantiles = mquantiles(Gs_samples, prob=[(1-p)/2, (1+p)/2], alphap=0, betap=1, axis=0)
        axis.plot(C_t_obs, mean_path, '--', color='C2', label="Mean")
        axis.plot(C_t_obs, median_path, color='C1', label="Median")
        axis.fill_between(C_t_obs, quantiles[0, :], quantiles[1, :],
                          color='C1', alpha=0.2, label="$[q_{0.025},q_{0.975}]$")
        axis.set_yscale('log')
        axis.set_ylim([1, 5*10**5])
        axis.set_title(titles[it+1])

        if it == -1:
            axis.set_ylabel('Cumulat. number of cases $C$ [$-$]')
            axis.legend(loc="upper left")

        # Histogram of samples at a fixed time
        # plt.figure()
        # plt.hist(Gs_samples[:, 50], bins=20)

    fig, axes = plt.subplots(3, 1, figsize=(8, 12))
    for it in np.arange(-1, 2):
        ax = axes[it+1]
        plot_forward_quantile(it, ax)

    ax.set_xlabel('Time $t$ [weeks]')

    plt.tight_layout()


if __name__ == '__main__':
    plot_forward_quantiles()
    plt.show()
