import env
import model

import numpy as np
import sys


dir = env.dir_as_G_jacG


def run(id, sample):
    print("Sample %i" % id)

    m = model.EbolaModel(sample, dt=0.5*1e-2)
    qoi = m.qoi()
    jac = m.jac()

    np.savetxt('%s/G%d.txt' % (dir, id), qoi)
    np.savetxt('%s/jacG%d.txt' % (dir, id), jac)


def usage():
    print("usage: id parameters")


if __name__ == "__main__":
    if len(sys.argv) == env.n_param + 2:
        run(int(sys.argv[1]), np.array(sys.argv[2:], dtype=float))
    else:
        usage()
