import env
import plotters

import matplotlib.pyplot as plt
import numpy as np
import pickle


k = env.k

with open("%s/gpr0_%id.bin" % (env.dir_tmp_it, k), 'rb') as gpr_file:
    gpr0 = pickle.load(gpr_file)

ys = gpr0.xs

if k == 1:
    ys_plt = np.linspace(np.min(ys), np.max(ys), num=1000)
    plotters.plot_gpr_1d(gpr0, ys_plt, log=False)
elif k == 2:
    y1s_plt = np.linspace(np.min(ys[:, 0]), np.max(ys[:, 0]), num=100)
    y2s_plt = np.linspace(np.min(ys[:, 1]), np.max(ys[:, 1]), num=100)
    plotters.plot_gpr_2d(gpr0, y1s_plt, y2s_plt)

plt.show()
