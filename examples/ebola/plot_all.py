import env

from plot_as import plot_as
from plot_as_distances import plot_as_distances
from plot_data_fitted import plot_data_fitted
from plot_forward_quantiles import plot_forward_quantiles
from plot_jac_verif import plot_jac_verif
from plot_poly_surr import plot_poly_surr
from plot_posterior_stats import plot_posterior_stats
from plot_summary_it_0_1 import plot_summary_it_0_1


import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns

plt.rcParams["patch.force_edgecolor"] = True
sns.set()
sns.set_style('whitegrid')
sns.set_context('paper', font_scale=1.5)
mpl.rcParams['lines.linewidth'] = 2


# plot_as()
# plot_as_distances()
# plot_data_fitted()
# plot_forward_quantiles()
# plot_jac_verif()
# plot_poly_surr()
# plot_posterior_stats()
# plot_summary_it_0_1()

plt.show()
