import env
import plotters
from uq_tools import asm
from uq_tools import mcmc
from uq_tools.approx import AdaptiveGPR

import numpy as np
import numpy.linalg as la
import numpy.random as rnd
from sklearn.gaussian_process.kernels import RBF
import sys
import time as tm

import matplotlib.pyplot as plt


k = env.k
W1, W2 = env.W1, env.W2

# Construct the response surface -----------------
xs = env.samples
ys = np.dot(xs, W1)
misfits = env.misfits

poly_order = 4
resp_surface = asm.response_surface(xs, misfits, W1, poly_order=poly_order)

# kernel = 1.0 * RBF(length_scale=np.ones(k))
# # noise_var = 0.121515  # 1D, It. 0
# noise_var = 220890  # 2D, It. 1
# # resp_surface = AdaptiveGPR(ys, misfits, kernel=kernel, noise_var=3*noise_var,
# #                            n_restart_optimizer=20, log=True)
# resp_surface = AdaptiveGPR(ys, misfits, kernel=kernel, noise_var=noise_var,
#    n_restart_optimizer = 20, log = False)
# ------------------------------------------------

# plotters.summaryPlot2D(W1[:, 0], W1[:, 1], xs, misfits, with_fit=True, poly_order=6)
# plt.show()
# exit()

prior_samples = env.prior_sample(N=10000)
prior_y = asm.marginalPriorY(prior_samples, W1, kde_bandwidth=0.12, kde_kernel='gaussian')

# y0 = ys[np.argmin(misfits)]
# y0 = np.zeros(k)
y0 = np.array([0.46])  # , 0.0, 0.0])  # , 0.0])  # , 0.0])

y_samples = asm.as_mcmc_with_response_surface(
    lambda y: resp_surface(y)*0.01,
    W1, W2,
    proposal_sampler=lambda yk: rnd.normal(loc=yk, scale=0.065),
    priorY=prior_y,
    y1=y0,
    steps=25000,
    nPlotAccptRate=500)

burnin = 5000
y_samples = y_samples[burnin:]
np.savetxt('%s/y_post_samples_%id.txt' % (env.dir_tmp_it, k), y_samples)

plotters.plot_density_diag(y_samples)
plt.show()
