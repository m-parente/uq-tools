import env
import model

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

sns.set()


x_ = env.calib_params_fitted
p_ = env.fromCalibrationToPhysicalParameters(np.array([x_]))[0]
m_ = model.EbolaModel(x_)
jac_ = m_.jac_p_x()

ds = np.array([1e-2, 1e-3, 1e-4, 1e-5, 1e-6])
diffs = np.empty((env.n_param, len(ds)))

for i in range(len(ds)):
    dx = ds[i]
    print("dx: %f" % dx)

    x = x_ + dx*np.ones(env.n_param)
    p = env.fromCalibrationToPhysicalParameters(np.array([x]))[0]

    diffs[:, i] = np.abs(p - p_ - np.dot(jac_, x-x_)) / np.abs(p_)

for i in range(3):
    plt.figure()
    for j in range(i*env.n_param//3, (i+1)*env.n_param//3):
        plt.loglog(ds, diffs[j, :], 'o-', label=r'$e_{%i}$' % (j+1))
    plt.loglog(ds, ds, label=r'$1^{st}$ order')
    plt.loglog(ds, ds**2, label=r'$2^{nd}$ order')
    plt.legend()
    plt.xlabel('dx')
    plt.ylabel('Error')
    plt.tight_layout()

plt.show()
