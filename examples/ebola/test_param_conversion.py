import env

import numpy as np


calib_params = env.prior_sample(N=10000)

assert(np.allclose(calib_params,
                   env.fromPhysicalToCalibrationParameters(
                       env.fromCalibrationToPhysicalParameters(calib_params))))

print("Test succeeded.")
