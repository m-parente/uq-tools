import env
import plotters
from uq_tools import asm
from uq_tools import mcmc
from uq_tools.approx import AdaptiveGPR

import numpy as np
import numpy.linalg as la
import numpy.random as rnd
from sklearn.gaussian_process.kernels import RBF
import sys
import time as tm

import matplotlib.pyplot as plt


k = env.k
W1, W2 = env.W1, env.W2

prior_y = asm.marginalPriorY(env.prior_samples, W1, kde_bandwidth=0.12, kde_kernel='gaussian')

# ys = np.dot(xs, W1)
# y0 = ys[np.argmin(misfits)]
y0 = np.array([0.46, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])


def prior_cond_sampler(y):
    return asm.choose_uniform_cond_sample(
        env.x_samples_prev, y, W1, W2, sampler=None, y_tol=1*1e-1)


print("Started MCMC algorithm...")
y_samples = asm.as_mcmc_with_averaged_misfit(
    W1, W2,
    misfit=env.misfit,
    proposal_sampler=lambda yk: rnd.normal(loc=yk, scale=0.00175),
    priorY=prior_y,
    prior_cond_sampler=prior_cond_sampler,
    y1=y0,
    M=20,
    steps=25000,
    nPlotAccptRate=10)

# y_samples = asm.as_mcmc_with_response_surface(
#     lambda y: resp_surface(y)**1.,
#     W1, W2,
#     proposal_sampler=lambda yk: rnd.normal(loc=yk, scale=0.0175),
#     priorY=prior_y,
#     y1=y0,
#     steps=25000,
#     nPlotAccptRate=500)

burnin = 5000
y_samples = y_samples[burnin:]
# np.savetxt('%s/y_post_samples_%id.txt' % (env.dir_tmp_it, k), y_samples)

plotters.plot_density_diag(y_samples)
plt.show()
