import env
import plotters
from uq_tools.approx import AdaptiveGPR

import matplotlib.pyplot as plt
import numpy as np
from sklearn.gaussian_process.kernels import RBF


k = env.k
W1 = env.W1

xs = env.samples
ys = np.dot(xs, W1)

data = env.misfits
# data = np.log(data)

kernel = 1.0 * RBF(length_scale=np.ones(k))
gpr = AdaptiveGPR(ys, data, kernel=kernel, noise_var=0.121515,
                  n_restart_optimizer=20, log=True)  # 1D, It. 0
# gpr = AdaptiveGPR(ys, data, kernel=kernel, noise_var=218889, n_restart_optimizer=20)  # 2D, It. 1
# gpr = AdaptiveGPR(ys, data, kernel=kernel, noise_var=628352,
#                   n_restart_optimizer=20, log=False)  # 1D, It. 2
# gpr = AdaptiveGPR(ys, data, kernel=kernel, noise_var=291129,
#                   n_restart_optimizer=20, log=False)  # 2D, It. 2

if k == 1:
    ys_plt = np.linspace(-1.5, 1.5, num=100)
    plotters.plot_gpr_1d(gpr, ys_plt, log=True)
elif k == 2:
    y1s_plt = np.linspace(0.2, 0.7, num=100)
    y2s_plt = np.linspace(-1.5, 1.5, num=100)
    plotters.plot_gpr_2d(gpr, y1s_plt, y2s_plt, log=False)

plt.show()
