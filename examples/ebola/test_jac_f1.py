import env
import model

import matplotlib.pyplot as plt
import numpy as np


m_ = model.EbolaModel(env.calib_params_fitted)

t = 30
y1_ = np.array([6517, 440, 141, 978, 139, 129])
f1_ = m_._f1(y1_, t)
jac_f1_ = m_._jac_f1(y1_, t)

ds = np.array([1e-1, 1e-2, 1e-3, 1e-4, 1e-5])

diffs = np.empty((len(y1_), len(ds)))

for i in range(len(ds)):
    dy = ds[i]

    y1 = y1_ + dy*np.ones(len(y1_))
    f1 = m_._f1(y1, t)

    diffs[:, i] = np.abs(f1 - f1_ - np.dot(jac_f1_, y1-y1_)) / np.abs(f1_)

plt.figure()
for i in range(len(y1_)):
    plt.loglog(ds, diffs[i, :], 'o-', label=r'$e_{%i}$' % (i+1))
plt.loglog(ds, ds, label=r'$1^{st}$ order')
plt.loglog(ds, ds**2, label=r'$2^{nd}$ order')
plt.legend()
plt.xlabel('dx')
plt.ylabel('Error')
plt.tight_layout()

plt.show()
