import env
import plotters
from uq_tools.approx import AdaptiveGPR
from uq_tools import asm
from uq_tools.mcmc import mh_mcmc_post
from uq_tools.stats.regression import ExtendedGPR
from uq_tools.sampling import adapt_mh_temp
from uq_tools.stats import metrics

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd
import pickle
import seaborn as sns
from sklearn.gaussian_process.kernels import Matern, RBF

# rnd.seed(1)
# sns.set()


k = env.k
W1 = env.W1

xs = env.samples
ys = np.dot(xs, W1)

data = np.array([env.misfitG(G) for G in env.Gs])

# kernel = 1.0 * Matern(length_scale=np.ones(k), nu=1.5)
kernel = 1.0 * RBF(length_scale=np.ones(k))

# Estimate noise for (possibly logarithmic) data
# gpr = ExtendedGPR(kernel=kernel, n_restarts_optimizer=20)
# # gpr.fit(ys, np.log(data))
# gpr.fit(ys, data)

# estim_noise_var = gpr.estimate_noise_var()
# estim_noise_var = 0.144894  # 1D, It. 0
estim_noise_var = 218889  # 2D, It. 1
print("Estimated noise variance: %f" % estim_noise_var)
# exit()
##


# Estimate prior on active subspace
prior_samples = env.prior_sample(N=10000)
prior_y = asm.marginalPriorY(prior_samples, W1, kde_bandwidth=0.12, kde_kernel="gaussian")
# ys_plt = np.linspace(-1.7, 1.7, num=100)
# plt.figure()
# plt.plot(ys_plt, prior_y(ys_plt[:, np.newaxis]))
# plotters.plot_contour(ys_plt, ys_plt, prior_y)
# plt.show()
# exit()
##

# gpr = AdaptiveGPR(xs=ys, data=data, kernel=kernel,
#                   noise_var=3*estim_noise_var, n_restart_optimizer=20, log=True)
gpr = AdaptiveGPR(xs=ys, data=data, kernel=kernel,
                  noise_var=estim_noise_var, n_restart_optimizer=20, log=False)

gpr0 = gpr.copy()
# with open("%s/gpr0_%id.bin" % (env.dir_tmp_it, k), 'wb') as gpr_file:
#     pickle.dump(gpr0, gpr_file)
# exit()


def posterior_y(gpr):
    # return lambda y: np.exp(-gpr(y)/1e3)*prior_y(y)
    return lambda y: np.exp(-gpr(y)/1e3)*prior_y(y)


if k == 1:
    ys_plt = np.linspace(np.min(ys), np.max(ys), num=100)

    # plt.figure()
    # plt.plot(ys_plt, (posterior_y(gpr0))(ys_plt[:, np.newaxis]))
    plotters.plot_gpr_1d(gpr, ys_plt, log=True)
    plotters.plot_gpr_1d(gpr, ys_plt)
elif k == 2:
    y1s_plt = np.linspace(np.min(ys[:, 0]), np.max(ys[:, 0]), num=100)
    y2s_plt = np.linspace(np.min(ys[:, 1]), np.max(ys[:, 1]), num=100)

    plotters.plot_contour(y1s_plt, y2s_plt, posterior_y(gpr0))
    # plotters.plot_gpr_2d(gpr, ys_plt, ys_plt, log=True)
    plotters.plot_gpr_2d(gpr, y1s_plt, y2s_plt)
# plt.show()
# exit()

# y0 = ys[np.argmin(data)]
y0 = np.array([0.4, 0.0])
# print(y0)
# exit()

# Iteration 0 ###############
L = 50
T = 100
K = 100
# c = 0.0001  # 1D
c = 0.001  # 2D
# c = 0.00001  # 3D
tol_kl = 1.0

# 1D
# samples, temps, update_pts, \
#     avg_dists, kl_divs = adapt_mh_temp(misfit_approx=gpr,
#                                        misfit_full=env.gN,
#                                        prior=prior_y,
#                                        proposal_sampler=lambda xk: rnd.normal(
#                                            loc=xk, scale=0.05),
#                                        x0=y0,
#                                        n_steps=20000,
#                                        uncert_tol0=150,
#                                        L=L, T=T, K=K, c=c, tol_kl=tol_kl,
#                                        proposal_sampler_kl=lambda yk: rnd.normal(loc=yk, scale=0.015))

# # 2D
samples, temps, update_pts, \
    avg_dists, kl_divs = adapt_mh_temp(misfit_approx=gpr,
                                       misfit_full=env.gN,
                                       prior=prior_y,
                                       proposal_sampler=lambda xk: rnd.normal(
                                           loc=xk, scale=0.0075),
                                       #    proposal_sampler=lambda yk: rnd.multivariate_normal(
                                       #        mean=yk, cov=np.diag([0.015, 0.005])),
                                       x0=y0,
                                       n_steps=40000,
                                       uncert_tol0=50,
                                       L=L, T=T, K=K, c=c, tol_kl=tol_kl,
                                       proposal_sampler_kl=lambda yk: rnd.normal(loc=yk, scale=0.015))

# # 3D
# samples, temps, update_pts, \
#     avg_dists, kl_divs = adapt_mh_temp(misfit_approx=gpr,
#                                        misfit_full=env.gN,
#                                        prior=prior_y,
#                                        proposal_sampler=lambda xk: rnd.normal(
#                                            loc=xk, scale=0.002),
#                                        x0=y0,
#                                        n_steps=30000,
#                                        uncert_tol0=100,
#                                        L=L, T=T, K=K, c=c, tol_kl=tol_kl,
#                                        proposal_sampler_kl=lambda yk: rnd.normal(loc=yk, scale=0.015))
#############################


# Iteration 1 ##############
# L = 50
# T = 100
# K = 100
# # c = 0.00001  # 1D
# c = 0.00001  # 2D
# tol_kl = 0.1

# # 1D
# samples, temps, update_pts, \
#     avg_dists, kl_divs = adapt_mh_temp(misfit_approx=gpr,
#                                        misfit_full=env.gN,
#                                        prior=prior_y,
#                                        proposal_sampler=lambda xk: rnd.normal(
#                                            loc=xk, scale=0.015),
#                                        x0=y0,
#                                        n_steps=20000,
#                                        uncert_tol0=50,
#                                        L=L, T=T, K=K, c=c, tol_kl=tol_kl,
#                                        proposal_sampler_kl=lambda yk: rnd.normal(loc=yk, scale=0.015))

# # 2D
# samples, temps, update_pts, \
#     avg_dists, kl_divs = adapt_mh_temp(misfit_approx=gpr,
#                                        misfit_full=env.gN,
#                                        prior=prior_y,
#                                        #    proposal_sampler=lambda xk: rnd.normal(
#                                        #        loc=xk, scale=0.0075),
#                                        proposal_sampler=lambda yk: rnd.multivariate_normal(
#                                            mean=yk, cov=np.diag([0.025, 0.05])),
#                                        x0=y0,
#                                        n_steps=40000,
#                                        uncert_tol0=100,
#                                        L=L, T=T, K=K, c=c, tol_kl=tol_kl,
#                                        proposal_sampler_kl=lambda yk: rnd.multivariate_normal(
#                                            mean=yk, cov=np.diag([0.025, 0.05])))
#############################

# Iteration 2 ##############
# L = 50
# T = 100
# K = 100
# # c = 0.001  # 1D
# # c = 0.0001  # 2D
# c = 0.0001  # 3D
# tol_kl = 1.

# # # 2D
# # samples, temps, update_pts, \
# #     avg_dists, kl_divs = adapt_mh_temp(misfit_approx=gpr,
# #                                        misfit_full=env.gN,
# #                                        prior=prior_y,
# #                                        proposal_sampler=lambda xk: rnd.normal(
# #                                            loc=xk, scale=0.015),
# #                                        x0=y0,
# #                                        n_steps=20000,
# #                                        uncert_tol0=30,
# #                                        L=L, T=T, K=K, c=c, tol_kl=tol_kl,
# #                                        proposal_sampler_kl=lambda yk: rnd.normal(loc=yk, scale=0.015))

# # 3D
# samples, temps, update_pts, \
#     avg_dists, kl_divs = adapt_mh_temp(misfit_approx=gpr,
#                                        misfit_full=env.gN,
#                                        prior=prior_y,
#                                        proposal_sampler=lambda xk: rnd.normal(
#                                            loc=xk, scale=0.01),
#                                        x0=y0,
#                                        n_steps=20000,
#                                        uncert_tol0=50,
#                                        L=L, T=T, K=K, c=c, tol_kl=tol_kl,
#                                        proposal_sampler_kl=lambda yk: rnd.normal(loc=yk, scale=0.01))
#############################

# def temp(uncert):
#     return 1/(c*uncert**2 + 1)


# def misfit(y):
#     mf_approx_y, uncert_y = gpr(y, return_uncert=True)
#     temp_y = temp(uncert_y)
#     return mf_approx_y*temp_y


# samples_fine = mh_mcmc_post(misfit=misfit,
#                             prior=prior_y,
#                             proposal_sampler=lambda yk: rnd.normal(loc=yk, scale=0.1),
#                             x0=samples[-1, :],
#                             steps=20000,
#                             nPlotAccptRate=500)
# print("KL(gpr||gpr0): %f" % metrics.kl_div_post(gpr, gpr0, samples_fine))

np.savetxt("%s/y_post_samples_%id.txt" % (env.dir_tmp_it, k), samples_fine)
# with open("%s/gpr_adapt_%id.bin" % (env.dir_tmp_it, k), 'wb') as gpr_file:
#     pickle.dump(gpr, gpr_file)
# exit()

if k == 1:
    plt.figure()
    plt.plot(ys_plt, posterior_y(gpr)(ys_plt[:, np.newaxis]))
    # plotters.plotSamples(samples_fine, bins=40)
    plotters.plot_gpr_1d(gpr, ys_plt, log=False)
    plotters.plot_gpr_1d(gpr, ys_plt, log=True)
else:
    plotters.plot_density_diag(samples_fine)

    if k == 2:
        plotters.plot_contour(y1s_plt, y2s_plt, posterior_y(gpr))
        plotters.plot_gpr_2d(gpr, y1s_plt, y2s_plt, log=False)

sns.set()
plotters.plot_adapt_mh_stats(samples, temps, update_pts, avg_dists, L, kl_divs)

print("Number updates: %i" % len(update_pts))
print(kl_divs)

plt.show()
