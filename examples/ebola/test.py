import env
import model
import plotters
from uq_tools import asm

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd
import seaborn as sns


k = env.k

W1 = env.W1
W2 = env.W2

prior_samples = env.prior_sample(N=10000)
# ys_ext = rnd.uniform(low=0.4, high=0.6, size=200)
ys_ext = rnd.normal(loc=0.46, scale=0.05, size=200)
if k == 1:
    ys_ext = ys_ext[:, np.newaxis]
xs_ext = [np.dot(W1, y) +
          np.dot(W2, asm.choose_uniform_cond_sample(
                 prior_samples, y, W1, W2, sampler=None, y_tol=5*1e-3)) for y in ys_ext]
Gs_ext = [model.EbolaModel(x, dt=0.5*1e-2).qoi() for x in xs_ext]
np.savetxt("%s/x_samples_ext.txt" % env.dir_tmp_it, xs_ext)
np.savetxt("%s/Gs_ext.txt" % env.dir_tmp_it, Gs_ext)
exit()

y_samples = np.loadtxt("%s/y_post_samples_%id.txt" % (env.dir_tmp_it, k))
# y_samples = np.loadtxt("%s/x_samples_prev.txt" % env.dir_tmp_it)
if len(np.shape(y_samples)) == 1:
    y_samples = y_samples[:, np.newaxis]
plotters.plot_density_diag(y_samples[:, :2])
plt.show()
exit()

# np.savetxt("%s/y_post_samples_%id_new.txt" % (env.dir_tmp_it, k),
#            rnd.uniform(low=(0.4, -0.85, -0.85),
#                        high=(0.65, 0.0, 0.3),
#                        size=(20000, 3)))
# samples = np.loadtxt("%s/y_samples_prev_1d.txt" % env.dir_tmp_it)
# plt.figure()
# plt.plot(samples, 'o')
# plt.show()
# exit()

np.savetxt("%s/y_post_samples_%id.txt" % (env.dir_tmp_it, k), np.ones(20000)*0.42)
exit()

plt.show()
