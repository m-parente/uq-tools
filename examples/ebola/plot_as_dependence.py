import env
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import numpy as np
import numpy.linalg as la
import seaborn as sns

sns.set()
sns.set_context('paper', font_scale=1.5)
sns.set_style('whitegrid')
mpl.rcParams['lines.linewidth'] = 2


def get_eigvec_matrix(it):
    return np.loadtxt('tmp_it_%i/%s/asm_eigVecs.txt' % (it, env.settings.dir_as))

# L denotes the maximum number of iterations done.
# Let W0 denote the eigenvector matrix from iteration 0, and WL from iteration L.
# This script computes how many columns k0 in W0 are needed such that the corresponding span contains
# the span of kL columns from WL.
# The resulting pairs are plotted.

# To check whether the column space of a matrix B is contained in the column space of a matrix A is
# performed by the following procedure.
# Set C = [A B] and compare rank(C) == rank(A).
# In our case, we have to check if ran([WL[:,:kL]]) \subseteq ran([W0[:,:k0]]), i.e.,
# A = ran([W0[:,:k0]]) and B = ran([WL[:,:kL]]).


W0 = get_eigvec_matrix(0)
L = 2
WL = get_eigvec_matrix(L)

n = np.shape(W0)[0]

kL_max = 2
kLs = np.arange(1, kL_max+1)
k0s = np.empty(len(kLs))

for i in range(len(kLs)):
    kL = kLs[i]
    WLkL = WL[:, :kL]

    for k0 in range(1, n+1):
        W0k0 = W0[:, :k0]
        A = np.hstack((W0k0, WLkL))
        _, s, _ = la.svd(A)

        rank_A = la.matrix_rank(A, tol=1e-2)
        rank_W0k0 = k0  # by orthogonality

        if k0 < 5:
            print(s, rank_A, k0)

        if rank_A == rank_W0k0:
            k0s[i] = k0
            print("end")
            break

print(k0s)

plt.figure()
plt.plot(kLs, k0s, 'o-')
plt.xticks(kLs)
plt.yticks(k0s)
# plt.gca().xaxis.set_major_locator(MaxNLocator(integer=True))
# plt.gca().yaxis.set_major_locator(MaxNLocator(integer=True))
plt.tight_layout()
plt.show()
