import env
import model

import numpy as np


param = env.calib_params_fitted

m = model.EbolaModel(param, dt=1e-3)
m._solve_forward_problem()

S, E, I, R, D, H, B, C = m.y

x = m.eta_interv*(1-m.delta_H)*m.gamma_H + (m.gamma_H+m.kappa)*(1-m.delta)*m.gamma
y = m.eta_interv*m.theta_interv + (m.gamma_H+m.kappa)*m.beta_interv
z = (1-m.delta_H)*m.gamma_H*m.beta_interv - (1-m.delta)*m.gamma*m.theta_interv
u = m.eta_interv*m.delta_H*m.gamma_H + (m.gamma_H+m.kappa)*m.delta*m.gamma
v = m.delta_H*m.gamma_H*m.beta_interv - m.delta*m.gamma*m.theta_interv

T = int(env.T_interv/m.dt)
# N = S[T] + E[T] + I[T] + R[T] # Assumption is that N(t) is constant for t >= T_interv
N = S + E + I + R

V1 = x*np.log(S) + y/N*R + z/N*H + m.phi_interv/(m.b_interv*N)*x*B
V2 = u*np.log(S) + y/N*D + v/N*H + (m.phi_interv/(m.b_interv*N)*u + y/N)*B


def is_invariant(V):
    return len([i for i in range(len(V1)-1) if not np.isclose(V1[i], V1[i+1])]) == 0


assert(is_invariant(V1))
assert(is_invariant(V2))

print("Test succeeded.")
