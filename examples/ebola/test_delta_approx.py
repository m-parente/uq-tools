import env
import model

import numpy as np


m = model.EbolaModel(env.prior_sample(), dt=1e-2)

t = m.t
dt = m.dt

# Use trapezoidal rule on the time mesh of the forward problem
for t_obs in env.C_t_obs:
    riesz_repr = env.get_qoi_riesz_repr(t_obs)
    approxs = riesz_repr(t)

    val = 0.5*dt*(approxs[0] + approxs[-1] + 2*np.sum(approxs[1:-1]))
    assert(np.allclose(1., val))

print("Test succeeded.")
