import env
from uq_tools.stats.regression import ExtendedGPR

from glob import glob
import natsort
import numpy as np
from sklearn.gaussian_process.kernels import RBF


# k = 1
# W1 = np.loadtxt("tmp_it_1/as/asm_eigVecs.txt")
# W1_1 = W1[:, :k]

# xs = np.loadtxt("tmp_it_0/as_G_jacG/samples.txt")
# ys = np.dot(xs, W1_1)

# G_files = natsort.natsorted(glob('tmp_it_0/as_G_jacG/G*.txt'))
# Gs = np.array([np.loadtxt(G_file) for G_file in G_files])
# data = np.array([env.misfitG(G) for G in Gs])

k = env.k
W1 = env.W1

xs = env.samples
ys = np.dot(xs, W1)

data = env.misfits

# data = np.log(data)

kernel = 1.0 * RBF(length_scale=np.ones(k))
gpr = ExtendedGPR(kernel=kernel, n_restarts_optimizer=20)

estim_noise = gpr.estimate_noise_var(ys, data)

print("Estimated noise level: %f" % estim_noise)
