import env
import model

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd
import seaborn as sns

sns.set()


dt = 0.5*1e-2

# x_ = env.calib_params_fitted
x_ = env.prior_sample()
s_ = env.inv_proxy(x_)
m_ = model.EbolaModel(x_, dt=dt)

print("Compute proxy misfit at s_...")
G_ = m_.qoi()
mf_ = env.misfitG(G_)

print("Compute gradient of proxy misfit at s_...")
jacG_ = m_.jac()
# np.savetxt("%s/jacG_.txt" % env.dir_tmp, jacG_)
# jacG_ = np.loadtxt("%s/jacG_.txt" % env.dir_tmp)
grad_mf_ = env.grad_misfitG(G_, jacG_)
grad_mf_proxy_ = env.jac_proxy(s_).T @ grad_mf_

dss = np.array([1e-1, 1e-2, 1e-3, 1e-4, 1e-5])
diffs = np.empty(len(dss))

for i, ds in enumerate(dss):
    print("ds: %f" % ds)

    s = s_ + ds*np.ones(env.n_param)
    x = env.proxy(s)
    m = model.EbolaModel(x, dt=dt)
    G = m.qoi()
    mf = env.misfitG(G)
    diffs[i] = np.abs(mf - mf_ - np.dot(grad_mf_proxy_, s-s_)) / np.abs(mf_)

plt.figure()
plt.loglog(dss, diffs, 'o-')
plt.loglog(dss, dss, label=r'$1^{st}$ order')
plt.loglog(dss, dss**2, label=r'$2^{nd}$ order')
plt.legend()
plt.xlabel('dx')
plt.ylabel('Error')
plt.tight_layout()

plt.show()
