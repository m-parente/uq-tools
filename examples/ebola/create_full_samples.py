import env
from uq_tools import asm

import numpy as np
import numpy.random as rnd


dir_tmp_it = env.dir_tmp_it

k = env.k
W1 = env.W1
W2 = env.W2

y_samples = np.loadtxt("%s/y_post_samples_%id.txt" % (dir_tmp_it, k))
if k == 1:
    y_samples = y_samples[:, np.newaxis]
# print(np.shape(y_samples))
# exit()

x_samples = [np.dot(W1, y) +
             np.dot(W2, asm.choose_uniform_cond_sample(
                 env.x_samples_prev, y, W1, W2, sampler=None, y_tol=5*1e-3)) for y in y_samples]
# exit()

np.savetxt("%s/x_post_samples.txt" % dir_tmp_it, x_samples)
