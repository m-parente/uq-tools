import env
import model

import matplotlib.pyplot as plt
import numpy as np


t = 50

x_ = env.calib_params_fitted
p_ = env.fromCalibrationToPhysicalParameters(np.array([x_]))[0]
m_ = model.EbolaModel(x_)
y = m_._solve_forward_problem()[:, int(t/m_.dt)]
f_ = m_._f(y, t)
jac_f_p_ = m_._jac_f_p(y, t)

# print jac_f_p_
# exit()

ds = np.array([1e-1, 1e-2, 1e-3, 1e-4, 1e-5])

diffs = np.empty((len(f_), len(ds)))

for i in range(len(ds)):
    dx = ds[i]

    x = x_ + dx*np.ones(env.n_param)
    # x = x_ + dx*np.concatenate((np.zeros(15), [1], np.zeros(5)))
    p = env.fromCalibrationToPhysicalParameters(np.array([x]))[0]
    m = model.EbolaModel(x)
    f = m._f(y, t)

    diffs[:, i] = np.abs(f - f_ - np.dot(jac_f_p_, p-p_)) / np.abs(f_)

plt.figure()
for i in range(len(f_)):
    plt.loglog(ds, diffs[i, :], 'o-', label=r'$e_{%i}$' % (i+1))
plt.loglog(ds, ds, label=r'$1^{st}$ order')
plt.loglog(ds, ds**2, label=r'$2^{nd}$ order')
plt.legend()
plt.xlabel('dx')
plt.ylabel('Error')
plt.tight_layout()

plt.show()
