import env

import matplotlib.pyplot as plt
import numpy as np

dir_as = env.dir_as


def plot_as():
    eigvals = env.L
    W = env.W
    min_eigvals = np.loadtxt('%s/asm_minEigVals.txt' % dir_as)
    max_eigvals = np.loadtxt('%s/asm_maxEigVals.txt' % dir_as)

    num_plot_eigvals = 8

    plt.figure()
    ran = range(1, num_plot_eigvals + 1)
    plt.plot(ran, eigvals[:num_plot_eigvals], 'o-', color='C0')
    plt.plot(ran, min_eigvals[:num_plot_eigvals], color='C0')
    plt.plot(ran, max_eigvals[:num_plot_eigvals], color='C0')
    plt.fill_between(ran, min_eigvals[:num_plot_eigvals],
                     max_eigvals[:num_plot_eigvals], facecolor='C0', alpha=0.2)
    plt.xticks(ran)
    plt.xlabel("Eigenvalue index")
    plt.ylabel("Eigenvalue")
    plt.yscale('log')
    plt.tight_layout()

    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
    axes = [ax1, ax2, ax3, ax4]

    for i in range(len(axes)):
        ax = axes[i]
        ax.bar(range(1, env.n_param + 1), W[:, i])
        ax.set_xlabel(r'$\tilde{\bf w}^{(%i)}_%i$' % (env.iteration, i+1))
        ax.set_xticks([1, 5, 10, 15, 20])
        ax.set_yticks([-1, -0.5, 0, 0.5, 1])

    plt.tight_layout()


if __name__ == '__main__':
    plot_as()
    plt.show()
