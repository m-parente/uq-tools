import env

from uq_tools import ode
from uq_tools import quad

import numpy as np
import scipy as sp


class EbolaModel:
    def __init__(self, parameters, dt=1e-2):
        self.calib_params = parameters
        self.phys_params = env.fromCalibrationToPhysicalParameters(np.array([self.calib_params]))[0]
        self.n_param = env.n_param

        self.beta, self.beta_interv, self.t_beta, \
            self.theta, self.theta_interv, self.t_theta, \
            self.phi, self.phi_interv, self.t_phi, \
            self.eta, self.eta_interv, self.t_eta, \
            self.b, self.b_interv, self.t_b, \
            self.gamma, self.alpha, self.gamma_H, \
            self.delta, self.delta_H, self.kappa = self.phys_params

        self.dt = dt
        self.T = env.T
        self.n_steps = int(self.T / self.dt)
        self.y1_0 = env.y1_0
        self.y0 = env.y0
        self.b0 = env.b0
        self.c0 = env.c0
        self.T_interv = env.T_interv
        self.n_states = len(self.y0)

        self.t = np.arange(0., self.T+self.dt, step=self.dt)

    def _t_to_step(self, t):
        fs = np.round(t/self.dt)
        if np.isscalar(fs):
            return int(fs)
        return np.array([int(f) for f in fs], dtype=int)

    def _a(self, t, a, a_interv, t_a):
        return a if t < self.T_interv else \
            a_interv + (a-a_interv) * np.exp(-np.log(2) / t_a * (t-self.T_interv))

    def _beta(self, t):
        return self._a(t, self.beta, self.beta_interv, self.t_beta)

    def _theta(self, t):
        return self._a(t, self.theta, self.theta_interv, self.t_theta)

    def _phi(self, t):
        return self._a(t, self.phi, self.phi_interv, self.t_phi)

    def _eta(self, t):
        return self._a(t, self.eta, self.eta_interv, self.t_eta)

    def _b(self, t):
        return self._a(t, self.b, self.b_interv, self.t_b)

    def _da_da(self, t, a, a_interv, t_a):
        return 1 if t < self.T_interv else \
            np.exp(-np.log(2) / t_a * (t-self.T_interv))

    def _da_da_interv(self, t, a, a_interv, t_a):
        return 0 if t < self.T_interv else \
            1 - np.exp(-np.log(2) / t_a * (t-self.T_interv))

    def _da_dt_a(self, t, a, a_interv, t_a):
        return 0 if t < self.T_interv else \
            (a-a_interv) * np.exp(-np.log(2) / t_a * (t-self.T_interv)) * \
            (np.log(2) / (t_a*t_a)) * (t-self.T_interv)

    def _f1(self, y1, t):
        S, E, I, R, D, H = y1
        N = float(S + E + I + R)
        lambda_ = self._beta(t)*I/N + self._phi(t)*D/N + self._theta(t)*H/N

        return np.array([-lambda_*S,
                         lambda_*S-self.alpha*E,
                         self.alpha*E - (self.gamma+self._eta(t))*I + self.kappa*H,
                         (1-self.delta)*self.gamma*I + (1-self.delta_H)*self.gamma_H*H,
                         self.delta*self.gamma*I + self.delta_H*self.gamma_H*H - self._b(t)*D,
                         self._eta(t)*I - (self.gamma_H+self.kappa)*H])

    def _jac_f1(self, y1, t):
        S, E, I, R, D, H = y1
        N = float(S + E + I + R)
        lambda_ = self._beta(t)*I/N + self._phi(t)*D/N + self._theta(t)*H/N
        dlambda_dSER = -self._beta(t)*I/(N*N) - self._phi(t)*D/(N*N) - self._theta(t)*H/(N*N)
        dlambda_dI = self._beta(t)*(S+E+R)/(N*N) - self._phi(t)*D/(N*N) - self._theta(t)*H/(N*N)
        dlambda_dD = self._phi(t)/N
        dlambda_dH = self._theta(t)/N

        return np.array([[-lambda_-dlambda_dSER*S, -dlambda_dSER*S, -dlambda_dI*S, -dlambda_dSER*S, -dlambda_dD*S, -dlambda_dH*S],
                         [lambda_+dlambda_dSER*S, dlambda_dSER*S-self.alpha,
                             dlambda_dI*S, dlambda_dSER*S, dlambda_dD*S, dlambda_dH*S],
                         [0., self.alpha, -(self.gamma+self._eta(t)), 0., 0., self.kappa],
                         [0., 0., (1-self.delta)*self.gamma, 0., 0.,
                             (1-self.delta_H)*self.gamma_H],
                         [0., 0., self.delta*self.gamma, 0., -
                             self._b(t), self.delta_H*self.gamma_H],
                         [0., 0., self._eta(t), 0., 0., -(self.gamma_H+self.kappa)]])

    def _f(self, y, t):
        S, E, I, R, D, H, B, C = y
        return np.concatenate((self._f1(y[:len(self.y1_0)], t), [self._b(t)*D, self.alpha*E]))

    def _jac_f(self, y, t):
        jac_f = np.zeros((self.n_states, self.n_states))
        jac_f[:len(self.y1_0), :len(self.y1_0)] = self._jac_f1(y[:6], t)
        jac_f[6, 4] = self._b(t)
        jac_f[7, 1] = self.alpha
        return jac_f

    def _solve_forward_problem(self):
        if hasattr(self, 'y'):
            return self.y

        rtol_newt = 1e-6
        y1 = ode.bw_euler(self._f1, self._jac_f1, dt=self.dt, y0=self.y1_0, n_steps=self.n_steps,
                          rtol_newt=rtol_newt, atol_newt=rtol_newt*np.array([1e4, 1e1, 1e1, 1e-2, 1e0, 1e0]))

        self.S, self.E, self.I, self.R, self.D, self.H = \
            y1[0, :], y1[1, :], y1[2, :], y1[3, :], y1[4, :], y1[5, :]

        bs = [self._b(t) for t in self.t]
        self.B = np.concatenate([[self.b0], self.b0 + self.dt*np.cumsum(bs[1:]*self.D[1:])])
        self.C = np.concatenate([[self.c0], self.c0 + self.alpha*self.dt*np.cumsum(self.E[1:])])

        self.y = np.vstack([y1, self.B, self.C])
        return self.y

    def _solve_forward_problem2(self):
        from scipy.integrate import solve_ivp

        def fun(t, y):
            return self._f(y, t)

        sol = sp.integrate.solve_ivp(fun, [0., self.T], self.y0, method='RK45',
                                     dense_output=True, t_eval=self.t,
                                     rtol=1e-6, atol=1e-6)
        self.t = sol.t
        self.y = sol.y
        self.sol = sol.sol

        y = sol.y
        self.S, self.E, self.I, self.R, self.D, self.H, self.B, self.C = \
            y[0, :], y[1, :], y[2, :], y[3, :], y[4, :], y[5, :], y[6, :], y[7, :]

        return self.y

    def qoi(self):
        if hasattr(self, '_qoi'):
            return self._qoi

        self._solve_forward_problem2()

        self._qoi = self.C[self._t_to_step(env.C_t_obs)]
        # self._qoi = np.concatenate(
        #     (self.C[self._t_to_step(env.C_t_obs)],
        #      self.D[self._t_to_step(env.D_t_obs)]))

        # riesz_reprs = [env.get_qoi_riesz_repr(env.C_t_obs[i]) for i in range(env.n_data)]
        # delta_approx = np.array([[riesz_repr(t) for t in self.t] for riesz_repr in riesz_reprs])
        # I = np.multiply(delta_approx, self.C)
        # self._qoi = quad.trapez_rule(I, self.dt)

        return self._qoi

    def _solve_adjoint_problem(self, riesz_repr):
        self._solve_forward_problem()

        def A(step):
            return -self._jac_f(self.y[:, step], step*self.dt).T

        def b(step):
            b = np.zeros(self.n_states)
            b[7] = riesz_repr(step*self.dt)
            return -b

        return ode.reverse_cn_linear(A, b, np.zeros(len(self.y0)), self.T, self.dt)

    def _solve_adjoint_problem2(self, ind_variable, t_obs):
        self._solve_forward_problem2()

        def A(t):
            return -self._jac_f(self.sol(t), t).T

        def fun(t, y):
            return np.dot(A(t), y)

        yt_obs = np.zeros(self.n_states)
        yt_obs[ind_variable] = 1.
        t_eval = [t for t in self.t[::-1] if t <= t_obs]
        adj_sol = sp.integrate.solve_ivp(fun, [t_obs, 0.], yt_obs, method='RK45',
                                         t_eval=t_eval,
                                         rtol=1e-6, atol=1e-6)

        # Append correct amount of zeros and return
        return np.concatenate((adj_sol.y[:, ::-1], np.zeros((len(adj_sol.y), len(self.t)-len(t_eval)))), axis=1)

    def _jac_f_p(self, y, t):
        S, E, I, R, D, H, B, C = y
        N = float(S + E + I + R)
        jac_f_p = np.zeros((self.n_states, self.n_param))

        # df1
        jac_f_p[0, 0] = -self._da_da(t, self.beta, self.beta_interv, self.t_beta) * I / N * S
        jac_f_p[0, 1] = -self._da_da_interv(t, self.beta, self.beta_interv, self.t_beta) * I / N * S
        jac_f_p[0, 2] = -self._da_dt_a(t, self.beta, self.beta_interv, self.t_beta) * I / N * S

        jac_f_p[0, 3] = -self._da_da(t, self.theta, self.theta_interv, self.t_theta) * H / N * S
        jac_f_p[0, 4] = -self._da_da_interv(t, self.theta,
                                            self.theta_interv, self.t_theta) * H / N * S
        jac_f_p[0, 5] = -self._da_dt_a(t, self.theta, self.theta_interv, self.t_theta) * H / N * S

        jac_f_p[0, 6] = -self._da_da(t, self.phi, self.phi_interv, self.t_phi) * D / N * S
        jac_f_p[0, 7] = -self._da_da_interv(t, self.phi, self.phi_interv, self.t_phi) * D / N * S
        jac_f_p[0, 8] = -self._da_dt_a(t, self.phi, self.phi_interv, self.t_phi) * D / N * S

        # df2
        jac_f_p[1, :9] = -jac_f_p[0, :9]
        jac_f_p[1, 16] = -E

        # df3
        jac_f_p[2, 9] = -self._da_da(t, self.eta, self.eta_interv, self.t_eta) * I
        jac_f_p[2, 10] = -self._da_da_interv(t, self.eta, self.eta_interv, self.t_eta) * I
        jac_f_p[2, 11] = -self._da_dt_a(t, self.eta, self.eta_interv, self.t_eta) * I

        jac_f_p[2, 15] = -I
        jac_f_p[2, 16] = E
        jac_f_p[2, 20] = H

        # df4
        jac_f_p[3, 15] = (1-self.delta) * I
        jac_f_p[3, 17] = (1-self.delta_H) * H
        jac_f_p[3, 18] = -self.gamma * I
        jac_f_p[3, 19] = -self.gamma_H * H

        # df5
        jac_f_p[4, 12] = -self._da_da(t, self.b, self.b_interv, self.t_b) * D
        jac_f_p[4, 13] = -self._da_da_interv(t, self.b, self.b_interv, self.t_b) * D
        jac_f_p[4, 14] = -self._da_dt_a(t, self.b, self.b_interv, self.t_b) * D

        jac_f_p[4, 15] = self.delta * I
        jac_f_p[4, 17] = self.delta_H * H
        jac_f_p[4, 18] = self.gamma * I
        jac_f_p[4, 19] = self.gamma_H * H

        # df6
        jac_f_p[5, 9:12] = -jac_f_p[2, 9:12]
        jac_f_p[5, 17] = -H
        jac_f_p[5, 20] = -H

        # df7
        jac_f_p[6, 12:15] = -jac_f_p[4, 12:15]

        # df8
        jac_f_p[7, 16] = E

        return jac_f_p

    def jac_p_x(self):
        if hasattr(self, '_jac_p_x'):
            return self._jac_p_x

        diag = np.empty(21)
        idx_lin = np.concatenate((list(range(0, 9)), [11, 14, 18, 19, 20]))
        diag[idx_lin] = 0.5*(env._ubs[idx_lin]-env._lbs[idx_lin])
        idx_inv = np.array([9, 10, 12, 13, 15, 16, 17])
        diag[idx_inv] = 0.5*(env._lbs[idx_inv]-env._ubs[idx_inv]) \
            * np.power(0.5*(env._ubs[idx_inv]-env._lbs[idx_inv])
                       * self.calib_params[idx_inv] + 0.5*(env._ubs[idx_inv]+env._lbs[idx_inv]), -2)
        self._jac_p_x = np.diag(diag)

        return self._jac_p_x

    def jac(self):
        if hasattr(self, '_jac'):
            return self._jac

        jac_p = np.empty((env.n_data, env.n_param))

        ts_obs = env.C_t_obs
        inds_variable = 7*np.ones(len(env.C_t_obs), dtype=int)
        # ts_obs = np.concatenate((env.C_t_obs, env.D_t_obs))
        # inds_variable = np.concatenate(
        #     (7*np.ones(len(env.C_t_obs), dtype=int),
        #      4*np.ones(len(env.D_t_obs), dtype=int)))

        for i in range(env.n_data):
            t_obs = ts_obs[i]
            ind_variable = inds_variable[i]

            # riesz_repr = env.get_qoi_riesz_repr(t_obs)
            # phi = self._solve_adjoint_problem(riesz_repr)
            phi = self._solve_adjoint_problem2(ind_variable, t_obs)

            # Use trapezoidal rule for integral
            # I = np.transpose([np.dot(self._jac_f_p(self.y[:, step], step*self.dt).T, phi[:, step])
            #                   for step in range(self.n_steps+1)])
            # jac_p[i, :] = quad.trapez_rule(I, self.dt)

            # Use midpoint rule for integral
            I = np.transpose([np.dot(self._jac_f_p(self.y[:, step+1], (step+1)*self.dt).T,
                                     phi[:, step+1] + phi[:, step]) for step in range(self.n_steps)])
            jac_p[i, :] = 0.5*self.dt * np.sum(I, axis=1)

        # NOTE: Apply chain rule for scaled parameters
        self._jac = np.dot(jac_p, self.jac_p_x())

        return self._jac


class AbstractEbolaModel:
    def instantiate(self, parameters):
        return EbolaModel(parameters, dt=1e-2)
