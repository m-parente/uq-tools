import env
import plotters
from uq_tools.approx import AdaptiveGPR

import matplotlib.pyplot as plt
import matplotlib.legend_handler as legend_handler
import numpy as np
from sklearn.gaussian_process.kernels import RBF


def plot_summary_it_0_1():
    k = 1
    W0 = np.loadtxt("tmp_it_0/as/asm_eigVecs.txt")
    W0_1 = W0[:, :k]

    W1 = np.loadtxt("tmp_it_1/as/asm_eigVecs.txt")
    W1_1 = W1[:, :k]

    xs = env.samples
    y0s = np.dot(xs, W0_1)
    y1s = np.dot(xs, W1_1)

    misfits = env.misfits

    xs_ext = np.loadtxt("tmp_it_0/x_samples_ext.txt")
    y0s_ext = np.dot(xs_ext, W0_1)
    y1s_ext = np.dot(xs_ext, W1_1)
    Gs_ext = np.loadtxt("tmp_it_0/Gs_ext.txt")
    misfits_ext = [env.misfitG(G) for G in Gs_ext]

    y0s_plt = np.linspace(np.min(y0s)-0.075, np.max(y0s)+0.075, num=100)
    plt.plot(y0s, misfits, 'o', color='C1', alpha=0.5, label=r"Samples ${\bf X}_j$", zorder=2)
    plt.plot(y0s_ext, misfits_ext, 'o', color='C7', alpha=0.5,
             label="Samples manually added", zorder=1)
    plt.yscale('log')
    plt.ylim([5*10**2, 5*10**6])
    plt.xlabel(r'${\tilde{\bf w}_1}^\top{\bf x}$')
    plt.ylabel(r'$f^{\bf d}({\bf x})$')
    plt.legend()
    plt.tight_layout()

    fig, (ax1, ax2) = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(12, 5))

    y0s_plt = np.linspace(np.min(y0s)-0.075, np.max(y0s)+0.075, num=100)
    kernel = 1.0 * RBF(length_scale=np.ones(k))
    gpr = AdaptiveGPR(y0s, misfits, kernel=kernel, noise_var=0.121515,
                      n_restart_optimizer=20, log=True)

    plotters.plot_gpr_1d(gpr, y0s_plt, log=True, title=False,
                         uncert_band=False, ax=ax1, alpha=0.7, zorder=4)
    pts1 = ax1.scatter(y0s_ext, misfits_ext, c='C3', alpha=0.5, zorder=3)
    ax1.set_yscale('log')
    ax1.set_xlabel(r'${\tilde{\bf w}^{(0)}_1}^\top{\bf x}$')
    ax1.set_ylabel(r'$f^{\bf d}({\bf x})$')
    ax1.set_ylim([5*10**2, 5*10**6])

    # y1s_plt = np.linspace(np.min(y1s)-0.075, np.max(y1s)+0.075, num=100)
    ax2.scatter(y1s, misfits, c='C1', alpha=0.5)
    pts2 = ax2.scatter(y1s_ext, misfits_ext, c='C2', alpha=0.5)
    ax2.set_yscale('log')
    ax2.set_xlabel(r'${\tilde{\bf w}^{(1)}_1}^\top{\bf x}$')

    ax1.legend([r"Samples ${\bf X}^{(0)}_j$ from It. 0", "GPR surrogate for $\log(g^{(0)})$"])
    ax2.legend([(pts1, pts2)], ['Same samples manually added'], numpoints=1,
               handler_map={tuple: legend_handler.HandlerTuple(ndivide=None)})

    plt.tight_layout()


if __name__ == '__main__':
    plot_summary_it_0_1()
    plt.show()
