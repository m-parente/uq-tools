import env
import model

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd
import seaborn as sns

sns.set()


# dt = env.dt
dt = 0.5*1e-2

x_ = env.calib_params_fitted
m_ = model.EbolaModel(x_, dt=dt)

print("Compute QoI at x_...")
G_ = m_.qoi()

print("Compute Jacobian at x_...")
jacG_ = m_.jac()

ds = np.array([1e-1, 1e-2, 1e-3, 1e-4, 1e-5])
diffs = np.empty((env.n_data, len(ds)))

for i in range(len(ds)):
    dx = ds[i]
    print("dx: %f" % dx)

    x = x_ + dx*np.ones(env.n_param)
    m = model.EbolaModel(x, dt=dt)
    G = m.qoi()

    diffs[:, i] = np.abs(G - G_ - np.dot(jacG_, x-x_)) / np.abs(G_)

# np.savetxt("%s/diffs_jac.txt" % env.dir_tmp, diffs)

# Plot only some components randomly
# if len(diffs) > 10:
#     diffs = diffs[rnd.choice(len(diffs), 10), :]
diffs = diffs[[0, 10, 20, 30, 40, 50], :]

plt.figure()
for i in range(len(diffs)):
    plt.loglog(ds, diffs[i, :], 'o-', label=r'$e_{%i}$' % (i+1))
plt.loglog(ds, ds, label=r'$1^{st}$ order')
plt.loglog(ds, ds**2, label=r'$2^{nd}$ order')
plt.legend()
plt.xlabel('dx')
plt.ylabel('Error')
plt.tight_layout()

plt.show()
