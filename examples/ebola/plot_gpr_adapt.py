import env
import plotters

import matplotlib.pyplot as plt
import numpy as np
import pickle


k = env.k

with open("%s/gpr_adapt_%id.bin" % (env.dir_tmp_it, k), 'rb') as gpr_file:
    gpr_adapt = pickle.load(gpr_file)

ys = gpr_adapt.xs

if k == 1:
    ys_plt = np.linspace(np.min(ys), np.max(ys), num=1000)
    plotters.plot_gpr_1d(gpr_adapt, ys_plt, log=True)
elif k == 2:
    y1s_plt = np.linspace(np.min(ys[:, 0]), np.max(ys[:, 0]), num=100)
    y2s_plt = np.linspace(np.min(ys[:, 1]), np.max(ys[:, 1]), num=100)
    plotters.plot_gpr_2d(gpr_adapt, y1s_plt, y2s_plt, log=True)

plt.show()
