import model
from misc import Settings
from uq_tools import asm
from uq_tools import bayinv as bi
from uq_tools import utils

from glob import glob
import natsort
import numpy as np
import numpy.random as rnd

# rnd.seed(1)


settings = Settings('settings')
_s = settings

_daysPerWeek = 7.

_lbs = np.array([_s.beta_lb,
                 _s.beta_interv_lb,
                 _s.t_beta_lb / _daysPerWeek,
                 _s.theta_lb,
                 _s.theta_interv_lb,
                 _s.t_theta_lb / _daysPerWeek,
                 _s.phi_lb,
                 _s.phi_interv_lb,
                 _s.t_phi_lb / _daysPerWeek,
                 _s.eta_inv_lb / _daysPerWeek,
                 _s.eta_interv_inv_lb / _daysPerWeek,
                 _s.t_eta_lb / _daysPerWeek,
                 _s.b_inv_lb / _daysPerWeek,
                 _s.b_interv_inv_lb / _daysPerWeek,
                 _s.t_b_lb / _daysPerWeek,
                 _s.gamma_inv_lb / _daysPerWeek,
                 _s.alpha_inv_lb / _daysPerWeek,
                 _s.gamma_H_inv_lb / _daysPerWeek,
                 _s.delta_lb,
                 _s.delta_H_lb,
                 _s.kappa_lb])
_ubs = np.array([_s.beta_ub,
                 _s.beta_interv_ub,
                 _s.t_beta_ub / _daysPerWeek,
                 _s.theta_ub,
                 _s.theta_interv_ub,
                 _s.t_theta_ub / _daysPerWeek,
                 _s.phi_ub,
                 _s.phi_interv_ub,
                 _s.t_phi_ub / _daysPerWeek,
                 _s.eta_inv_ub / _daysPerWeek,
                 _s.eta_interv_inv_ub / _daysPerWeek,
                 _s.t_eta_ub / _daysPerWeek,
                 _s.b_inv_ub / _daysPerWeek,
                 _s.b_interv_inv_ub / _daysPerWeek,
                 _s.t_b_ub / _daysPerWeek,
                 _s.gamma_inv_ub / _daysPerWeek,
                 _s.alpha_inv_ub / _daysPerWeek,
                 _s.gamma_H_inv_ub / _daysPerWeek,
                 _s.delta_ub,
                 _s.delta_H_ub,
                 _s.kappa_ub])

n_param = len(_lbs)
dt = _s.dt
T = _s.T
y1_0 = np.array(_s.y1_0)
b0 = _s.b0
c0 = _s.c0
y0 = np.concatenate([y1_0, [b0, c0]])
T_interv = _s.T_interv

C_t_obs = np.arange(1, 60)
# C_t_obs = np.array([40])
D_t_obs = np.arange(1, 60)
# D_t_obs = np.array([40])
n_data = len(C_t_obs)  # + len(D_t_obs)


def fromCalibrationToPhysicalParameters(calib_params):
    return fromInterimToPhysicalParameters(fromCalibrationToInterimParameters(calib_params))


def fromPhysicalToCalibrationParameters(phys_params):
    return fromInterimToCalibrationParameters(fromPhysicalToInterimParameters(phys_params))


def fromCalibrationToInterimParameters(calib_params):
    return 0.5*(_ubs-_lbs)*calib_params + 0.5*(_ubs+_lbs)


def fromInterimToCalibrationParameters(inter_params):
    return 2./(_ubs-_lbs)*inter_params - (_ubs+_lbs)/(_ubs-_lbs)


def fromInterimToPhysicalParameters(inter_params):
    phys_params = inter_params

    phys_params[:, 9] = 1./inter_params[:, 9]  # eta
    phys_params[:, 10] = 1./inter_params[:, 10]  # eta_interv
    phys_params[:, 12] = 1./inter_params[:, 12]  # b
    phys_params[:, 13] = 1./inter_params[:, 13]  # b_interv
    phys_params[:, 15] = 1./inter_params[:, 15]  # gamma
    phys_params[:, 16] = 1./inter_params[:, 16]  # alpha
    phys_params[:, 17] = 1./inter_params[:, 17]  # gamma_H

    return phys_params


def fromPhysicalToInterimParameters(phys_params):
    inter_params = phys_params

    inter_params[:, 9] = 1./phys_params[:, 9]  # eta
    inter_params[:, 10] = 1./phys_params[:, 10]  # eta_interv
    inter_params[:, 12] = 1./phys_params[:, 12]  # b
    inter_params[:, 13] = 1./phys_params[:, 13]  # b_interv
    inter_params[:, 15] = 1./phys_params[:, 15]  # gamma
    inter_params[:, 16] = 1./phys_params[:, 16]  # alpha
    inter_params[:, 17] = 1./phys_params[:, 17]  # gamma_H

    return inter_params


def prior_sample(N=1):
    return rnd.uniform(low=-1.0, high=1.0, size=(N, n_param)) if N > 1 else \
        rnd.uniform(low=-1.0, high=1.0, size=n_param)


def prior(x):
    return 2**-n_param if np.all(np.logical_and(x <= 1.0, x >= -1.0)) else 0


def _inv(x):
    return 1./x


phys_params_fitted = np.array([0.532, 0.505, 12.5/_daysPerWeek,
                               0.328, 0.095, 11.1/_daysPerWeek,
                               2.104, 1.115, 10.3/_daysPerWeek,
                               _inv(4.8/_daysPerWeek), _inv(4.1/_daysPerWeek), 27.1/_daysPerWeek,
                               _inv(5.4/_daysPerWeek), _inv(4.9/_daysPerWeek), 21.2/_daysPerWeek,
                               _inv(10.4/_daysPerWeek), _inv(10.0/_daysPerWeek),
                               _inv(4.6/_daysPerWeek), 0.73, 0.61, 0.0025])
calib_params_fitted = fromPhysicalToCalibrationParameters(np.array([phys_params_fitted]))[0]
param_true = calib_params_fitted  # For synthetical inversion


def get_qoi_riesz_repr(t_obs):
    return lambda t: np.sqrt(_s.factor_delta_approx/np.pi) * np.exp(-_s.factor_delta_approx * (t-t_obs)*(t-t_obs))


data_file = _s.data_file
data = np.loadtxt(data_file)
noise_level = _s.noise_level
inv_noise_cov = np.diag(1./(data*noise_level)**2)

bay_inv_pb = bi.BayesianInverseProblem(data, inv_noise_cov,
                                       absPdeProblem=model.AbstractEbolaModel())
misfitG = bay_inv_pb.misfitG
misfit = bay_inv_pb.misfit
grad_misfitG = bay_inv_pb.misfitGradientG


def proxy(s): return 2*utils.cdf_sn(s) - 1


def inv_proxy(x): return utils.icdf_sn((x+1) / 2)


def jac_proxy(s): return np.diag([12*np.exp(-0.5*si**2)/np.sqrt(2*np.pi) for si in s])


n_boots = _s.n_boots

k = _s.k

# Stored files
iteration = _s.iteration
dir_tmp = _s.dir_tmp
dir_tmp_it = "tmp_it_%i" % iteration
dir_as_G_jacG = "%s/%s" % (dir_tmp_it, _s.dir_as_G_jacG)
dir_as = "%s/%s" % (dir_tmp_it, _s.dir_as)

samples = np.loadtxt('%s/samples.txt' % dir_as_G_jacG)

G_files = natsort.natsorted(glob('%s/G*.txt' % dir_as_G_jacG))
jacG_files = natsort.natsorted(glob('%s/jacG*.txt' % dir_as_G_jacG))
Gs = np.array([np.loadtxt(G_file) for G_file in G_files])
jacGs = np.array([np.loadtxt(jacG_file) for jacG_file in jacG_files])

misfits = np.array([misfitG(G) for G in Gs])

L = np.loadtxt('%s/asm_eigVals.txt' % dir_as)
W = np.loadtxt('%s/asm_eigVecs.txt' % dir_as)
W1 = W[:, :k]
W2 = W[:, k:]

# dir_tmp_prev = "%s/tmp_it_prev" % dir_tmp_it
# dir_as_G_jacG_prev = "%s/%s" % (dir_tmp_prev, _s.dir_as_G_jacG)
# dir_as_prev = "%s/%s" % (dir_tmp_prev, _s.dir_as)
# W_prev = np.loadtxt('%s/asm_eigVecs.txt' % dir_as_prev)
# k_prev = 1
# W1_prev = W_prev[:, :k_prev]


# MC approximation for data misfit function on active subspace
# prior_samples = prior_sample(N=100000)
# x_samples_prev = prior_samples
x_samples_prev = np.loadtxt("%s/x_samples_prev.txt" % dir_tmp_it)


def gN(y):
    ys = np.atleast_2d(y)
    M = len(ys)
    N = 1

    misfits = np.empty(len(ys))

    for i in range(M):
        zs = asm.choose_uniform_cond_sample(x_samples_prev, ys[i], W1, W2, y_tol=1.5*1e-1, N=N)
        # misfits[i] = np.average([misfit(np.dot(W1, ys[i]) + np.dot(W2, z)) for z in zs])
        misfits[i] = misfit(np.dot(W1, ys[i]) + np.dot(W2, zs))

    return misfits if M > 1 else misfits[0]
