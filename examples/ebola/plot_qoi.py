import env

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd

# rnd.seed(0)


data = env.data
Gs = env.Gs
Gs = Gs[rnd.randint(0, len(Gs), size=20)]

xs_plt = np.arange(1, np.shape(Gs)[1]+1)
plt.figure()
for G in Gs:
    plt.plot(xs_plt, G)
    print("Misfit: %f" % env.bay_inv_pb.misfitG(G))
plt.plot(xs_plt, data, 'o')

plt.show()
