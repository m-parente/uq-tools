import env
import model

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd
import seaborn as sns

sns.set()


dt = 0.5*1e-2

x_ = env.calib_params_fitted
m_ = model.EbolaModel(x_, dt=dt)

print("Compute misfit at x_...")
G_ = m_.qoi()
mf_ = env.misfitG(G_)

print("Compute gradient of misfit at x_...")
jacG_ = m_.jac()
# np.savetxt("%s/jacG_.txt" % env.dir_tmp, jacG_)
# jacG_ = np.loadtxt("%s/jacG_.txt" % env.dir_tmp)
grad_mf_ = env.grad_misfitG(G_, jacG_)

ds = np.array([1e-1, 1e-2, 1e-3, 1e-4, 1e-5])
diffs = np.empty(len(ds))

for i, dx in enumerate(ds):
    print("dx: %f" % dx)

    x = x_ + dx*np.ones(env.n_param)
    m = model.EbolaModel(x, dt=dt)
    G = m.qoi()
    mf = env.misfitG(G)

    diffs[i] = np.abs(mf - mf_ - np.dot(grad_mf_, x-x_)) / np.abs(mf_)

plt.figure()
plt.loglog(ds, diffs, 'o-')
plt.loglog(ds, ds, label=r'$1^{st}$ order')
plt.loglog(ds, ds**2, label=r'$2^{nd}$ order')
plt.legend()
plt.xlabel('dx')
plt.ylabel('Error')
plt.tight_layout()

plt.show()
