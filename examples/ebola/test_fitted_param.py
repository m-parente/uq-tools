import env

import numpy as np


assert(np.allclose(env.calib_params_fitted,
                   env.fromPhysicalToCalibrationParameters(
                       env.fromCalibrationToPhysicalParameters(np.array([env.calib_params_fitted])))[0]))

print("Test succeeded.")
