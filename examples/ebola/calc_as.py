import env
import plotters
from uq_tools import asm, utils

import numpy as np


settings = env.settings

result = asm.computeActiveSubspaceFromSamples(env.Gs, env.jacGs, env.bay_inv_pb, env.n_boots)

eigvals, eigvecs, min_eigvals, max_eigvals, min_subsperrs, max_subsperrs, mean_subsperrs = result

n_plot_eigvals = 10
# plotters.plotActiveSubspace(eigvals, eigvecs, n_plot_eigvals, min_eigvals,
#                             max_eigvals, min_subsperrs, max_subsperrs, mean_subsperrs)

utils.save_active_subspace(eigvals, eigvecs, min_eigvals, max_eigvals,
                           min_subsperrs, max_subsperrs, mean_subsperrs,
                           dir=env.dir_as)
