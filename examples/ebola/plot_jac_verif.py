import env

import matplotlib.pyplot as plt
import numpy as np


def plot_jac_verif():
    ds = np.array([1e-1, 1e-2, 1e-3, 1e-4, 1e-5])
    idx_comps = [9, 19, 29, 39, 49]

    diffs = np.loadtxt("%s/diffs_jac.txt" % env.dir_tmp)

    plt.figure()
    for idx_comp in idx_comps:
        plt.loglog(ds, diffs[idx_comp, :], 'o-', label=r'$\hat{e}_{%i}$' % (idx_comp+1))
    plt.loglog(ds, ds, '--', label=r'$1^{st}$ order')
    plt.loglog(ds, ds**2, '--', label=r'$2^{nd}$ order')
    plt.legend()
    plt.xlabel('$h$')
    plt.ylabel('Error')
    plt.tight_layout()


if __name__ == '__main__':
    plot_jac_verif()
    plt.show()
