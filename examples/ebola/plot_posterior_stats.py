import env

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns


def plot_posterior_stats():
    x_post_samples = np.loadtxt("%s/x_post_samples.txt" % env.dir_tmp_it)
    # x_post_samples = env.fromCalibrationToPhysicalParameters(x_post_samples)
    print(np.shape(x_post_samples))
    print(np.std(x_post_samples, axis=0))

    fig, axes = plt.subplots(3, 7)

    for i in range(env.n_param):
        ax = axes[int(i/7), i % 7]
        sns.distplot(x_post_samples[:, i], bins=10, norm_hist=True, kde=False, ax=ax)

    plt.tight_layout()


if __name__ == '__main__':
    plot_posterior_stats()
    plt.show()
