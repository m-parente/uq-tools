import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import numpy as np
import numpy.linalg as la
from scipy import stats
import seaborn as sns

sns.set()
sns.set_context('paper', font_scale=1.5)
sns.set_style('whitegrid')
mpl.rcParams['lines.linewidth'] = 2

n = 21

W0 = stats.ortho_group.rvs(dim=n)
W1 = W0 + 1e-14  # stats.ortho_group.rvs(dim=n)

k1_max = 4
k1s = np.arange(1, k1_max+1)
k0s = np.empty(len(k1s))

for i in range(len(k1s)):
    k1 = k1s[i]
    W1k1 = W1[:, :k1]

    for k0 in range(1, n+1):
        W0k0 = W0[:, :k0]
        A = np.hstack((W0k0, W1k1))
        _, s, _ = la.svd(A)

        rank_A = la.matrix_rank(A, tol=1e-3)
        rank_W0k0 = k0  # by orthogonality

        if k0 < 5:
            print(s, rank_A, k0)

        if rank_A == rank_W0k0:
            k0s[i] = k0
            print("end")
            break

print(k0s)

plt.figure()
plt.plot(k1s, k0s, 'o-')
plt.xticks(k1s)
plt.yticks(k0s)
# plt.gca().xaxis.set_major_locator(MaxNLocator(integer=True))
# plt.gca().yaxis.set_major_locator(MaxNLocator(integer=True))
plt.tight_layout()
plt.show()
