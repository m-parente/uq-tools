import env

import plotters as plot
from uq_tools import asm

import numpy as np
import numpy.random as rnd
from sklearn import metrics

import matplotlib.pyplot as plt

rnd.seed(0)


xs = env.prior_sample(size=20)  # int(np.ceil(10*env.n*np.log(env.n))))
fs = np.array([env.f(x) for x in xs])

g_regr = asm.response_surface(xs, fs, env.W1, poly_order=2)

if env.k == 1:
    plot.summaryPlot(env.W1[:, 0], xs, fs, with_fit=True,
                     plot_range=np.linspace(-4, 4))
elif env.k == 2:
    plot.summaryPlot2D(env.W1[:, 0], env.W1[:, 1], xs, fs, with_fit=True)

plt.show()

# y1s = y2s = np.linspace(-2, 2, 100)
# plot.plotContour(y1s, y2s, lambda y: np.abs(env.g(y) - g_regr(y)))

ys_prior = env.prior_y_sample(size=1000)
gys = [env.g(y) for y in ys_prior]
mse_g_gregr = np.sum(
    [(gy - g_regr(y))**2 for y, gy in zip(ys_prior, gys)]) / float(len(ys_prior))
expct_g = np.sum(gys) / float(len(gys))
print(mse_g_gregr, expct_g, np.sqrt(mse_g_gregr)/expct_g)
