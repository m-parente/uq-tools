from misc import Settings
from uq_tools import utils

import numpy as np
import numpy.linalg as la
import scipy.stats as stat


settings = Settings('settings')

n = settings.n
sigma = settings.noise_level
x_true = settings.x_true

# np.random.seed(0)
A_W = stat.ortho_group.rvs(dim=n)
A_L = np.diag(
    10**np.insert(np.linspace(1.8, 1.0, 8, endpoint=False), [0, 1], [2., 1.99]))

A = np.dot(np.dot(A_W, A_L), A_W.T)
data = np.dot(A, x_true)
print("Data: %s" % data)
Gamma_inv = np.diag(1./(sigma*data)**2)

tmp_M = np.dot(np.dot(A.T, Gamma_inv), A)
C = np.dot(np.dot(tmp_M, np.eye(n)+np.outer(x_true, x_true)), tmp_M)

L, W = utils.getEigenpairs(C)

assert(np.allclose(np.dot(W, W.T), np.eye(n)))

np.savetxt('%s/C.txt' % settings.tmp_dir, C)
np.savetxt('%s/A.txt' % settings.tmp_dir, A)
