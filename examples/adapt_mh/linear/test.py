import env
import plotters
from uq_tools import approx

import matplotlib.pyplot as plt
import numpy as np
import numpy.linalg as la
import numpy.random as rnd
from sklearn.gaussian_process.gpr import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import ConstantKernel as C, Matern, RBF, WhiteKernel

# rnd.seed(0)


settings = env.settings

n = 10

w = rnd.uniform(low=-1.0, high=1.0, size=n)
w = w / la.norm(w)

eps = 0.75


def g(y):
    # return 0.5*(y+1.5)**2
    return y**4 - 6*y**2


def f(x):
    return g(np.dot(w, x)) + eps*la.norm(x)


# xs = rnd.normal(size=(200, n))
xs = rnd.randn(200, n)
print(xs)

fs = [f(x) for x in xs]
ys = np.dot(xs, w)

plt.plot(ys, fs, 'o')

# ys_plt = np.linspace(-2., 2., num=100)
# gs_plt = np.array([g(y) for y in ys_plt])
# plt.plot(ys_plt, gs_plt)

plt.show()
