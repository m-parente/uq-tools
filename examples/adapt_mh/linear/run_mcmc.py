import env
import plotters
from uq_tools import asm
from uq_tools import mcmc
from uq_tools import approx

import numpy as np
import numpy.linalg as la
import numpy.random as rnd
from sklearn import gaussian_process as gp
from sklearn.gaussian_process.kernels import ConstantKernel as C, Matern, RBF, WhiteKernel
import sys
import time as tm

import matplotlib.pyplot as plt

rnd.seed(0)


settings = env.settings

mcmc_dir = env.mcmc_dir

W1, W2 = env.W1, env.W2

# Construct the approximation for g --------------
xs = env.prior_sample(20)  # size=int(np.ceil(10*env.n*np.log(env.n))))
# misfits = np.array([env.f(x) for x in xs])

# resp_surface = asm.response_surface(xs, misfits, W1, poly_order=2)

ys = np.dot(xs, W1)
# ys = np.linspace(-1., 1., 8)[:, np.newaxis]
misfits = np.array([env.g_(y) for y in ys])

# Store as test data
# np.savetxt('tmp/xs_test_gp_2d.txt', ys)
# np.savetxt('tmp/data_test_gp_2d.txt', misfits)
# exit()


kernel = C(1.0, constant_value_bounds=(1e-10, 1e10)) * \
    RBF(length_scale=1.0, length_scale_bounds=(1e-5, 1e5)) + \
    WhiteKernel(noise_level=1.0, noise_level_bounds=(1e-5, 1e5))
# kernel = 1.0*Matern(length_scale=1.0, length_scale_bounds=(1e-5, 1e5))
g_approx = approx.GaussianProcessRegressor(ys, misfits, kernel=kernel)

xs_plt = np.linspace(-4, 4, num=20)
plotters.plot_gpr_1d(g_approx, np.linspace(-4, 4, num=100))
# plotters.plot_gpr_2d(g_approx, xs_plt, xs_plt)
plt.show()
exit()
# ------------------------------------------------
exit()

# print g_approx(np.array([[1.0]]))
# exit()

# ys_plt = np.linspace(-2.5, 2.5, 100)
# plotters.plot_gpr_1d(g_approx, ys_plt)
# plotters.summaryPlot(W1[:, 0], xs, misfits, with_fit=True)

# plt.show()
# exit()

if len(sys.argv) < 2:
    start = tm.time()

    # y_samples = asm.as_mcmc_with_response_surface(
    #     resp_surface,
    #     W1, W2,
    #     proposal_sampler=lambda yk: rnd.normal(loc=yk, scale=0.1),
    #     priorY=env.prior_y,
    #     y1=np.zeros(env.k),
    #     steps=5 * 10**4,
    #     nPlotAccptRate=1000)

    # exit()

    max_updates = 500

    # y_samples, beta_avg, ids_updates, misfit_approxes, xs_updates, diffs_updates = asm.as_mcmc_adapt_2stage_full(
    #     W1, W2,
    #     g_approx=g_approx,
    #     misfit_full=env.f,
    #     prior_y=env.prior_y,
    #     proposal_sampler=lambda yk: rnd.normal(loc=yk, scale=0.05),
    #     cond_prior_sampler=env.prior_z_y_sample,
    #     N=1000,
    #     y1=np.zeros(env.k),
    #     n_steps=1500,
    #     subchain_length=100,
    #     uncert_g_tol=1. * 1e-1,
    #     max_updates=max_updates,
    #     finite_adaption_factor=1e-1,
    # )

    # y_samples, avg_alpha, avg_theta, ids_updates, misfit_approxes, xs_updates, diffs_updates = asm.as_mcmc_adapt_2stage_eps(
    #     W1, W2,
    #     g_approx=g_approx,
    #     misfit_full=env.f,
    #     prior_y=env.prior_y,
    #     proposal_sampler=lambda yk: rnd.normal(loc=yk, scale=0.1),
    #     cond_prior_sampler=env.prior_z_y_sample,  # lambda y, _: env.z_samples,
    #     N=1,
    #     y0=np.zeros(env.k),
    #     n_steps=100,
    #     uncert_g_tol=4.5 * 1e-2,
    #     max_updates=max_updates,
    #     finite_adaption_factor=1e-1,
    #     n_finite_adaption_updates=40,
    #     mod_plot_accptrate=5
    # )

    y_samples, avg_theta, ids_updates, misfit_approxes, xs_updates, diffs_updates = asm.as_mcmc_adapt_rnd_eps(
        W1, W2,
        g_approx=g_approx,
        misfit_full=env.f,
        prior_y=env.prior_y,
        proposal_sampler=lambda yk: rnd.normal(loc=yk, scale=0.1),
        cond_prior_sampler=env.prior_z_y_sample,
        N=1,
        y0=np.zeros(env.k),
        n_steps=100,
        uncert_g_tol=10. * 1e-2,
        max_updates=max_updates,
        finite_adaption_factor=1e-1,
        n_finite_adaption_updates=40,
        mod_plot_accptrate=5
    )

    # gpr = misfit_approxes[-1]
    # np.savetxt('tmp/xs_test_gp_1d.txt', gpr.xs)
    # np.savetxt('tmp/data_test_gp_1d.txt', gpr.data)
    # exit()

    # print "Avg. alpha: %f" % avg_alpha
    print("Avg. theta: %f" % avg_theta)
    print("# updates: %i" % len(ids_updates) + \
        (" (maximum # of updates reached)" if len(
            ids_updates) == max_updates else ""))

    plt.figure()
    plt.scatter(ids_updates, diffs_updates)
    plt.xlabel('Update Ids')
    plt.ylabel('Difference value')

    if env.k == 1:
        plt.figure()
        plt.hist(xs_updates, bins=20)
        plt.xlabel('Update location')

        # mf_approx = misfit_approxes[-1]
        # np.savetxt('../test_xs.txt', mf_approx.xs)
        # np.savetxt('../test_data.txt', mf_approx.data)

        xs_plt = np.linspace(-4, 4, 100)
        for i in np.arange(len(ids_updates), step=20):
            mf_approx = misfit_approxes[i]
            plotters.plot_gpr_1d(mf_approx, xs_plt)

        plotters.plot_gpr_1d(misfit_approxes[-1], xs_plt)
    elif env.k == 2:
        plt.figure()
        plt.hist2d(xs_updates[:, 0], xs_updates[:, 1], bins=50, range=((-4, 4), (-4, 4)))
        plt.title('Update locations')

        xs_plt = np.linspace(-4, 4, 20)
        for i in np.arange(len(ids_updates)-151, len(ids_updates), step=50):
            mf_approx = misfit_approxes[i]
            plotters.plot_gpr_2d(mf_approx, xs_plt, xs_plt)

    plt.show()
    exit()

    durat = tm.time() - start

    burn_in = settings.burn_in_act_var
    min_ess, ess, corr_times, acfs = mcmc.stats(
        y_samples, burnIn=burn_in, maxlag=None)
    print("MCMC stats (min ess, ess, corr times): %f,%s,%s" % (
        min_ess, ess, corr_times))

    eff_y_samples = mcmc.pickEffSamples(y_samples, burn_in, maxlag=None)
    print("# Eff. samples: %i" % len(eff_y_samples))
    print("Elapsed seconds: %f" % durat)
    print("# Eff. samples / sec: %f" % (len(eff_y_samples) / durat))

    np.savetxt('%s/y_samples.txt' % mcmc_dir, y_samples)
    np.savetxt('%s/eff_y_samples.txt' % mcmc_dir, eff_y_samples)
    np.savetxt('%s/y_autocorrs.txt' % mcmc_dir, acfs)
    exit()
else:
    if sys.argv[1] == 'f':
        effYSamples = np.loadtxt('%s/effYSamples_5d.txt' % mcmcDir)
    else:
        print("Unknown argument '%s'" % sys.argv[1])
        exit()

effYSamples = effYSamples[rnd.choice(len(effYSamples), 100)]

start = tm.time()
xSamples = asm.activeToOriginalMCMC(effYSamples,
                                    W1, W2,
                                    prior=env.prior,
                                    proposal_sampler=lambda zk: rnd.normal(loc=zk, scale=0.8),
                                    z1=np.zeros(np.shape(W2)[1]),
                                    stepsPerActiveSample=1 * 10**5,
                                    burnIn=settings.burnInInactVar,
                                    nPlotAccptRate=10000)
xSamples = np.concatenate(xSamples)
print("%f sec elapsed." % (tm.time() - start))
print('# of xSamples: %i' % len(xSamples))

np.savetxt('%s/xSamples_5d.txt' % mcmc_dir, xSamples)
