from misc import Settings
from uq_tools import asm
from uq_tools import utils

import numpy as np
import numpy.linalg as la
import numpy.random as rnd
import scipy.stats as stat


settings = Settings('settings')

tmp_dir = settings.tmp_dir
mcmc_dir = settings.mcmc_dir

sigma = settings.noise_level
x_true = np.array(settings.x_true)

n = settings.n
k = settings.k

A = np.loadtxt('%s/A.txt' % tmp_dir)
C = np.loadtxt('%s/C.txt' % tmp_dir)

L, W = utils.getEigenpairs(C)
L1, L2 = L[:k], L[k:]
W_split = np.split(W, [k], axis=1)
W1, W2 = W_split[0], W_split[1]

data = np.dot(A, x_true)
Gamma_inv_sqrt = np.sqrt(np.diag(1./(sigma*data)**2))

prior = stat.multivariate_normal(mean=np.zeros(n)).pdf
prior_y = stat.multivariate_normal(mean=np.zeros(k)).pdf


def prior_sample(size=1):
    return rnd.multivariate_normal(mean=np.zeros(n), cov=np.eye(n), size=size)


def prior_y_sample(size=1):
    return rnd.multivariate_normal(mean=np.zeros(k), cov=np.eye(k), size=size)


def prior_z_y_sample(y=None, size=10):
    return rnd.multivariate_normal(mean=np.zeros(n-k), cov=np.eye(n-k), size=size)


def f(x):
    # return 0.5 * sigma**(-2) * np.dot(np.dot(x.T, la.matrix_power(A, 2)), x)
    return 0.5 * la.norm(np.dot(np.dot(Gamma_inv_sqrt, A), x-x_true), 2)**2


def gN(y, N=10):
    if np.isscalar(y):
        y = np.array([y])
    zs = prior_z_y_sample(y=y, size=N)
    return np.average([f(np.dot(W1, y) + np.dot(W2, z)) for z in zs])


z_samples = prior_z_y_sample(size=100)


def g(y):
    if np.isscalar(y):
        y = np.array([y])
    return np.average([f(np.dot(W1, y) + np.dot(W2, z)) for z in z_samples])


def g_(y):
    if np.isscalar(y):
        y = np.array([y])
    return gN(y, N=1)


def posterior_y(y):
    return np.exp(-g(y)) * prior_y(y)


# assert g(np.array([0, 0])) == 0.5*np.sum(np.sqrt(L2))

# expct_g = 0.5*np.sum(np.sqrt(L))

xs_resp = prior_sample(size=20)  # int(np.ceil(10*n*np.log(n))))
_misfits = np.array([f(x) for x in xs_resp])
# _misfits = np.array([g(y) for y in np.dot(xs_resp, W1)])


resp_surface = asm.response_surface(xs_resp, _misfits, W1, poly_order=2)
