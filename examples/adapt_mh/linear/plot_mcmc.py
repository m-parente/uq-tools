import env

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

sns.set()


y_samples = np.loadtxt('%s/y_samples.txt' % env.mcmc_dir)
eff_y_samples = np.loadtxt('%s/eff_y_samples.txt' % env.mcmc_dir)

plt.figure()
ax_post = plt.gca()
sns.distplot(eff_y_samples, hist=False, ax=ax_post, color='C1')
ax_post.tick_params('y', colors='C1')
ax_prior = ax_post.twinx()
ys = np.linspace(-4, 4, 100)
ax_prior.plot(ys, env.prior_y(ys))
ax_prior.tick_params('y', colors='C0')
ax_prior.grid(False)

plt.figure()
sns.distplot(y_samples, rug=True)

# ys = np.linspace(-4, 4, 100)
# ns = [10, 20, 50, 100, 200, 500, 1000]

# gys = [env.g(y) for y in ys]
# for n in ns:
#     plt.figure()
#     sns.rugplot(y_samples[:n])
#     # plt.plot(ys, env.resp_surface(ys[:, np.newaxis]))
#     plt.plot(ys, [env.resp_surface([[y]])[0] for y in ys])
#     plt.plot(np.dot(env.xs_resp, env.W1),
#              env.resp_surface(np.dot(env.xs_resp, env.W1)), 'o', color='C0')
#     plt.plot(ys, gys)
#     # plt.ylim([1400, 2300])
#     plt.savefig('tmp_pics/mcmc_%i.png' % n)

plt.show()
