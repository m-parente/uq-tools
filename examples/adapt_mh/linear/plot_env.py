import env
import plotters as plot

import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns

sns.set()


ax = plt.subplot()
ax.set_yscale('log')
ax.plot(env.L, 'o-')

# xs = ys = np.linspace(0, 2, 100)
# X, Y = np.meshgrid(xs, ys)
# Z = np.reshape([(x-1)**2+(y-1)**2 for y in ys for x in xs],
#                (len(xs), len(ys))).T
# plt.figure()
# plt.contour(X, Y, Z)
# plt.show()
# exit()

if env.k == 1:
    ys = np.linspace(-4, 4, 100)
    plt.figure()
    plt.plot(ys, [env.prior_y(y) for y in ys])

    ys = np.linspace(-4, 4, 100)
    plt.figure()
    plt.plot(ys, [env.g(y) for y in ys])
    plt.plot(ys, [env.g_(y) for y in ys])

    # plt.plot(ys, [env.posterior_y(y) for y in ys])
    # print [env.posterior_y(y) for y in ys]
elif env.k == 2:
    y1s = y2s = np.linspace(-4, 4, 100)
    plot.plot_contour(y1s, y2s, env.prior_y)

    y1s = np.linspace(-8, 8, 100)
    y2s = np.linspace(-8, 8, 100)
    plot.plot_contour(y1s, y2s, env.g)
    plot.plot_contour(y1s, y2s, env.g_)

    # y1s = np.linspace(-5, 5, 100)
    # y2s = np.linspace(-5, 0, 500)
    # plot.plotSurface(y1s, y2s, env.posterior_y)

plt.show()
