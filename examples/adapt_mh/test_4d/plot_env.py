import env
import plotters

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

sns.set()

ys_plt = np.linspace(-4., 4., num=100)

fig, (ax1, ax2) = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(10, 5))

plotters.plot_contour(ys_plt, ys_plt, env.prior_y, ax=ax1)
plotters.plot_contour(ys_plt, ys_plt, env.unnorm_posterior_y, ax=ax2)
plt.tight_layout()

plotters.plot_contour(ys_plt, ys_plt, env.g)
plt.title(r'$g(y)$')

plt.show()
