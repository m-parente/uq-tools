import env
import plotters
from uq_tools.approx import AdaptiveGPR
from uq_tools.mcmc import mh_mcmc_post
from uq_tools.sampling import adapt_mh_temp
from uq_tools.stats import metrics

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd
import seaborn as sns
from sklearn.gaussian_process.kernels import RBF

rnd.seed(4)
# sns.set()


N = 200
ys = env.prior_y_sample(N=N)

noise = env.noise_var

ds = env.gN(ys)

Y = np.atleast_2d(ys)
kernel = 1.0 * RBF(length_scale=np.ones(env.n))
gpr = AdaptiveGPR(ys, ds, kernel=kernel, noise_var=noise, n_restart_optimizer=10)
gpr0 = gpr.copy()


def posterior_y(gpr):
    return lambda y: np.exp(-gpr(y))*env.prior_y(y)


L = 20
T = 50
K = 100
c = 0.1
tol_kl = 0.05

samples, temps, updates, kl_divs, n_updates = adapt_mh_temp(misfit_approx=gpr,
                                                            misfit_full=env.gN,
                                                            prior=env.prior_y,
                                                            proposal_sampler=lambda xk: rnd.normal(
                                                                loc=xk, scale=0.075),
                                                            x0=np.zeros(env.n),
                                                            n_steps=10000,
                                                            uncert_tol0=10,
                                                            L=L, T=T, K=K, c=c, tol_kl=tol_kl)

samples_fine = mh_mcmc_post(misfit=gpr,
                            prior=env.prior_y,
                            proposal_sampler=lambda yk: rnd.normal(loc=yk, scale=0.075),
                            x0=samples[-1, :],
                            steps=10000,
                            nPlotAccptRate=500)
print("KL(gpr||gpr0): %f" % metrics.kl_div_post(gpr, gpr0, samples_fine))

samples_exct = mh_mcmc_post(misfit=env.g_4d,
                            prior=env.prior_y,
                            proposal_sampler=lambda yk: rnd.normal(loc=yk, scale=0.075),
                            x0=np.array(env.y0),
                            steps=10000,
                            nPlotAccptRate=500)
print("KL(g||gpr): %f" % metrics.kl_div_post(env.g_4d, gpr, samples_exct))
print("KL(g||gpr0): %f" % metrics.kl_div_post(env.g_4d, gpr0, samples_exct))

plotters.plot_density_diag(samples_fine)
plotters.plot_density_diag(samples_exct)

sns.set()
plotters.plot_adapt_mh_stats(samples, temps, updates, L, kl_divs)

update_locs = gpr.xs
fig, (ax1, ax2) = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(12, 8))
ax1.scatter(update_locs[:, 0], update_locs[:, 1], s=2)
ax1.set_xlabel(r"$y_1$")
ax1.set_ylabel(r"$y_2$")
ax2.scatter(update_locs[:, 2], update_locs[:, 3], s=2)
ax2.set_xlabel(r"$y_3$")
ax2.set_ylabel(r"$y_4$")
fig.suptitle("Update locations")
plt.tight_layout()

print("Number updates: %i" % n_updates)

plt.show()
