import numpy as np
import numpy.random as rnd
from scipy.stats import multivariate_normal

n = 4
y0 = np.array([2., 2., 2., 2.])


a1 = 200
a2 = a1
a3 = a1
a4 = a1
c = 10


def g_4d(y):
    y = np.atleast_2d(y)
    return a1*(y[:, 0]-y0[0])**2 + a2*(y[:, 1]-y0[1])**2 + \
        a3*(y[:, 2]-y0[2])**2 + a4*(y[:, 3]-y0[3])**2 + c


prior_dist = multivariate_normal(mean=np.zeros(n), cov=np.eye(n))


def prior_y(y):
    return prior_dist.pdf(y)


def prior_y_sample_4d(N=1):
    return rnd.multivariate_normal(mean=np.zeros(n), cov=np.eye(n)) if N == 1 else \
        rnd.multivariate_normal(mean=np.zeros(n), cov=np.eye(n), size=N)


g = g_4d
prior_y_sample = prior_y_sample_4d

noise_var = 1000


def gN(y):
    y = np.atleast_2d(y)
    N = len(y)
    return g(y)[0] + rnd.normal(loc=0.0, scale=np.sqrt(noise_var)) if N == 1 else \
        g(y) + rnd.multivariate_normal(mean=np.zeros(N), cov=noise_var*np.eye(N))


def unnorm_posterior_y(y):
    return np.exp(-g(y))*prior_y(y)
