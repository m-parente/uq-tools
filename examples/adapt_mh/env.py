import numpy as np
import numpy.random as rnd


numberOfParameters = 2


def misfit(x):
    return (x+1.2)**(-2) + np.exp(x+2.2)
    # return ((x+1.2)**(-2) + np.exp(x+2.2)) * ((y+1.2)**(-2) + np.exp(y+2.2))


def prior(x):
    return np.logical_and(x >= -1, x <= 1)*1
    # return 2**-numberOfParameters if np.all(np.logical_and(x <= 1, x >= -1)) else 0


def prior_sample(n=None):
    return rnd.uniform(low=-1, high=1, size=n) if n != None \
        else rnd.uniform(low=-1, high=1)
    # return rnd.uniform(low=-1, high=1, size=(n, numberOfParameters)) if n != None \
    #     else rnd.uniform(low=-1, high=1, size=numberOfParameters)


def posterior(x):
    return np.exp(-misfit(x))*prior(x)
