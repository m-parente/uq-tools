import env
import plotters
from uq_tools.approx import AdaptiveGPR
from uq_tools.sampling import adapt_mh

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd
from sklearn.gaussian_process.kernels import RBF

rnd.seed(1)


N = 10
ys = env.prior_y_sample(N=N)

noise_var = env.noise_var

ds = env.gN(ys)

Y = np.atleast_2d(ys)
kernel = 1.0 * RBF()
gpr = AdaptiveGPR(ys, ds, kernel=kernel, noise_var=noise_var, n_restart_optimizer=10)

ys_plt = np.linspace(-5., 5., num=100)
# ys_plt = np.linspace(-2.5, 2.5, num=100)

# plotters.plot_gpr_2d(gpr, ys_plt, ys_plt)
# plt.show()
# exit()


def posterior_y(beta):
    return lambda y: np.exp(-beta*gpr(y, return_uncert=False))*env.prior_y(y)


# betas = np.linspace(0, 0.4, num=10)

# for beta in betas:
#     ax = plotters.plot_contour(ys_plt, ys_plt, posterior_y(beta))
#     plotters.plot_contour(ys_plt, ys_plt, env.unnorm_posterior_y, ax=ax)

ys_test = [(1-a)*np.zeros(2) + a*env.y0 for a in np.linspace(0, 1, num=6)]
uncerts = gpr(ys_test)[1]
betas = 1/(1*uncerts+1)

fig, axes = plt.subplots(2, 3, sharex=True, sharey=True, figsize=(15, 10))

for i in range(len(ys_test)):
    y_test = ys_test[i]
    uncert = uncerts[i]
    beta = betas[i]

    ax = axes[int(i/3), i % 3]
    plotters.plot_contour(ys_plt, ys_plt, posterior_y(beta), ax=ax)
    plotters.plot_contour(ys_plt, ys_plt, posterior_y(1), ax=ax)
    ax.scatter(y_test[0], y_test[1], c='r', zorder=3)
plt.tight_layout()

n_updates = adapt_mh(misfit_approx=gpr,
                     misfit_full=env.gN,
                     prior=env.prior_y,
                     proposal_sampler=lambda xk: rnd.normal(loc=xk, scale=0.05),
                     x0=np.zeros(env.n),
                     n_steps=1000,
                     uncert_tol=10)
print("Number updates: %i" % n_updates)

uncerts = gpr(ys_test)[1]
betas = 1/(1*uncerts+1)

fig, axes = plt.subplots(2, 3, sharex=True, sharey=True, figsize=(15, 10))

for i in range(len(ys_test)):
    y_test = ys_test[i]
    uncert = uncerts[i]
    beta = betas[i]

    ax = axes[int(i/3), i % 3]
    plotters.plot_contour(ys_plt, ys_plt, posterior_y(beta), ax=ax)
    plotters.plot_contour(ys_plt, ys_plt, posterior_y(1), ax=ax)
    ax.scatter(y_test[0], y_test[1], c='r', zorder=3)
plt.tight_layout()

print(uncerts)
print(betas)
# exit()

plt.show()
