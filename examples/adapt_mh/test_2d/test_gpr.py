import env
import plotters
from uq_tools.approx import AdaptiveGPR
from uq_tools.stats.regression import ExtendedGPR

import matplotlib.pyplot as plt
import numpy as np
import pickle
from sklearn.gaussian_process.kernels import Matern, RBF, WhiteKernel


with open("tmp/gpr_adapt.bin", 'rb') as gpr_file:
    gpr = pickle.load(gpr_file)

print(gpr.gpr.alpha)
# exit()
# gpr.gpr.alpha = env.noise_var
gpr.update_params()
print(gpr.gpr.alpha)
# exit()
# gpr._fit()
# gpr.gpr.n_restart_optimizer = 100

ys_plt = np.linspace(-4, 4, num=100)
plotters.plot_gpr_2d(gpr, ys_plt, ys_plt)

plt.show()
