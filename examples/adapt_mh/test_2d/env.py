import numpy as np
import numpy.random as rnd
from scipy.stats import multivariate_normal

# n = 1
# y0 = [1.]
n = 2
y0 = np.array([2., 2.])

# y01 = [2., 2.]
# y02 = [-2., 2.]
# y03 = [-2., -2.]
# y04 = [2., -2.]

a1 = 20
a2 = a1
c = 10


def g_1d(y):
    return a1*(y-y0[0])**2 + c


def g_2d(y):
    y = np.atleast_2d(y)
    return a1*(y[:, 0]-y0[0])**2 + a2*(y[:, 1]-y0[1])**2 + c


# 4 minima
# def g_2d(x):
#     y = np.atleast_2d(y)
#     return ((y[:, 0]-y01[0])**2 + (y[:, 1]-y01[1])**2) * \
#         ((y[:, 0]-y02[0])**2 + (y[:, 1]-y02[1])**2) * \
#         ((y[:, 0]-y03[0])**2 + (y[:, 1]-y03[1])**2) * \
#         ((y[:, 0]-y04[0])**2 + (y[:, 1]-y04[1])**2)


prior_dist = multivariate_normal(mean=np.zeros(n), cov=np.eye(n))


def prior_y(y):
    return prior_dist.pdf(y)


def prior_y_sample_1d(N=1):
    return rnd.normal(loc=0.0, scale=1.0, size=N)


def prior_y_sample_2d(N=1):
    return rnd.multivariate_normal(mean=np.zeros(n), cov=np.eye(n)) if N == 1 else \
        rnd.multivariate_normal(mean=np.zeros(n), cov=np.eye(n), size=N)


g = g_2d
prior_y_sample = prior_y_sample_2d

noise_var = 1000


def gN(y):
    y = np.atleast_2d(y)
    N = len(y)
    return g(y)[0] + rnd.normal(loc=0.0, scale=np.sqrt(noise_var)) if N == 1 else \
        g(y) + rnd.multivariate_normal(mean=np.zeros(N), cov=noise_var*np.eye(N))


def unnorm_posterior_y(y):
    return np.exp(-g(y))*prior_y(y)
