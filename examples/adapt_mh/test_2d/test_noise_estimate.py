import env
import plotters
from uq_tools.stats.regression import ExtendedGPR

import matplotlib.pyplot as plt
import numpy as np
from sklearn.gaussian_process.kernels import Matern, RBF


N = 200
ys = env.prior_y_sample(N=N)
data = env.gN(ys)

# kernel = 1.0 * RBF(length_scale=1.0)
kernel = 1.0 * Matern(length_scale=1.0, nu=1.5)
gpr = ExtendedGPR(kernel=kernel, n_restarts_optimizer=20)

gpr.fit(ys, data)

estim_noise = gpr.estimate_noise()

print("Noise level: %.3f" % env.noise_var)
print("Estimated noise level: %.3f" % estim_noise)
