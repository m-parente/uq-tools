import env
import plotters
from uq_tools.approx import AdaptiveGPR
from uq_tools.mcmc import mh_mcmc_post
from uq_tools.sampling import adapt_mh_temp
from uq_tools.stats import metrics

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd
import pickle
import seaborn as sns
from sklearn.gaussian_process.kernels import RBF, Matern

# rnd.seed(1)
# sns.set()


N = 30
ys = env.prior_y_sample(N=N)

noise_var = env.noise_var

ds = env.gN(ys)

# kernel = 1.0 * RBF(length_scale=np.ones(env.n))
kernel = 1.0 * Matern(length_scale=np.ones(env.n), nu=1.5)
gpr = AdaptiveGPR(ys, ds, kernel=kernel, noise_var=noise_var, n_restart_optimizer=20)
# gpr = AdaptiveGPR(ys, ds, kernel=kernel, n_restart_optimizer=100)
gpr0 = gpr.copy()

ys_plt = np.linspace(-5., 5., num=100)
# ys_plt = np.linspace(-2.5, 2.5, num=100)


def posterior_y(gpr):
    return lambda y: np.exp(-gpr(y))*env.prior_y(y)


fig, ax = plotters.plot_contour(ys_plt, ys_plt, posterior_y(gpr0))
plotters.plot_gpr_2d(gpr, ys_plt, ys_plt)
# # plotters.plot_contour(ys_plt, ys_plt, env.unnorm_posterior_y)
# plt.show()
# exit()

L = 50
T = 100
K = 100
c = 0.01
tol_kl = 0.05

samples, temps, update_pts, \
    avg_dists, kl_divs = adapt_mh_temp(misfit_approx=gpr,
                                       misfit_full=env.gN,
                                       prior=env.prior_y,
                                       proposal_sampler=lambda xk: rnd.normal(
                                           loc=xk, scale=0.2),
                                       x0=np.zeros(env.n),
                                       n_steps=50000,
                                       uncert_tol0=10,
                                       L=L, T=T, K=K, c=c, tol_kl=tol_kl,
                                       proposal_sampler_kl=lambda xk: rnd.normal(loc=xk, scale=0.3))

with open("tmp/gpr_adapt.bin", 'wb') as gpr_file:
    pickle.dump(gpr, gpr_file)

samples_fine = mh_mcmc_post(misfit=gpr,
                            prior=env.prior_y,
                            proposal_sampler=lambda yk: rnd.normal(loc=yk, scale=0.3),
                            x0=samples[-1, :],
                            steps=10000,
                            nPlotAccptRate=500)
print("KL(gpr||gpr0): %f" % metrics.kl_div_post(gpr, gpr0, samples_fine))

samples_exct = mh_mcmc_post(misfit=env.g_2d,
                            prior=env.prior_y,
                            proposal_sampler=lambda yk: rnd.normal(loc=yk, scale=0.3),
                            x0=np.array([2, 1.6]),
                            steps=10000,
                            nPlotAccptRate=500)
print("KL(g||gpr): %f" % metrics.kl_div_post(env.g_2d, gpr, samples_exct))
print("KL(g||gpr0): %f" % metrics.kl_div_post(env.g_2d, gpr0, samples_exct))

plotters.plot_contour(ys_plt, ys_plt, posterior_y(gpr), ax=ax)
plotters.plot_contour(ys_plt, ys_plt, env.unnorm_posterior_y, ax=ax)
fig.tight_layout()

plotters.plot_density_diag(samples_exct)
plotters.plot_density_diag(samples_fine)

plotters.plot_gpr_2d(gpr, ys_plt, ys_plt)

sns.set()
plotters.plot_adapt_mh_stats(samples, temps, update_pts, avg_dists, L, kl_divs)

print("Number updates: %i" % len(update_pts))
print(kl_divs)

plt.show()
