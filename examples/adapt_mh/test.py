import plotters

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

sns.set()

x01 = [2., 2.]
x02 = [-2., 2.]
x03 = [-2., -2.]
x04 = [2., -2.]


def f(x):
    x = np.atleast_2d(x)
    return ((x[:, 0]-x01[0])**2 + (x[:, 1]-x01[1])**2) * \
        ((x[:, 0]-x02[0])**2 + (x[:, 1]-x02[1])**2) * \
        ((x[:, 0]-x03[0])**2 + (x[:, 1]-x03[1])**2) * \
        ((x[:, 0]-x04[0])**2 + (x[:, 1]-x04[1])**2)


xs_plt = np.linspace(-3, 3, num=100)
ys_plt = np.linspace(-3, 3, num=100)

plotters.plot_contour(xs_plt, ys_plt, f, levels=20)
ax_surf = plotters.plot_surface(xs_plt, ys_plt, f)[1]
ax_surf.set_zlim([0., 20000.])

plt.show()
