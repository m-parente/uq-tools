from misc import Settings
import model
import uq_tools.bayinv as bi

from glob import glob
import natsort
import numpy as np
import numpy.linalg as la
import numpy.random as rnd
import os
import scipy.stats as stats


settings = Settings('settings')

tmpDir = settings.tmpDir
asDir = settings.asDir
mcmcDir = settings.mcmcDir
os.system('mkdir -p %s && mkdir -p %s && mkdir -p %s' %
          (tmpDir, asDir, mcmcDir))
samplesDir = tmpDir

GFiles = natsort.natsorted(glob('%s/G*.txt' % samplesDir))
jacGFiles = natsort.natsorted(glob('%s/jacG*.txt' % samplesDir))

xs = np.loadtxt('%s/xs.txt' % samplesDir)

Gs = [np.loadtxt(GFile) for GFile in GFiles]
jacGs = [np.loadtxt(jacGFile) for jacGFile in jacGFiles]

numberOfParameters = 6


def prior_sample(n=None):
    # return rnd.uniform(low=-1, high=1, size=(n, numberOfParameters)) if n != None \
    #     else rnd.uniform(low=-1, high=1, size=numberOfParameters)
    return rnd.normal(loc=0., scale=0.1, size=(n, numberOfParameters)) if n != None \
        else rnd.normal(loc=0., scale=0.1, size=numberOfParameters)


_nrv = stats.multivariate_normal(mean=np.zeros(numberOfParameters),
                                 cov=np.diag(numberOfParameters * [0.01]))


def prior(x):
    # return 2**-numberOfParameters if np.all(np.logical_and(x <= 1, x >= -1)) else 0
    return _nrv.pdf(x)


data = np.loadtxt(settings.dataFile)
_stddevs = data * [settings.noiseLevelPray, settings.noiseLevelPredator]
noiseCovMat = np.diag(_stddevs**2)
invNoiseCovMat = np.diag(1 / _stddevs**2)

bayInvPb = bi.BayesianInverseProblem(
    data, model.AbstractPredatorPreyODEModel(), invNoiseCovMat)

misfit = bayInvPb.misfitG
