import env
import model

import plotters
from uq_tools import asm, bayinv as bi

import numpy as np
import numpy.linalg as la


settings = env.settings

asDir = settings.asDir

nBoot = settings.numberBootstrapIterations

absPb = model.AbstractPredatorPreyODEModel()
bayInvPb = bi.BayesianInverseProblem(env.data, absPb, env.invNoiseCovMat)

jac_x = [np.diag(np.exp(x)) for x in env.xs]

result = asm.computeActiveSubspaceFromSamples(
    env.Gs, env.jacGs, bayInvPb, nBoot, scaleMatrices=jac_x, asDir=asDir)

eigVals, eigVecs, minEigVals, maxEigVals, minSubspaceErrors, maxSubspaceErrors, meanSubspaceErrors = result

numPlotEigVals = 6
plotters.plotActiveSubspace(eigVals, eigVecs, numPlotEigVals, minEigVals, maxEigVals,
                            minSubspaceErrors, maxSubspaceErrors, meanSubspaceErrors, asDir=asDir)
