import env

import plotters
from uq_tools import asm

import numpy as np

import matplotlib.pyplot as plt
from sklearn import metrics

settings = env.settings

samplesDir = settings.tmpDir
asDir = settings.asDir

W = np.loadtxt('%s/asm_eigVecs.txt' % asDir)
w1 = W[:, 0]
w2 = W[:, 1]
w3 = W[:, 2]
w4 = W[:, 3]
w5 = W[:, 4]
w6 = W[:, 5]

xs = env.xs
Gs = env.Gs

misfits = list(map(env.misfit, Gs))

print(np.max(misfits), np.argmax(misfits))
print(np.min(misfits), np.argmin(misfits))

print(w1)

# plotters.summaryPlot(w, xs, misfits, with_fit=True, poly_order=2)
plotters.summaryPlot(w1, xs, misfits, with_fit=True, poly_order=4)
plotters.summaryPlot2D(w1, w2, xs, misfits, with_fit=True, poly_order=4)

poly_order = 4
resp_surf = asm.response_surface(
    xs, misfits, np.transpose([w1, w2, w3]), poly_order=poly_order)
print("3D r2 score: %f" % metrics.r2_score(
    misfits, resp_surf(np.dot(xs, np.transpose([w1, w2, w3])))))

resp_surf = asm.response_surface(xs, misfits, np.transpose(
    [w1, w2, w3, w4]), poly_order=poly_order)
print("4D r2 score: %f" % metrics.r2_score(
    misfits, resp_surf(np.dot(xs, np.transpose([w1, w2, w3, w4])))))

resp_surf = asm.response_surface(
    xs, misfits, np.transpose([w1, w2, w3, w4, w5]), poly_order=poly_order)
print("5D r2 score: %f" % metrics.r2_score(
    misfits, resp_surf(np.dot(xs, np.transpose([w1, w2, w3, w4, w5])))))

plt.show()
