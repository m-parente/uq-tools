import model

import numpy.linalg as la
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns

sns.set()


x = np.array([0., 0., 0., 0., 0., 0.])
ds = np.array([1e-1, 1e-2, 0.5 * 1e-2, 1e-3, 0.5 * 1e-3, 1e-4, 0.5 * 1e-4])
errs = np.empty((2, len(ds)))

pb_x = model.PredatorPreyODEModel(0, x)
q_x = pb_x.getQoI()
jac_x = pb_x.getJacobian()

for i in range(len(ds)):
    dx = ds[i] * np.ones(len(x))  # * x

    pb_xdx = model.PredatorPreyODEModel(1, x + dx)
    q_xdx = pb_xdx.getQoI()

    errs[:, i] = np.abs(q_xdx - q_x - np.dot(jac_x, dx))

plt.figure()
plt.loglog(ds, errs[0, :], 'o-')
plt.loglog(ds, errs[1, :], 'o-')
plt.loglog(ds, ds)
plt.loglog(ds, ds**2)
plt.legend([r'$e_1$', r'$e_2$', r'$1^{st}$ order', r'$2^{nd}$ order'])
plt.xlabel('dx')
plt.ylabel('Error')
plt.show()
