import numpy as np
from run_steve import *

import matplotlib.pyplot as plt

inputs = np.array([[1.5, 1.5, 1.5, 1.5, 1.5, 1.5]])

(X, ee, jac, adj_sol) = lb_model2(inputs, plot=False)
print(X)

plt.figure()
plt.plot(list(range(np.shape(X)[1])), X[0, :])
plt.plot(list(range(np.shape(X)[1])), X[1, :])
plt.show()
