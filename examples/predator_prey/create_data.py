import model

from misc import Settings

import numpy as np
import numpy.random as rnd

settings = Settings('settings')

truth = settings.truth

pb = model.AbstractPredatorPreyODEModel().instantiate(truth)
qoi = pb.getQoI()

stddevs = qoi * [settings.noiseLevelPray, settings.noiseLevelPredator]

noise = rnd.multivariate_normal(mean=np.zeros(2), cov=np.diag(stddevs**2))

np.savetxt(settings.dataFile, qoi + noise)
