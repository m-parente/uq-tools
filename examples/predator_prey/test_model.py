import model

import numpy as np

import matplotlib.pyplot as plt

pb = model.AbstractPredatorPreyODEModel().instantiate([0., 0., 0., 0., 0., 0.])

q = pb.getQoI()
pb.getJacobian()

print(q)

plt.figure()
plt.plot(np.linspace(0., pb.T, pb.n_steps + 1), pb.y[0, :])
plt.plot(np.linspace(0., pb.T, pb.n_steps + 1), pb.y[1, :])
plt.legend([r'$y_1$', r'$y_2$'])

plt.figure()
plt.plot(np.linspace(0., pb.T, pb.n_steps + 1), pb.phi[0, :])
plt.plot(np.linspace(0., pb.T, pb.n_steps + 1), pb.phi[1, :])
plt.legend([r'$\phi_1$', r'$\phi_2$'])

plt.show()
