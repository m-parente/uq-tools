import env

import multiprocessing as mp
import numpy as np
import os
import sys


settings = env.settings

tmpDir = settings.tmpDir

M = settings.numberSamples
samples = [env.prior_sample() for _ in range(M)]
np.savetxt('%s/xs.txt' % tmpDir, samples)

scriptName = 'create_qoi_jac.py'

cmds = ''
for i in range(M):
    sample = samples[i]
    cmd = sys.executable + " " + scriptName + " " + \
        str(i) + " " + ' '.join(str(p) for p in sample)
    cmds += cmd + ' > /dev/null\n'

launcherFile = '%s/asmLauncher_runs.txt' % tmpDir

with open(launcherFile, 'w') as file:
    file.write(cmds)

os.environ["LAUNCHER_JOB_FILE"] = launcherFile
os.environ["LAUNCHER_DIR"] = os.environ["SW_DIR"] + "/launcher"
os.environ["LAUNCHER_PPN"] = repr(mp.cpu_count() - 1)

os.system("bash $LAUNCHER_DIR/paramrun")
