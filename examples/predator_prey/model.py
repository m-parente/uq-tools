from misc import Settings
from uq_tools import ode
# from scipy.integrate import ode
# from uq_tools import quad

import numpy as np

settings = Settings('settings')

parameterScaleMatrix = 0.5 * np.diag([settings.y0_pray_ub - settings.y0_pray_lb,
                                      settings.y0_pred_ub - settings.y0_pred_lb,
                                      settings.eps1_ub - settings.eps1_lb,
                                      settings.gamma1_ub - settings.gamma1_lb,
                                      settings.eps2_ub - settings.eps2_lb,
                                      settings.gamma2_ub - settings.gamma2_lb])


def toRealRange(samples):
    transl = 0.5 * np.array([settings.y0_pray_ub + settings.y0_pray_lb,
                             settings.y0_pred_ub + settings.y0_pred_lb,
                             settings.eps1_ub + settings.eps1_lb,
                             settings.gamma1_ub + settings.gamma1_lb,
                             settings.eps2_ub + settings.eps2_lb,
                             settings.gamma2_ub + settings.gamma2_lb])

    return np.dot(parameterScaleMatrix, samples) + transl


class PredatorPreyODEModel:
    def __init__(self, id, parameters):
        self.id = id
        self.parameters = parameters
        # toRealRange(parameters)
        self.x = np.exp(parameters) + [1., 1., 0., 0., 0., 0.]

        self.y0_pray = self.x[0]  # y0_1
        self.y0_pred = self.x[1]  # y0_2
        self.eps1 = self.x[2]
        self.gamma1 = self.x[3]
        self.eps2 = self.x[4]
        self.gamma2 = self.x[5]

        self.y01 = self.y0_pray
        self.y02 = self.y0_pred
        self.y0 = np.array([self.y01, self.y02])
        self.T = 30.
        self.dt = 0.001
        self.n_steps = int(self.T / self.dt)
        self.t_ = self.T
        self.T1 = 10.
        self.T2 = 15.

    def getQoI(self):
        if hasattr(self, 'qoi'):
            return self.qoi

        def f(y, t):
            y1 = y[0] + self.y01
            y2 = y[1] + self.y02
            return np.array([y1 * (self.eps1 - self.gamma1 * y2),
                             -y2 * (self.eps2 - self.gamma2 * y1)])

        def jac(y, t):
            y1 = y[0] + self.y01
            y2 = y[1] + self.y02
            return np.array([[self.eps1 - self.gamma1 * y2, -self.gamma1 * y1],
                             [self.gamma2 * y2, -(self.eps2 - self.gamma2 * y1)]])

        self.yh = ode.bw_euler(f, jac, dt=self.dt,
                               #    y0=self.y0, n_steps=self.n_steps)
                               y0=np.zeros(2), n_steps=self.n_steps)
        self.y = self.y0[:, np.newaxis] + self.yh

        # self.y = np.empty((2, self.n_steps + 1))
        # self.y[:, 0] = self.y0

        # integ = ode(f, jac) \
        #     .set_integrator('vode') \
        #     .set_initial_value(self.y0, 0.)
        # i = 1
        # while integ.successful() and integ.t < self.T:
        #     self.y[:, i] = integ.integrate(integ.t + self.dt)
        #     i += 1

        tk = int(self.T1 / self.dt)
        tkN = int(self.T2 / self.dt)

        # self.qoi = self.y[:, int(self.t_ / self.dt)]
        self.qoi = self.dt * np.sum(self.y[:, tk:tkN + 1],
                                    axis=1) / float(self.T2 - self.T1)

        return self.qoi

    def getJacobian(self):
        if hasattr(self, 'jac'):
            return self.jac

        self.getQoI()

        self.jac = np.empty((2, len(self.parameters)))

        # Transposed Jacobian of RHS (f(\hat{y},t)) of the equation wrt to y
        def minus_jacf_y_tr(step):
            y1, y2 = self.y[:, step]
            return -np.array([[self.eps1 - self.gamma1 * y2, -self.gamma1 * y1],
                              [self.gamma2 * y2, -(self.eps2 - self.gamma2 * y1)]]).T

        # Jacobian of equation wrt to the six parameters
        def jacf_p(step):
            y1, y2 = self.y[:, step]
            return np.array([[self.eps1 - self.gamma1 * y2, -self.gamma1 * y1, y1, -y1 * y2, 0., 0.],
                             [self.gamma2 * y2, -(self.eps2 - self.gamma2 * y1), 0., 0., -y2, y1 * y2]])
            # return np.array([[y1, -y1 * y2, 0., 0.],
            #                  [0., 0., -y2, y1 * y2]])

        # Solve adjoint equation for pray
        def minus_dhdy(step):
            t = step * self.dt
            return -np.array([1. / float(self.T2 - self.T1) if t >= self.T1 and t <= self.T2 else 0., 0.])

        phi = ode.reverse_cn_linear(minus_jacf_y_tr, minus_dhdy,
                                    np.zeros(2), self.T, self.dt)
        self.phi = phi
        # self.jac[0, :2] = phi[:, 0]
        self.jac[0, :] = np.array([1, 0, 0, 0, 0, 0]) \
            + 0.5 * self.dt * np.sum([np.dot(jacf_p(step).T, phi[:, step + 1] + phi[:, step])
                                      for step in range(self.n_steps)], axis=0)

        # Solve adjoint equation for predator
        def minus_dhdy(step):
            t = step * self.dt
            return -np.array([0., 1. / float(self.T2 - self.T1) if t >= self.T1 and t <= self.T2 else 0.])

        phi = ode.reverse_cn_linear(minus_jacf_y_tr, minus_dhdy,
                                    np.zeros(2), self.T, self.dt)
        # self.jac[1, :2] = phi[:, 0]
        self.jac[1, :] = np.array([0, 1, 0, 0, 0, 0]) \
            + 0.5 * self.dt * np.sum([np.dot(jacf_p(step).T, phi[:, step + 1] + phi[:, step])
                                      for step in range(self.n_steps)], axis=0)

        return self.jac
        # return np.dot(self.jac, parameterScaleMatrix)


class AbstractPredatorPreyODEModel:
    def instantiate(self, parameters, id=-1):
        return PredatorPreyODEModel(id, parameters)
